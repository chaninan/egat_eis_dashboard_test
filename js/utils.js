function show_byclass(pID)
{
    $("#" + pID).removeClass("hide");
}

function hide_byclass(pID)
{
    $("#" + pID).addClass("hide");
}

function gotop()
{
    $("html, body").animate({scrollTop: 0}, "slow");
}

function gotop_modal()
{
    $(".modal").animate({scrollTop: 0}, "slow");
}

function reset_form(pForm) {
    $("#" + pForm).find("input:not('.not_clear'), select, textarea").val('');
    //$("#" + pForm).find('input:checkbox').removeAttr('checked');
    $("#" + pForm).find('input:checkbox').prop("checked", false);
    $("#" + pForm).find('input:radio').prop("checked", false);
}

function ask_leave_page()
{
    //ask before leave page
    window.onbeforeunload = function () {
        return 'ข้อมูลยังไม่ถูกบันทึก แน่ใจหรือไม่ที่จะออกหน้านี้ ?';
    };
}

/* Ajax Waiting */

function ajax_disable_btn_obj(pObj)
{
    $(pObj).prop("disabled", true);
    $(pObj).after("<span id='ajax-loading'>&nbsp;&nbsp;&nbsp;&nbsp; Please wait...</span>");
}
function ajax_enable_btn_obj(pObj)
{
    $(pObj).prop("disabled", false);
    $("#ajax-loading").remove();
}
function ajax_disable_btn_nowaiting(pObj)
{
    $(pObj).prop("disabled", true);
}
function ajax_enable_btn_nowaiting(pObj)
{
    $(pObj).prop("disabled", false);
}

function ajax_disable_btn(pID)
{
    $("#" + pID).prop("disabled", true);
    $("#" + pID).after("<span id='ajax-loading'>&nbsp;&nbsp;&nbsp;&nbsp; Please wait...</span>");
}
function ajax_enable_btn(pID)
{
    $("#" + pID).prop("disabled", false);
    $("#ajax-loading").remove();
}

function ajax_show_waiting(pID)
{
    $("#" + pID).after("<span id='ajax-loading'>&nbsp;&nbsp;&nbsp;&nbsp; Please wait...</span>");
}

function ajax_show_waiting_obj(pObj)
{
    $(pObj).after("<span id='ajax-loading'>&nbsp;&nbsp;&nbsp;&nbsp; Please wait...</span>");
}

function ajax_hide_waiting()
{
    $("#ajax-loading").remove();
}

function show_modal_waiting()
{
    $("body").append("<div id='modal_waiting_frame'><div id='modal_waiting_img'>โปรดรอสักครู่ ....</div></div>");
    gotop();
}

function hide_modal_waiting()
{
    $("#modal_waiting_frame").remove();
}

/* End Ajax Waiting */

//input type=checkbox
function toggle_check_byclass(pThis, pClass)
{
    $("." + pClass).each(function ()
    {
        $(this).prop("checked", pThis.checked);
    });
}
function check_checkbox_by_element(pEle) {
    $(pEle).prop("checked", true);

}
function uncheck_checkbox_by_element(pEle) {
    $(pEle).prop("checked", false);
}
function un_check(pID)
{
    $("#" + pID).prop("checked", false);
}

function show_msgbox(pMessage, pClass, pID)
{
    if (!pID) {
        pID = "msgbox";
    }

    $("#" + pID).removeClass().empty();
    if (pClass === "0") {
        pClass = "alert alert-danger";
    } else {
        pClass = "alert alert-success";
    }

    $("#" + pID).html(pMessage).addClass(pClass);
    if (pID === "msgbox") {
        gotop();
    } else {
        gotop_modal();
    }
}

function clear_msgbox(element) {
    $(element).removeClass().html('');
}

function scroll_to_top()
{
    $("html, body").animate({scrollTop: 0}, "slow");
}

function clear_txt(pID)
{
    $("#" + pID).val("");
    $("#" + pID).focus();
}

function next_focus(pID)
{
    $(pID).focus();
}

function show_calendar(pID)
{
    //alert(pID);
    $("#" + pID).datepicker("show");
}

//function confirm_delete()
//{
//    if (confirm("ต้องการลบข้อมูล ?"))
//        return true;
//    else
//        return false;
//}

function get_today_date()
{
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    return dd + '/' + mm + '/' + yyyy;
}

function date_diff_day(pDate1, pDate2)
{
    if (pDate1 != "" && pDate2 != "")
    { //input d/m/y
        var arrDate1 = pDate1.split("/");
        var arrDate2 = pDate2.split("/");
        var odate1 = new Date(arrDate1[2], arrDate1[1], arrDate1[0]);
        var odate2 = new Date(arrDate2[2], arrDate2[1], arrDate2[0]);

        return Math.round((odate2 - odate1) / (1000 * 60 * 60 * 24));
    } else
    {
        return 0;
    }
}

//custom jquery Function
$.fn.barcodeInput = function ()
{
    $(this).on("keypress", function (event)
    {

        //remove old messagebox
        $("#barcode-error-box").remove();

        var reg_engOnly = /[A-Za-z0-9]/g;
        var key = String.fromCharCode(event.which);

        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // keyCode == 13 is Enter 
        // keyCode == 9 is Tab 
        // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13 || event.keyCode == 9 || reg_engOnly.test(key))
        {
            return true;
        }

        //show message     
        $(this).after("<div id='barcode-error-box'>Barcode ไม่สามารถระบุเป็นภาษาไทยได้</div>");

        // If we got this far, just return false because a disallowed key was typed.
        return false;
    });
};

function get_max_classify(element) {

    var my_array = new Array();
    var max_val = 0;
    element.each(function () {
        my_array.push($(this).html());
    });
    max_val = my_array.max();
    return  max_val;

}

function sum(element) {
    var value = parseInt("0");
    element.each(function () {
        if (isNaN(parseInt($(this).val()))) {
            value += 0;
        } else {
            value += parseInt($(this).val())
        }
    });
    return value;
}

function prevent_body_enter()
{
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
}

function show_left_menu(pMenuID)
{
    if (pMenuID == 2)
    {
        $("#menu_mn_regis_vehicle").addClass("active");
    }
}
function clear_msgbox(element) {
    $(element).removeClass().html('');
}

function display_tooltip()
{
    $('[data-toggle="tooltip"]').tooltip();
}

//How to use 
// $("#id_of_textbox").enter_to_click("#id_of_button");
$.fn.enter_to_click = function (pTarget)
{

    $(this).keypress(function (event)
    {
        if (event.keyCode == 13)
        {
            event.preventDefault();
            $(pTarget).click();
            return false;
        }
    });
};
$.fn.enter_to_submit = function (pTarget)
{
    $(this).keypress(function (event)
    {
        if (event.keyCode == 13)
        {
            event.preventDefault();
            $(pTarget).submit();
            return false;
        }
    });
};
// $("#name_of_textbox").blur_to_click("#name_of_button");
$.fn.blur_to_click = function (pTarget)
{
    $(this).blur(function ()
    {
        $(pTarget).click();
        return false;
    });
};

$.fn.time_format = function () {
    $(this).on("keyup", function () {
        if ($(this).val().length == 2) {
            $(this).val($(this).val() + ":");
        }
    });
};

//prevent text input (except delete, backspace) 
$.fn.prevent_keydown = function ()
{
    $(this).keydown(
            function (e)
            {
                if (e.which == 8 || e.which == 46)
                {
                    $(this).val("");
                } else
                {
                    return false;
                }
            });
};

$.fn.empID_pattern_inputgroup = function (input_group)
{
    $(this).on("keypress", function (event)
    {
        //remove old messagebox
        $("#number-error-box").remove();

        var reg_engOnly = /[0-9]/g;
        var key = String.fromCharCode(event.which);

        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // keyCode == 13 is Enter 
        // keyCode == 9 is Tab 
        // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13 || event.keyCode == 9 || reg_engOnly.test(key))
        {
            return true;
        }

        //show message     
        $(input_group).after("<div id='number-error-box'>กรอกได้เฉพาะตัวเลขเท่านั้น</div>");

        // If we got this far, just return false because a disallowed key was typed.
        return false;
    });
};

/**
 * action
 * 1= not show message
 */
$.fn.only_numeric = function (action)
{
    $(this).on("keypress", function (event)
    {
        $(this).css("background-color", "");
        $(this).css("border-color", "");

        //remove old messagebox
        $("#number-error-box").remove();

        var reg_engOnly = /[0-9.]/g;
        var key = String.fromCharCode(event.which);

        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // keyCode == 13 is Enter 
        // keyCode == 9 is Tab 
        // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13 || event.keyCode == 9 || reg_engOnly.test(key))
        {
            return true;
        }

        if (action == "") {
            //show message     
            $(this).after("<div id='number-error-box'>กรอกได้เฉพาะตัวเลขเท่านั้น</div>");
        } else {
            $(this).css("background-color", "#f2dede");
            $(this).css("border-color", "#d43f3a");
        }


        // If we got this far, just return false because a disallowed key was typed.
        return false;
    });
};

$.fn.only_numbers = function ()
{
    $(this).on("keypress", function (event)
    {
        //remove old messagebox
        $("#number-error-box").remove();

        var reg_engOnly = /[0-9]/g;
        var key = String.fromCharCode(event.which);

        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // keyCode == 13 is Enter 
        // keyCode == 9 is Tab 
        // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13 || event.keyCode == 9 || reg_engOnly.test(key))
        {
            return true;
        }

        //show message     
        $(this).after("<div id='number-error-box'>กรอกได้เฉพาะตัวเลขเท่านั้น</div>");

        // If we got this far, just return false because a disallowed key was typed.
        return false;
    });
};

$.fn.mobile_pattern = function ()
{
    $(this).on("keypress", function (event)
    {
        //remove old messagebox
        $("#number-error-box").remove();

        var reg_engOnly = /[0-9]/g;
        var key = String.fromCharCode(event.which);

        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // keyCode == 13 is Enter 
        // keyCode == 9 is Tab 
        // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern
        if (event.keyCode == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 13 || event.keyCode == 9 || reg_engOnly.test(key))
        {
            return true;
        }

        //show message     
        $(this).after("<div id='number-error-box'>กรอกหมายเลขโทรศัพท์ 10 หลัก ด้วยรูปแบบดังนี้ 08XXXXXXXX</div>");

        // If we got this far, just return false because a disallowed key was typed.
        return false;
    });
};

//formatting number with comma Ex. 18,000 
$.fn.numeric_comma = function (hdd_input)
{
    $(this).blur(function (event)
    {
        if ($(this).val() !== "") {
            var new_val = parseFloat($(this).val().replace(/,/g, ""))
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            $(this).val(new_val);

            $(hdd_input).val(new_val.replace(/,/g, ""));
        }
    });
};

//when use input type="text" !! only
$.fn.money_pattern = function (hdd_input)
{
    $(this).blur(function (event)
    {
        if ($(this).val() !== "") {
            var new_val = parseFloat($(this).val().replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            $(this).val(new_val);

            $(hdd_input).val(new_val.replace(/,/g, ""));
        }
    });
};

$.fn.percent_pattern = function ()
{
    $(this).blur(function ()
    {
        if ($(this).val() !== "") {
            var cur_val = parseFloat($(this).val().replace(/,/g, ""));
            if (cur_val > 100) {
                cur_val = 100;
            }

            var new_val = parseFloat(cur_val)
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

            $(this).val(new_val);
        }
    });
};

$.fn.decimal_pattern = function (decimal_point)
{
    $(this).blur(function (event)
    {
        if ($(this).val() !== "") {
            var new_val = parseFloat($(this).val().replace(/,/g, ""))
                    .toFixed(decimal_point)
                    .toString();

            $(this).val(new_val);
        }
    });
};

//citizen pattern
$.fn.citizenID_pattern = function ()
{
    $(this).blur(function (event)
    {
        //remove old messagebox
        $("#number-error-box").remove();

        if ($(this).val().length !== 13) {
            //show message     
            $(this).after("<div id='number-error-box'>รหัสบัตรประชาชนไม่ถูกต้อง</div>");
            return false;
        }

        for (i = 0, sum = 0; i < 12; i++) {
            sum += parseFloat($(this).val().charAt(i)) * (13 - i);
        }

        if ((11 - sum % 11) % 10 !== parseFloat($(this).val().charAt(12))) {
            //show message     
            $(this).after("<div id='number-error-box'>รหัสบัตรประชาชนไม่ถูกต้อง</div>");
            return false;
        }
    });

    return true;
};

function check_citizen(val) {
    $("#display_citizen_error").removeClass().empty();

    if (val.length < 13) {
        //show message     
        $("#display_citizen_error").addClass("alert alert-danger").html("กรุณากรอก รหัสบัตรประชาชนให้ครบ 13 หลัก");
        return false;
    }

    for (i = 0, sum = 0; i < 12; i++) {
        sum += parseFloat(val.charAt(i)) * (13 - i);
    }

    if ((11 - sum % 11) % 10 !== parseFloat(val.charAt(12))) {
        //show message     
        $("#display_citizen_error").addClass("alert alert-danger").html("รหัสบัตรประชาชนไม่ถูกต้อง");
        return false;
    }
}

//Boot box function
function bootbox_add_data(param) {

    bootbox.alert({
        message: param.content,
        title: param.title,
        buttons: {
            Save: {
                label: "บันทึกข้อมูล",
                className: "btn-success"

            },
            cancel: {
                label: "ยกเลิก",
                className: "btn-default"
            }
        }
    });
}
function confirm_delete(param) {
    bootbox.alert({
        message: "ต้องการลบข้อมูลนี้หรือไม่ ?",
        title: "ลบข้อมูล",
        buttons: {
            delete: {
                label: "ลบข้อมูล",
                className: "btn-danger",
                callback: function () {
                    //alert(param);
//                    ajax_do_delete(param);

                }
            },
            cancel: {
                label: "ยกเลิก",
                className: "btn-default",
                callback: function () {
                    //dismiss
                }
            }
        }
    });

}
function warning_box(param, function_name) {
    bootbox.alert({
        message: param.message,
        title: param.title,
        buttons: {
            yes: {
                label: "ใช่",
                className: "btn-success",
                callback: function () {
                    //alert(param);
                    function_name(param.id);

                }
            },
            cancle: {
                label: "ยกเลิก",
                className: "btn-default",
                callback: function () {
                    //dismiss
                }
            }
        }
    });

}
function clear_text() {
    $("input").val('');
}
function form_submit(form_id, btn_id) {
    $("#" + form_id).on('submit', function () {
        ajax_disable_btn("#" + btn_id);
    });

}
function is_exist_session(status) {
    if (status.code) {
        var res_status = status.code[0];
        if (res_status === "401") {
            window.location = "uc/session_timeout";
        }
    }
}
function limit_driver(is_empty, text)
{
    if (is_empty === true)
    {
        return "<span class='text-success fs19'><b>" + text + " คน </b><span class='fa fa-check-circle'></span></span>";
    } else
    {
        return "<span class='text-danger fs19'><b>" + text + " คน </b><span class='glyphicon glyphicon-remove-circle'></span></span>";
    }
}
//input type=checkbox
function togglecheck_byclass(pClass, pChecked)
{
    $("." + pClass).each(function ()
    {
        if (pChecked) {
            $(this).prop("checked", pChecked);
        } else {
            $(this).prop("checked", pChecked);
        }
    });
}
$.fn.date_select_month = function (pElement) {
    pElement.datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+100",
        dateFormat: "dd/mm/yy"
    });
    return true;
}
function close_preview() {
    $("#img_prev").attr("src", "");
    hide_img_preview();
}
function preview_img(input) {
    show_img_preview();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img_prev')
                    .attr('src', e.target.result)
                    .height(200);
        };

        reader.readAsDataURL(input.files[0]);
    } else {
        var img = input.value;

    }
    $('#img_prev').attr('src', img).height(200);
    $("#hidden_pic").val('');
    $("#div_camera").hide();
    $("#btn_close").show();
    $("#div_btn").show();

}
function steam_pic() {
    $("#div_prev").hide();
    var streaming = false,
            video = document.querySelector('#video'),
            canvas = document.querySelector('#canvas'),
            // photo = document.querySelector('#photo'),
            capture_pic = document.querySelector('#btn_capture'),
            width = 320,
            height = 0;
    navigator.getMedia = (navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia);
    navigator.getMedia(
            {
                video: true,
                audio: false
            },
            function (stream) {
                if (navigator.mozGetUserMedia) {
                    video.mozSrcObject = stream;
                } else {
                    var vendorURL = window.URL || window.webkitURL;
                    video.src = vendorURL.createObjectURL(stream);
                }
                video.play();
            },
            function (err) {
                console.log("An error occured! " + err);
            }
    );
    video.addEventListener('canplay', function (ev) {
        if (!streaming) {
            height = video.videoHeight / (video.videoWidth / width);
            video.setAttribute('width', width);
            video.setAttribute('height', height);
            canvas.setAttribute('width', width);
            canvas.setAttribute('height', height);
            streaming = true;
            $("#btn_capture").show();

        }
    }, false);


    function takepicture() {
        canvas.width = width;
        canvas.height = height;
        canvas.getContext('2d').drawImage(video, 0, 0, width, height);
        var data = canvas.toDataURL('image/png');
        // photo.setAttribute('src', data);
        $("#hidden_pic").val(data);

        $("#div_btn").show();
    }

    capture_pic.addEventListener('click', function (ev) {
        takepicture();
        ev.preventDefault();
    }, false);

}
function btn_submit(is_update, btn_type, func) {

    if (is_update === 1) {
        $("#div_btn_submit").empty().html("<button id ='btn_submit' onclick ='" + func + "();' class='btn btn-success' type='" + btn_type + "'> บันทึกข้อมูล <span class='fa fa-floppy-o'></span></button>");
    } else {
        $("#div_btn_submit").empty().html("<button id ='btn_submit' onclick ='" + func + "();' class='btn btn-success' type='" + btn_type + "' > ต่อไป <span class='fa fa-chevron-right'></span></button>");
    }

}
function link_attach(link) {
    if (link !== "" && link) {
        return "<a href ='" + link + "' target='_blank' class ='data-drv'> [ <span class='fa fa-file'></span> เอกสาร ] </a>";
    }
}

function days_diff(startDate, endDate)
{
    if (startDate !== null && endDate !== null) {
        // Validate input
        if (endDate < startDate)
            return 0;

        // Calculate days between dates
        var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
        startDate.setHours(0, 0, 0, 1);  // Start just after midnight
        endDate.setHours(23, 59, 59, 999);  // End just before midnight
        var diff = endDate - startDate;  // Milliseconds between datetime objects    
        var days = Math.ceil(diff / millisecondsPerDay);

        return days;
    } else {

        if (startDate !== null && endDate === null) {
            return 1;
        } else {
            return 0;
        }
    }
}

function workingDaysBetweenDates(startDate, endDate)
{
    if (startDate !== null && endDate !== null) {

        var days = days_diff(startDate, endDate);

        // Subtract two weekend days for every week in between
        var weeks = Math.floor(days / 7);
        days = days - (weeks * 2);

        // Handle special cases
        var startDay = startDate.getDay();
        var endDay = endDate.getDay();

        // Remove weekend not previously removed.   
        if (startDay - endDay > 1)
            days = days - 2;

        // Remove start day if span starts on Sunday but ends before Saturday
        if (startDay == 0 && endDay != 6) {
            days = days - 1;
        }

        // Remove end day if span ends on Saturday but starts after Sunday
        if (endDay == 6 && startDay != 0) {
            days = days - 1;
        }

        return days;
    }
}
function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}

// Table repeat row
$.fn.table_header_fix = function (pHeight)
{
    var def_table_height = 400;
    if (pHeight !== undefined)
    {
        def_table_height = pHeight;
    }

    //check have class tbl_fix_head ?
    if (!$(this).hasClass("tbl_fix_head"))
    {
        $(this).addClass("tbl_fix_head");
    }

    //wrap div for scroll
    $wrap_scroll = $("<div class='wrap-tbl-fix-head-scroll'>");
    $wrap_table_width = $("<div class='wrap-tbl-fix-head-fix-width'>");
    $wrap_table_width.attr("style", "height:" + def_table_height + "px;");
    $wrap_scroll.prepend($wrap_table_width);

    $(this).before($wrap_scroll);
    $wrap_table_width.append($(this));

    /* Expect all div
     * --> div scroll 
     * ----> div fix width, height
     * -------> table repeat
     */
};
function clear_mdl_bg() {
    $('.modal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

