function pie_chart_status_render(pChartID, pData)
{
    var ctx = $(pChartID);
    var myPiechart = new Chart(ctx,
            {
                type: 'pie',
                data: pData,
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    tooltips: {
                        callbacks: {
                            label: function (tooltipItem, data)
                            {
                                var title = data.labels[tooltipItem.index];
                                var color = data.datasets[0].backgroundColor[tooltipItem.index];
                                var value = data.datasets[0].data[tooltipItem.index];
                                return  " รายการ " + title + " จำนวน : " + value + " รายการ";
                            }
                        },
                        displayColors: true
                    }
                }
            });

    return myPiechart;
}

function bar_chart_usage_render(pChartID, pData)
{
    var ctx = $(pChartID);
    var myBarChart = new Chart(ctx,
            {
                type: 'bar',
                data: pData,
                options: {
                    responsive: true,
                    legend: {
                        display: false
                    },
                    title: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value) {
                                        return number_format_with_comma(value);
                                    }
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: 'บาท',
                                    fontColor: "#8F8F8F"
                                }
                            }],
                        xAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'ประเภท',
                                    fontColor: "#8F8F8F"
                                }
                            }]
                    },
                    tooltips: {
                        // mode: 'label',
                        callbacks: {
                            label: function (tooltipItem, data)
                            {
                                var legend_label = data.datasets[tooltipItem.datasetIndex].label;
                                return " " + legend_label + " " + number_format_with_comma(tooltipItem.yLabel) + " บาท ";
                            }
                        }
                    }
                }
            });

    return myBarChart;
}

function number_format_with_comma(pValue)
{
    return pValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}