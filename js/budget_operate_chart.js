function bugget_bar_chart_render(pChartID, pData)
{
    //console.log(chart_data);
    var ctx = $(pChartID);
    //var ctx = document.getElementById("overall_bar_chart").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: pData,
        options: {
            responsive: true,
            legend: {
                position: 'bottom'
            },
            title: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            //beginAtZero: true,
                            callback: function (value) {
                                return number_format_with_comma(value);
                            }
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'บาท',
                            fontColor: "#8F8F8F"
                        }
                    }],
                xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'เดือน',
                            fontColor: "#8F8F8F"
                        }
                    }]
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function (tooltipItem, data)
                    {
                        var legend_label = data.datasets[tooltipItem.datasetIndex].label;
                        return " " + legend_label + " " + number_format_with_comma(tooltipItem.yLabel) + " บาท ";
                    }
                }
            }
        }

    }); //end myChart

    return myChart;
}

function budget_line_chart(pChartID, pData)
{
    //console.log(chart_data);
    var ctx = $(pChartID);
    //var ctx = document.getElementById("overall_bar_chart").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: pData,
        options: {
            responsive: true,
            legend: {
                position: 'bottom'
            },
            title: {
                display: false
            },
            scales: {
                yAxes: [{
                        ticks: {
                            // beginAtZero: true,
                            callback: function (value) {
                                return number_format_with_comma(value);
                            }
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'บาท',
                            fontColor: "#8F8F8F"
                        }
                    }],
                xAxes: [{scaleLabel: {
                            display: true,
                            labelString: 'เดือน',
                            fontColor: "#8F8F8F"
                        }}]
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function (tooltipItem, data)
                    {
                        var legend_label = data.datasets[tooltipItem.datasetIndex].label;
                        return " " + legend_label + " " + number_format_with_comma(tooltipItem.yLabel) + " บาท ";
                    }
                }
            }
        }

    }); //end myChart

    return myChart;
}

function budget_bar_horizontal_chart(pChartID, pData)
{
    //console.log(chart_data);
    var ctx = $(pChartID);
    //var ctx = document.getElementById("overall_bar_chart").getContext("2d");
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: pData,
        options: {
            responsive: true,
            legend: {
                position: 'bottom'
            },
            title: {
                display: false
            },
            scales: {
                yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'เดือน',
                            fontColor: "#8F8F8F"
                        }
                    }],
                xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'บาท',
                            fontColor: "#8F8F8F"
                        },
                        ticks: {
                            callback: function (value) {
                                return number_format_with_comma(value);
                            },
                            beginAtZero: true
                        }
                    }]
            },
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function (tooltipItem, data)
                    {
                        var legend_label = data.datasets[tooltipItem.datasetIndex].label;
                        return " " + legend_label + " " + number_format_with_comma(tooltipItem.xLabel) + " บาท ";
                    }
                }
            }
        }

    }); //end myChart

    return myChart;
}

function number_format_with_comma(pValue)
{
    return pValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
