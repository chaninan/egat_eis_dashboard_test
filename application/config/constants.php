<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/* ========================= Customs ======================= */

define("CON_TITLE", "con_title");
define("CON_SUB_TITLE", "con_sub_title");
define("CON_RIGHT_CONTENT", "con_right_content");
define("CON_META_DESCRIPTION", "con_meta_description");
define("CON_META_KEYWORDS", "con_meta_keywords");
define("CON_META_AUTHOR", "con_meta_author");
define("CON_TEMPLATE_CONTAINER", "con_template_container");
define("CON_MENU_USER_SELECTED", "con_menu_user_selected");

define("CON_MSG_SHOW", "con_msg_show");
define("CON_MSG_CLASS", "con_msg_class");
define("CON_MSG_TEXT", "con_msg_text");
define("CON_MSG_TARGET_ID", "con_msg_target_id");
define("CON_MSG_BOX_ID", "con_msg_box_id");

define("CON_PLEASE_SELECT", "-- กรุณาเลือก --");
define("CON_SELECT_ALL", "ทั้งหมด");
define("CON_BTN_SAVE", "บันทึกข้อมูล");
define("CON_BTN_CANCEL", "ยกเลิก");
define("CON_BTN_EDIT", "แก้ไข");
define("CON_BTN_DELETE", "ลบ");

define("CON_MSG_SAVE_COMPELTE", "บันทึกข้อมูลเรียบร้อย");
define("CON_MSG_SAVE_INCOMPELTE", "มีข้อผิดพลาดในการบันทึกข้อมูล !!");
define("CON_MSG_EDIT_COMPLETE", "แก้ไขข้อมูลเรียบร้อย");
define("CON_MSG_DEL_COMPLETE", "ลบข้อมูลเรียบร้อย");
define("CON_MSG_ITEM_NOTFOUND", "ไม่พบข้อมูล !!");
define("CON_MSG_NO_RECORD", "ไม่มีข้อมูล !!");
define("CON_MSG_ERROR", "เกิดข้อผิดพลาด");
define("CON_MSG_CAN_NOT_RECEIVE_DATA", "ไม่สามารถรับค่าข้อมูลจาก Client ได้");

define("CON_MSG_LOGIN_COMPLETE", "เข้าสู่ระบบเรียบร้อย");
define("CON_MSG_USER_PWD_FAIL", "รหัสพนักงาน หรือรหัสผ่าน ไม่ถูกต้อง");

define("CON_MSG_ERROR_NETWORK", "ไม่สามารถเชื่อมต่อกับ Server ได้");

define("CON_DATE_NOW", date("Y-m-d H:i:s"));

//Color 
//budget operation
define("CON_COLOR_PLAN", "rgba(43,172,210,0.6)"); 
define("CON_COLOR_ACTUAL", "rgba(255,144,49,0.8)"); 
define("CON_COLOR_PA_GREEN", "rgba(140, 237, 135,0.5)"); 
//budget investment
define("CON_COLOR_INV_NO_ACTION", "rgba(120, 120, 120, 0.8)");
define("CON_COLOR_INV_TOR", "rgba(255, 61, 61, 0.8)");
define("CON_COLOR_INV_PR", "rgba(255, 204, 54, 0.8)");
define("CON_COLOR_INV_PO", "rgba(0, 142, 189, 0.8)");
define("CON_COLOR_INV_ACTUAL", "rgba(0, 179, 45,0.8)");

//UNIT
define("CON_UNITID_GSD_CENTER", 1); //สก.อบก
define("CON_UNITID_GENERAL", 2); //กบท.
define("CON_UNITID_VEHICLE", 3); //กพน.
define("CON_UNITID_CIVIL", 4); //กยธ.
define("CON_UNITID_BUILDING", 5); //กอค.
define("CON_DEPT_GSD", 9324000); //กอค.

//TRIMester 
define("CON_TRIMESTER_1", 1);
define("CON_TRIMESTER_2", 2);
define("CON_TRIMESTER_3", 3);
define("CON_TRIMESTER_4", 4);

define("CON_PA", 95);

define("CON_APP_VERSION", "20161130");
define("CON_APP_IS_TEST", TRUE);

//Pagination
define("CON_PER_PAGE",20);