<?php

class MUser_login extends CI_Model
{
    /* =========================
     *        select
     * ======================= */
    function get_by_empID($pID)
    {
        $this->db->where("empn", $pID);
        $q = $this->db->get("user_gsd");

        return $q->row_array();
    }

    function get_user_session_by_empID($pEmpID)
    {
        $this->db->where("empn", $pEmpID);
        $q = $this->db->get("user_gsd_session");

        return $q->row_array();
    }

    /**
     * Function นี้ เพื่อดูค่า generate_key และ update "Last_upate" ให้เป็นเวลาปัจจุบัน
     * @param type $pEmpID
     * @return type
     */
    function get_generate_key_update_time_visit($pEmpID)
    {
        //select data from #user_egat_session
        $this->db->select('empn, generate_key');
        $this->db->where("empn", $pEmpID);
        $q = $this->db->get("user_gsd_session");
        $dr_session = $q->row_array();
        if(!empty($dr_session))
        { //update last_update time
            $this->db->where("empn", $pEmpID);
            $this->db->set("last_update", date("Y-m-d H:i:s"));
            $this->db->update("user_gsd_session");
        }

        return $dr_session;
    }

    /* =========================
     *        update
     * ======================= */
    function reset_try_login($pEmpID)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->set("try_login", "1");
        $this->db->where("empn", $pEmpID);
        $this->db->update("user_gsd");

        $this->db->trans_complete();

        if($this->db->trans_status())
        {
            //send value back
            $res["status"] = TRUE;
        }
        else
        {
            //not all complete
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    function update_user_session($pDr)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //update
        $this->db->where("empn", $pDr["empn"]);
        unset($pDr["empn"]);
        $this->db->update("user_gsd_session", $pDr);

        $this->db->trans_complete();

        if($this->db->trans_status())
        {
            //send value back
            $res["status"] = TRUE;
        }
        else
        {
            //not all complete
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    /* =========================
     *        Insert
     * ======================= */
    /**
     * บันทึกสถานะการ login 
     * @param String $pEmpID หมายเลขประจำตัว
     * @param String $pStatus [fail, success]
     */
    function insert_log_login($pEmpID, $pStatus = "fail")
    {
        $res = array("status" => FALSE, "msg" => "");
        //1.update user_egat.try_login = (1 or +1), user_egat.try_login_date = now() 
        //2.Insert user_egat_log_login
        //3.If success --> Insert @user_egat_session

        $this->db->trans_start();
        if($pStatus == "success")
        {
            $this->db->set("try_login", 1);
        }
        else
        {
            $this->db->set("try_login", "try_login+1", FALSE);
        }

        //Update table #user_egat 
        $this->db->set("try_login_date", date("Y-m-d H:i:s"));
        $this->db->where("empn", $pEmpID);
        $this->db->update("user_gsd");

        //Insert login log
        $tbl_log["empn"] = $pEmpID;
        $tbl_log["login_result"] = $pStatus;
        $tbl_log["user_agent"] = $this->input->user_agent();
        $tbl_log["ip_address"] = $this->input->ip_address();
        $tbl_log["create_date"] = date("Y-m-d H:i:s");
        $this->db->insert("user_gsd_log_login", $tbl_log);

        $this->db->trans_complete();

        if($this->db->trans_status())
        {
            //send value back
            $res["status"] = TRUE;
        }
        else
        {
            //not all complete
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    function insert_user_session($pDr)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->insert("user_gsd_session", $pDr);

        $this->db->trans_complete();

        if($this->db->trans_status())
        {
            //send value back
            $res["status"] = TRUE;
        }
        else
        {
            //not all complete
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    /* =========================
     *        Delete
     * ======================= */
    function delete_user_session($pEmpID)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->where("empn", $pEmpID);
        $this->db->delete("user_gsd_session");

        $this->db->trans_complete();

        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

}
