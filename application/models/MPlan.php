<?php

class MPlan extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */

    function get_owner($pPlan_ownerID = "")
    {
        if ($pPlan_ownerID !== "")
        {
            $this->db->where("plan_ownerID", $pPlan_ownerID);
            $q = $this->db->get("plan_owner");
            return $q->row_array();
        }
        else
        {
            $this->db->order_by("owner_type", "asc");
            $this->db->order_by("plan_ownerID", "asc");
            $q = $this->db->get("plan_owner");
            return $q->result_array();
        }
    }

    function get_priority($pPriorityID = "")
    {
        if ($pPriorityID !== "")
        {
            $this->db->where("priorityID", $pPriorityID);
            $q = $this->db->get("priority");
            return $q->row_array();
        }
        else
        {
            $this->db->order_by("level", "asc");
            $this->db->order_by("priorityID", "asc");
            $q = $this->db->get("priority");
            return $q->result_array();
        }
    }

    function get_status($pStatusCode = "")
    {
        if ($pStatusCode !== "")
        {
            $this->db->where("status_code", $pStatusCode);
            $q = $this->db->get("plan_status");
            return $q->row_array();
        }
        else
        {
            $q = $this->db->get("plan_status");
            return $q->result_array();
        }
    }

    function get_all_priority($where = "")
    {
        if ($where != "")
        {
            $this->db->where($where);
        }
        $this->db->order_by("level", "asc");
        $this->db->order_by("priorityID", "asc");
        $q = $this->db->get("priority");
        return $q->result_array();
    }

    function view_all($pArrWhere, $pOrderBy = "planID", $pOrderDirection = "ASC", $pLimitBegin = "", $pLimit = "")
    {
        $res_data = array("data" => "", "found_rows" => 0);

        $this->db->select("sql_calc_found_rows *", FALSE);

        if (!empty($pArrWhere))
        {//have where condition
            if (isset($pArrWhere["plan_name"]) && !empty($pArrWhere["plan_name"]))
            {
                $this->db->like("plan_name", $pArrWhere["plan_name"]);
            }
            if (isset($pArrWhere["year"]) && !empty($pArrWhere["year"]))
            {
                $this->db->where("year", trim($pArrWhere["year"]));
            }
            if (isset($pArrWhere["owner"]) && !empty($pArrWhere["owner"]))
            {
                foreach ($pArrWhere["owner"] as $key => $val)
                {
                    $where = "(";
                    foreach ($pArrWhere["owner"] as $k => $v)
                    {
                        $where .= "FIND_IN_SET (" . $v . ",owner)";
                        if (($k + 1) < sizeof($pArrWhere["owner"]))
                        {
                            $where .= " OR ";
                        }
                    }
                    $where .= ")";
                }
                $this->db->where($where);
            }
            if (isset($pArrWhere["priority"]) && !empty($pArrWhere["priority"]))
            {
                foreach ($pArrWhere["priority"] as $key => $val)
                {
                    $where = "(";
                    foreach ($pArrWhere["priority"] as $k => $v)
                    {
                        $where .= "FIND_IN_SET (" . $v . ",priority)";
                        if (($k + 1) < sizeof($pArrWhere["priority"]))
                        {
                            $where .= " OR ";
                        }
                    }
                    $where .= ")";
                }
                $this->db->where($where);
            }
        }

        if (($pLimit !== "") && ($pLimitBegin !== ""))
        {
            $this->db->limit($pLimit, $pLimitBegin);
        }

        //ordering
        $this->db->order_by($pOrderBy, $pOrderDirection);
        $q = $this->db->get("view_all_plan");
//        var_dump($this->db->last_query());
        //data
        $res_data["data"] = $q->result_array();

        //found_rows
        $sql_fr = "select found_rows() as 'fr';";
        $q_fr = $this->db->query($sql_fr)->row_array();
        $res_data["found_rows"] = $q_fr["fr"];

        return $res_data;
    }

    function get_plan_by_id($planID)
    {
        $query = $this->db->get_where("view_all_plan", array("planID" => $planID));
        return $query->row_array();
    }

    function get_plan($pArrWhere)
    {
        $query = $this->db->get_where("view_all_plan", $pArrWhere);
        return $query->result_array();
    }

    function insert($plan, $plan_target)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //insert @plan
        $this->db->insert("plan", $plan);
        $planID = $this->db->insert_id();

        //insert @plan_target     
        $target = array();
        foreach ($plan_target as $k => $item)
        {
            $target[$k]["planID"] = $planID;
            $target[$k]["month"] = $k + 1;
            $target[$k]["percentage"] = $item;
        }
        $this->db->insert_batch("plan_target", $target);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function update($planID, $plan, $plan_target)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //1. update @plan
        $this->db->update("plan", $plan, array("planID" => $planID));

        //2. update @plan_target
        //  2.1 delete @plan_target
        $this->db->delete("plan_target", array("planID" => $planID));

        //  2.2 insert @plan_target
        $target = array();
        foreach ($plan_target as $k => $item)
        {
            $target[$k]["planID"] = $planID;
            $target[$k]["month"] = $k + 1;
            $target[$k]["percentage"] = $item;
        }
        $this->db->insert_batch("plan_target", $target);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function delete($planID)
    {
        $this->db->trans_start();

        //1. delete @plan_target
        $this->db->delete("plan_target", array("planID" => $planID));

        //2. delete @plan        
        $this->db->delete("plan", array("planID" => $planID));

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function get_plan_target_by_planID($planID)
    {
        $query = $this->db->get_where("plan_target", array("planID" => $planID));
        return $query->result_array();
    }

    function mark_is_complete($planID)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //1. update @plan
        $this->db->where("planID", $planID);
        $this->db->update("plan", array("is_complete" => "1", "updateDate" => date("Y-m-d H:i:s")));

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }
        return $res;
    }

    function get_data_plan_for_chart($year, $month, $owner = "")
    {
        $sql = "SELECT pr.`status`,pr.status_name,count(*) as count
                FROM view_plan_report as pr
                LEFT JOIN view_all_plan AS p ON p.planID = pr.planID
                WHERE (pr.target_percent <> 0 OR (pr.target_percent = 0 AND pr.`month` > (SELECT MAX(`month`) FROM view_plan_report WHERE target_percent <> 0 AND planID = pr.planID GROUP BY planID)))
                AND p.`year` = " . $year . "
                AND `month` = " . $month;
        if ($owner != "")
        {
            $sql.= " AND FIND_IN_SET(" . $owner . ",p.owner) > 0";
        }
        $sql .= " GROUP BY pr.status";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
    }

    function view_list_plan($year, $month, $owner = "", $pOrderBy = "", $pOrderDirection = "ASC", $pLimitBegin = "", $pLimit = "")
    {
        $sql = " SELECT SQL_CALC_FOUND_ROWS *
                FROM view_plan_report as pr
                LEFT JOIN view_all_plan AS p ON p.planID = pr.planID
                WHERE (pr.target_percent <> 0 OR (pr.target_percent = 0 AND pr.`month` > (SELECT MAX(`month`) FROM view_plan_report WHERE target_percent <> 0 AND planID = pr.planID GROUP BY planID)))
                AND p.`year` = " . $year . "
                AND `month` = " . $month;
        if ($owner != "")
        {
            $sql.= " AND FIND_IN_SET(" . $owner . ",p.owner) > 0";
        }
//        $sql .= " ORDER BY p.planID, pr.`month`";

        if ($pOrderBy !== "")
        {
            $sql .= " ORDER BY " . $pOrderBy . " " . $pOrderDirection;
        }

        if (($pLimit !== "") && ($pLimitBegin !== ""))
        {
            $sql .= " LIMIT " . $pLimit . " OFFSET " . $pLimitBegin;
        }

        $query = $this->db->query($sql);
        $result = $query->result_array();
//        var_dump($this->db->last_query());
        $num = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        $arr_result["found_rows"] = $num;
        $arr_result["data"] = $result;
        return $arr_result;
    }
    
    function get_cnt_list_plan($year, $month, $owner = "", $pOrderBy = "", $pOrderDirection = "ASC", $pLimitBegin = "", $pLimit = "")
    {
        $sql = " SELECT *
                FROM view_plan_report as pr
                LEFT JOIN view_all_plan AS p ON p.planID = pr.planID
                WHERE (pr.target_percent <> 0 OR (pr.target_percent = 0 AND pr.`month` > (SELECT MAX(`month`) FROM view_plan_report WHERE target_percent <> 0 AND planID = pr.planID GROUP BY planID)))
                AND p.`year` = " . $year . "
                AND `month` = " . $month;
        if ($owner != "")
        {
            $sql.= " AND FIND_IN_SET(" . $owner . ",p.owner) > 0";
        }
    }

}
