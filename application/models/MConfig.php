<?php
class Mconfig extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function get_all($pType)
    {
        $this->db->where("config_type", $pType);
        $this->db->order_by("seq", "ASC");
        $q =  $this->db->get("config");
        
        return $q->result_array();
    }
}

