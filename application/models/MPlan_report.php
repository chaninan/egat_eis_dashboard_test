<?php

class MPlan_report extends CI_Model
{

    function save_report($plan_report)
    {
        /* 1. check already report ?
          => 2. IF no => insert
          => 3. IF yes => update */

        // 1.
        $q = $this->db->get_where("plan_report", array("planID" => $plan_report["planID"], "month" => $plan_report["month"]));
        //found_rows
        $sql_fr = "select found_rows() as 'fr';";
        $q_fr = $this->db->query($sql_fr)->row_array();
        $count = $q_fr["fr"];
        if ($count == 0)
        {
            // 2.            
            return $this->insert($plan_report);
        }
        else
        {
            // 3.
            return $this->update($plan_report);
        }
    }

    function insert($plan_report)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        unset($plan_report["updateDate"]);
        unset($plan_report["updateBy"]);
        //insert @plan_report
        $this->db->insert("plan_report", $plan_report);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function update($plan_report)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $planID = $plan_report["planID"];
        $month = $plan_report["month"];
        $new_actual_percent = $plan_report["percentage"];

        // get current actual_percent
        $dr_plan_report = $this->get_one_plan_report(array("planID" => $planID, "month" => $month));
        $curr_actual_percent = $dr_plan_report["actual_percent"];

        // 1. กรณี ค่าใหม่ เพิ่มเป็น 100%
        if ($new_actual_percent == 100)
        {
            if (!$this->utils_plan->check_month_is_last_report($planID, $month))
            {
                // - ให้ลบ record รายงานแผนเดือนถัดมานั้นทิ้ง
                $this->db->where("planID", $planID);
                $this->db->where("month >", $month);
                $this->db->delete("plan_report");

                // - update is_complete = 0
                $set_update["is_complete"] = 0;
                $set_update["updateDate"] = date("Y-m-d H:i:s");
                $set_update["updateBy"] = $plan_report["updateBy"];
                $this->db->where("planID", $planID);
                $this->db->update("plan", $set_update);
            }
        }
        // 2. กรณี ค่าเดิมเป็น 100% แต่ค่าใหม่ลดลงจากเดิม
        if (($curr_actual_percent == 100) && ($new_actual_percent < 100))
        {
            $this->load->model("mplan");
            if ($this->utils_plan->check_is_plan_report_complete($planID))
            {
                // - update is_complete = 0
                $set_update["is_complete"] = 0;
                $set_update["updateDate"] = date("Y-m-d H:i:s");
                $set_update["updateBy"] = $plan_report["updateBy"];
                $this->db->where("planID", $planID);
                $this->db->update("plan", $set_update);
            }
        }

        unset($plan_report["createDate"]);
        unset($plan_report["createBy"]);
        // update @plan_report
        $this->db->where("planID", $planID);
        $this->db->where("month", $month);
        $this->db->update("plan_report", $plan_report);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function delete($plan_report)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();
        
        //1. delete @plan_report
        $this->db->where("planID", $plan_report["planID"]);
        $this->db->where("month", $plan_report["month"]);
        $this->db->delete("plan_report");

        //2. update is_complete = 0
//        $set_update["is_complete"] = 0;
//        $set_update["updateDate"] = $plan_report["updateDate"];
//        $set_update["updateBy"] = $plan_report["updateBy"];
//        $this->db->where("planID", $plan_report["planID"]);
//        $this->db->update("plan", $set_update);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function cancel_plan($plan_report)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        /* 1. เช็คว่ามีเดือนที่จะ cancel หรือไม่?
          ==> มี : 1.1 update percentage ='c'
          ==> ไม่มี : 1.2 insert ใหม่ ด้วย percentage ='c'
          2. ลบรายงานเดือนถัดมา
          3. update is_complete = 2 */
        // 1.
        $dr_plan_report = $this->get_one_report(array("planID" => $plan_report["planID"], "month" => $plan_report["month"]));
        $createBy = $plan_report["createBy"];
        if (!empty($dr_plan_report))
        { // 1.1 update @plan_report            
            $plan_report["updateDate"] = date("Y-m-d H:i:s");
            $plan_report["updateBy"] = $createBy;
            $plan_report["attach_file"] = null;
            $this->db->where("planID", $plan_report["planID"]);
            $this->db->where("month", $plan_report["month"]);
            $this->db->update("plan_report", $plan_report);
        }
        else
        { // 1.2 insert @plan_report            
            $plan_report["createDate"] = date("Y-m-d H:i:s");
            $plan_report["createBy"] = $createBy;
            $this->db->insert("plan_report", $plan_report);
        }

        // 2.ลบ record รายงานแผนเดือนถัดมา
        $this->db->where("planID", $plan_report["planID"]);
        $this->db->where("month >", $plan_report["month"]);
        $this->db->delete("plan_report");

        // 3. update is_complete = 2
        $set_update["is_complete"] = 2;
        $set_update["updateDate"] = date("Y-m-d H:i:s");
        $set_update["updateBy"] = $createBy;
        $this->db->where("planID", $plan_report["planID"]);
        $this->db->update("plan", $set_update);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function get_one_plan_report($pArrWhere)
    {
        $query = $this->db->get_where("view_plan_report", $pArrWhere);
        return $query->row_array();
    }

    function get_plan_report_by_id($planID)
    {
        $query = $this->db->get_where("view_plan_report", array("planID" => $planID));
        return $query->result_array();
    }
    
    function get_plan_report($pArrWhere)
    {
        $query = $this->db->get_where("view_plan_report", $pArrWhere);
        return $query->result_array();
    }

    function get_one_report($pArrWhere)
    {
        $this->db->order_by("planID", "asc");
        $this->db->order_by("month", "asc");
        $query = $this->db->get_where("plan_report", $pArrWhere);
        return $query->row_array();
    }

    function get_report($pArrWhere)
    {
        $this->db->order_by("planID", "asc");
        $this->db->order_by("month", "asc");
        $query = $this->db->get_where("plan_report", $pArrWhere);
        return $query->result_array();
    }

    // หารายการที่ยังไม่ได้รายงาน
    function get_count_pending_report($pArrWhere)
    {
        $this->db->select("count(*) as count");
        $this->db->from("view_plan_report");
        $this->db->where("actual_percent is null");
        $this->db->where($pArrWhere);

        $query = $this->db->get();
        $result = $query->row_array();
        return $result["count"];
    }

    function get_lastest_plan_report($planID)
    {
        $sql = "SELECT * 
               FROM view_plan_report
               WHERE planID = " . $planID . "
               AND actual_percent is not null
               ORDER BY `month` desc
               LIMIT 1";

        $query = $this->db->query($sql);
        return $query->row_array();
    }

}
