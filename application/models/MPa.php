<?php

class MPa extends CI_Model {

    function insert($pa)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //insert @pa
        $this->db->insert("pa", $pa);

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function update($pa)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();
        $paID = $pa["paID"];
        //update @pa
        $this->db->update("pa", $pa, array("paID" => $paID));

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function delete($paID)
    {
        $this->db->trans_start();

        //1. delete @pa       
        $this->db->delete("pa", array("paID" => $paID));

        //set transaction complete
        $this->db->trans_complete();
        if ($this->db->trans_status())
        {
            $res["status"] = TRUE;
            $this->db->trans_commit();
        }
        else
        {
            error_log("error", $this->db->_error_message());
            $error_msg = $this->my_log->write_log($this->db->_error_message());
            $this->db->trans_rollback();
            $res["msg"] = $error_msg;
        }

        return $res;
    }

    function view_all($pArrWhere, $pOrderBy = "createDate", $pOrderDirection = "DESC", $pLimitBegin = "", $pLimit = "")
    {
        $res_data = array("data" => "", "found_rows" => 0);

        $this->db->select("sql_calc_found_rows *", FALSE);

        if (!empty($pArrWhere))
        {//have where condition
//            if (isset($pArrWhere["reserveID"]) && !empty($pArrWhere["reserveID"]))
//            {
//                $this->db->where("reserveID", $pArrWhere["reserveID"]);
//            }
//            if (isset($pArrWhere["reserve_no"]) && !empty($pArrWhere["reserve_no"]))
//            {
//                $this->db->like("reserve_no", trim($pArrWhere["reserve_no"]));
//            }            
        }

        if (($pLimit !== "") && ($pLimitBegin !== ""))
        {
            $this->db->limit($pLimit, $pLimitBegin);
        }

        //ordering
        $this->db->order_by("year", "asc");
        $this->db->order_by("period", "asc");
        $this->db->order_by("priorityID", "asc");
        $this->db->order_by($pOrderBy, $pOrderDirection);
        $q = $this->db->get("view_all_pa");

        //data
        $res_data["data"] = $q->result_array();
        //found_rows
        $sql_fr = "select found_rows() as 'fr';";
        $q_fr = $this->db->query($sql_fr)->row_array();
        $res_data["found_rows"] = $q_fr["fr"];

        return $res_data;
    }

    function get_one_pa($pArrWhere)
    {
        $query = $this->db->get_where("view_all_pa", $pArrWhere);
        return $query->row_array();
    }

    function get_pa_point($pArrWhere)
    {
        $query = $this->db->get_where("view_all_pa", $pArrWhere);
        $result = $query->row_array();
        if (!empty($result))
        {
            return $result["point"];
        }
    }

}
