<?php

class MBudget_operate extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function get($pBudget_operateID)
    {
        $sql = "SELECT bo.*, u.unit_name
                from budget_operate as bo 
                        LEFT JOIN unit as u on u.unitID = bo.unitID 
                where budget_operateID = {$pBudget_operateID};";
        $q = $this->db->query($sql);
        $dr_budget = $q->row_array();
        if(!empty($dr_budget))
        {
            $dr_budget["month_name"] = $this->utils->get_months($dr_budget["month"]);
            $dr_budget["year_thai"] = ($dr_budget["year"]) + 543;
            return $dr_budget;
        }
    }

    function check_year_exist($pYear)
    {
        $this->db->select("year");
        $this->db->distinct();
        if(is_array($pYear))
        {//case param is array
            $this->db->where_in("year", $pYear);
        }
        else
        {
            $this->db->where("year", $pYear);
        }

        $q = $this->db->get("budget_operate");

        return $q->result_array();
    }

    function get_view_all_admin()
    {
        $sql = "select  sum(bo.plan) as 'sum_plan', sum(bo.actual) as 'sum_actual' , u.unit_name, bo.`year`
                from budget_operate as bo
                        LEFT JOIN unit as u on (bo.unitID = u.unitID)
                GROUP BY bo.unitID, bo.`year`
                ORDER BY bo.`year` DESC , bo.unitID ASC;";

        $q = $this->db->query($sql);

        return $q->result_array();
    }

    function get_view_admin($pYear)
    {
        $sql = "select  bo.*, u.unit_name, if(ISNULL(bo.updateBy),uc.`name`, uu.`name`) as latest_name, IF(ISNULL(bo.updateDate), bo.createDate, bo.updateDate) as latest_date,  cnt_remark.cnt as 'count_remark'
                from budget_operate as bo
                        LEFT JOIN unit as u on (bo.unitID = u.unitID)
                        LEFT JOIN user_gsd as uc on (bo.createBy = uc.empn)
                        LEFT JOIN user_gsd as uu on (bo.updateBy = uu.empn)
			LEFT JOIN (SELECT count(budget_operateID) as cnt , budget_operateID from budget_operate_remark GROUP BY budget_operateID) as cnt_remark on cnt_remark.budget_operateID = bo.budget_operateID
                where bo.`year` = '{$pYear}' 
                ORDER BY bo.unitID ASC, bo.`year` DESC , bo.`month` ASC;";

        $q = $this->db->query($sql);

        return $q->result_array();
    }

    function get_edit_admin($pYear)
    {
        $sql = "select bo.*, u.unit_name, cnt_remark.cnt as 'count_remark'
                from budget_operate as bo
                    LEFT JOIN unit as u on (bo.unitID = u.unitID)
                    LEFT JOIN (SELECT count(budget_operateID) as cnt , budget_operateID from budget_operate_remark GROUP BY budget_operateID) as cnt_remark on cnt_remark.budget_operateID = bo.budget_operateID
                WHERE bo.`year` = {$pYear}
                ORDER BY  bo.unitID ASC, bo.`month` ASC ;";

        $q = $this->db->query($sql);

        return $q->result_array();
    }

    function get_graph_bar_plan_actual($pYear, $pUnitID = "")
    {
        $sql = "SELECT IFNULL(sum(plan), 0) as 'plan', IFNULL(sum(actual),0) as 'actual' , `month`, budget_operateID
            from budget_operate
            where `year` = {$pYear}";

        if($pUnitID != "")
        {
            $sql .= " and unitID = " . $pUnitID;
        }

        $sql .= " GROUP BY `month`, `year`, budget_operateID
                 ORDER BY `month` ASC;";

        $q = $this->db->query($sql);

        //insert month name
        $dt_data = $q->result_array();
        if(!empty($dt_data))
        {//have data
            foreach($dt_data as &$i)
            {//push month name
                $i["month_name"] = $this->utils->get_months($i["month"]);
            }
        }

        return $dt_data;
    }

    /**
     * SQL เพื่อหาข้อมูลของการใช้งบประมาณทำการ เฉพาะภาพรวมฝ่าย เนื่องจากต้องไม่มี group by ID
     */
    function get_graph_bar_plan_actual_overall($pYear, $pUnitID = "")
    {
        $sql = "SELECT IFNULL(sum(plan), 0) as 'plan', IFNULL(sum(actual),0) as 'actual' , `month`
            from budget_operate
            where `year` = {$pYear}";

        if($pUnitID != "")
        {
            $sql .= " and unitID = " . $pUnitID;
        }

        $sql .= " GROUP BY `month`, `year`
                 ORDER BY `month` ASC;";

        $q = $this->db->query($sql);

        //insert month name
        $dt_data = $q->result_array();
        if(!empty($dt_data))
        {//have data
            foreach($dt_data as &$i)
            {//push month name
                $i["month_name"] = $this->utils->get_months($i["month"]);
            }
        }

        return $dt_data;
    }

    function get_graph_bar_each_month($pBudget_operateID)
    {
        $this->db->where("budget_operateID", $pBudget_operateID);
        $q = $this->db->get("budget_operate");

        $dr_budget = $q->row_array();
        if(!empty($dr_budget))
        {
            $dr_budget["month_name"] = $this->utils->get_months($dr_budget["month"]);
        }

        return $dr_budget;
    }

    /**
     * 1. หาค่ารวม ของ Plan
     * 2. หาค่ารวม แยกตามเดือน ของ actual, Plan
     * 3. หา accumulate ของ Actual
     * 4. หา accumulate ของ Plan
     * @param type $pYear
     * @param type $pUnitID
     * @return array 1.sum_plan 2.dt_accumulate_actual[month, month_name, accumulate] 3.dt_accumulate_plan[month, month_name, accumulate]
     */
    function get_graph_line_plan_accumulate($pYear, $pUnitID = "")
    {
        //1.
        $sql_sum_plan = "SELECT sum(plan) as sum_plan
                        from budget_operate
                        where `year` = '{$pYear}' ";
        if($pUnitID != "")
        {
            $sql_sum_plan .= " and unitID = " . $pUnitID;
        }

        $q_sum_plan = $this->db->query($sql_sum_plan);
        $dr_sum_plan = $q_sum_plan->row_array();
        $sum_plan = $dr_sum_plan["sum_plan"];

        //2.
        $sql_sum_each_month = "SELECT IFNULL(sum(plan), 0) as 'plan', IFNULL(sum(actual),0) as 'actual' , `month`
                from budget_operate
                where `year` = {$pYear}";

        if($pUnitID != "")
        {
            $sql_sum_each_month .= " and unitID = " . $pUnitID;
        }

        $sql_sum_each_month .= " GROUP BY `month`, `year`  
                 ORDER BY `month` ASC;";

        $q_sum_each_month = $this->db->query($sql_sum_each_month);
        $dt_sum_each_month = $q_sum_each_month->result_array();

        if(!empty($dt_sum_each_month))
        {
            //insert month name
            foreach($dt_sum_each_month as &$i)
            {//push month name
                $i["month_name"] = $this->utils->get_months($i["month"]);
            }

            //accumulate
            $dt_accumulate_plan = array();
            $dt_accumulate_actual = array();
            $sum_plan = 0;
            $sum_actual = 0;

            //3.
            foreach($dt_sum_each_month as $item)
            {
                if(doubleval($item["actual"]) != 0)
                {
                    $tmp_actual["month"] = $item["month"];
                    $sum_actual += doubleval($item["actual"]);
                    $tmp_actual["accumulate"] = $sum_actual;
                    $tmp_actual["month_name"] = $item["month_name"];
                    //keep in array
                    $dt_accumulate_actual[] = $tmp_actual;
                }
            }

            //4.
            foreach($dt_sum_each_month as $item)
            {
                $tmp_plan["month"] = $item["month"];
                $sum_plan += doubleval($item["plan"]);
                $tmp_plan["accumulate"] = $sum_plan;
                $tmp_plan["month_name"] = $item["month_name"];
                //keep in array
                $dt_accumulate_plan[] = $tmp_plan;
            }

            return array("sum_plan" => $sum_plan,
                "dt_accumulate_actual" => $dt_accumulate_actual,
                "dt_accumulate_plan" => $dt_accumulate_plan);
        }
    }

    function get_all_budget_remark($pBudget_operateID)
    {
        $sql = "SELECT bor.*, ugc.`name` as 'create_name', ugu.`name` as 'update_name'
                from budget_operate_remark as bor 
                        LEFT JOIN user_gsd as ugc on ugc.empn =bor.createBy
                        LEFT JOIN user_gsd as ugu on ugu.empn = bor.updateBy
                where bor.budget_operateID = {$pBudget_operateID} 
                order by  bor.createDate ASC; ";
        $q = $this->db->query($sql);

        return $q->result_array();
    }

    function get_budget_remark($pBudget_operate_remarkID)
    {
        $this->db->where("budget_operate_remarkID", $pBudget_operate_remarkID);
        $q = $this->db->get("budget_operate_remark");

        return $q->row_array();
    }

    function get_latest_update($pYear, $pUnitID = "")
    {
        $sql = "select  IFNULL(max(updateDate), max(createDate) )  as latest_update
			from budget_operate
			where year = {$pYear}";
        if($pUnitID != "")
        {
            $sql .= " and unitID = {$pUnitID}";
        }

        $q = $this->db->query($sql);
        $dr = $q->row_array();

        if(!empty($dr))
        {
            return $dr["latest_update"];
        }
        else
        {
            return "";
        }
    }

    function get_count_remark_by_budgetID($pBudget_operateID)
    {
        $sql = "SELECT count(*) as 'cnt_remark'
                from budget_operate_remark
                where budget_operateID = {$pBudget_operateID}";
        $q = $this->db->query($sql);
        $dr = $q->row_array();

        if(!empty($dr))
        {
            return $dr["cnt_remark"];
        }
        else
        {
            return 0;
        }
    }

    /* =========================
     *        Insert
     * ======================= */
    function insert_budget($pDT_budget)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->insert_batch("budget_operate", $pDT_budget);

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    function insert_budget_remark($pDT)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->insert("budget_operate_remark", $pDT);

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    /* =========================
     *        Update
     * ======================= */
    function update_batch($pData, $pMonth, $pYear)
    {
        $res = array("status" => FALSE, "msg" => "");

        $this->db->trans_start();

        foreach($pData as $key => $value)
        {
            $this->db->where("month", $pMonth);
            $this->db->where("year", $pYear);
            $this->db->where("unitID", $key);
            $this->db->set("plan", $value["plan"]);
            $this->db->set("actual", $value["actual"]);
            $this->db->update("budget_operate");
        }

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    function update_remark($pBudget_operate_remarkID, $pData)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->where("budget_operate_remarkID", $pBudget_operate_remarkID);
        $this->db->update("budget_operate_remark", $pData);

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    function delete_remark($pID)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->where("budget_operate_remarkID", $pID);
        $this->db->delete("budget_operate_remark");

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

}
