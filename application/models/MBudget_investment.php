<?php

class MBudget_investment extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function is_duplicate_item_name($pItem_name, $pUnitID, $pYear, $pID = "")
    {
        $this->db->where("item_name", $pItem_name);
        $this->db->where("year", $pYear);
        $this->db->where("unitID", $pUnitID);

        if($pID != "")
        {//not ID
            $this->db->where("unitID !=", $pID);
        }

        $q = $this->db->get("budget_investment");
        $count_row = $q->num_rows();
        if($count_row > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    function get_all_by_unit_year($pUnitID, $pYear, $pTrimester = "")
    {
        $sql = "SELECT *
                from vw_budget_investment 
                where unitID = {$pUnitID} and year = {$pYear} ";
        if($pTrimester != "")
        {
            $sql .= " and trimester = {$pTrimester}";
        }
        $sql .= " ORDER by latest_status_seq ASC;";

        $q = $this->db->query($sql);
        return $q->result_array();
    }

    function get($pBudget_investmentID)
    {
        $this->db->where('budget_investmentID', $pBudget_investmentID);
        $q = $this->db->get("vw_budget_investment");

        return $q->row_array();
    }

    function get_all_log($pBudget_investmentID)
    {
        $this->db->where("budget_investmentID", $pBudget_investmentID);
        $this->db->order_by("createDate", "ASC");
        $q = $this->db->get("vw_budget_investment_update");

        return $q->result_array();
    }

    function count_by_year_unit($pYear, $ptrimester = "", $pUnitID = "")
    {
        $this->db->select("count(*) as 'cnt'");
        $this->db->from("budget_investment");
        $this->db->where("year", $pYear);
        if($ptrimester != "")
        {
            $this->db->where("trimester", $ptrimester);
        }
        if($pUnitID != "")
        {
            $this->db->where("unitID", $pUnitID);
        }

        $q = $this->db->get();
        if(!empty($q))
        {
            $dr = $q->row_array();
            return intval($dr["cnt"]);
        }
        else
        {
            return 0;
        }
    }

    function get_pie_data_overall($pYear, $pTrimester, $pUnitID = "")
    {
        $sql = "select cfg.*, COALESCE(cnt_bi.cnt ,0) as 'cnt_status'
                from config as cfg
                       LEFT JOIN 
                       ( select count(*) as 'cnt', latest_statusID, bi.`year`, bi.trimester
                        from budget_investment as bi 
                        where bi.`year` = {$pYear} and bi.trimester = {$pTrimester}  ";

        if($pUnitID != "")
        {
            $sql .= " and bi.unitID = {$pUnitID}";
        }

        $sql .= " GROUP BY bi.latest_statusID, bi.trimester, bi.`year`  ) as cnt_bi on 
                        (cfg.configID = cnt_bi.latest_statusID)
        where cfg.config_type = 'investment_status' 
        order by cfg.seq ASC;";

        $q = $this->db->query($sql);

        return $q->result_array();
    }

    function get_bar_data_usage_overall($pYear, $pTrimester, $pUnitID = "")
    {
        $sql = "SELECT sum(plan) as 'plan', sum(actual) as 'actual'
                from budget_investment
               where `year` = {$pYear} and trimester = {$pTrimester}";
        if($pUnitID != "")
        {
            $sql .= " and unitID = {$pUnitID}";
        }

        $q = $this->db->query($sql);

        return $q->row_array();
    }

    function get_latest_update_date($pYear, $pTrimester, $pUnitID = "")
    {
        $sql = "SELECT IFNULL(max(updateDate), max(createDate)) as 'latest_update'
                from budget_investment
                where `year` = {$pYear}  and trimester = {$pTrimester} ";
        if($pUnitID != "")
        {
            $sql .= "and unitID = {$pUnitID}";
        }

        $q = $this->db->query($sql);

        if(!empty($q))
        {
            $dr = $q->row_array();
            return $dr["latest_update"];
        }
        else
        {
            return date("Y-m-d H:i:s");
        }
    }

    /* =========================
     *        Insert
     * ======================= */
    function insert($pData)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->insert("budget_investment", $pData);

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    /**
     * Function นี้ Update ที่ #budget_investment และ Insert ที่ #budget_investment_update
     * @param type $pData
     * @return type
     */
    function insert_budget_update($pData)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $budget_investmentID = $pData["budget_investmentID"];
        //update #budget_investment
        $dr_update["latest_statusID"] = $pData["latest_statusID"];
        $dr_update["latest_work_statusID"] = $pData["latest_work_statusID"];
        $dr_update["updateBy"] = $pData["createBy"];
        $dr_update["updateDate"] = $pData["createDate"];
        if(isset($pData["actual"]))
        {//check have actual ? 
            $dr_update["actual"] = $pData["actual"];
            //unset for #budget_investment_update
            unset($pData["actual"]);
        }
        if(isset($pData["remark"]))
        {//check have remark ?
            $dr_update["latest_remark"] = $pData["remark"];
        }

        $this->db->where("budget_investmentID", $budget_investmentID);
        $this->db->update("budget_investment", $dr_update);

        //Insert #budget_investment_update
        $this->db->insert("budget_investment_update", $pData);

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

    /* =========================
     *        Update
     * ======================= */
    function update($pID, $pData)
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        $this->db->where("budget_investmentID", $pID);
        $this->db->update("budget_investment", $pData);

        $this->db->trans_complete();
        if($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        return $res;
    }

}
