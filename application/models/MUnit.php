<?php

class MUnit extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */

    function get_all()
    {
        $this->db->order_by("unitID", "asc");
        $q = $this->db->get("unit");

        return $q->result_array();
    }

    function get($pUnitID)
    {
        $this->db->where("unitID", $pUnitID);
        $q = $this->db->get("unit");

        return $q->row_array();
    }

    function get_gsd_unit($pWhere = "")
    {
        if ($pWhere == "")
        {
            $q = $this->db->get("view_gsd_unit");
            return $q->result_array();
        }
        else
        {
            $q = $this->db->get_where("view_gsd_unit", $pWhere);
            return $q->row_array();
        }
    }

}
