<?php

class MUnit_sub extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function get_all()
    {
        $this->db->order_by("unit_subID", "asc");
        $q = $this->db->get("unit_sub");

        return $q->result_array();
    }

    function get($pUnit_subID)
    {
        $this->db->where("unit_subID", $pUnit_subID);
        $q = $this->db->get("unit_sub");

        return $q->row_array();
    }

}
