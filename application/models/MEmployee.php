<?php

class MEmployee extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */

    function view_active_employee($year, $unit = "", $vut = "", $is_unit = "0")
    {
        $this->db->where("year", $year);

        if ($unit !== "")
        {
            if ($is_unit == "1") // กอง
            {
                $this->db->like("dept", substr($unit, 0, 6), 'after');
            }
            else // 
            {
                $this->db->where("dept", $unit);
            }
        }
        if ($vut !== "")
        {
            $this->db->where("q_level", $vut);
        }

        $this->db->order_by("dept");
        $this->db->order_by("ladum2", "DESC");

        $query = $this->db->get("view_active_employee");
        $result = $query->result_array();
        $num = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        $arr_result["cnt"] = $num;
        $arr_result["result"] = $result;
        return $arr_result;
    }

    function view_retire_employee($year, $unit = "", $vut = "", $is_unit = "0")
    {
        $this->db->where("year", $year);

        if ($unit !== "")
        {
            if ($is_unit == "1")
            {
                $this->db->like("dept", substr($unit, 0, 6), 'after');
            }
            else
            {
                $this->db->where("dept", $unit);
            }
        }
        if ($vut !== "")
        {
            $this->db->where("q_level", $vut);
        }

        $this->db->order_by("dept");
        $this->db->order_by("ladum2", "DESC");


        $query = $this->db->get("view_retire_employee");
        $result = $query->result_array();
        $num = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        $arr_result["cnt"] = $num;
        $arr_result["result"] = $result;
        return $arr_result;
    }

    /*     * ********************************************************************************** */
    /*     * ********************************************************************************** */
    /*     * ********************************************************************************** */

    function get_data_for_chart($pType, $start_year, $end_year, $pDept = "", $is_unit = "0")
    {
        $sql = $this->get_sql_data_by_type($start_year, $end_year, $pType, $pDept, $is_unit);
        $q = $this->db->query($sql);
//        var_dump($this->db->last_query());
        $result = $q->result_array();
        switch ($pType)
        {
            case "pos" : $key_field = "postb";
                break;
            case "level" : $key_field = "ladum2";
                break;
            case "vut" : $key_field = "level_name";
                break;
            case "age" : $key_field = "agetow";
                break;
        }
        $arr_result["tbl_header_label"] = array_unique(array_column($result, $key_field));     
        $arr_result["result"] = $result;

        return $arr_result;
    }

    function get_sql_data_by_type($start_year, $end_year, $pType, $pDept, $is_unit)
    {
        // get where condition
        $sql_where = " WHERE 1=1";
        if ($pDept !== "")
        {
            if ($is_unit == "1")
            {
                $sql_where .= " AND dept like '" . substr($pDept, 0, 6) . "%'";
            }
            else
            {
                $sql_where .= " AND dept=" . $pDept;
            }
        }

        $sql_where .= " AND year >= " . $start_year;
        $sql_where .= " AND year <= " . $end_year;

        if ($pType == "vut")
        {
            $sql = "SELECT act.year, act.q_level, v.level_name, IFNULL(active_emp,0) as active_emp, IFNULL(retire_emp,0) as retire_emp, IFNULL(replace_emp,0) as replace_emp
                    FROM (SELECT year, q_level, count(*) as active_emp FROM view_active_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, q_level) AS act";
            $sql .= " LEFT JOIN (SELECT year, q_level, count(*) as retire_emp FROM view_retire_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, q_level) AS ret ON (ret.q_level = act.q_level AND ret.year = act.year)";
            $sql .= " LEFT JOIN (SELECT year, q_level, count(*) AS replace_emp FROM view_replacement_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, q_level) AS rpl ON (rpl.q_level = act.q_level AND rpl.year = act.year)";
            $sql .= " JOIN view_vut AS v ON v.q_level = act.q_level";
            $sql .= " ORDER BY q_level";
        }
        elseif ($pType == "level")
        {
            $sql = "SELECT act.year, act.ladum2, IFNULL(active_emp,0) as active_emp, IFNULL(retire_emp,0) as retire_emp
                    FROM (SELECT year, ladum2, count(*) as active_emp FROM view_active_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, ladum2) AS act";
            $sql .= " LEFT JOIN (SELECT year, ladum2, count(*) as retire_emp FROM view_retire_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, ladum2) AS ret ON (ret.ladum2 = act.ladum2 AND ret.year = act.year)";
            $sql .= " ORDER BY ladum2 desc";
        }
        elseif ($pType == "age")
        {
            $sql = "SELECT act.year, act.agetow, IFNULL(active_emp,0) as active_emp, IFNULL(retire_emp,0) as retire_emp
                    FROM (SELECT year, agetow, count(*) as active_emp FROM view_active_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, agetow) AS act";
            $sql .= " LEFT JOIN (SELECT year, agetow,count(*) as retire_emp FROM view_retire_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, agetow) AS ret ON (ret.agetow = act.agetow AND ret.year = act.year)";
            $sql .= " ORDER BY agetow";
        }
        elseif ($pType == "pos")
        {
            $sql = "SELECT act.year, act.postID,act.postb, IFNULL(active_emp,0) as active_emp, IFNULL(retire_emp,0) as retire_emp, IFNULL(replace_emp,0) as replace_emp
                    FROM (SELECT year, SUBSTRING_INDEX(postc,\" \",1) as postID, postb, count(*) as active_emp FROM view_active_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, postb) AS act";
            $sql .= " LEFT JOIN (SELECT year, SUBSTRING_INDEX(postc,\" \",1) as postID, postb, count(*) as retire_emp FROM view_retire_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, postb) AS ret ON (ret.postb = act.postb AND ret.year = act.year)";
            $sql .= " LEFT JOIN (SELECT year, postID, post_name, count(*) AS replace_emp FROM view_replacement_employee";
            $sql .= $sql_where;
            $sql .= " GROUP BY year, postID) AS rpl ON (rpl.post_name = act.postb AND rpl.year = act.year)";
            $sql .= " ORDER BY act.postID";
        }

        return $sql;
    }

    function get_count_emp_by_dept($where)
    {
        $cur_year = date("Y") + 543;
        $table_name = "user_gsd_" . $cur_year;

        $sql_total = "SELECT count(*) as count FROM " . $table_name . $where;
        $q_cnt = $this->db->query($sql_total);
        $total = $q_cnt->row_array();
        return $total["count"];
    }

    function get_information_date()
    {
        $curr_year = date('Y') + 543;
        $table_name = "user_gsd_" . $curr_year;
        $sql = "select DATE_FORMAT(MAX(updateDate),'%Y-%m-%d') as last_update_date from gsd_employee." . $table_name;
        $q = $this->db->query($sql);
        $result = $q->row_array();
        return $result["last_update_date"];
    }

}
