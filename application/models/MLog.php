<?php

class MLog extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function get_summary_sync_log()
    {
        $sql = "SELECT sl.*
                FROM user_gsd_sync_log as sl
                JOIN (
                SELECT max(updateDate) as mx_date, serviceName
                FROM user_gsd_sync_log 
                GROUP BY serviceName
                ) as mx
                ON sl.updateDate = mx.mx_date AND sl.serviceName = mx.serviceName";
        $q = $this->db->query($sql);

        return $q->result_array();
    }   

}
