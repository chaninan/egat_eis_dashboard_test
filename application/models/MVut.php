<?php

class MVut extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function get_all()
    {
        $q = $this->db->get("vut");

        return $q->result_array();
    }

    function get($pUnitID)
    {
        $this->db->where("vutID", $pUnitID);
        $q = $this->db->get("vut");

        return $q->row_array();
    }

}
