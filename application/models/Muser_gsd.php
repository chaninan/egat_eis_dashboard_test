<?php

class Muser_gsd extends CI_Model
{
    /* =========================
     *        Select
     * ======================= */
    function get_by_empID($pEmpn)
    {
        $this->db->where("empn", $pEmpn);
        $q =  $this->db->get("user_gsd");
        
        return $q->row_array();
    }  
    
}