<?php

function already_login()
{
    $res = FALSE;
    if(class_exists('login_utils'))
    {
         $ci = & get_instance();
         $res = $ci->login_utils->have_login();
    }
    
    return $res;
}

function get_name_who_login()
{
    $res = "-";
    if(class_exists('login_utils'))
    {
        $ci = & get_instance();
        $res = $ci->login_utils->get_data("name");
    }
    return $res;
}
