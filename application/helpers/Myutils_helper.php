<?php

//Method นี้ไว้สำหรับ render <div id='msgbox'> </div>
//msgbox = array("is_show" => FALSE, "msg" => "", "class" => "", "target_id" => "");

function msgbox($pID = "")
{
    $def_msgboxID = "msgbox";
    //get Msgbox value
    $ci = & get_instance();
    $msgbox_val = $ci->utils->get_msgbox_data();

    //ตรวจสอบว่ามีการกำหนด Target ID เหมือนกับ ID Msgbox นี้หรือไม่​?
    if ($msgbox_val["target_id"] == $pID)
    {//if TRUE will display msgbox (กรณีที่ ไม่ได้กำหนดก็ค่าอะไรมาก็จะแสดง)
        $id = ($pID == "" ? $def_msgboxID : $pID);
        echo "<div id='" . $id . "' class='" . $msgbox_val["class"] . "'> " . $msgbox_val["msg"] . " </div>";
    }
    else
    {//if FALSE will display msgbox, But no have CLASS and TEXT
        echo "<div id='" . $pID . "' class=''></div>";
    }
}

function menu_user_active($pMenuID, $con_menu_user_selected)
{
    if (is_array($pMenuID) && in_array($con_menu_user_selected, $pMenuID))
    {//กรณีที่ MenuID เป็น Array
        echo " active";
    }
    elseif ($pMenuID == $con_menu_user_selected)
    {
        echo " active";
    }
}

function date_thai_print($pDate)
{
    $date = strtotime($pDate);
    return date("d/m/", $date) . (date("Y", $date) + 543) . " " . date("H:i", $date);
}

function highligh_debt($pPlan, $pActual)
{
    if ($pActual > $pPlan)
    {
        return "text-danger-bold";
    }
}

function string_null_dash($pData)
{
    if (!empty($pData))
    {
        return $pData;
    }
    else
    {
        return "-";
    }
}

function display_no_record_msg()
{
    return "<div class='text-center'><label class='label label-warning'>" . CON_MSG_NO_RECORD . "</label></div>";
}

/* =========================
 *        Pagination
 * ======================= */

function heading_sortable($pCol_display, $pSortBy)
{
    $ci = & get_instance();
    $sortDi = $def_sortDi = "ASC";
    $icon = "";

    //check have previous query string ?
    if ($_SERVER['QUERY_STRING'] != "")
    {
        $arr_get = $ci->input->get();

        //check sort direction 
        //if current sort is it will invert sort direction
        $cur_sortBy = $cur_sortDi = "";
        if ($ci->input->get("sortBy"))
        {
            $cur_sortBy = $ci->input->get("sortBy");
            unset($arr_get["sortBy"]);
        }

        if ($ci->input->get("sortDi"))
        {
            $cur_sortDi = $ci->input->get("sortDi");
            unset($arr_get["sortDi"]);
        }

        if ($pSortBy == $cur_sortBy)
        {
            //render icon 
            if ($cur_sortDi != "")
            {
                $sortDi = ($cur_sortDi == "ASC" ? "DESC" : "ASC");

                if ($sortDi == "ASC")
                {
                    $icon = "<i class='fa  fa-sort-asc icon-sorting'></i>";
                }
                else
                {
                    $icon = "<i class='fa  fa-sort-desc icon-sorting'></i>";
                }
            }
        }

        //build query 
        $q_sorting = array("sortBy" => $pSortBy, "sortDi" => $sortDi);
        $query_sorting = http_build_query($q_sorting);

        $query_prev = "";
        if (count($arr_get) > 0)
        {
            $query_prev = http_build_query($arr_get);
        }

        //check have really prev query ?
        if ($query_prev !== "")
        {
            $query = $query_prev . "&" . $query_sorting;
        }
        else
        {
            $query = $query_sorting;
        }
    }
    else
    {
        //build query 
        $q_sorting = array("sortBy" => $pSortBy, "sortDi" => $sortDi);
        $query = http_build_query($q_sorting);
    }

    //concat url
    $url = current_url() . "?" . $query;


    return "<a href='{$url}'>{$icon} {$pCol_display} </a>";
}

// -- End Pagination

/**
 * สถานะการรายงานแผน โดยรวม
 * @param type $planID
 * @return type
 */
function get_plan_status($planID)
{
    $ci = & get_instance();
    $ci->load->library("utils_plan");
    $result = $ci->utils_plan->get_status_plan_report($planID);
    $text = $result["text"];
    $text_color = get_status_text($result["status"]);

    return "<span class='" . $text_color . "'>" . $text . "</span>";
}

function get_status_text($statusID)
{
    $ci = & get_instance();
    
    if ($statusID == "0")
    {
        $text_class = "text-default";
    }
    elseif ($statusID == "1")
    {
        $text_class = "text-danger";
    }
    elseif ($statusID == "2")
    {
        $text_class = "text-warning";
    }
    elseif ($statusID == "3")
    {
        $text_class = "text-success";
    }
    elseif ($statusID == "4")
    {
        $text_class = "text-primary";
    }
    elseif ($statusID == "5")
    {
        $text_class = "text-muted";
    }

    return $text_class;
}

function get_status_label($planID)
{
    $ci = & get_instance();
    $ci->load->library("utils_plan");
    $result = $ci->utils_plan->get_status_plan_report($planID);
    $text = $result["text"];
    if ($result["status"] == "0")
    {
        $text_class = "label-brown";
    }
    elseif ($result["status"] == "1")
    {
        $text_class = "label-danger";
    }
    elseif ($result["status"] == "2")
    {
        $text_class = "label-warning";
    }
    elseif ($result["status"] == "3")
    {
        $text_class = "label-success";
    }
    elseif ($result["status"] == "4")
    {
        $text_class = "label-primary";
    }
    elseif ($result["status"] == "5")
    {
        $text_class = "label-default";
    }

    return "<label class='label control-label " . $text_class . "'>" . $text . "</label>";
}
