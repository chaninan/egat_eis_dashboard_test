<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | เข้าสู่ระบบ</title>
        <!-- Tell the browser to be responsive to screen width -->       

        <?php $this->load->view("templates/favicon"); ?>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url("css/bootstrap.min.css"); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("css/font-awesome.min.css"); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url("css/ionicons.min.css"); ?> ">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("css/AdminLTE_theme/AdminLTE.css"); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url("css/AdminLTE_theme/skins/skin-yellow.min.css"); ?>">

        <!-- Custom style -->        
        <link rel="stylesheet" href="<?php echo base_url("css/user.css"); ?> ">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->        

        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url("js/jquery-1.12.3.min.js"); ?>"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url("js/bootstrap.min.js"); ?>"></script>
    </head>
    <body>
        <div class="container">
            <div class="row" id="wrap-logo">
                <div class="col-xs-12">
                    <div class="text-center">  
                        <div class="visible-xs text-center"><img src="<?php echo base_url("images/logo_gsd.png"); ?>" width="80" />  </div>
                        <h1>  
                            <img class="hidden-xs" src="<?php echo base_url("images/logo_gsd.png"); ?>" width="90" />        
                            ระบบ Dashboard ฝ่ายบริการ
                        </h1>
                        <?php if(CON_APP_IS_TEST == TRUE): ?>
                            <label class="label label-danger" id="banner-test">ระบบทดสอบ</label>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div id="bg_admin">
                        <?php echo msgbox(); ?>
                        <form action="<?php echo $this->utils->full_url(); ?>" method="POST">
                            <div class="form-group">
                                <label for="empn"><i class="fa fa-user"></i> รหัสพนักงาน</label>
                                <input type="text" class="form-control" id="empn" name="empn" placeholder="รหัสพนักงาน" value="<?php echo set_value("empn"); ?>">
                            </div>
                            <div class="form-group">
                                <label for="pwd"><i class="fa fa-key"></i> รหัสผ่าน</label>
                                <input type="password" class="form-control" id="pwd" name="pwd" placeholder="รหัสผ่าน" >
                            </div>
                            <div class="text-center">
                                <?php if(isset($backurl)): ?>
                                    <input type="hidden" name="backurl" id="backurl" value="<?php echo $backurl; ?>" />
                                <?php endif; ?>
                                <button type="submit" class="btn btn-success">เข้าสู่ระบบ</button>
                            </div>
                            <div class="remark-login">
                                *ใช้รหัสผ่านเดียวกับ E-mail กฟผ.
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <b>Version</b> <?php echo CON_APP_VERSION; ?>       
                        </div>
                        <strong>พัฒนาโดย : </strong> กลุ่มงานสารสนเทศ ฝ่ายบริการ
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>

<script>
    $(function ()
    {
        $("#empn").focus();
    });
</script>

<style>
    #wrap-logo
    {
        margin-top: 10px;
    }
    h1
    {
        font-family: "Kanit","Helvetica Neue", Helvetica, Arial, sans-serif;
        font-weight: 300;
        color:#fff;
        text-shadow: 1px 2px 3px #949494;
    }
    body
    {
        width :100%;
        min-height: 890px;
        height: 100%;
        z-index: -999;
        background: url('<?php echo base_url("images/bg_user.jpg"); ?>') no-repeat bottom center;
        background-size: cover;    
    }
    #bg_admin
    {
        background-color: rgba(187, 187, 187,0.7);
        border-radius: 10px;
        padding: 15px 20px;
        margin-top: 60px;
    }
    .remark-login
    {
        font-size: 14px;
        color:#A61F4F;
        padding-top: 10px;
    }
    footer
    {
        position: fixed;
        bottom: 0px;
        background-color: rgba(236, 236, 234,0.6);
        width: 100%;
        padding: 10px;

    }
    /* xs */
    @media (max-width: 767px)
    { 
        #wrap-logo h1        
        {
            font-size: 28px;
        }
        #bg_admin
        {
            margin-top: 20px;
        }
        footer
        {
            font-size: 14px;
        }
    }
</style>