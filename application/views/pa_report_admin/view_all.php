<?php // $this->load->view("pa_report_admin/pnl_search") ?>

<div class="padding-top-bottom10 text-right">
    <button class="btn btn-sm btn-success" onclick="show_mdl_add_pa()"><i class="glyphicon glyphicon-plus"></i> เพิ่ม PA ใหม่</button>
</div>
<br/>

<?php if (!empty($dt_pa)) : ?>
    <div class="pull-left">
        รวมทั้งหมด <b>5</b> รายการ
    </div>

    <table class="table table-hover table-bordered table-condensed">
        <thead>
            <tr>
                <th>#</th>
                <th>ความสำคัญ</th>            
                <th>คะแนน</th>
                <th>เอกสารแนบ</th>
                <th>ข้อมูลเมื่อ</th>
                <th>ดำเนินการ</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $last_year = "";
            $last_period = "";

            foreach ($dt_pa as $row) :
                if ($row["year"] != $last_year) :
                    ?>
                    <tr style="background-color: #ededed">
                        <td colspan="8">ประจำปี <?php echo $row["year"]; ?> </td>
                    </tr>   
                <?php endif; ?>

                <?php if ($row["period"] != $last_period) : ?>
                    <tr style="background-color: #F8F8F8">
                        <td colspan="8">งวด <?php echo $row["period_name"]; ?></td>
                    </tr>
                <?php endif; ?>

                <tr>
                    <td></td>
                    <td><?php echo $row["priority_name"] ?></td>            
                    <td class="text-center"><?php echo $row["point"] ?></td>
                    <td class="text-center">
                        <?php if (!empty($row["attach_file"])): ?> 
                            <a href="<?php echo site_url($row["attach_file"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a></td>
                        <?php
                    else:
                        echo "-";
                    endif;
                    ?>
                    <td class="text-center"><?php echo (!empty($row["updateDate"])) ? $this->utils->datetime_to_thai($row["updateDate"]) :  $this->utils->datetime_to_thai($row["createDate"])?></td>
                    <td class="text-center">
                        <button type="button" class="btn btn-xs btn-warning" onclick="show_mdl_edit_pa('<?php echo $this->utils->encID($row["paID"]) ?>')"><i class="fa fa-pencil"></i> แก้ไข</button>
                        <button type="button" class="btn btn-xs btn-danger" onclick="show_mdl_delete_pa('<?php echo $this->utils->encID($row["paID"]) ?>')"><i class="fa fa-trash"></i> ลบ</button>
                    </td>
                </tr>            
                <?php
                $last_year = $row["year"];
                $last_period = $row["period"];
            endforeach;
            ?>        
        </tbody>
    </table>
<?php else : ?>
    <?php echo display_no_record_msg(); ?>
<?php endif; ?>

<div id="div_pa_detail"></div>
<!-- Modal Add -->
<?php $this->load->view("pa_report_admin/mdl_add_pa") ?>
<!-- End Modal Add-->

<script>
    function show_search_body()
    {
        $("#wrap-panel-body").slideToggle("slow", "linear", function () {
            if ($("#link_show_search i").hasClass("fa fa-caret-up"))
            {
                $("#link_show_search").html("(แสดง <i class='fa fa-caret-down'></i>)");

            } else
            {
                $("#link_show_search").html("(ซ่อน <i class='fa fa-caret-up'></i>)");
            }

        });
    }

    function show_mdl_edit_pa(paID) {
        var url = "<?php echo site_url("pa_report_admin/ajax_get_detail_edit_pa") ?>";
        $.post(url, {paID: paID}, function (data) {
            $("#div_pa_detail").empty().html(data);
            $("#mdl_edit_pa").modal('show');
        }, "html");
    }
    
    function show_mdl_delete_pa(paID)
    {
        bootbox.dialog({
            title: "ยืนยันต้องการลบข้อมูล <i class='fa fa-question-circle'></i>",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i>  คุณต้องการลบรายการ PA นี้ใช่หรือไม่</div>",
            buttons: {
                confirm: {
                    label: "ลบข้อมูล",
                    className: 'btn-danger glyphicon glyphicon-trash',
                    callback: function () {
                        var url = "<?php echo site_url("pa_report_admin/ajax_delete_pa") ?>";
                        $.post(url, {paID: paID}, function (data) {
                            if (data.status === "0") {
                                show_msgbox(data.msg, data.status);
                            } else {
                                window.location = data.next_url;
                            }
                        }, "json");
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-default '
                }
            }
        });
    }

</script>
