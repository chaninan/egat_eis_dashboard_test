<div class="modal fade" id="mdl_edit_pa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูล PA</h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">
                    <form id="frm_edit_pa" class="form-horizontal">
                        <?php msgbox("mdl_msgbox") ?>
                        <input type="hidden" id="paID" name="paID" value="<?php echo $this->utils->encID(set_value("paID")) ?>">
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-4 control-label">ประจำปี</label>
                            <label class="col-sm-8 control-label-normal-value"><?php echo $dr_pa["year"]; ?></label>
                        </div>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-4 control-label">ประจำงวด</label>
                            <label class="col-sm-8 control-label-normal-value"><?php echo $dr_pa["period_name"]; ?></label>
                        </div>                           
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-4 control-label">ความสำคัญ</label>
                            <label class="col-sm-8 control-label-normal-value"><?php echo $dr_pa["priority_name"]; ?></label>
                        </div>    
                        <div class="form-group form-group-little-padding">
                        </div>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-4 control-label">คะแนน PA <span class="text-danger fs18">*</span></label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="point_pa" name="point_pa" value="<?php echo set_value("point_pa") ?>"/> 
                            </div>                             
                        </div> 
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-4 control-label">เอกสารแนบ</label>
                            <?php if (!empty($dr_pa["attach_file"])): ?> 
                                <label id="cur_attach" class="col-sm-6 control-label-normal-value" style="display:block">
                                    <a href="<?php echo site_url($dr_pa["attach_file"]); ?>" target="_blank"><span class="fa fa-file-excel-o"></span> เอกสาร </a>
                                    <a href="#" class="text-danger" onclick="delete_attach()">| <i class="fa fa-remove"></i> ลบ</a>
                                </label>
                            <?php endif; ?>

                            <div id="add_attach" class="col-sm-6" style="<?php echo empty($dr_pa["attach_file"]) ? "display:block" : "display:none" ?>">
                                <input type="file" name="attach_file"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .xls และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-primary" onclick="save_edit_pa()">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $("#point_pa").only_numeric("1");
        $("#point_pa").decimal_pattern(4); // ทศนิยม 4 ตำแหน่ง
    })

    function save_edit_pa() {
        var url = "<?php echo site_url("pa_report_admin/ajax_edit_pa") ?>";
        var data = new FormData($("#frm_edit_pa")[0]);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            async: true,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data)
            {
                if (data.status === "0")
                {
                    show_msgbox(data.msg, data.status, "mdl_msgbox");
                    gotop_modal();
                } else
                {
                    window.location = data.next_url;
                }
            }
        });
    }

    function delete_attach()
    {
        bootbox.dialog({
            title: "ยืนยันต้องการลบข้อมูล",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i>  คุณต้องการลบไฟล์แนบนี้ใช่หรือไม่</div>",
            buttons: {
                confirm: {
                    label: "ลบข้อมูล",
                    className: 'btn-danger glyphicon glyphicon-trash',
                    callback: function () {
                        $("#add_attach").show();
                        $("#cur_attach").hide();
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-default '
                }
            }
        });
    }

</script>
