<div id="panel_search" class="">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-search"></i> ค้นหา <a href="#" onclick="show_search_body()" class="fs12" id="link_show_search">(ซ่อน <i class="fa fa-caret-up"></i>)</a></h3>
        </div>
        <div id="wrap-panel-body" class="panel-body" style="display: block">
            <form class="form-horizontal" id="">
                <div class="form-group form-group-little-padding">
                    <label class="col-sm-offset-3 col-sm-2 control-label">ประจำปี</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="" name="" onchange="">
                            <option value="">2560</option>
                            <option value="">2559</option>
                            <option value="">2558</option>
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-little-padding">
                    <label class="col-sm-offset-3 col-sm-2 control-label">ประจำงวด</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="" name="" onchange="">
                            <option value="">6 เดือนแรก</option>
                            <option value="">สิ้นปี</option>                                    
                        </select>
                    </div>
                </div>
                <div class="form-group form-group-little-padding">
                    <label class="col-sm-offset-3 col-sm-2 control-label">ความสำคัญ</label>
                    <div class="col-sm-3">
                        <label class="radio-inline"><input type="radio" name="pa" value="2">PA ฝ่าย</label>
                    </div>
                    <div class="col-sm-offset-5 col-sm-6">
                        <label class="radio-inline"><input type="radio" name="pa" value="7">PA กบท-ห.</label>
                        <label class="radio-inline"><input type="radio" name="pa" value="8">PA กพน-ห.</label>
                        <label class="radio-inline"><input type="radio" name="pa" value="9">PA กอค-ห.</label>
                        <label class="radio-inline"><input type="radio" name="pa" value="10">PA กยธ-ห.</label>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-info" onclick=""><i class="fa fa-search"></i> ค้นหา</button>
                    <a href="javascript:void(0);" class="fs14 text-muted" onclick="clear_form();"><span class="glyphicon glyphicon-repeat"></span> ล้างค่า </a>
                </div>
            </form>
        </div> 
    </div>
</div>