<div class="modal fade" id="mdl_add_pa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูล PA</h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">
                    <form id="frm_add_pa" class="form-horizontal">
                        <?php msgbox("mdl_msgbox") ?>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ประจำปี</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="year" name="year">
                                    <?php $cur_year = $this->utils->year_buddha_convert(date("Y")); ?>
                                    <?php
                                    for ($i = $cur_year - 1; $i < $cur_year + 5; $i++) :
                                        ?>                                        
                                        <option value="<?php echo $i ?>" <?php echo set_select("year", $i, ($i == $cur_year)) ?>><?php echo $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>                            
                        </div>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ประจำงวด</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="period" name="period">
                                    <option value="F">6 เดือนแรก</option>
                                    <option value="L">สิ้นปี</option>                                    
                                </select>
                            </div>                            
                        </div>                           
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ความสำคัญ <span class="text-danger fs18">*</span></label>
                            <div class="col-sm-9">
                                <label class="radio-inline"><input type="radio" name="priority" value="7">PA ฝ่ายบริการ</label>
                            </div>
                            <div class="col-sm-9">
                                <label class="radio-inline"><input type="radio" name="priority" value="8">PA กบท.</label>
                                <label class="radio-inline"><input type="radio" name="priority" value="9">PA กพน.</label>
                                <label class="radio-inline"><input type="radio" name="priority" value="10">PA กอค.</label>
                                <label class="radio-inline"><input type="radio" name="priority" value="11">PA กยธ.</label>
                            </div>
                        </div>    
                        <div class="form-group form-group-little-padding">
                        </div>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">คะแนน PA <span class="text-danger fs18">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="point_pa" name="point_pa"/>                                
                            </div>                             
                        </div> 
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">เอกสารแนบ</label>
                            <div class="col-sm-6">
                                <input type="file" id="attach_file" name="attach_file"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .xls และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>                            
                        </div>                   
                    </form>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-primary" onclick="save_add_pa()">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>

<script>

    $(function () {
        $("#point_pa").only_numeric("1");
        $("#point_pa").decimal_pattern(4); // ทศนิยม 4 ตำแหน่ง
    })

    function show_mdl_add_pa()
    {
        reset_form();
        clear_msgbox("#mdl_msgbox");
        $("#mdl_add_pa").modal("show");
    }

    function save_add_pa() {
        var url = "<?php echo site_url("pa_report_admin/ajax_add_pa") ?>";
        var data = new FormData($("#frm_add_pa")[0]);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            async: true,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data)
            {
                if (data.status === "0")
                {
                    show_msgbox(data.msg, data.status, "mdl_msgbox");
                    gotop_modal();
                } else
                {
                    window.location = data.next_url;
                }
            }
        });
    }

    function reset_form()
    {
        var mdl = $("#frm_add_pa");
        var cur_year = "<?php echo $this->utils->year_buddha_convert(date("Y")); ?>";
        mdl.find("#year").val(cur_year);
        mdl.find("#period").val("F");
        mdl.find("input:radio").prop("checked", false);
        mdl.find("#point_pa").val("");
        mdl.find("input[type=file]").val("");
        $("#point_pa").css("background-color", "");
        $("#point_pa").css("border-color", "");
    }

</script>
