<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
        <div id="bg_admin">
            <?php echo msgbox(); ?>
            <form action="<?php echo $this->utils->full_url(); ?>" method="POST">
                <div class="form-group">
                    <label for="empn"><i class="fa fa-user"></i> รหัสพนักงาน</label>
                    <input type="text" class="form-control" id="empn" name="empn" placeholder="รหัสพนักงาน" value="<?php echo set_value("empn"); ?>">
                </div>
                <div class="form-group">
                    <label for="pwd"><i class="fa fa-key"></i> รหัสผ่าน</label>
                    <input type="password" class="form-control" id="pwd" name="pwd" placeholder="รหัสผ่าน" >
                </div>
                <div class="text-center">
                    <?php if(isset($backurl)): ?>
                        <input type="hidden" name="backurl" id="backurl" value="<?php echo $backurl; ?>" />
                    <?php endif; ?>
                    <button type="submit" class="btn btn-success">เข้าสู่ระบบ</button>
                </div>
                <div class="remark-login">
                    *ใช้รหัสผ่านเดียวกับ E-mail กฟผ.
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $(function ()
    {
        $("#empn").focus();
    });
</script>

<style>
    body
    {
        width :100%;
        min-height: 890px;
        height: 100%;
        z-index: -999;
        background: url('<?php echo base_url("images/bg_admin.jpg"); ?>') no-repeat bottom center;
        background-size: cover;    
    }
    #bg_admin
    {
        background-color: rgba(187, 187, 187,0.6);
        border-radius: 10px;
        padding: 15px 20px;
    }
    .main-footer
    {
        position: fixed;
        bottom: 0px;
        width: 100%;
    }
    .remark-login
    {
        font-size: 14px;
        color:#A61F4F;
        padding-top: 10px;
    }
</style>