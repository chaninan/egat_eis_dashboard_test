<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">ประวัติรายการงบลงทุน : <?php echo $dr_budget["item_name"]; ?></h4>
    <div class="budget-desc">
        <div><b>เบิกจ่ายไตรมาส : </b> <?php echo $dr_budget["trimester_name"] ?> <b>วงเงินขอตั้ง : </b> <?php echo number_format($dr_budget["plan"],2); ?> บาท</div>        
    </div>    
</div>
<div class="modal-body">
    <?php if(!empty($dt_log)): ?>
        <h4 class="text-info">ประวัติการทำรายการ</h4>
        <table class="table table-bordered table-hover table-responsive table-striped">
            <thead>
                <tr>
                    <th>ลำดับ</th>
                    <th>ความก้าวหน้าของแผน</th>
                    <th>สถานะรายการ </th>
                    <th>หมายเหตุ </th>
                    <th>เมื่อวันที่</th>
                    <th>โดย</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                foreach($dt_log as $row):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $cnt; ?></td>
                        <td class="text-center"><?php echo $row["latest_work_status_name"]; ?></td>
                        <td class="text-center"><?php echo $row["latest_status_name"]; ?></td>
                        <td><?php echo $row["remark"]; ?></td>
                        <td class="text-center"><?php echo date_thai_print($row["createDate"]); ?></td>
                        <td><?php echo $row["create_name"]; ?></td>
                    </tr>
                    <?php
                    $cnt++;
                endforeach;
                ?>

            </tbody>
        </table>

<?php else: ?>
        <div class="text-center">
            <label class="label label-warning"><?php echo CON_MSG_NO_RECORD ?></label>
        </div>
<?php endif; ?>
</div>