<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">ปรับปรุงความก้าวหน้าของแผน : <?php echo $dr_budget["item_name"]; ?></h4>
    <div class="budget-desc">
        <div><b>เบิกจ่ายไตรมาส : </b> <?php echo $dr_budget["trimester_name"] ?> <b>วงเงินขอตั้ง : </b> <?php echo number_format($dr_budget["plan"], 2); ?> บาท</div>        
    </div>   
</div>
<div class="modal-body">
    <?php echo msgbox("mdl_biu_msgbox"); ?> 
    <form id="mdl_budget_invest_update">
        <div class="form-group">
            <label for="latest_work_statusID">ความก้าวหน้าของแผน <span class="text-danger">*</span> </label>
            <select class="form-control" id="latest_work_statusID" name="latest_work_statusID" >
                <option value="">กรุณาเลือก</option>
                <?php foreach($dt_work_status as $work_item): ?>
                    <option value="<?php echo $work_item["configID"] ?>"><?php echo $work_item["config_display"] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="latest_statusID">สถานะรายการ <span class="text-danger">*</span></label>            
            <select class="form-control" id="latest_statusID" name="latest_statusID" onchange="mdl_biu_onchange_latest_status(this);">
                <option value="">กรุณาเลือก</option>
                <?php foreach($dt_invest_status as $status_item): ?>
                    <option value="<?php echo $status_item["configID"] ?>"><?php echo $status_item["config_display"] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group hide" id="row_actual">
            <label for="dd">เบิกจ่ายจริง <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="actual" name="actual" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true" />
        </div>
        <div class="form-group">
            <label for="latest_statusID">หมายเหตุ</label>            
            <textarea id="remark" name="remark" class="form-control" rows="5"></textarea>
        </div>
        <div class="text-center">
            <input type="hidden" id="budget_investmentID" name="budget_investmentID" value="<?php echo $dr_budget["budget_investmentID"]; ?>"/>
            <input type="hidden" id="current_url" name="current_url" value="<?php echo $current_url; ?>"/>
            <button type="button" class="btn btn-success" onclick="mdl_biu_do_insert();"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก </button>
        </div>
    </form>
</div>

<script>
    $(function ()
    {
        $("#mdl_budget_invest_update #actual").inputmask();
    });

    function mdl_biu_onchange_latest_status(pObj)
    {
        if ($(pObj).val() == "<?php echo $show_actual_investment_statusID; ?>")
        {
            $("#row_actual").removeClass("hide");
            $("#actual").focus();
        } else
        {
            $("#actual").val("");
            $("#row_actual").addClass("hide");
        }
    }

    function mdl_biu_do_insert()
    {
        var url = "<?php echo site_url("budget_investment_admin/ajax_do_update_status"); ?>";

        $.post(url,
                $("#mdl_budget_invest_update").serialize(),
                function (data)
                {
                    $("#mdl_biu_msgbox").removeClass().html("");

                    if (data.status == "1")
                    {
                        window.location = data.next_url;
                    } else
                    {//fail
                        $("#mdl_biu_msgbox").addClass("alert alert-danger").html(data.msg);
                    }

                }, "json");
    }
</script>
