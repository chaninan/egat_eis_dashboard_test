<div class="row">
    <div class="col-xs-12">        
        <?php msgbox(); ?>
    </div>
    <div class="col-xs-12">
        <form class="form-horizontal" method="GET" action="<?php echo current_url(); ?>">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="unitID">หน่วยงานผู้รับผิดชอบ</label>
                <div class="col-sm-3">
                    <select class="form-control" id="search_unitID" name="search_unitID" >
                        <option value="">กรุณาเลือก</option>
                        <?php foreach($dt_unit as $unit): ?>
                            <option value="<?php echo $unit["unitID"]; ?>" <?php echo set_select("search_unitID", $unit["unitID"]); ?> ><?php echo $unit["unit_name"]; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <label class="col-sm-2 control-label" for="year">ปีงบประมาณ</label>
                <div class="col-sm-3">
                    <select class="form-control" id="search_year" name="search_year" >
                        <option value="">กรุณาเลือก</option>
                        <?php foreach($dt_year as $year): ?>
                            <option value="<?php echo $year; ?>" <?php echo set_select("search_year", $year); ?> ><?php echo $this->utils->year_buddha_convert($year); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>            
            <div class="text-center">
                <button type="submit" class="btn btn-primary">แสดง</button>
            </div>
        </form>
    </div>
</div>

<hr/>

<?php if(isset($dt_budget)): ?>
    <div class="row">
        <div class="col-xs-12">
            <h4>รายการงบลงทุน หน่วยงาน : <?php echo $table_title; ?></h4>
            <div class="padding-top-bottom10 text-right">
                <a class="btn btn-info" href="<?php echo site_url("budget_investment_admin/view_all?search_year={$selected_year}"); ?>"> <i class="glyphicon glyphicon-chevron-left"></i> กลับ</a>
                <button class="btn btn-success" onclick="show_mdl_create_operate_investment()"><i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล</button>
            </div>
            <?php if(!empty($dt_budget)): ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อรายการ</th>
                                <th>เบิกจ่ายไตรมาส</th>
                                <th>สถานะรายการ</th>
                                <th>ความก้าวหน้าของแผน</th>
                                <th>หมายเหตุ</th>
                                <th>วงเงินขอตั้ง</th>
                                <th>เบิกจ่ายจริง</th>
                                <th>ปรับปรุงข้อมูล</th>
                                <th>แก้ไข</th>
                                <th>ประวัติ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cnt = 1;
                            foreach($dt_budget as $item):
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $cnt; ?></td>
                                    <td><?php echo $item["item_name"]; ?></td>
                                    <td class="text-center"><?php echo $item["trimester_name"]; ?></td>
                                    <td class="text-center"><?php echo $item["latest_status_name"]; ?></td>
                                    <td class="text-center"><?php echo $item["latest_progress_name"]; ?></td>
                                    <td><?php echo string_null_dash($item["latest_remark"]); ?></td>
                                    <td class="text-right"><?php echo number_format($item["plan"], 2); ?></td>
                                    <td class="text-right">
                                        <span class="<?php echo highligh_debt($item["plan"], $item["actual"]); ?>">
                                            <?php echo (!empty($item["actual"]) ? number_format($item["actual"], 2) : "-"); ?>
                                        </span>
                                    </td>
                                    <td class="text-center"><button class="btn btn-xs btn-info" onclick="show_mdl_update_operate_investment(<?php echo $item["budget_investmentID"]; ?>);"><i class="glyphicon glyphicon-refresh"></i> ปรับปรุง</button></td>
                                    <td class="text-center"><button class="btn btn-xs btn-warning" onclick="show_mdl_edit_operate_investment(<?php echo $item["budget_investmentID"]; ?>)"><i class="glyphicon glyphicon-pencil"></i> แก้ไข</button></td>
                                    <td class="text-center"><button class="btn btn-xs btn-primary" onclick="show_mdl_show_log_operate_investment(<?php echo $item["budget_investmentID"]; ?>)"><i class="glyphicon glyphicon-info-sign"></i> ประวัติ</button></td>
                                </tr>
                                <?php
                                $cnt++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php else : ?>
                <?php echo display_no_record_msg(); ?>
            <?php endif; ?>
        </div>
    </div>

    <!--Modal create--> 
    <div class="modal fade" tabindex="-1" role="dialog" id="mdl_create_budget_investment">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">สร้างรายการงบลงทุน</h4>
                </div>
                <div class="modal-body">
                    <?php msgbox("mdl_cbi_msgbox"); ?>
                    <form id="mdl_cbi_frm_create">
                        <div class="form-group">
                            <label for="item_name">ชื่อรายการ <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="item_name" name="item_name" >
                        </div>
                        <div class="form-group">
                            <label for="trimester">เบิกจ่ายไตรมาส <span class="text-danger">*</span></label>
                            <select class="form-control" id="trimester" name="trimester" >
                                <option value="">กรุณาเลือก</option>
                                <?php foreach($dt_trimester as $trimester): ?>
                                    <option value="<?php echo $trimester["configID"] ?>"><?php echo $trimester["config_display"] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="latest_statusID">สถานะรายการ <span class="text-danger">*</span></label>
                            <select class="form-control" id="latest_statusID" name="latest_statusID" >
                                <option value="">กรุณาเลือก</option>
                                <?php foreach($dt_invest_status as $status): ?>
                                    <option value="<?php echo $status["configID"] ?>"><?php echo $status["config_display"] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="plan">วงเงินขอตั้ง <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" id="plan" name="plan" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true" />
                        </div>
                        <input type="hidden" name="unitID" value="<?php echo $selected_unitID; ?>" />
                        <input type="hidden" name="year" value="<?php echo $selected_year; ?>" />
                        <input type="hidden" name="next_url" value="<?php echo $this->utils->full_url(); ?>"/>
                        <div class="text-center">
                            <button type="button" class="btn btn-success" onclick="do_create();">บันทึก</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--End Modal create--> 

    <div class="modal fade" tabindex="-1" role="dialog" id="mdl_budget_invest_template">
        <div class="modal-dialog" role="document" id="mdl_config">
            <div class="modal-content" id="mdl_placeholder"></div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php endif; //end check selected unit ? ?>

<?php $this->load->view("templates/script_inputmask"); ?>

<script>
    $(function ()
    {
        $("#plan").inputmask();
    });
    function show_mdl_create_operate_investment()
    {
        $("#mdl_create_budget_investment").modal("show");
    }

    function do_create()
    {
        var url = "<?php echo site_url("budget_investment_admin/ajax_create"); ?>";

        $("#mdl_cbi_msgbox").removeClass();

        $.post(url,
                $("#mdl_cbi_frm_create").serialize(),
                function (data)
                {
                    if (data.status == "1")
                    {
                        //$("#mdl_cbi_msgbox").addClass("alert alert-success").html(data.msg);
                        window.location = data.next_url;
                    } else
                    {
                        $("#mdl_cbi_msgbox").addClass("alert alert-danger").html(data.msg);
                    }
                }, "json");
    }

    function show_mdl_update_operate_investment(pBudgetID)
    {
        var current_url = "<?php echo $this->utils->full_url(); ?>";
        var url = "<?php echo site_url("budget_investment_admin/ajax_show_update_status"); ?>";
        $.post(url,
                {"budget_investmentID": pBudgetID, "current_url": current_url},
                function (data)
                {
                    if (data.status == "1")
                    {
                        $("#mdl_budget_invest_template #mdl_placeholder").html(data.msg);
                        show_modal_normal();
                        $("#mdl_budget_invest_template").modal("show");
                    }
                }, "json");
    }

    function show_mdl_edit_operate_investment(pBudgetID)
    {
        var current_url = "<?php echo $this->utils->full_url(); ?>";
        var url = "<?php echo site_url("budget_investment_admin/ajax_show_edit"); ?>";
        $.post(url,
                {"budget_investmentID": pBudgetID, "current_url": current_url},
                function (data)
                {
                    if (data.status == "1")
                    {
                        $("#mdl_budget_invest_template #mdl_placeholder").html(data.msg);
                        show_modal_normal();
                        $("#mdl_budget_invest_template").modal("show");
                    }
                }, "json");
    }

    function show_mdl_show_log_operate_investment(pBudgetID)
    {
        var current_url = "<?php echo $this->utils->full_url(); ?>";
        var url = "<?php echo site_url("budget_investment_admin/ajax_show_log"); ?>";
        $.post(url,
                {"budget_investmentID": pBudgetID, "current_url": current_url},
                function (data)
                {
                    if (data.status == "1")
                    {
                        $("#mdl_budget_invest_template #mdl_placeholder").html(data.msg);
                        show_modal_big();
                        $("#mdl_budget_invest_template").modal("show");
                    }
                }, "json");
    }

    function show_modal_big()
    {
        $("#mdl_budget_invest_template #mdl_config").addClass("modal-lg");
    }

    function show_modal_normal()
    {
        $("#mdl_budget_invest_template #mdl_config").removeClass("modal-lg");
    }

</script>