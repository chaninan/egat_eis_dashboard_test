<div class="row">
    <div class="col-xs-12">        
        <?php msgbox(); ?>
    </div>
    <div class="col-xs-12">
        <form class="form-horizontal" method="GET" action="<?php echo current_url(); ?>">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="unitID">หน่วยงานผู้รับผิดชอบ</label>
                <div class="col-sm-3">
                    <select class="form-control" id="search_unitID" name="search_unitID" >
                        <option value="">ทั้งหมด</option>
                        <?php foreach($dt_unit as $unit): ?>
                            <option value="<?php echo $unit["unitID"]; ?>" <?php echo set_select("search_unitID", $unit["unitID"]); ?> ><?php echo $unit["unit_name"]; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <label class="col-sm-2 control-label" for="year">ปีงบประมาณ</label>
                <div class="col-sm-3">
                    <select class="form-control" id="search_year" name="search_year" >
                        <?php foreach($dt_year as $year): ?>
                            <option value="<?php echo $year; ?>" <?php echo set_select("search_year", $year); ?> ><?php echo $this->utils->year_buddha_convert($year); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>            
            <div class="text-center">
                <button type="submit" class="btn btn-primary">แสดง</button>
            </div>
        </form>
    </div>
</div>

<?php if(isset($dt_budget_unit) && !empty($dt_budget_unit)): ?>
    <?php foreach($dt_budget_unit as $budget_unit): ?>
        <?php $unit = $budget_unit["dr_unit"]; ?>
        <div class="row">
            <div class="col-xs-12">
                <h4 class="title-budget-investment">
                    รายการงบลงทุน หน่วยงาน : <?php echo $unit["unit_name"]; ?> 
                    <a class="btn btn-xs btn-success pull-right link-edit" href="<?php echo site_url("budget_investment_admin/create?search_unitID=" . $unit["unitID"] . "&search_year=" . $search_year); ?>" ><i class="glyphicon glyphicon-pencil"></i> เพิ่ม/แก้ไข รายการ</a>
                </h4>
                <?php if(!empty($budget_unit["dt_budget"])): ?>
                    <?php $dt_budget = $budget_unit["dt_budget"]; ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-responsive table-striped">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>ชื่อรายการ</th>
                                    <th>เบิกจ่ายไตรมาส</th>
                                    <th>สถานะรายการ</th>
                                    <th>ความก้าวหน้าของแผน</th>
                                    <th>หมายเหตุ</th>
                                    <th>วงเงินขอตั้ง</th>
                                    <th>เบิกจ่ายจริง</th>
                                    <th>ปรับปรุงข้อมูล</th>
                                    <th>ประวัติ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cnt = 1;
                                foreach($dt_budget as $item):
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $cnt; ?></td>
                                        <td><?php echo $item["item_name"]; ?></td>
                                        <td class="text-center"><?php echo $item["trimester_name"]; ?></td>
                                        <td class="text-center"><?php echo $item["latest_status_name"]; ?></td>
                                        <td class="text-center"><?php echo $item["latest_progress_name"]; ?></td>
                                        <td><?php echo string_null_dash($item["latest_remark"]); ?></td>
                                        <td class="text-right"><?php echo number_format($item["plan"], 2); ?></td>
                                        <td class="text-right">
                                            <span class="<?php echo highligh_debt($item["plan"], $item["actual"]); ?>">
                                                <?php echo (!empty($item["actual"]) ? number_format($item["actual"], 2) : "-"); ?>
                                            </span>
                                        </td>
                                        <td class="text-center"><button class="btn btn-xs btn-info" onclick="show_mdl_update_operate_investment(<?php echo $item["budget_investmentID"]; ?>);"><i class="glyphicon glyphicon-refresh"></i> ปรับปรุง</button></td>
                                        <td class="text-center"><button class="btn btn-xs btn-primary" onclick="show_mdl_show_log_operate_investment(<?php echo $item["budget_investmentID"]; ?>)"><i class="glyphicon glyphicon-info-sign"></i> ประวัติ</button></td>
                                    </tr>
                                    <?php
                                    $cnt++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php else : ?>
                    <?php echo display_no_record_msg(); ?>
                <?php endif; ?>
            </div>
        </div>
        <hr/>
    <?php endforeach; ?>

    <div class="modal fade" tabindex="-1" role="dialog" id="mdl_budget_invest_template">
        <div class="modal-dialog" role="document" id="mdl_config">
            <div class="modal-content" id="mdl_placeholder"></div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php endif; ?>

<?php $this->load->view("templates/script_inputmask"); ?>

<script>
    $(function ()
    {
        $("#plan").inputmask();
    });

    function show_mdl_update_operate_investment(pBudgetID)
    {
        var current_url = "<?php echo $this->utils->full_url(); ?>";
        var url = "<?php echo site_url("budget_investment_admin/ajax_show_update_status"); ?>";
        $.post(url,
                {"budget_investmentID": pBudgetID, "current_url": current_url},
                function (data)
                {
                    if (data.status == "1")
                    {
                        $("#mdl_budget_invest_template #mdl_placeholder").html(data.msg);
                        show_modal_normal()
                        $("#mdl_budget_invest_template").modal("show");
                    }
                }, "json");
    }

    function show_mdl_show_log_operate_investment(pBudgetID)
    {
        var current_url = "<?php echo $this->utils->full_url(); ?>";
        var url = "<?php echo site_url("budget_investment_admin/ajax_show_log"); ?>";
        $.post(url,
                {"budget_investmentID": pBudgetID, "current_url": current_url},
                function (data)
                {
                    if (data.status == "1")
                    {
                        $("#mdl_budget_invest_template #mdl_placeholder").html(data.msg);
                        show_modal_big();
                        $("#mdl_budget_invest_template").modal("show");
                    }
                }, "json");
    }

    function show_modal_big()
    {
        $("#mdl_budget_invest_template #mdl_config").addClass("modal-lg");
    }

    function show_modal_normal()
    {
        $("#mdl_budget_invest_template #mdl_config").removeClass("modal-lg");
    }

</script>