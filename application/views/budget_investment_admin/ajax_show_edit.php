<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">แก้ไขรายการงบลงทุน : <?php echo $dr_budget["item_name"]; ?></h4>
    <div class="budget-desc">
        <div><b>เบิกจ่ายไตรมาส : </b> <?php echo $dr_budget["trimester_name"] ?> <b>วงเงินขอตั้ง : </b> <?php echo number_format($dr_budget["plan"],2); ?> บาท</div>        
    </div>   
</div>
<div class="modal-body">
    <?php echo msgbox("mdl_bie_msgbox"); ?> 
    <form id="mdl_bie_frm_update">
        <div class="form-group">
            <label for="item_name">ชื่อรายการ <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="item_name" name="item_name" value="<?php echo $dr_budget["item_name"]; ?>" />
        </div>
        <div class="form-group">
            <label for="trimester">เบิกจ่ายไตรมาส <span class="text-danger">*</span></label>
            <select class="form-control" id="trimester" name="trimester" >
                <option value="">กรุณาเลือก</option>
                <?php foreach($dt_trimester as $trimester): ?>
                    <option value="<?php echo $trimester["configID"] ?>" <?php echo ($dr_budget["trimester"] == $trimester["configID"] ? "selected" : "") ?>><?php echo $trimester["config_display"] ?></option>
                <?php endforeach; ?>
            </select>
        </div>        
        <div class="form-group">
            <label for="plan">วงเงินขอตั้ง <span class="text-danger">*</span></label>
            <input type="text" class="form-control" id="plan" name="plan" data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true" value="<?php echo $dr_budget["plan"]; ?>" />
        </div>
        <div class="text-center">
            <input type="hidden" name="budget_investmentID" value="<?php echo $dr_budget["budget_investmentID"]; ?>" />
            <input type="hidden" id="current_url" name="current_url" value="<?php echo $current_url; ?>"/>
            <button type="button" class="btn btn-success" onclick="mdl_bie_do_insert();">บันทึก</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
        </div>
    </form>
</div>
<script>
    $(function ()
    {
        $("#mdl_bie_frm_update #plan").inputmask();
    });

    function mdl_bie_do_insert()
    {
        var url = "<?php echo site_url("budget_investment_admin/ajax_do_edit"); ?>";

        $.post(url,
                $("#mdl_bie_frm_update").serialize(),
                function (data)
                {
                    $("#mdl_bie_msgbox").removeClass().html("");

                    if (data.status == "1")
                    {
                        window.location = data.next_url;
                    } else
                    {//fail
                        $("#mdl_bie_msgbox").addClass("alert alert-danger").html(data.msg);
                    }
                }, "json");
    }
</script>