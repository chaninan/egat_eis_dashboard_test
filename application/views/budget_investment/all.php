<div class="row">
    <div class="col-md-6 col-md-offset-2">     
        <form action="<?php echo current_url(); ?>" method="GET" class="form-horizontal">
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">ประจำปี</label>
                <div class="col-xs-10 col-sm-7 ">
                    <select class="form-control" id="year" name="year" >
                        <option value="">กรุณาเลือก</option>
                        <?php $cur_year = date("Y"); ?>
                        <?php foreach($dt_year as $item): ?>
                            <option value="<?php echo $item; ?>" <?php echo set_select("year", $item, ($item == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-2 col-sm-2 ">
                    <button type="submit" class="btn btn-primary">ตกลง</button>
                </div>
            </div>        
        </form>
    </div>
</div>
<div class="row">
    <?php foreach($dt_all_trimester as $item_tri): ?>
        <div class="col-md-6">
            <!-- Box -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $item_tri["title"]; ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="tilte-chart">จำนวนรายการแยกตามสถานะ</h4>
                            <?php if(empty($item_tri["dt_table_pie"])): ?>
                                <?php echo display_no_record_msg(); ?>
                            <?php else: ?>
                                <canvas id="chart_pie_status_<?php echo $item_tri["trimester"]; ?>"  height="250"></canvas>   
                                <table class="table table-bordered table-hover table-responsive table-striped table-pie-desc">
                                    <thead>
                                        <tr>
                                            <th>สถานะรายการ</th>
                                            <th>จำนวน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $ind_color = 0;
                                        foreach($item_tri["dt_table_pie"] as $tr):
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="chart_color_preview" style="background-color: <?php echo $data_table_status_color[$ind_color]; ?>"></span>
                                                    <?php echo $tr["config_display"]; ?>
                                                </td>
                                                <td class="text-center"><?php echo $tr["cnt_status"]; ?></td>
                                            </tr>
                                            <?php
                                            $ind_color ++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>                                
                            <?php endif; ?>     
                        </div>
                        <div class="col-sm-6">
                            <h4 class="tilte-chart">แสดงการใช้งบประมาณ</h4>
                            <?php if(empty($item_tri["dt_table_pie"])): ?>
                                <?php echo display_no_record_msg(); ?>
                            <?php else: ?>
                                <canvas id="chart_bar_usage_<?php echo $item_tri["trimester"]; ?>" class="chart_bar_usage" height="250"></canvas>   
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <?php if(!empty($item_tri["dt_table_pie"])): ?>
                                <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($item_tri["latest_update"]); ?> </div>                                
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>    
    <?php endforeach; ?>
</div>

<script src="<?php echo base_url("js/budget_investment_chart.js"); ?>"></script>

<script>
    $(function ()
    {
<?php foreach($dt_all_trimester as $item_tri): ?>
    <?php if(!empty($item_tri["dt_table_pie"])): ?>
                //pie 
                pie_chart_status_render("#chart_pie_status_<?php echo $item_tri["trimester"]; ?>", <?php echo $item_tri["json_pie_tri"]; ?>);
                //bar
                bar_chart_usage_render("#chart_bar_usage_<?php echo $item_tri["trimester"]; ?>", <?php echo $item_tri["json_bar_tri"]; ?>);
    <?php endif; ?>
<?php endforeach; ?>
    });
</script>

<style>
    .chart_color_preview
    {
        display: block;
        float: left;
        margin-top: 5px;
        margin-right: 6px;
        width: 10px;
        height: 10px;
    }
    .table-pie-desc
    {
        margin-top: 10px;
        font-size: 0.8em;
    }
    .chart_bar_usage
    {
        margin-top: 15px;
    }
    .tilte-chart
    {
        color: #888;
        font-size: 16px;
        margin-bottom: 18px;
    }
</style>