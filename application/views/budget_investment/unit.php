<div class="row">
    <div class="col-md-6 col-md-offset-2">     
        <form action="<?php echo current_url(); ?>" method="GET" class="form-horizontal">
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">ประจำปี</label>
                <div class="col-xs-10 col-sm-7 ">
                    <select class="form-control" id="year" name="year" >
                        <option value="">กรุณาเลือก</option>
                        <?php $cur_year = date("Y"); ?>
                        <?php foreach($dt_year as $item): ?>
                            <option value="<?php echo $item; ?>" <?php echo set_select("year", $item, ($item == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-2 col-sm-2 ">
                    <button type="submit" class="btn btn-primary">ตกลง</button>
                </div>
            </div>            
        </form>
    </div>
</div>
<div class="row">
    <?php foreach($dt_all_trimester as $item_tri): ?>
        <div class="col-md-6">
            <!-- Box -->
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">ภาพรวมฝ่าย รายการเบิกจ่าย<?php echo $item_tri["trimester_name"]; ?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="tilte-chart">จำนวนรายการแยกตามสถานะ</h4>
                            <?php if(empty($item_tri["dt_table_pie"])): ?>
                                <?php echo display_no_record_msg(); ?>
                            <?php else: ?>
                                <canvas id="chart_pie_status_<?php echo $item_tri["trimester"]; ?>"  height="150"></canvas>   
                                <table class="table table-bordered table-hover table-responsive table-striped table-pie-desc">
                                    <thead>
                                        <tr>
                                            <th>สถานะรายการ</th>
                                            <th>จำนวน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $ind_color = 0;
                                        foreach($item_tri["dt_table_pie"] as $tr):
                                            ?>
                                            <tr>
                                                <td>
                                                    <span class="chart_color_preview" style="background-color: <?php echo $data_table_status_color[$ind_color]; ?>"></span>
                                                    <?php echo $tr["config_display"]; ?>
                                                </td>
                                                <td class="text-center"><?php echo $tr["cnt_status"]; ?></td>
                                            </tr>
                                            <?php
                                            $ind_color ++;
                                        endforeach;
                                        ?>
                                    </tbody>
                                </table>                                
                            <?php endif; ?>     
                        </div>
                        <div class="col-sm-6">
                            <h4 class="tilte-chart">แสดงการใช้งบประมาณ</h4>
                            <?php if(empty($item_tri["dt_table_pie"])): ?>
                                <?php echo display_no_record_msg(); ?>
                            <?php else: ?>
                                <canvas id="chart_bar_usage_<?php echo $item_tri["trimester"]; ?>" class="chart_bar_usage" height="250"></canvas>   
                                <div class="text-center" style="margin-top: 10px;">
                                    <button onclick="goto('#title_detail_trimester');" class="btn btn-sm btn-primary"><i class="fa fa-list-ul"></i> แสดงรายการทั้งหมด</button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <?php if(!empty($item_tri["dt_table_pie"])): ?>
                                <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($item_tri["latest_update"]); ?> </div>                                
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>    
    <?php endforeach; ?>
</div>
<h3 id="title_detail_trimester">รายละเอียดรายการ แยกตามไตรมาส</h3>
<?php foreach($dt_all_trimester as $item_tri): ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><?php echo $item_tri["trimester_name"]; ?></h3>
        </div>
        <div class="box-body">                
            <?php if(!empty($item_tri["dt_table_detail"])): ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-responsive table-striped">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อรายการ</th>
                                <th>เบิกจ่ายไตรมาส</th>
                                <th>สถานะรายการ</th>
                                <th>ความก้าวหน้าของแผน</th>
                                <th>หมายเหตุ</th>
                                <th>วงเงินขอตั้ง</th>
                                <th>เบิกจ่ายจริง</th>
                                <th>ประวัติ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cnt = 1;
                            foreach($item_tri["dt_table_detail"] as $item):
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $cnt; ?></td>
                                    <td><?php echo $item["item_name"]; ?></td>
                                    <td class="text-center"><?php echo $item["trimester_name"]; ?></td>
                                    <td class="text-center"><?php echo $item["latest_status_name"]; ?></td>
                                    <td class="text-center"><?php echo $item["latest_progress_name"]; ?></td>
                                    <td><?php echo string_null_dash($item["latest_remark"]); ?></td>
                                    <td class="text-right"><?php echo number_format($item["plan"], 2); ?></td>
                                    <td class="text-right">
                                        <span class="<?php echo highligh_debt($item["plan"], $item["actual"]); ?>">
                                            <?php echo (!empty($item["actual"]) ? number_format($item["actual"], 2) : "-"); ?>
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-xs btn-primary" onclick="show_mdl_show_log_operate_investment(<?php echo $item["budget_investmentID"]; ?>)">
                                            <i class="glyphicon glyphicon-list"></i> ประวัติ
                                        </button></td>
                                </tr>
                                <?php
                                $cnt++;
                            endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            <?php else: ?>
                <?php echo display_no_record_msg(); ?>
            <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>

<div class="modal fade" tabindex="-1" role="dialog" id="mdl_budget_invest_template">
    <div class="modal-dialog" role="document" id="mdl_config">
        <div class="modal-content" id="mdl_placeholder"></div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="<?php echo base_url("js/budget_investment_chart.js"); ?>"></script>

<script>
$(function ()
{
<?php foreach($dt_all_trimester as $item_tri): ?>
<?php if(!empty($item_tri["dt_table_pie"])): ?>
            //pie 
            pie_chart_status_render("#chart_pie_status_<?php echo $item_tri["trimester"]; ?>", <?php echo $item_tri["json_pie_tri"]; ?>);
            //bar
            bar_chart_usage_render("#chart_bar_usage_<?php echo $item_tri["trimester"]; ?>", <?php echo $item_tri["json_bar_tri"]; ?>);
<?php endif; ?>
<?php endforeach; ?>
});

function show_mdl_show_log_operate_investment(pBudgetID)
{
    var current_url = "<?php echo $this->utils->full_url(); ?>";
    var url = "<?php echo site_url("budget_investment_admin/ajax_show_log"); ?>";
    $.post(url,
            {"budget_investmentID": pBudgetID, "current_url": current_url},
            function (data)
            {
                if (data.status == "1")
                {
                    $("#mdl_budget_invest_template #mdl_placeholder").html(data.msg);
                    show_modal_big();
                    $("#mdl_budget_invest_template").modal("show");
                }
            }, "json");
}

function show_modal_big()
{
    $("#mdl_budget_invest_template #mdl_config").addClass("modal-lg");
}

function goto(pID)
{
    var prev_last = $(pID);
    var location = prev_last.offset();
    var location_top = location.top;
    $("html body").animate({scrollTop: location_top}, "slow");

}
</script>

<style>
    .chart_color_preview
    {
        display: block;
        float: left;
        margin-top: 5px;
        margin-right: 6px;
        width: 10px;
        height: 10px;
    }
    .table-pie-desc
    {
        margin-top: 10px;
        font-size: 0.8em;
    }
    .chart_bar_usage
    {
        margin-top: 15px;
    }
    .tilte-chart
    {
        color: #888;
        font-size: 16px;
        margin-bottom: 18px;
    }
</style>