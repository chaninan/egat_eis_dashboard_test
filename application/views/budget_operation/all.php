<div class="row">
    <div class="col-md-6 col-md-offset-2">     
        <form action="<?php echo current_url(); ?>" method="GET" class="form-horizontal">
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">ประจำปี</label>
                <div class="col-xs-10 col-sm-7 ">
                    <select class="form-control" id="year" name="year" >
                        <option value="">กรุณาเลือก</option>
                        <?php $cur_year = date("Y"); ?>
                        <?php foreach($dt_year as $item): ?>
                            <option value="<?php echo $item; ?>" <?php echo set_select("year", $item, ($item == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-2 col-sm-2 ">
                    <button type="submit" class="btn btn-primary">ตกลง</button>
                </div>
            </div>            
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <!-- Bar CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">ภาพรวมฝ่าย การใช้งบประมาณทำการแยกตามเดือน</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_overall)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>
                        <canvas id="overall_bar_chart"  style="height:250px"></canvas>   
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_overall); ?> </div>
                    <?php endif; ?>                 
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-6">
        <!--Line CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">ภาพรวมฝ่าย การใช้งบประมาณทำการสะสม</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_line_accummulate)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>
                        <canvas id="overall_line_chart" style="height:250px"></canvas>
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_overall); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <!-- Bar CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">ส่วนกลางฝ่ายบริการ</h3>
                <div class="box-tools pull-right">                    
                    <a class="btn btn-box-tool" href="<?php echo site_url("budget_operation/unit/" . CON_UNITID_GSD_CENTER); ?>" ><i class="fa fa-external-link-square"></i> รายละเอียด</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_gsd_center)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?> 
                        <canvas id="gsd_center_bar_chart" style="height:200px"></canvas> 
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_gsd_center); ?> </div>
                    <?php endif; ?>

                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-4">
        <!-- Bar CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">กองบริการทั่วไป</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-box-tool" href="<?php echo site_url("budget_operation/unit/" . CON_UNITID_GENERAL); ?>" ><i class="fa fa-external-link-square"></i> รายละเอียด</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_general)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>       
                        <canvas id="general_bar_chart" style="height:250px"></canvas>   
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_general); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-4">
        <!-- Bar CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">กองบริการยานพาหนะ</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-box-tool" href="<?php echo site_url("budget_operation/unit/" . CON_UNITID_VEHICLE); ?>" ><i class="fa fa-external-link-square"></i> รายละเอียด</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_vehicle)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>           
                        <canvas id="vehicle_bar_chart" style="height:250px"></canvas>    
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_vehicle); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-md-4">
        <!-- Bar CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">กองบำรุงรักษาโยธา</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-box-tool" href="<?php echo site_url("budget_operation/unit/" . CON_UNITID_CIVIL); ?>" ><i class="fa fa-external-link-square"></i> รายละเอียด</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_civil)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>
                        <canvas id="civil_bar_chart" style="height:250px"></canvas>
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_civil); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-4">
        <!-- Bar CHART -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">กองระบบงานอาคาร</h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-box-tool" href="<?php echo site_url("budget_operation/unit/" . CON_UNITID_BUILDING); ?>" ><i class="fa fa-external-link-square"></i> รายละเอียด</a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_building)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>       
                        <canvas id="building_bar_chart" style="height:250px"></canvas>  
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_building); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

<!-- page script -->
<script src="<?php echo base_url("js/budget_operate_chart.js"); ?>"></script>
<script>
    $(function ()
    {
        //Bar chart render
        //ภาพรวมฝ่าย
        var data_overall = <?php echo $data_overall; ?>;
        var bar_chart_overall = bugget_bar_chart_render("#overall_bar_chart", data_overall);
        $("#overall_bar_chart").click(
                function (evt)
                {
                    //get elements from click chart
                    var activePoints = bar_chart_overall.getElementsAtEvent(evt);
                    //dataset of this chart 
                    var dt_chart = data_overall;
                    if (activePoints.length > 0)
                    {
                        //console.log(activePoints);                    
                        //console.log(dt_chart);
                        //ดึงข้อมูลเมื่อกด click ที่ chart
                        console.log("selectd : " +
                                dt_chart.datasets[0].label + " " + dt_chart.datasets[0].data[activePoints[0]._index] + " " +
                                dt_chart.datasets[1].label + " " + dt_chart.datasets[1].data[activePoints[1]._index]);
                    }

                }
        );

        //สก.อบก.
        var data_gsd_center = <?php echo $data_gsd_center; ?>;
        var bar_chart_gsd_center = bugget_bar_chart_render("#gsd_center_bar_chart", data_gsd_center);

        //กบท.
        var data_general = <?php echo $data_general; ?>;
        var bar_chart_general = bugget_bar_chart_render("#general_bar_chart", data_general);

        //กพน.
        var data_vehicle = <?php echo $data_vehicle; ?>;
        var bar_chart_vehicle = bugget_bar_chart_render("#vehicle_bar_chart", data_vehicle);

        //กยธ.
        var data_civil = <?php echo $data_civil; ?>;
        var bar_chart_vehicle = bugget_bar_chart_render("#civil_bar_chart", data_civil);

        //กอค.
        var data_building = <?php echo $data_building; ?>;
        //console.log(data_building);
        var bar_chart_building = bugget_bar_chart_render("#building_bar_chart", data_building);

        //line chart
        var data_overall_line = <?php echo $data_line_accummulate; ?>;
        //console.log(data_overall_line);
        var obudget_line_chart = budget_line_chart("#overall_line_chart", data_overall_line);
    });
</script>