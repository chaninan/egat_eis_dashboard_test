<div class="row">
    <div class="col-md-6 col-md-offset-2">     
        <form action="<?php echo current_url(); ?>" method="GET" class="form-horizontal">
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">ประจำปี</label>
                <div class="col-xs-10 col-sm-7 ">
                    <select class="form-control" id="year" name="year" >
                        <option value="">กรุณาเลือก</option>
                        <?php $cur_year = date("Y"); ?>
                        <?php foreach($dt_year as $item): ?>
                            <option value="<?php echo $item; ?>" <?php echo set_select("year", $item, ($item == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-xs-2 col-sm-2 ">
                    <button type="submit" class="btn btn-primary">ตกลง</button>
                </div>
            </div>            
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <!-- Bar CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">การใช้งบประมาณทำการแยกตามเดือน</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_graph_bar)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>
                        <canvas id="overall_bar_chart" style="height:250px"></canvas> 
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_all); ?> </div>
                    <?php endif; ?>                 
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-md-6">
        <!--Line CHART -->
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">ภาพรวมฝ่าย การใช้งบประมาณทำการสะสม</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="chart">
                    <?php if(empty($data_line_accummulate)): ?>
                        <?php echo display_no_record_msg(); ?>
                    <?php else: ?>
                        <canvas id="overall_line_chart" style="height:250px"></canvas>
                        <div class="information-date">ข้อมูล ณ วันที่ : <?php echo date_thai_print($update_date_all); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

<?php if(!empty($arr_dt_chart_month)): ?>
    <div class="row">
        <div class="col-xs-12">
            <h3>แสดงรายละเอียดแยกตามเดือน</h3>
        </div>
    </div>
    <div class="row">
        <?php foreach($arr_dt_chart_month as $dr_month): ?>
            <div class="col-md-4">
                <!-- Bar CHART -->
                <div class="box box-info">
                    <div class="box-header with-border <?php echo box_header_sign($dr_month["show_sign"]) ?>">
                        <h3 class="box-title">เดือน <?php echo $dr_month["month_name"] ?> 
                            <?php if($dr_month["show_sign"] == "danger"): ?>
                                <i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i>
                            <?php elseif($dr_month["show_sign"] == "success"): ?>
                                <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                            <?php endif; ?>
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="chart">
                            <?php if(empty($data_graph_bar)): ?>
                                <?php echo display_no_record_msg(); ?>
                            <?php else: ?>
                                <canvas id="<?php echo "chart_month_" . $dr_month["month"]; ?>" style="height:80px"></canvas>  
                                <b>เหตุผล :</b>
                                <?php if(!empty($dr_month["dt_remark"])): ?>
                                    <button class="btn btn-primary btn-xs" type="button" onclick="show_mdl_remark_detail('<?php echo $dr_month["budget_operateID"]; ?>')">
                                        <i class="fa fa-list"></i> แสดงเหตุผล (<?php echo $dr_month["count_remark"]; ?>)
                                    </button>                                  
                                <?php else: ?>
                                    <button class="btn btn-default btn-sm" disabled="">
                                        ไม่มี
                                    </button>
                                <?php endif; ?>      
                            <?php endif; ?>                 
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>


<!-- page script -->
<script src="<?php echo base_url("js/budget_operate_chart.js"); ?>"></script>
<script>
$(function ()
{
    var data_overall = <?php echo $data_graph_bar; ?>;
    var bar_chart_overall = bugget_bar_chart_render("#overall_bar_chart", data_overall);

    //line chart
    var data_overall_line = <?php echo $data_line_accummulate; ?>;
    var obudget_line_chart = budget_line_chart("#overall_line_chart", data_overall_line);
        <?php if(!empty($arr_dt_chart_month)): ?>
        <?php foreach($arr_dt_chart_month as $dr_month): ?>
            //loop for plot charts
            var data_months = <?php echo $dr_month["graph_data"]; ?>;
            //console.log(data_month_jan);
            var chart_id = "#chart_month_<?php echo $dr_month["month"]; ?>";
            budget_bar_horizontal_chart(chart_id, data_months);
        <?php endforeach; ?>
        <?php endif; ?>
});
function toggle_remark(pObject)
{
    $(pObject).siblings(".table-remark").toggleClass("hidden");
    if ($(pObject).html() == "แสดงเหตุผล")
    {
        $(pObject).html("ซ่อนเหตุผล");
    } else
    {
        $(pObject).html("แสดงเหตุผล");
    }
}

</script>

<?php
function box_header_sign($pType)
{
    if($pType == "danger")
    {
        return "box-header-danger";
    }
    else if($pType == "success")
    {
        return "box-header-success";
    }
}
?>

<?php $this->load->view("budget_operation/mdl_remark_detail"); ?>