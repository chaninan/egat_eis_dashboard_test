<div id="mdl_remark_detail" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">เหตุผลการใช้งบประมาณทำการ <span id="mdl_remark_detail_title"></span> </h4>
            </div>
            <div class="modal-body">
                <div id="mdl_remark_detail_res_table"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    function show_mdl_remark_detail(pBudget_operationID)
    {
        var url = "<?php echo site_url("budget_operation/ajax_render_remark_table"); ?>";

        $.post(url,
                {"budget_operateID": pBudget_operationID},
                function (data)
                {
                    if (data.status == "1")
                    {
                        $("#mdl_remark_detail_res_table").html(data.data);
                        //budget detail
                        var obudget_detail = jQuery.parseJSON(data.budget_detail);
                        $("#mdl_remark_detail_title").html(obudget_detail.unit_name + " เดือน " + obudget_detail.month_name + " " + obudget_detail.year_thai);
                    }
                }, "json");
        $("#mdl_remark_detail").modal("show");
    }
</script>