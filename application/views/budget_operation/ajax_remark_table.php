<?php if(isset($dt) && !empty($dt)): ?>
    <table class="table table-bordered table-striped table-remark">
        <thead>
            <tr>
                <th>ครั้ง</th>
                <th style="width:60%">เหตุผล</th>
                <th>วันที่</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $cnt = 1;
            foreach($dt as $item):
                ?>                                            
                <tr>
                    <td class="text-center"><?php echo $cnt; ?></td>
                    <td><?php echo $item["remark"]; ?></td>
                    <td>                                                        
                        <small> <?php echo print_remark_date($item); ?></small>
                    </td>
                </tr>
                <?php
                $cnt++;
            endforeach;
            ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="text-center">
        <label class="label label-warning"><?php echo CON_MSG_NO_RECORD; ?></label>
    </div>
<?php endif; ?>
<?php
function print_remark_date($pDT_remark_date)
{
    $txt = "<b>สร้างโดย </b> : " . $pDT_remark_date["create_name"] . " (" . date_thai_print($pDT_remark_date["createDate"]) . ")";
    if(!empty($pDT_remark_date["updateDate"]))
    {
        $txt .="<div>";
        $txt .= "<b>ปรับปรุงโดย</b> : " . $pDT_remark_date["update_name"] . " (" .date_thai_print($pDT_remark_date["updateDate"]) . ")";
        $txt .="</div>";
    }
    return $txt;
}
?>