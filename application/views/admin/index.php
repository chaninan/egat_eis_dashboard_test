<div class="alert alert-success">
    <h3>งบประมาณ</h3>
    <a class="btn btn-default btn-lg" href="<?php echo site_url("budget_operation_admin/view_all"); ?>"> <i class="fa fa-money"></i> งบประมาณทำการ</a>
    <a class="btn btn-default btn-lg" href="<?php echo site_url("budget_investment_admin/view_all"); ?>"><i class="fa fa-bitcoin"></i> งบลงทุน</a>
</div>
<div class="alert alert-success">
    <h3>แผนปฏิบัติการ</h3>
    <a class="btn btn-default btn-lg" href="<?php echo site_url("action_plan_admin/view_all"); ?>"> <i class="fa fa-list-ol"></i> แผนปฏิบัติการ</a>
    <a class="btn btn-default btn-lg" href="<?php echo site_url("action_plan_report_admin/view_all"); ?>"> <i class="fa fa-check-square-o"></i> รายงานแผนปฏิบัติการ</a>
    <a class="btn btn-default btn-lg" href="<?php echo site_url("pa_report_admin/view_all"); ?>"> <i class="fa fa-star-o"></i> อัพเดทข้อมูล PA</a>
</div>
<div class="alert alert-success">
    <h3>การตั้งค่าระบบ</h3>
    <a class="btn btn-default btn-lg" href="<?php echo site_url("sync_data/user_gsd/icon/admin"); ?>"><i class="fa fa-database"></i> <i class="fa fa-user"></i> Update ฐานข้อมูลพนักงาน</a>
</div>
