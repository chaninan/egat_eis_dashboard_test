ทั้งหมด <?php echo $dt_cnt_emp ?> คน
<?php if (!empty($dt_list_emp)) : ?>
    <table id="tbl_list_emp" class="fs14 table table-condensed table-hover table-striped table-bordered table-responsive">
        <div class="col-md-12 text-center"><b><?php echo $title ?></b></div>
        <thead>
            <tr>
                <th class="text-center" style="width:5%">#</th>
                <th class="text-center" style="width:8%">รหัสพนักงาน</th>
                <th class="text-center" style="width:25%">ชื่อ-สกุล</th>
                <th class="text-center" style="width:20%">ตำแหน่ง</th>
                <th class="text-center" style="width:10%">คุณวุฒิ</th>
                <th class="text-center" style="width:10%">สังกัด</th>                
            </tr>
        </thead>
        <tbody>        
            <?php
            $i = 1;
            foreach ($dt_list_emp as $row) :
                ?>
                <tr>
                    <td class="text-center"><?php echo $i ?></td>
                    <td class="text-center"><?php echo $row["empn"] ?></td>
                    <td><?php echo $row["title"] . " " . $row["name"] ?></td>
                    <td><?php echo $row["p_nname"] ?></td>
                    <td><?php echo $row["level_name"] ?></td>
                    <?php if ($row["dept"] == CON_DEPT_GSD): ?>
                        <td class="text-center"><?php echo $row["d_abb"] ?></td>
                    <?php else: ?>
                        <td class="text-center"><?php echo $row["gong"] ?> | <?php echo $row["d_abb"] ?></td>                              
                    <?php endif; ?>                    
                </tr>
                <?php
                $i++;
            endforeach;
            ?>
        </tbody>
    </table>
<?php else : ?>
    <?php echo display_no_record_msg() ?>
<?php endif; ?>

<script>
    $(function () {
        $('#tbl_list_emp').DataTable({
            "pageLength": 25,
            "language": {
                "search": "ค้นหา :",
                "lengthMenu": "แสดง _MENU_ รายการ",
                "info": "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                "zeroRecords": "ไม่พบข้อมูล !!! ",
                "infoEmpty": "แสดง 0 ถึง 0 จาก 0 รายการ",
                "infoFiltered": "(ค้นหาจากทั้งหมด _MAX_ รายการ)",
                "paginate": {
                    "first": "หน้าแรก",
                    "last": "หน้าสุดท้าย",
                    "previous": "ก่อนหน้า",
                    "next": "ถัดไป",
                }
            }

        });
    });

</script>
