<style>
    .btn-custom {
        background-color :rgba(118, 202, 212, 0.4);
        border-color : #fff;
        font-weight: bold;
        height: 30px;        
    }

    .btn-custom:active, .btn-custom.active {
        background-color:rgba(118, 202, 212, 1);
    }

    .btn-custom:hover, .btn-custom.hover {
        background-color:rgba(118, 202, 212, 1);       
    }

    .btn {
        border-radius:0px;
    }
    .no-padding-width{
        padding-left:0px !important; 
        padding-right:0px !important;
        width:0px
    }
    .wrap-scroll-table .table>thead>tr>th,
    .wrap-scroll-table .table>tbody>tr>td {
        /*border-top: 1px #000 solid !important;*/
        /*border: 1px solid #FFF !important;*/
    }

    .wrap-scroll-table .table>tbody>tr:hover{
        background-color: #FBF9A2 !important;
    }

    .wrap-scroll-table tbody tr:nth-child(odd) {
        /*background-color: #E8EFF7 !important;*/
        /*background-color: rgba(118, 202, 212, 0.1);*/
    }
    .wrap-scroll-table tbody tr:nth-child(even) {
        /*background-color: #F7FBFF !important;*/
    }
    .wrap-scroll-table thead tr{
        background-color:rgba(118, 202, 212, 0.6);
        /*background-color: #bddaef !important;*/
    }   
    .unit {
        font-weight: bold;
    }
</style>
<div class="row">
    <div class="col-md-offset-3 col-md-6">     
        <form class="form-horizontal" method="POST">
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">ปี</label>
                <?php $cur_year = date("Y"); ?>
                <div class="col-xs-10 col-sm-3">
                    <select class="form-control" id="start_year" name="start_year" onchange="change_valid_end_year(this)">                        
                        <?php foreach ($dt_year as $item): ?>
                            <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("start_year", $this->utils->year_buddha_convert($item), ($item == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>                           
                </div>
                <label class="col-xs-12 col-sm-1 control-label no-padding-width">-</label>                
                <div class="col-xs-10 col-sm-3">
                    <select class="form-control" id="end_year" name="end_year" >                        
                        <?php foreach ($dt_year as $item): ?>
                            <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("end_year", $this->utils->year_buddha_convert($item), ($item == $cur_year + 3)); ?> style="<?php echo ($item <= $cur_year) ? "display:none" : "display:block" ?>"><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">หน่วยงาน</label>
                <div class="col-xs-10 col-sm-6">
                    <select class="form-control" id="unit" name="unit" onchange="onchange_unit()">
                        <option value="">ทั้งหมด</option>
                        <?php
                        $cnt = 0;
                        foreach ($dt_gsd_unit as $item):
                            if ($cnt !== 0) :
                                $selected = "";
                                if ((isset($ddl_unit) && $ddl_unit == $item["unit_sub_dept"]))
                                {
                                    if ((isset($ddl_is_unit) && $ddl_is_unit == $item["is_unit"]))
                                    {
                                        $selected = "selected";
                                    }                                   
                                }                               
                                ?>
                                <option <?php echo ($item["is_unit"] == "1") ? "class='unit'" : "" ?> value="<?php echo $item["unit_sub_dept"] ?>" <?php echo $selected ?>><?php echo $item["unit_name"] . " (" . $item["unit_abb"] . ")"; ?></option>
                                <?php
                            endif;
                            $cnt++;
                            ?>
                        <?php endforeach; ?>
                    </select>
                    <input type="hidden" name="is_unit" id="is_unit"/>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">คุณวุฒิ</label>
                <div class="col-xs-10 col-sm-6">
                    <select class="form-control" id="vut" name="vut">
                        <option value="">ทั้งหมด</option>
                        <?php foreach ($dt_vut as $item): ?>
                            <option value="<?php echo $item["q_level"]; ?>" <?php echo set_select("vut", $item["q_level"]) ?>><?php echo $item["level_name"]; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <button type="submit" id="btn_submit" class="btn btn-primary">ตกลง</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="col-md-12"> 
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title">แสดงรายชื่ออัตรากำลังผู้ปฏิบัติงานเทียบกับผู้เกษียณอายุ <?php echo $header_title ?></h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button type="button" id="active" class="btn btn-custom btn-emp-type active" onclick="get_employee_by_type('active')">ผู้ปฏิบัติงาน</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="retire" class="btn btn-custom btn-emp-type" onclick="get_employee_by_type('retire')">ผู้เกษียณอายุ</button>  
                    </div>
                </div>

                <div class="btn-group btn-group-justified">
                    <?php for ($start_year; $start_year <= $end_year; $start_year++) : ?>
                        <div class="btn-group">
                            <button type="button" class="btn btn-custom btn-year <?php echo "btn-year-" . $start_year ?> <?php echo ($default_year == $start_year) ? "active" : "" ?>" onclick="get_employee_by_year($(this))"><?php echo $start_year; ?></button>
                        </div>  
                    <?php endfor; ?>
                </div>
            </div> 
            <div class="form-group">
                <div class="row">                    
                    <div class="col-xs-12">
                        <div class="wrap-scroll-table" id="tbl_list_employee">                
                            <?php $this->load->view("employee/tbl_list_employee") ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>   

    function change_valid_end_year(pObj)
    {
        $("#end_year").children("option").removeAttr("style");
        var start_year = pObj.value;
        $("#end_year").children("option").filter(function () {
            return this.value < start_year;
        }).css("display", "none");
        $("#end_year").val(start_year);
    }

    function onchange_unit()
    {
        var is_unit;
        if ($("#unit :selected").hasClass("unit")) { // เช็คว่าเป็น unit
            is_unit = "1";
        } else {
            is_unit = "0";
        }
        $("#is_unit").val(is_unit);
    }

    function get_employee_by_type(pType) {
        // remove default selected 
        $(".btn-emp-type").removeClass("active");
        var type = pType;
        var year = $(".btn-year.active").text();
        get_employee(type, year);
    }

    function get_employee_by_year(pYear) {
        var type = $(".btn-emp-type.active").prop("id");
        var year = pYear.text();
        get_employee(type, year);
    }

    function get_employee(type, year) {
        // remove default selected        
//        $("#tbl_list_employee").empty();
        $(".btn-year").removeClass("active");
        $(".btn-year").prop("disabled", true);
        $(".btn-emp-type").prop("disabled", true);
        $("body,.btn-custom").css({'cursor': 'wait'});

        var unit = $("#unit").val();
        var vut = $("#vut").val();
        var is_unit;
        if ($("#unit :selected").hasClass("unit")) { // เช็คว่าเป็น unit
            is_unit = "1";
        } else {
            is_unit = "0";
        }

        var url = "<?php echo site_url("employee/ajax_get_employee_by_year"); ?>";

        $.post(url, {type: type, year: year, unit: unit, vut: vut, is_unit: is_unit}, function (data) {
            if (data) {
                // set active to clicked elm 
                $("." + "btn-year-" + year).addClass("active");
                $("#" + type).addClass("active");
                // show
                $("#tbl_list_employee").html(data);
            }
        }, "html").always(function () {
            $(".btn-year").prop("disabled", false);
            $(".btn-emp-type").prop("disabled", false);
            $(".btn-custom").css({'cursor': 'pointer'});
            $("body").css({'cursor': 'default'});
        });
    }

    function get_active_employee() {
        get_active_employee_by_year($(".btn-year.active"));
    }

</script>



