<div class="row">
    <div class="col-md-offset-3 col-md-6">     
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">ปี</label>
                <div class="col-xs-10 col-sm-3">
                    <select class="form-control" id="start_year" name="start_year" onchange="change_valid_end_year(this)">
                        <?php $cur_year = date("Y"); ?>
                        <?php foreach ($dt_year as $item): ?>
                            <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("start_year", $item, ($item == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>                           
                </div>
                <label class="col-xs-12 col-sm-1 control-label no-padding-width">-</label>                
                <div class="col-xs-10 col-sm-3">
                    <select class="form-control" id="end_year" name="end_year" >
                        <?php $cur_year = date("Y"); ?>
                        <?php foreach ($dt_year as $item): ?>
                            <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("end_year", $item, ($item == $cur_year + 3)); ?> style="<?php echo ($item <= $cur_year) ? "display:none" : "display:block" ?>"><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-12 col-sm-3 control-label" for="year">หน่วยงาน</label>
                <div class="col-xs-10 col-sm-6">
                    <select class="form-control" id="unit" name="unit" >
                        <option value="">ทั้งหมด</option>
                        <?php
                        $cnt = 0;
                        foreach ($dt_gsd_unit as $item):
                            if ($cnt !== 0) :
                                ?>
                                <option <?php echo ($item["is_unit"] == "1") ? "class='unit'" : "" ?> value="<?php echo $item["unit_sub_dept"] ?>"><?php echo $item["unit_name"] . " (" . $item["unit_abb"] . ")"; ?></option>
                                <?php
                            endif;
                            $cnt++;
                            ?>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div> 
            <div class="form-group">
                <div class="text-center">
                    <button type="button" id="btn_submit" class="btn btn-primary" onclick="do_submit()">ตกลง</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div id="div_dashboard_chart">
        <?php $this->load->view("employee/dashboard") ?>
    </div>
</div>

<script>

    function change_valid_end_year(pObj)
    {
        $("#end_year").children("option").removeAttr("style");
        var start_year = pObj.value;
        $("#end_year").children("option").filter(function () {
            return this.value < start_year;
        }).css("display", "none");
        $("#end_year").val(start_year);
    }

    function do_submit() {
        var url = "<?php echo site_url("employee/ajax_get_chart"); ?>";
        var start_year = $("#start_year").val();
        var end_year = $("#end_year").val();
        var unit = $("#unit").val();
        var is_unit;
        if ($("#unit :selected").hasClass("unit")) { // เช็คว่าเป็น unit
            is_unit = "1";
        } else {
            is_unit = "0";
        }

        $("#btn_submit").prop("disabled", true);
        $("body,#btn_submit").css({'cursor': 'wait'});
        $.post(url, {start_year: start_year, end_year: end_year, unit: unit, is_unit: is_unit}, function (data) {
            if (data) {
                $("#div_dashboard_chart").html(data);
            }
        }, "html").always(function () {
            $("#btn_submit").prop("disabled", false);
            $("body,#btn_submit").css({'cursor': 'default'});
        });
    }

</script>
<style>    
    .btn {
        border-radius:0px;
    }
    .no-padding-width{
        padding-left:0px !important; 
        padding-right:0px !important;
        width:0px
    }

    .wrap-scroll-table .table>thead>tr>th,
    .wrap-scroll-table .table>tbody>tr>td {
        /*border-top: 1px #000 solid !important;*/
        /*border: 1px solid #6AA3D4 !important;*/
    }
    .wrap-scroll-table .table>tbody>tr:hover{
        background-color: #FBF9A2 !important;
    }

    .wrap-scroll-table tbody tr:nth-child(odd) {
        background-color: #E8EFF7 !important;
    }
    .wrap-scroll-table tbody tr:nth-child(even) {
        background-color: #F7FBFF !important;
    }
    .wrap-scroll-table thead tr{
        background-color: #bddaef !important;
    }   
    .unit {
        font-weight: bold;
    }
</style>


