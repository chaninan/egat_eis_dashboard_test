<?php $status_color = ["#3485BB", "#6DA9CF", "#39A330", "#A4D880", "#EB494A", "#F49095", "#F68721", "#FDAA4B", "#AE77B2", "#E194BC", "#939393", "#4593A8", "#83BE97"]; ?>

<div class="col-md-6"> 
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title" style="display:block">
                <div class="text-center">
                    <b>จำแนกตามตำแหน่ง</b>
                </div>               
            </h3>
        </div>
        <div class="box-body">
            <div id="chart">
                <?php if (empty($data_chart_pos)): ?>
                    <?php echo display_no_record_msg(); ?>
                <?php else: ?>
                    <canvas id="pos_chart" height="150"></canvas>  
                    <div class="information-date text-right">ข้อมูล ณ วันที่ : <?php echo $this->utils->convert_thai_date_abbr($info_update_date); ?> </div>
                    <div class="text-muted text-right fs12">แท่ง#1 : ผู้ปฏิบัติงาน | แท่ง#2 : ผู้เกษียณอายุ | แท่ง#3 : อัตราทดแทน </div>
                <?php endif; ?>     
            </div>
            <div>
                <table class="fs12 table table-condensed table-hover table-striped table-responsive">
                    <thead>
                        <tr><br><label class="control-label fs14">ผู้ปฏิบัติงานเทียบกับผู้เกษียณอายุ จำแนกตามตำแหน่ง</label></th>
                    </tr>
                    <tr style="background-color:rgba(118, 202, 212, 0.6)">
                        <th colspan="2" class="text-center">ปี</th>
                        <?php
                        $ind_color = 0;
                        foreach ($label_pos as $item):
                            ?>
                            <th class="text-center">
                                <span class="chart_color_preview" style="background-color: <?php echo $status_color[$ind_color]; ?>"></span>
                                <?php echo $item ?>
                            </th>
                            <?php
                            $ind_color ++;
                        endforeach;
                        ?> 
                        <th class="text-center">รวม (คน)</th>
                    </tr>
                    <tr></tr>
                    </thead>
                    <tbody>                        
                        <?php 
                            foreach ($data_summary_pos["active"] as $year => $item) : ?>
                            <tr>                                        
                                <td class="text-center"><?php echo $year ?></td>
                                <td>ผู้ปฏิบัติงาน</td>
                                <?php foreach ($data_summary_pos["active"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                                    
                                <?php endforeach; ?>
                            </tr>
                            <tr>                                        
                                <td class="text-center"></td>
                                <td>ผู้เกษียณ</td>
                                <?php foreach ($data_summary_pos["retire"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                             
                                <?php endforeach; ?>
                            </tr>
                            <tr>                                        
                                <td class="text-center"></td>
                                <td>อัตราทดแทน</td>
                                <?php foreach ($data_summary_pos["replace"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                             
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>            
        </div>            
    </div>
</div>
<div class="col-md-6"> 
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title" style="display:block">
                <div class="text-center">
                    <b>จำแนกตามคุณวุฒิ</b>
                </div>               
            </h3>
        </div>
        <div class="box-body">
            <div id="chart">
                <?php if (empty($data_vut_level)): ?>
                    <?php echo display_no_record_msg(); ?>
                <?php else: ?>
                    <canvas id="vut_chart" height="150"></canvas>  
                    <div class="information-date text-right">ข้อมูล ณ วันที่ : <?php echo $this->utils->convert_thai_date_abbr($info_update_date); ?> </div>
                    <div class="text-muted text-right fs12">แท่ง#1 : ผู้ปฏิบัติงาน | แท่ง#2 : ผู้เกษียณอายุ | แท่ง#3 : อัตราทดแทน </div>
                <?php endif; ?>     
            </div>
            <div>                
                <table class="fs12 table table-condensed table-hover table-striped table-responsive">
                    <thead>
                        <tr><br><label class="control-label fs14">ผู้ปฏิบัติงานเทียบกับผู้เกษียณอายุ จำแนกตามคุณวุฒิ</label></th>
                    </tr>
                    <tr style="background-color:rgba(118, 202, 212, 0.6)">
                        <th colspan="2" class="text-center">ปี</th>                        
                        <?php
                        $ind_color = 0;
                        foreach ($label_vut as $item):
                            ?>
                            <th class="text-center">
                                <span class="chart_color_preview" style="background-color: <?php echo $status_color[$ind_color]; ?>"></span>
                                <?php echo $item ?>
                            </th>
                            <?php
                            $ind_color ++;
                        endforeach;
                        ?> 
                        <th class="text-center">รวม (คน)</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data_summary_vut["active"] as $year => $item) : ?>
                            <tr>                                        
                                <td class="text-center"><?php echo $year ?></td>
                                <td>ผู้ปฏิบัติงาน</td>
                                <?php foreach ($data_summary_vut["active"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                                    
                                <?php endforeach; ?>
                            </tr>
                            <tr>                                        
                                <td class="text-center"></td>
                                <td>ผู้เกษียณอายุ</td>
                                <?php foreach ($data_summary_vut["retire"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                             
                                <?php endforeach; ?>
                            </tr>
                            <tr>                                        
                                <td class="text-center"></td>
                                <td>อัตราทดแทน</td>
                                <?php foreach ($data_summary_vut["replace"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                             
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>            
        </div>            
    </div>
</div>
<div class="col-md-6"> 
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title" style="display:block">
                <div class="text-center">
                    <b>จำแนกตามระดับ</b>
                </div>               
            </h3>
        </div>
        <div class="box-body">
            <div id="chart">
                <?php if (empty($data_chart_level)): ?>
                    <?php echo display_no_record_msg(); ?>
                <?php else: ?>
                    <canvas id="level_chart" height="150"></canvas>  
                    <div class="information-date text-right">ข้อมูล ณ วันที่ : <?php echo $this->utils->convert_thai_date_abbr($info_update_date); ?> </div>
                    <div class="text-muted text-right fs12">แท่ง#1 : ผู้ปฏิบัติงาน | แท่ง#2 : ผู้เกษียณอายุ</div>
                <?php endif; ?>     
            </div>
            <div>                
                <table class="fs12 table table-condensed table-hover table-striped table-responsive">
                    <thead>
                        <tr><br><label class="control-label fs14">ผู้ปฏิบัติงานเทียบกับผู้เกษียณอายุ จำแนกตามระดับ</label></th>
                    </tr>
                    <tr style="background-color:rgba(118, 202, 212, 0.6)">
                        <th colspan="2" class="text-center">ปี</th>                        
                        <?php
                        $ind_color = 0;
                        foreach ($label_level as $item):
                            ?>
                            <th class="text-center">
                                <span class="chart_color_preview" style="background-color: <?php echo $status_color[$ind_color]; ?>"></span>
                                <?php echo $item ?>
                            </th>
                            <?php
                            $ind_color ++;
                        endforeach;
                        ?> 
                        <th class="text-center">รวม (คน)</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data_summary_level["active"] as $year => $item) : ?>
                            <tr>                                        
                                <td class="text-center"><?php echo $year ?></td>
                                <td>ผู้ปฏิบัติงาน</td>
                                <?php foreach ($data_summary_level["active"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                                    
                                <?php endforeach; ?>
                            </tr>
                            <tr>                                        
                                <td class="text-center"></td>
                                <td>ผู้เกษียณอายุ</td>
                                <?php foreach ($data_summary_level["retire"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                             
                                <?php endforeach; ?>
                            </tr>                            
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div>
                    <span class="chart_color_preview" style="background-color: <?php echo $status_color[10]; ?>"></span> 
                    <div class="fs12">&nbsp; 00 สัญญาจ้างพิเศษ</div>
                </div>
            </div>            
        </div>            
    </div>
</div>


<div class="col-md-6"> 
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title" style="display:block">
                <div class="text-center">
                    <b>จำแนกตามช่วงอายุ</b>
                </div>               
            </h3>
        </div>
        <div class="box-body">
            <div id="chart">
                <?php if (empty($data_age_level)): ?>
                    <?php echo display_no_record_msg(); ?>
                <?php else: ?>
                    <canvas id="age_chart" height="150"></canvas>                      
                    <div class="information-date text-right">ข้อมูล ณ วันที่ : <?php echo $this->utils->convert_thai_date_abbr($info_update_date); ?> </div>
                    <div class="text-muted text-right fs12">แท่ง#1 : ผู้ปฏิบัติงาน | แท่ง#2 : ผู้เกษียณอายุ</div>
                <?php endif; ?>     
            </div>
            <div>                
                <table class="fs12 table table-condensed table-hover table-striped table-responsive">
                    <thead>
                        <tr><br><label class="control-label fs14">ผู้ปฏิบัติงานเทียบกับผู้เกษียณอายุ จำแนกตามช่วงอายุ</label></th>
                    </tr>
                    <tr style="background-color:rgba(118, 202, 212, 0.6)">
                        <th colspan="2" class="text-center">ปี</th>
                        <?php
                        $ind_color = 0;
                        foreach ($label_age as $item):
                            ?>
                            <th class="text-center">
                                <span class="chart_color_preview" style="background-color: <?php echo $status_color[$ind_color]; ?>"></span>
                                <?php echo $item ?>
                            </th>
                            <?php
                            $ind_color ++;
                        endforeach;
                        ?> 
                        <th class="text-center">รวม (คน)</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data_summary_age["active"] as $year => $item) : ?>
                            <tr>                                        
                                <td class="text-center"><?php echo $year ?></td>
                                <td>ผู้ปฏิบัติงาน</td>
                                <?php foreach ($data_summary_age["active"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                                    
                                <?php endforeach; ?>
                            </tr>
                            <tr>                                        
                                <td class="text-center"></td>
                                <td>ผู้เกษียณอายุ</td>
                                <?php foreach ($data_summary_age["retire"][$year] as $row) : ?>
                                    <td class="text-center"><?php echo ($row != 0) ? $row : "-" ?></td>                             
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>            
        </div>            
    </div>
</div>

<script>
    $(function ()
    {
        //Bar chart render        
        var data_bar_pos = <?php echo $data_chart_pos; ?>;
        if (data_bar_pos !== 0) {
            employee_bar_chart_by_type("#pos_chart", data_bar_pos, "ตำแหน่ง");
        }

        var data_bar_level = <?php echo $data_chart_level; ?>;
        if (data_bar_level !== 0) {
            employee_bar_chart_by_type("#level_chart", data_bar_level, "ระดับ");
        }

        var data_bar_vut = <?php echo $data_vut_level; ?>;
        if (data_bar_vut !== 0) {
            employee_bar_chart_by_type("#vut_chart", data_bar_vut, "คุณวุฒิ");
        }

        var data_bar_age = <?php echo $data_age_level; ?>;
        if (data_bar_age !== 0) {
            employee_bar_chart_by_type("#age_chart", data_bar_age, "ช่วงอายุ");
        }

    });

    function employee_bar_chart_by_type(pChartID, pData, pType) {
        var ctx = $(pChartID);
        var myChart = new Chart(ctx, {
            type: 'horizontalBar',
//            type: 'bar',
            data: pData,
            options: {
                title: {
                    display: true,
                    text: "ผู้ปฏิบัติงานเทียบกับผู้เกษียณอายุ จำแนกตาม" + pType,
                },
                legend: {
                    display: false,
//                    position: "right"
                },
                tooltips: {
                    mode: 'y',
//                    intersect: false,
                    callbacks: {
                        title: function (tooltipItem, data)
                        {
                            var xIndex = tooltipItem[0].datasetIndex;
                            var type = "ผู้ปฏิบัติงาน";
                            if (pType === "ตำแหน่ง") {
                                if (xIndex > 8) {
                                    type = "ผู้เกษียณอายุ";
                                }
                                if (xIndex > 17) {
                                    type = "อัตราทดแทน";
                                }
                            }
                            if (pType === "คุณวุฒิ") {
                                if (xIndex > 4) {
                                    type = "ผู้เกษียณอายุ";
                                }
                                if (xIndex > 9) {
                                    type = "อัตราทดแทน";
                                }
                            }
                            if (pType === "ระดับ") {
                                if (xIndex > 9) {
                                    type = "ผู้เกษียณอายุ";
                                }
                            }
                            if (pType === "ช่วงอายุ") {
                                if (xIndex > 7) {
                                    type = "ผู้เกษียณอายุ";
                                }
                            }
                            return type + " " + tooltipItem[0].yLabel;
                        },
                        label: function (tooltipItem, data)
                        {
                            var legend_label = data.datasets[tooltipItem.datasetIndex].label;
                            return pType + " " + legend_label + " : " + tooltipItem.xLabel + " คน";
                        }
                    }

                },
                responsive: true,
                scales: {
                    xAxes: [{
                            stacked: true,
                            scaleLabel: {
                                display: true,
                                labelString: 'คน',
                                fontSize: "14"
                            }
                        }],
                    yAxes: [{
                            stacked: true,
                            maxBarThickness: 12                            
                        }]
                }
            }
        });

        return myChart;
    }

</script>
<style>
    .chart_color_preview
    {
        display: block;
        float: left;
        margin-top: 3px;
        border : 1px solid #000;
        /*margin-right: 6px;*/
        width: 10px;
        height: 10px;
    }
</style>
