<div class="modal fade" id="mdl_add_plan" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">เพิ่มแผนปฏิบัติการ</h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">                    
                    <form id="frm_add_plan" class="form-horizontal">
                        <?php msgbox("mdl_msgbox") ?>
                        <h5>รายละเอียดแผนปฏิบัติการ</h5>

                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ชื่อแผนปฏิบัติการ <span class="text-danger fs18">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="plan_name" name="plan_name" placeholder="เช่น แผนการบริหารงบประมาณทำการ">
                            </div>  
                        </div>  
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ประจำปี <span class="text-danger fs18">*</span></label>
                            <div class="col-sm-6">
                                <select class="form-control" id="year" name="year" onchange="">
                                    <?php $cur_year = $this->utils->year_buddha_convert(date("Y")); ?>
                                    <?php
                                    for ($i = $cur_year - 1; $i < $cur_year + 5; $i++) :
                                        ?>                                        
                                        <option value="<?php echo $i ?>" <?php echo set_select("year", $i, ($i == $cur_year)) ?>><?php echo $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>                            
                        </div> 
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ผู้รับผิดชอบ <span class="text-danger fs18">*</span></label>
                            <div class=" col-sm-6">
                                <label class="control-label">กอง</label>
                            </div>
                            <div class="col-sm-6">
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="1">กบท-ห.</label>
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="2">กพน-ห.</label>
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="3">กอค-ห.</label>
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="4">กยธ-ห.</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <label class="control-label">กลุ่มงาน</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="5">กลุ่มงานแผนและประเมินผล</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="6">กลุ่มงานวิชาการและพัฒนา</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="7">กลุ่มงานคุณภาพและความปลอดภัย</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="8">กลุ่มงานสารสนเทศ อบก.</label>
                            </div>
                        </div>

                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ความสำคัญ</label>
                            <div class="col-sm-9">
                                <label class="col-sm-3 checkbox-inline"><input type="checkbox" name="priority[]" value="1">PA สายงาน</label>
                                <label class="col-sm-3 checkbox-inline"><input type="checkbox" name="priority[]" value="4">แผนงานสายงาน</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-9">
                                <label class="col-sm-3 checkbox-inline"><input type="checkbox" name="priority[]" value="2">PA ฝ่าย</label>
                                <label class="col-sm-3 checkbox-inline"><input type="checkbox" name="priority[]" value="5">แผนงานฝ่าย</label>
                            </div>
                            <div class="col-sm-offset-3 col-sm-9">
                                <label class="col-sm-3 checkbox-inline"><input type="checkbox" name="priority[]" value="3">PA กอง</label>
                                <label class="col-sm-3 checkbox-inline"><input type="checkbox" name="priority[]" value="6">แผนงานกอง</label>
                            </div>                            
                        </div>

                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">เอกสาร BCG1</label>
                            <div class="col-sm-9">                                
                                <input type="file" name="file_bcg1"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .pdf และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>                            
                        </div>   
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">เอกสาร BCG2</label>
                            <div class="col-sm-9">                                
                                <input type="file" name="file_bcg2"/>   
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .pdf และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>                            
                        </div>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">เอกสาร BCG3-5</label>
                            <div class="col-sm-9">                                
                                <input type="file" name="file_bcg3-5"/>   
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .pdf และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>                            
                        </div>

                        <h5>กำหนดความก้าวหน้างานตามเป้าหมาย (Cumulative Target Planing)</h5>                        
                        <div class="form-group form-group-little-padding">
                            <!--<label class="col-sm-3 control-label">Cumulative Target (%)</label>-->
                            <div class="col-sm-12">
                                <table class="table table-hover table-bordered table-striped table-responsive table-condensed">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th colspan="3">ไตรมาสที่ 1</th>
                                            <th colspan="3">ไตรมาสที่ 2</th>
                                            <th colspan="3">ไตรมาสที่ 3</th>
                                            <th colspan="3">ไตรมาสที่ 4</th>
                                        </tr>
                                        <tr>
                                            <th style="width:5%">#</th>
                                            <?php foreach ($dt_month as $row) : ?>
                                                <th><?php echo $row ?></th>
                                            <?php endforeach; ?>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center text-muted fs12">% <span class="text-danger fs18">*</span> สะสม </td>                                            
                                            <?php foreach ($dt_month as $key => $row) : ?>
                                                <td><input type="text" class="form-control input-percent" style="padding:3px; height:30px; text-align:center" name="target_month[]"></td>
                                            <?php endforeach; ?>
                                        </tr>                                       
                                    </tbody>
                                </table>  
                                <div class="form-group form-group-little-padding">
                                    <div class="col-sm-6 text-left">
                                        <span class="help-block">หมายเหตุ ให้ระบุเพียงตัวเลข % สะสม โดยไม่ต้องใส่เครื่องหมาย %</span> 
                                    </div>                            
                                    <div class="col-sm-6 text-right">
                                        <span class="text-danger fs12">กรุณาระบุข้อมูลในช่องที่มีเครื่องหมาย * ให้ครบถ้วน</span> 
                                    </div>                            
                                </div>
                            </div>                           
                        </div> 
                    </form>
                    <div class="code-comment">
                        ก่อนบันทึก 
                        1). เช็คชื่อแผน + ประจำปี ต้องไม่ซ้ำ
                        2). % รวม ต้องเท่ากับ 100%
                    </div>
                </div>
            </div>

            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-primary" onclick="save_add_plan()">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            </div>

        </div>
    </div>
</div>

<script>
    $(function () {
        $(".input-percent").only_numeric("1");
        $(".input-percent").percent_pattern();
    });
    function show_mdl_add_plan()
    {
        reset_form();
        clear_msgbox("#mdl_msgbox");
        $("#mdl_add_plan").modal("show");
    }

    function save_add_plan() {
        var url = "<?php echo site_url("action_plan_admin/ajax_add_plan") ?>";
        var data = new FormData($("#frm_add_plan")[0]);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            async: true,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data)
            {
                if (data.status === "0")
                {
                    show_msgbox(data.msg, data.status, "mdl_msgbox");
                    gotop_modal();
                } else
                {
                    window.location = data.next_url;
                }
            }
        });
    }

    function reset_form()
    {
        var mdl = $("#frm_add_plan");
        mdl.find("#plan_name").val('');
        mdl.find('input:checkbox').prop("checked", false);
        mdl.find("input[name='target_month[]']").val('');
        mdl.find("input[type=file]").val('');
        var cur_year = "<?php echo $this->utils->year_buddha_convert(date("Y")); ?>";
        mdl.find("#year").val(cur_year);

        $(".input-percent").css("background-color", "");
        $(".input-percent").css("border-color", "");
    }


</script>