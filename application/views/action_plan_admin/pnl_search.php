<div id="panel_search" class="">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><i class="fa fa-search"></i> ค้นหา <span class="fs12" id="link_show_search">(แสดง <i class="fa fa-caret-down"></i>)</span></h3>
        </div>
        <div id="wrap-panel-body" class="panel-body" style="<?php echo (isset($have_search) && $have_search == true) ? "display: block" : "display: none" ?>">
            <form class="form-horizontal" id="frm_search" name="frm_search" method="POST">
                <div class="form-group">                    
                    <label class="col-md-2 control-label">ชื่อแผนปฏิบัติการ</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="plan_name" name="plan_name" value="<?php echo set_value("plan_name") ?>">
                    </div>
                    <label class="col-md-2 control-label">ประจำปี</label>
                    <div class="col-md-3">
                        <select class="form-control" id="year" name="year">
                            <option value=""><?php echo CON_SELECT_ALL ?></option>
                            <?php $cur_year = $this->utils->year_buddha_convert(date("Y")); ?>
                            <?php
                            for ($i = $cur_year - 1; $i < $cur_year + 5; $i++) :
                                ?>                                        
                                <option value="<?php echo $i ?>" <?php echo set_select("year", $i, ($i == $cur_year)) ?>><?php echo $i ?></option>
                            <?php endfor; ?>
                        </select>
                    </div>  
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">ผู้รับผิดชอบ</label>
                    <div class="col-md-4">
                        <label class="control-label">กอง</label>                     
                        <div >
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="1" <?php echo set_checkbox("owner[]", "1") ?>>กบท-ห.</label>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="2" <?php echo set_checkbox("owner[]", "2") ?>>กพน-ห.</label>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="3" <?php echo set_checkbox("owner[]", "3") ?>>กอค-ห.</label>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="4" <?php echo set_checkbox("owner[]", "4") ?>>กยธ-ห.</label>
                        </div>
                        <label class="control-label">กลุ่มงาน</label>
                        <div>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="5" <?php echo set_checkbox("owner[]", "5") ?>>กลุ่มงานแผนและประเมินผล</label>
                        </div>
                        <div>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="6" <?php echo set_checkbox("owner[]", "6") ?>>กลุ่มงานวิชาการและพัฒนา</label>
                        </div>
                        <div>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="7" <?php echo set_checkbox("owner[]", "7") ?>>กลุ่มงานคุณภาพและความปลอดภัย</label>
                        </div>
                        <div>
                            <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="8" <?php echo set_checkbox("owner[]", "8") ?>>กลุ่มงานสารสนเทศ อบก.</label>
                        </div>
                    </div>
                    <label class="col-md-2 control-label">ความสำคัญ</label>
                    <div class="col-md-4">
                        <label class="checkbox-inline col-md-4"><input type="checkbox" name="priority[]" value="1" <?php echo set_checkbox("priority[]", "1") ?>>PA สายงาน</label>
                        <label class="checkbox-inline col-md-6"><input type="checkbox" name="priority[]" value="4" <?php echo set_checkbox("priority[]", "4") ?>>แผนงานสายงาน</label>
                    </div>
                    <div class="col-md-offset-2 col-md-4">
                        <label class="checkbox-inline col-md-4"><input type="checkbox" name="priority[]" value="2" <?php echo set_checkbox("priority[]", "2") ?>>PA ฝ่าย</label>
                        <label class="checkbox-inline col-md-6"><input type="checkbox" name="priority[]" value="5" <?php echo set_checkbox("priority[]", "5") ?>>แผนงานฝ่าย</label>
                    </div>
                    <div class="col-md-offset-2 col-md-4">
                        <label class="checkbox-inline col-md-4"><input type="checkbox" name="priority[]" value="3" <?php echo set_checkbox("priority[]", "3") ?>>PA กอง</label>
                        <label class="checkbox-inline col-md-6"><input type="checkbox" name="priority[]" value="6" <?php echo set_checkbox("priority[]", "6") ?>>แผนงานกอง</label>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-info"><i class="fa fa-search"></i> ค้นหา</button>
                    <a href="javascript:void(0);" class="fs14 text-muted" onclick="clear_form();"><span class="glyphicon glyphicon-repeat"></span> ล้างค่า </a>
                </div>
            </form>
        </div> 
    </div>
</div>
<script>
    $('.panel-heading').click(function () {
        show_search_body();
    });

    function show_search_body()
    {
        $("#wrap-panel-body").slideToggle("slow", "linear", function () {
            if ($("#link_show_search i").hasClass("fa fa-caret-up"))
            {
                $("#link_show_search").html("(แสดง <i class='fa fa-caret-down'></i>)");

            } else
            {
                $("#link_show_search").html("(ซ่อน <i class='fa fa-caret-up'></i>)");
            }
        });
    }

</script>