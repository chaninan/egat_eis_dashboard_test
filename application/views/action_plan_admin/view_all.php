<?php echo msgbox(); ?>

<?php $this->load->view("action_plan_admin/pnl_search") ?>

<div class="padding-top-bottom10 text-right">
    <button class="btn btn-sm btn-success" onclick="show_mdl_add_plan()"><i class="glyphicon glyphicon-plus"></i> เพิ่มแผนฯใหม่</button>
</div>

<?php if (!empty($dt_plan)) : ?>
    <div class="pull-left">
        รวมทั้งหมด <b><?php echo $dt_plan_cnt ?></b> รายการ
    </div>
    <table class="fs14 table table-hover table-bordered table-striped table-condensed table-responsive">
        <thead>
            <tr>
                <th style="width:5%">#</th>
                <th style="width:10%"><?php echo heading_sortable("ประจำปี", "year"); ?></th>
                <th style="width:40%"><?php echo heading_sortable("รายชื่อแผนปฏิบัติการ", "plan_name"); ?></th>
                <th style="width:20%">ผู้รับผิดชอบ</th>
                <th style="width:10%">ความสำคัญ</th>                
                <th style="width:5%">แก้ไข</th>
                <th style="width:5%">ลบ</th>
            </tr>
        </thead>
        <tbody>
            <?php $cnt = 1; ?>
            <?php foreach ($dt_plan as $row) : ?>
                <tr>
                    <td class="text-center"><?php echo $cnt; ?></td>
                    <td class="text-center"><?php echo $row["year"]; ?></td>
                    <td><a href="#" onclick="show_mdl_view_plan('<?php echo $this->utils->encID($row["planID"]) ?>')"> <?php echo $row["plan_name"]; ?></a></td>
                    <td><?php echo str_replace(",", "<br/>" . "- ", "- " . $row["full_owner"]) ?></td>
                    <td><?php echo (!empty($row["priority"])) ? str_replace(",", "<br/>" . "- ", "- " . $row["full_priority"]) : "-" ?></td>
                    <!--<td class="text-center"><a href="javascript:void(0)" onclick="show_mdl_view_plan('<?php // echo $this->utils->encID($row["planID"])    ?>')"><i class="fa fa-eye text-success"></i> </a></td>-->
                    <td class="text-center"><a href="javascript:void(0)" onclick="show_mdl_edit_plan('<?php echo $this->utils->encID($row["planID"]) ?>')"><i class="fa fa-pencil text-warning"></i> </a></td>
                    <td class="text-center"><a href="javascript:void(0)" onclick="show_mdl_delete_plan('<?php echo $this->utils->encID($row["planID"]) ?>', '<?php echo $row["plan_name"]; ?>')"><i class="fa fa-trash text-danger"></i> </a></td>
                </tr>
                <?php $cnt++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
<?php else : ?>
    <?php echo display_no_record_msg(); ?>
<?php endif; ?>

<div id="div_plan_detail"></div>


<!-- Modal Add -->
<?php $this->load->view("action_plan_admin/mdl_add_plan") ?>
<!-- End Modal Add-->

<script>

    function show_mdl_view_plan(planID) {
        var url = "<?php echo site_url("action_plan_admin/ajax_view_plan") ?>";
        $.post(url, {planID: planID}, function (data) {
            $("#div_plan_detail").empty().html(data);
            $("#mdl_view_plan").modal('show');
        }, "html");
    }

    function show_mdl_edit_plan(planID) {
        var url = "<?php echo site_url("action_plan_admin/ajax_get_detail_edit_plan") ?>";
        $.post(url, {planID: planID}, function (data) {
            $("#div_plan_detail").empty().html(data);
            $("#mdl_edit_plan").modal('show');
        }, "html");
    }

    function show_mdl_delete_plan(planID, plan_name)
    {
        bootbox.dialog({
            title: "ยืนยันต้องการลบข้อมูล <i class='fa fa-question-circle'></i>",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i>  คุณต้องการลบแผนปฏิบัติการ \"" + plan_name + "\" นี้ใช่หรือไม่</div>",
            buttons: {
                confirm: {
                    label: "ลบข้อมูล",
                    className: 'btn-danger glyphicon glyphicon-trash',
                    callback: function () {
                        var url = "<?php echo site_url("action_plan_admin/ajax_delete_plan") ?>";
                        $.post(url, {planID: planID}, function (data) {
                            if (data.status === "0") {
                                show_msgbox(data.msg, data.status);
                            } else {
                                window.location = "<?php echo site_url("action_plan_admin/view_all") ?>";
                            }
                        }, "json");
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-default '
                }
            }
        });
    }
</script>
