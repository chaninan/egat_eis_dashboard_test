<div class="modal fade" id="mdl_view_plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">รายละเอียดแผนปฏิบัติการ</h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">
                    <form class="form-horizontal form-horizontal-no-margin-bottom">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ชื่อแผนปฏิบัติการ</label>
                            <label class="col-sm-9 control-label-normal-value"><?php echo $dr_plan["plan_name"]; ?></label>
                        </div>                           
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ประจำปี</label>
                            <label class="col-sm-9 control-label-normal-value"><?php echo $dr_plan["year"]; ?></label>
                        </div>                           
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ผู้รับผิดชอบ</label>
                            <label class="col-sm-9 control-label-normal-value"><?php echo str_replace(",", "<br/>", $dr_plan["full_owner"]) ?></label>
                        </div>                           
                        <div class="form-group">
                            <label class="col-sm-3 control-label">ความสำคัญ</label>
                            <label class="col-sm-9 control-label-normal-value"><?php echo str_replace(",", " | ", $dr_plan["full_priority"]) ?></label>                                                      
                        </div>                           
                        <div class="form-group">
                            <label class="col-sm-3 control-label">เอกสาร BCG1</label>
                            <label class="col-sm-9 control-label-normal-value">
                                <?php if (!empty($dr_plan["file_bcg1"])): ?> 
                                    <a href="<?php echo site_url($dr_plan["file_bcg1"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a>
                                    <?php
                                else:
                                    echo "-";
                                endif;
                                ?>
                            </label>                            
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">เอกสาร BCG2</label>
                            <label class="col-sm-9 control-label-normal-value">
                                <?php if (!empty($dr_plan["file_bcg2"])): ?> 
                                    <a href="<?php echo site_url($dr_plan["file_bcg2"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a>
                                    <?php
                                else:
                                    echo "-";
                                endif;
                                ?>
                            </label>                            
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">เอกสาร BCG3-5</label>
                            <label class="col-sm-9 control-label-normal-value">
                                <?php if (!empty($dr_plan["file_bcg3-5"])): ?> 
                                    <a href="<?php echo site_url($dr_plan["file_bcg3-5"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a>
                                    <?php
                                else:
                                    echo "-";
                                endif;
                                ?>
                            </label>                            
                        </div>   
                        <h5>กำหนดความก้าวหน้างานตามเป้าหมาย (Cumulative Target Planing)</h5>                        
                        <div class="form-group form-group-little-padding">
                            <!--<label class="col-sm-3 control-label">Cumulative Target (%)</label>-->
                            <div class="col-sm-12">
                                <table class="table table-hover table-bordered table-striped table-responsive table-condensed">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th colspan="3">ไตรมาสที่ 1</th>
                                            <th colspan="3">ไตรมาสที่ 2</th>
                                            <th colspan="3">ไตรมาสที่ 3</th>
                                            <th colspan="3">ไตรมาสที่ 4</th>
                                        </tr>
                                        <tr>
                                            <th style="width:5%">#</th>
                                            <?php foreach ($dt_month as $row) : ?>
                                                <th><?php echo $row ?></th>
                                            <?php endforeach; ?>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center text-muted fs12">% <span class="text-danger fs18">*</span> สะสม </td>                                            
                                            <?php foreach ($dt_plan_target as $item): ?>
                                                <td class="text-center"><?php echo ($item["percentage"] != 0) ? $item["percentage"] : "-" ?></td>
                                            <?php endforeach; ?>     
                                        </tr>                                       
                                    </tbody>
                                </table>  
                                <div class="form-group form-group-little-padding">
                                    <div class="col-sm-6 text-left">
                                        <span class="help-block">หมายเหตุ ให้ระบุเพียงตัวเลข % สะสม โดยไม่ต้องใส่เครื่องหมาย %</span> 
                                    </div>                            
                                    <div class="col-sm-6 text-right">
                                        <span class="text-danger fs12">กรุณาระบุข้อมูลในช่องที่มีเครื่องหมาย * ให้ครบถ้วน</span> 
                                    </div>                            
                                </div>
                            </div>                           
                        </div>                            
                    </form>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>

<script>



</script>