<div class="modal fade" id="mdl_edit_plan" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">แก้ไขรายละเอียดแผนปฏิบัติการ</h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">
                    <?php msgbox("mdl_msgbox") ?>
                    <form id="frm_edit_plan" class="form-horizontal">
                        <input type="hidden" name="planID" value="<?php echo $this->utils->encID(set_value("planID")) ?>">
                        <h5>รายละเอียดแผนปฏิบัติการ</h5>                        
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ชื่อแผนปฏิบัติการ <span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="plan_name" name="plan_name" value="<?php echo set_value("plan_name") ?>"/> 
                            </div>                            
                        </div>                           
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ประจำปี <span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <select class="form-control" id="year" name="year" onchange="">
                                    <?php $cur_year = date("Y"); ?>
                                    <?php
                                    for ($i = $cur_year - 1; $i < $cur_year + 5; $i++) :
                                        ?>                                        
                                        <option value="<?php echo $this->utils->year_buddha_convert($i) ?>" <?php echo set_select("year", $this->utils->year_buddha_convert($i)) ?>><?php echo $this->utils->year_buddha_convert($i) ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>                            
                        </div>                           
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ผู้รับผิดชอบ <span class="text-danger">*</span></label>
                            <div class="col-sm-6">
                                <label class="control-label">กอง</label>
                            </div>                            
                            <div class="col-sm-offset-3 col-sm-6">
                                <?php foreach ($dt_owner as $item) : ?>
                                    <?php if ($item["owner_type"] == "1"): ?>
                                        <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="<?php echo $item["plan_ownerID"]; ?>" <?php echo (isset($item["is_checked"]) && $item["is_checked"]) ? "checked" : "" ?>><?php echo $item["owner_abb"]; ?></label>                                    
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                            <div class="col-sm-offset-3 col-sm-6">
                                <label class="control-label">กลุ่มงาน</label>
                            </div>
                            <?php foreach ($dt_owner as $item) : ?>
                                <?php if ($item["owner_type"] == "2"): ?>
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <label class="checkbox-inline"><input type="checkbox" name="owner[]" value="<?php echo $item["plan_ownerID"]; ?>" <?php echo (isset($item["is_checked"]) && $item["is_checked"]) ? "checked" : "" ?>><?php echo $item["owner_name"]; ?></label>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>

                        </div>                           
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-3 control-label">ความสำคัญ</label>
                            <?php $last_level = "" ?>
                            <?php
                            foreach ($dt_priority as $item):
                                if ($item["priority_group"] == "1"):
                                    if ($last_level !== $item["level"]):
                                        if ($last_level != ""):
                                            echo "</div>";
                                        endif;

                                        echo ($last_level == "") ? "<div class='col-sm-9'>" : "<div class='col-sm-offset-3 col-sm-9'>";
                                        echo "<label class='col-sm-3 checkbox-inline'><input type='checkbox' name='priority[]' value='" . $item["priorityID"] . "'" . ((isset($item['is_checked']) && $item['is_checked']) ? ' checked' : '') . ">" . $item['priority_name'] . "</label>";
                                    else:
                                        echo "<label class='col-sm-3 checkbox-inline'><input type='checkbox' name='priority[]' value='" . $item["priorityID"] . "'" . ((isset($item['is_checked']) && $item['is_checked']) ? ' checked' : '') . ">" . $item['priority_name'] . "</label>";
                                    endif;
                                endif;
                                $last_level = $item["level"];
                            endforeach;
                            echo "</div>";
                            ?>                        
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">เอกสาร BCG1</label>
                            <?php if (!empty($dr_plan["file_bcg1"])): ?> 
                                <label id="cur_attach_bcg1" class="col-sm-9 control-label-normal-value" style="display:block">
                                    <a href="<?php echo site_url($dr_plan["file_bcg1"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a>
                                    <a href="#" class="text-danger" onclick="delete_attach('bcg1')">| <i class="fa fa-remove"></i> ลบ</a>
                                </label>
                            <?php endif; ?>

                            <div id="add_attach_bcg1" class="col-sm-6" style="<?php echo empty($dr_plan["file_bcg1"]) ? "display:block" : "display:none" ?>">
                                <input type="hidden" id="hdd_is_delete_bcg1" name="is_delete_file_bcg1"/>
                                <input type="file" name="file_bcg1"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .pdf และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">เอกสาร BCG2</label>
                            <?php if (!empty($dr_plan["file_bcg2"])): ?> 
                                <label id="cur_attach_bcg2" class="col-sm-9 control-label-normal-value" style="display:block">
                                    <a href="<?php echo site_url($dr_plan["file_bcg2"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a>
                                    <a href="#" class="text-danger" onclick="delete_attach('bcg2')">| <i class="fa fa-remove"></i> ลบ</a>
                                </label>
                            <?php endif; ?> 

                            <div id="add_attach_bcg2" class="col-sm-6" style="<?php echo empty($dr_plan["file_bcg2"]) ? "display:block" : "display:none" ?>">
                                <input type="hidden" id="hdd_is_delete_bcg2" name="is_delete_file_bcg2"/>
                                <input type="file" name="file_bcg2"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .pdf และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>                            
                        </div>   
                        <div class="form-group">
                            <label class="col-sm-3 control-label">เอกสาร BCG3-5</label>
                            <?php if (!empty($dr_plan["file_bcg3-5"])): ?> 
                                <label id="cur_attach_bcg3" class="col-sm-9 control-label-normal-value" style="display:block">                                
                                    <a href="<?php echo site_url($dr_plan["file_bcg3-5"]); ?>" target="_blank"><span class="fa fa-file-pdf-o"></span> เอกสาร </a>
                                    <a href="#" class="text-danger" onclick="delete_attach('bcg3')">| <i class="fa fa-remove"></i> ลบ</a>                                
                                </label>
                            <?php endif; ?> 

                            <div id="add_attach_bcg3" class="col-sm-6" style="<?php echo empty($dr_plan["file_bcg3-5"]) ? "display:block" : "display:none" ?>">
                                <input type="hidden" id="hdd_is_delete_bcg3-5" name="is_delete_file_bcg3-5"/>
                                <input type="file" name="file_bcg3-5"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .pdf และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>

                        </div>   
                        <h5>กำหนดความก้าวหน้างานตามเป้าหมาย (Cumulative Target Planing)</h5>                        
                        <div class="form-group form-group-little-padding">
                            <!--<label class="col-sm-3 control-label">Cumulative Target (%)</label>-->
                            <div class="col-sm-12">
                                <table class="table table-hover table-bordered table-striped table-responsive table-condensed">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th colspan="3">ไตรมาสที่ 1</th>
                                            <th colspan="3">ไตรมาสที่ 2</th>
                                            <th colspan="3">ไตรมาสที่ 3</th>
                                            <th colspan="3">ไตรมาสที่ 4</th>
                                        </tr>
                                        <tr>
                                            <th style="width:5%">#</th>
                                            <?php foreach ($dt_month as $row) : ?>
                                                <th><?php echo $row ?></th>
                                            <?php endforeach; ?>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-center text-muted fs12">% <span class="text-danger fs18">*</span> สะสม </td>
                                            <?php
                                            foreach ($dt_month as $kmon => $kval):
                                                $key = array_search($kmon, array_column($dt_plan_target, 'month'));
                                                if (empty($key) && $key !== 0):
                                                    ?>
                                                    <td><input type="text" class="form-control input-percent target-input" name="target_month[]"></td>
                                                <?php else: ?>
                                                    <td><input type="text" class="form-control input-percent target-input" name="target_month[]" value="<?php echo $dt_plan_target[$key]["percentage"] ?>"></td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </tr>                                       
                                    </tbody>
                                </table>  
                                <div class="form-group form-group-little-padding">
                                    <div class="col-sm-6 text-left">
                                        <span class="help-block">หมายเหตุ ให้ระบุเพียงตัวเลข % สะสม โดยไม่ต้องใส่เครื่องหมาย %</span> 
                                    </div>                            
                                    <div class="col-sm-6 text-right">
                                        <span class="text-danger fs12">กรุณาระบุข้อมูลในช่องที่มีเครื่องหมาย * ให้ครบถ้วน</span> 
                                    </div>                            
                                </div>
                            </div>                           
                        </div>                           
                    </form>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-primary" onclick="save_edit_plan()">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div>
    </div>
</div>


<script>
    $(function () {
        $(".input-percent").only_numeric("1");
        $(".input-percent").money_pattern("");
    });

    function save_edit_plan()
    {
        var url = "<?php echo site_url("action_plan_admin/ajax_edit_plan") ?>";
        var data = new FormData($("#frm_edit_plan")[0]);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            async: true,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data)
            {
                if (data.status === "0")
                {
                    show_msgbox(data.msg, data.status, "mdl_msgbox");
                    gotop_modal();
                } else
                {
                    window.location = data.next_url;
                }
            }
        });
    }

    function delete_attach(file)
    {
        bootbox.dialog({
            title: "ยืนยันต้องการลบข้อมูล",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i>  คุณต้องการลบไฟล์แนบ BCG1,BCG2 นี้ใช่หรือไม่</div>",
            buttons: {
                confirm: {
                    label: "ลบข้อมูล",
                    className: 'btn-danger glyphicon glyphicon-trash',
                    callback: function () {
                        $("#add_attach_" + file).show();
                        $("#cur_attach_" + file).hide();
                        $("#hdd_is_delete_" + file).val("1");
                        //                        var url = "<?php // echo site_url("admin/ajax_delete_user")                                                                          ?>";
                        //                        $.post(url, {empID: empID}, function (data) {
                        //                            if (data.status === "0") {
                        //                                show_msgbox(data.msg, data.status);
                        //                            } else {
                        //                                window.location = "<?php // echo site_url("admin/view_all_user")                ?>";
                        //                            }
                        //                        }, "json");
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-default '
                }
            }
        });
    }

</script>