<!DOCTYPE html>
<html>
    <head>
        <title>Sync ข้อมูล</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            .body
            {
                font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
            }
        </style>
    </head>
    <body>
        <section>
            <h1>Sync ข้อมูลจาก hr_employee (10.20.8.123)</h1>
            <a href="<?php echo site_url("sync_data/user_gsd"); ?>">Sync ข้อมูล พนักงาน GSD</a> <br/>
            <a href="<?php echo site_url("sync_data/user_gsd_out"); ?>">Sync ข้อมูล พนักงาน GSD เกษียณ/ลาออก</a>
        </section>

        <div style="margin:10px 0px;">
            <table border="1" cellpadding='5'>
                <thead>
                    <tr>
                        <th>Service Name</th>
                        <th>Last run</th>
                        <th>Status</th>
                        <th>Run via</th>
                        <th>Run By</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($dt_log as $item) : ?>
                        <tr>
                            <td><?php echo $item["serviceName"] ?></td>
                            <td><?php echo $item["updateDate"] ?></td>                        
                            <td><?php echo $item["status"] ?></td>                        
                            <td><?php echo $item["update_via"] ?></td>                        
                            <td><?php echo $item["updateBy"] ?></td>                                                             
                        </tr>
                    <?php endforeach; ?>                    
                </tbody>

            </table>
        </div>

    </body>

</html>