<?php msgbox(); ?>
<form action="<?php echo site_url("budget_operation_admin/create"); ?>" method="POST" >
    <div class="form-group">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-3">
                <label for="year">ปีงบประมาณ <span class="text-danger">*</span></label>
                <select class="form-control" id="year" name="year" >
                    <option value="">กรุณาเลือก</option>
                    <?php
                    foreach($dt_year as $i)
                    {
                        _vw_render_year($i["year"], $i["created"]);
                    }
                    ?>
                </select>
                <p class="help-block">
                    กรณีที่เลือกไม่ได้ หมายถึงได้ทำการสร้าง รายการงบประมาณทำการแล้ว 
                </p>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label>งบประมาณที่ตั้ง <span class="text-danger">*</span></label>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th class="text-center">หน่วยงาน</th>
                    <?php $cnt_col = 0; ?>
                    <?php foreach($dt_month as $m): ?>
                        <th class="text-center"><?php echo $m; ?></th>
                        <?php $cnt_col++; ?>
                    <?php endforeach; ?>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dt_unit as $unit): ?>
                    <tr>
                        <td> <?php echo $unit["unit_name"]; ?></td>
                        <?php for($i = 1; $i <= $cnt_col; $i++): ?>
                            <td>
                                <?php $id = "txt_" . $unit["unitID"] . "_" . $i; ?>
                                <input type="text" class="budget_input form-control" 
                                       style="width:115px;" name="<?php echo $id; ?>" id="<?php echo $id; ?>" 
                                       value="<?php echo (isset($_POST[$id]) ? $_POST[$id] : "" ); ?>" 
                                       data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true"  />
                            </td>
                        <?php endfor; ?>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div> 

    <div class="text-center">
        <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>
        <a href="<?php echo site_url("budget_operation_admin/view_all"); ?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-chevron-left"></i> กลับ</a>
    </div>

</form>

<?php
function _vw_render_year($pYear, $pCreated)
{
    $ci = & get_instance();
    if($pCreated == FALSE)
    {//never create can select
        echo "<option value=\"" . $pYear . "\"" . set_select("year", $pYear) . " > " . $ci->utils->year_buddha_convert($pYear) . "</option>";
    }
    else
    {//already create can not select
        echo "<option value=\"" . $pYear . "\" disabled> " . $ci->utils->year_buddha_convert($pYear) . "</option>";
    }
}
?>

<?php $this->load->view("templates/script_inputmask"); ?>
<script>
    $(function ()
    {
        $(".budget_input").inputmask();       
    });
</script>

<style>
    select option:disabled
    {
        color: #ccc;
    }
    .budget_input
    {
        font-size: 13px;
    }
</style>