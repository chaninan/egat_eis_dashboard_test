<div class="text-right padding-top-bottom10">
    <a class="btn btn-sm btn-warning" href="<?php echo site_url("budget_operation_admin/edit/" . $year); ?>"><i class="glyphicon glyphicon-pencil"></i> แก้ไข</a>
    <a href="<?php echo site_url("budget_operation_admin/view_all"); ?>" class="btn btn-sm btn-info"> <i class="glyphicon glyphicon-chevron-left"></i> กลับ</a>
</div>
<?php if(!empty($dt_budget)): ?>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>หน่วยงาน</th>
                <th>ประเภท</th>
                <?php foreach($arr_month as $m): ?>
                    <th><?php echo $m; ?></th>
                <?php endforeach; ?>
                <th>รวม</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($dt_unit as $unit): ?>
                <!-- plan -->
                <tr>
                    <td class="font-weight-bold" rowspan="4"><?php echo $unit["unit_name"]; ?></td>
                    <td class="text-center">งบที่ตั้ง</td>
                    <?php $acc_plan = $acc_actual = 0; ?>
                    <?php foreach($dt_budget as $budget): ?>                        
                        <?php if($budget["unitID"] == $unit["unitID"]): ?>
                            <td class="text-right"><?php echo number_format($budget["plan"], 2); ?></td>
                            <?php
                            $arr_actual[] = array("value" => number_format($budget["actual"], 2), "css" => highligh_debt($budget["plan"], $budget["actual"]));
                            $arr_update[] = $budget["latest_name"] . " (" . date("d/m/Y H:i", strtotime($budget["latest_date"])) . ")";
                            $acc_actual += $budget["actual"];
                            $acc_plan += $budget["plan"];
                            $arr_remark[] = array("count_remark" => (!empty($budget["count_remark"]) ? $budget["count_remark"] : 0), "unitID" => $budget["unitID"], "budget_operateID" => $budget["budget_operateID"]);
                            ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="text-right font-weight-bold"><?php echo number_format($acc_plan, 2); ?></td>
                </tr>

                <!-- actual -->
                <tr>
                    <td class="text-center">ใช้จริง</td>
                    <?php foreach($arr_actual as $actual): ?>
                        <td class="text-right">
                            <span class="<?php echo $actual["css"]; ?>">
                                <?php echo $actual["value"]; ?>
                            </span>
                        </td>
                    <?php endforeach; ?>
                    <td class="text-right font-weight-bold"><?php echo number_format($acc_actual, 2); ?></td>
                </tr>

                <!--remark-->
                <tr>
                    <td class="text-center">เหตุผล</td>
                    <?php foreach($arr_remark as $remark): ?>
                        <td class="text-center">
                            <button class="btn btn-xs btn-primary" type="button" onclick="show_mdl_remark_detail('<?php echo $remark["budget_operateID"]; ?>');">
                                <i class="glyphicon glyphicon-list"></i> แสดง (<span id="lb_cnt_remark"><?php echo $remark["count_remark"]; ?></span>)
                            </button>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <!--update-->
                <tr>
                    <td class="text-center text-muted fs11">แก้ไขล่าสุด</td>
                    <?php foreach($arr_update as $update): ?>
                        <td class=" fs11 text-muted"><?php echo $update; ?></td>
                    <?php endforeach; ?>
                    <td class="">&nbsp;</td>
                </tr>

                <!-- unset actual value -->
                <?php
                unset($arr_actual);
                unset($arr_update);
                unset($arr_remark);
                ?>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="text-center padding-top-bottom10">
        <a class="btn btn-sm btn-warning" href="<?php echo site_url("budget_operation_admin/edit/" . $year); ?>"><i class="glyphicon glyphicon-pencil"></i> แก้ไข</a>
        <a href="<?php echo site_url("budget_operation_admin/view_all"); ?>" class="btn btn-sm btn-info"> <i class="glyphicon glyphicon-chevron-left"></i> กลับ</a>
    </div>
<?php else : ?>
    <div class="text-center">
        <label class="label label-danger">ไม่มีข้อมูล</label>
    </div>
<?php endif; ?>

<?php $this->load->view("budget_operation/mdl_remark_detail"); ?>