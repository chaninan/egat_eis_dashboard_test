<?php msgbox("mdl_remark_msgbox"); ?>
<div class="text-right padding-top-bottom5">
    <button class="btn btn-success" type="button" onclick="modal_bind_remark_create('<?php echo $budget_operateID; ?>')"><i class="glyphicon glyphicon-plus"></i> เพิ่มเหตุผล</button>
</div>
<?php if(isset($dt) && !empty($dt)): ?>
    <div class="wrap-mdl-remark-table">
        <table class="table table-bordered table-hover table-responsive table-striped">
            <thead>
                <tr>
                    <th style="width: 10%;">ลำดับ</th>
                    <th style="width: 50%;">เหตุผล</th>
                    <th >สร้างโดย</th>
                    <th >แก้ไขโดย</th>
                    <th>แก้ไข</th>
                    <th>ลบ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                foreach($dt as $item):
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $cnt; ?></td>

                        <td><?php echo $item["remark"]; ?></td>
                        <td class="fs12"><?php echo $item["create_name"]; ?> เมื่อ <?php echo date_thai_print($item["createDate"]); ?></td>
                        <td class="fs12">
                            <?php if(!empty($item["updateDate"])): ?>
                                <?php echo $item["update_name"]; ?> เมื่อ <?php echo date_thai_print($item["updateDate"]); ?>
                            <?php else: ?>
                                <div class="text-center"> - </div>
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-xs btn-warning" onclick="modal_bind_remark_edit('<?php echo $item["budget_operate_remarkID"]; ?>');"><i class="glyphicon glyphicon-edit"></i> แก้ไข</button>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-xs btn-danger" type="button" onclick="show_confirm_delete('<?php echo $item["budget_operate_remarkID"]; ?>');"><i class="glyphicon glyphicon-trash"></i> ลบ</button>
                        </td>  
                    </tr>               
                    <?php
                    $cnt++;
                endforeach;
                ?>
            </tbody>
        </table>

        <div class="mdl-remark-wrap-confirm-delete hidden">
            <h3>ยืนยันลบรายการนี้หรือไม่ ?</h3>
            <div class="text-center line-btn">
                <input type="hidden" id="mdl_budget_operate_remarkID"  />
                <button class="btn btn-danger" onclick="delete_remark();"> <i class="glyphicon glyphicon-trash"></i> ยืนยันลบ</button>
                <button class="btn btn-default" onclick="close_confirm_delete();"> ยกเลิก</button>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="text-center">
        <label class="label label-warning"><?php echo CON_MSG_NO_RECORD ?></label>
    </div>
<?php endif; ?>

<style>
    .wrap-mdl-remark-table
    {
        position: relative;
    }
    .mdl-remark-wrap-confirm-delete
    {
        position: absolute;
        top: 0px;
        left: 0px;
        width:100%;
        height: 100%;
        background:rgba(240,240,240,0.9);
        padding: 10px 20px 20px 20px;
    }
    .mdl-remark-wrap-confirm-delete h3
    {
        margin: 10px 0px;
    }
    .mdl-remark-wrap-confirm-delete .line-btn
    {
        margin-top: 30px;
    }
</style>

<script>
    var budget_operateID = "<?php echo $budget_operateID; ?>";
    function show_confirm_delete(pBudget_remarkID)
    {
        $("#mdl_budget_operate_remarkID").val(pBudget_remarkID);
        $(".mdl-remark-wrap-confirm-delete").removeClass("hidden");
    }
    function close_confirm_delete()
    {
        $(".mdl-remark-wrap-confirm-delete").addClass("hidden");
        $("#mdl_budget_operate_remarkID").val("");
        $("#mdl_budget_tbl_remark_id").val("");
    }

    function delete_remark()
    {
        var budget_operate_remarkID = $("#mdl_budget_operate_remarkID").val();
        if (budget_operate_remarkID != "")
        {
            var url = "<?php echo site_url("budget_operation_admin/ajax_remark_delete"); ?>";
            $.post(url,
                    {"budget_operate_remarkID": budget_operate_remarkID},
                    function (data)
                    {
                        if (data.status == "1")
                        {//delete complete                            
                            //refresh table (delete tr)
                            modal_bind_remark_table(budget_operateID, "delete_ok");                            
                        } else
                        {
                            $("#mdl_remark_msgbox").addClass("alert alert-danger").html(data.msg);
                        }
                    }, "json");
        } else
        {
            $("#mdl_remark_msgbox").addClass("alert alert-danger").html("กรุณาเลือกรายการที่ต้องการลบ");
        }
    }
</script>