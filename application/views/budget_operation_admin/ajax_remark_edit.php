<?php msgbox("mdl_remark_msgbox"); ?>

<div class="text-right">
    <button type="button" class="btn btn-default" onclick="modal_bind_remark_table('<?php echo $dt["budget_operateID"]; ?>');"><i class="glyphicon glyphicon-chevron-left"></i> กลับ</button>
</div>
<?php if(!empty($dt)): ?>
    <form id="mdl_remark_edit">
        <div class="form-group">
            <label for="remark">เหตุผล</label>
            <textarea class="form-control" rows="10" id="remark" name="remark"><?php echo $dt["remark"]; ?></textarea>
        </div>
        <input type="hidden" name="mdl_remark_hdd_budget_operate_remarkID" value="<?php echo $dt["budget_operate_remarkID"]; ?>" />
        <button type="button" class="btn btn-success" onclick="mdl_remark_edit_save();"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>
    </form>
<?php else : ?>
    <div class="text-center">
        <label class="label label-warning"><?php echo CON_MSG_NO_RECORD ?></label>
    </div>
<?php endif; ?>

<script>
    function mdl_remark_edit_save()
    {
        var url = "<?php echo site_url("budget_operation_admin/ajax_do_edit_remark"); ?>";
        $.post(url,
                $("#mdl_remark_edit").serialize(),
                function (data)
                {
                    if (data.status == "1")
                    {//update complete
                        $("#mdl_remark_msgbox").addClass("alert alert-success").html(data.msg);
                    } else
                    {
                        $("#mdl_remark_msgbox").addClass("alert alert-danger").html(data.msg);
                    }
                }, "json");
    }
</script>