<div class="padding-top-bottom10 text-right">
    <a class="btn btn-sm btn-success" href="<?php echo site_url("budget_operation_admin/create") ?>"><i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล</a>
</div>

<?php if(!empty($dt_budget)): ?>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>ประจำปี</th>
                <th>ประเภท</th>
                <?php foreach($dt_unit as $unit): ?>
                    <th><?php echo $unit["unit_name"]; ?></th>
                <?php endforeach; ?>
                <th>แก้ไข</th>
                <th>แสดง</th>
            </tr>
        </thead>
        <tbody>
            <?php $all_year = get_distance_year($dt_budget); ?>
            <?php foreach($all_year as $y): ?>
                <!-- plan -->
                <tr>
                    <td class="text-center font-weight-bold" rowspan="2">
                        <?php echo $this->utils->year_buddha_convert($y); ?>
                    </td>
                    <td class="text-center">งบที่ตั้ง</td>
                    <?php foreach($dt_budget as $budget): ?>
                        <?php if($budget["year"] == $y): ?>
                            <td class="text-right">
                                <?php $arr_actual[] = array("value" => number_format($budget["sum_actual"], 2), "css" => highligh_debt($budget["sum_plan"], $budget["sum_actual"])); ?>
                                <?php echo number_format($budget["sum_plan"], 2); ?>                           
                            </td>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td class="text-center" rowspan="2">
                        <a class="btn btn-xs btn-warning" href="<?php echo site_url("budget_operation_admin/edit/" . $y); ?>"><i class="glyphicon glyphicon-pencil"></i> แก้ไข</a>
                    </td>
                    <td class="text-center" rowspan="2">                        
                        <a class="btn btn-xs btn-info" href="<?php echo site_url("budget_operation_admin/view/" . $y); ?>"><i class="glyphicon glyphicon-eye-open"></i> แสดง</a>
                    </td>
                </tr>

                <!-- actual -->
                <tr>
                    <td class="text-center">ใช้จริง</td>
                    <?php foreach($arr_actual as $actual): ?>
                        <td class="text-right">
                            <span class="<?php echo $actual["css"]; ?>">
                                <?php echo $actual["value"]; ?>
                            </span>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <!-- unset actual value -->
                <?php unset($arr_actual); ?>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <div class="text-center">
        <label class="label label-danger">ไม่มีข้อมูล</label>
    </div>
<?php endif; ?>

<?php
function get_distance_year($pDT)
{
    $cur_year = "";
    $arr_year = array();
    foreach($pDT as $i)
    {
        if($cur_year != $i["year"])
        {
            $cur_year = $i["year"];
            $arr_year[] = $i["year"];
        }
    }

    return $arr_year;
}
?>

