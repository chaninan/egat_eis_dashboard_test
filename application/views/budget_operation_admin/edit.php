<div class="text-right padding-top-bottom10">
    <a href="<?php echo site_url("budget_operation_admin/view_all"); ?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-chevron-left"></i> กลับ</a>
</div>
<?php msgbox(); ?>
<?php if(!empty($dt_budget)): ?>
    <table class="table table-hover table-bordered table-striped">
        <thead>
            <tr>
                <th>เดือน</th>
                <th>ประเภท</th>
                <?php foreach($dt_unit as $unit): ?>
                    <th><?php echo $unit["unit_name"]; ?></th>
                <?php endforeach; ?>
                <th>บันทึก</th>
            </tr>
        </thead>
        <tbody>
            <?php $cnt_month = 1; ?>
            <?php foreach($arr_month as $month): ?>
                <?php $frm_id = "frm_" . $month; ?>
            <form id="<?php echo $frm_id; ?>">
                <!-- plan -->
                <tr>
                    <td class="font-weight-bold" rowspan="3" style="vertical-align: middle; text-align: center;" ><?php echo $month; ?></td>
                    <td class="text-center">งบที่ตั้ง</td>
                    <?php foreach($dt_budget as $budget): ?>                        
                        <?php if($budget["month"] == $cnt_month): ?>
                            <td class="text-right">
                                <span class="view_month view_month_<?php echo $cnt_month; ?>">
                                    <?php echo number_format($budget["plan"], 2); ?>                                    
                                </span>
                                <div class="edit_month edit_month_<?php echo $cnt_month; ?> hidden">
                                    <input type="text" class="form-control input-budget"
                                           name="plan_<?php echo $budget["unitID"]; ?>" 
                                           value="<?php echo number_format($budget["plan"], 2); ?>" 
                                           style="width:180px;"
                                           data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true" />
                                </div>
                            </td>
                            <?php
                            //เก็บค่าเพื่อ render actual
                            $arr_actual[] = array("actual" => number_format($budget["actual"], 2), "unitID" => $budget["unitID"] ,"css" => highligh_debt($budget["plan"], $budget["actual"]));
                            //เก็บค่าเพื่อ render Remark
                            $arr_remark[] = array("count_remark" => (!empty($budget["count_remark"]) ? $budget["count_remark"] : 0), "unitID" => $budget["unitID"], "budget_operateID" => $budget["budget_operateID"]);
                            ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <td rowspan="3" style="vertical-align: middle; text-align: center;" >
                        <div class="view_month_<?php echo $cnt_month; ?>">
                            <div class="view_month view_month_<?php echo $cnt_month; ?>">
                                <button class="btn btn-xs btn-warning" type="button" onclick="show_edit_month('<?php echo $cnt_month; ?>');"><i class="glyphicon glyphicon-pencil"></i> แก้ไข</button>
                            </div>
                            <div class="edit_month edit_month_<?php echo $cnt_month; ?> hidden">
                                <input type="hidden" name="month" value="<?php echo $cnt_month; ?>" />
                                <input type="hidden" name="year" value="<?php echo $year; ?>" />

                                <button class="btn btn-xs btn-success" type="button" onclick="save_edit('<?php echo $frm_id; ?>')"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>                                
                                <a href="javascript:void(0);" onclick="cancel_edit_month('<?php echo $cnt_month; ?>');">ยกเลิก</a>
                                <div class="fs11 text-muted padding-top-bottom5">(บันทึกทั้งเดือน)</div>
                            </div>
                        </div>                        
                    </td>
                </tr>

                <!-- actual -->
                <tr>
                    <td class="text-center">ใช้จริง</td>
                    <?php foreach($arr_actual as $actual): ?>
                        <td class="text-right">
                            <span class="view_month view_month_<?php echo $cnt_month; ?> <?php echo $actual["css"]; ?>">
                                <?php echo $actual["actual"]; ?>
                            </span>                            
                            <div class="edit_month edit_month_<?php echo $cnt_month; ?> hidden">
                                <input type="text" name="actual_<?php echo $actual["unitID"]; ?>" 
                                       class="form-control input-budget" 
                                       value="<?php echo $actual["actual"]; ?>" 
                                       style="width:180px;"
                                       data-inputmask="'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true"/>
                            </div>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <!--remark-->
                <tr>
                    <td class="text-center">เหตุผล</td>
                    <?php foreach($arr_remark as $remark): ?>
                        <td class="text-center">
                            <button class="btn btn-xs btn-primary" type="button" onclick="view_remark_budget_operate('<?php echo $remark["budget_operateID"]; ?>', this);">
                                <i class="glyphicon glyphicon-list"></i> แสดง (<span id="lb_cnt_remark"><?php echo $remark["count_remark"]; ?></span>)
                            </button>
                        </td>
                    <?php endforeach; ?>
                </tr>

                <!-- unset actual value -->
                <?php unset($arr_actual); ?>
                <?php unset($arr_remark); ?>
                <?php $cnt_month++; ?>
            </form>
        <?php endforeach; ?>
    </tbody>
    </table>    
<?php else : ?>
    <div class="text-center">
        <label class="label label-danger">ไม่มีข้อมูล</label>
    </div>
<?php endif; ?>

<div class="text-center padding-top-bottom10">
    <a href="<?php echo site_url("budget_operation_admin/view_all"); ?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-chevron-left"></i> กลับ</a>
</div>

<?php $this->load->view("templates/script_inputmask"); ?>

<div class="modal fade " tabindex="-1" role="dialog" id="mdl_remark_detail">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div id="res_canvas"></div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<style>
    table
    {
        table-layout: fixed;
    }
</style>

<script>

    var obj_btn_remark;
    var mdl_budget_operateID;
    $(function ()
    {
        $(".input-budget").inputmask();
        //$(".input-budget").inputmask();       

        //เมื่อปิด Modal
        $('#mdl_remark_detail').on('hidden.bs.modal', function ()
        {
            //Update Button count
            update_cnt_remark(obj_btn_remark, mdl_budget_operateID);

            //clear ค่า ID 
            mdl_budget_operateID = 0;
        });
    });

    function show_edit_month(pMonth)
    {
        $(".edit_month.edit_month_" + pMonth).removeClass("hidden");
        $(".view_month.view_month_" + pMonth).addClass("hidden");
    }
    function cancel_edit_month(pMonth)
    {
        $(".edit_month.edit_month_" + pMonth).addClass("hidden");
        $(".view_month.view_month_" + pMonth).removeClass("hidden");
    }
    function save_edit(pID)
    {
        var url = "<?php echo site_url("budget_operation_admin/ajax_do_edit"); ?>";
        $.post(url,
                $("#" + pID).serialize(),
                function (data)
                {
                    if (data.status === "1")
                    {
                        window.location = data.next_url;
                    } else
                    {
                        $("#msgbox").html(data.msg).addClass("alert alert-danger");
                    }
                }, "json");
    }

    function view_remark_budget_operate(pBudget_operateID, pObj)
    {
        //เก็บ Object ของ Button เพื่อใช้ update จำนวน count เมื่อปิด modal
        obj_btn_remark = pObj;
        var url = "<?php echo site_url("budget_operation_admin/ajax_get_remark"); ?>";
        $.post(url,
                {"budget_operateID": pBudget_operateID},
                function (res)
                {
                    if (res.status == "1")
                    {
                        modal_bind_title(res.data);
                        modal_bind_remark_table(res.data.budget_operateID);
                        //keep ID
                        mdl_budget_operateID = pBudget_operateID;
                    }
                }, "json");

        $("#mdl_remark_detail").modal("show");
    }

    function update_cnt_remark(pObj, pBudget_operateID)
    {
        var url_get_count_remark = "<?php echo site_url("budget_operation_admin/ajax_get_count_remark"); ?>";

        $.post(url_get_count_remark,
                {"budget_operateID": pBudget_operateID},
                function (data)
                {
                    $(pObj).find("#lb_cnt_remark").html(data.res);
                }, "json");
    }

    function modal_bind_title(pData)
    {
        //bind title
        $("#mdl_remark_detail .modal-title").html("เหตุผลการใช้งบประมาณทำการ " + pData.unit_name + " เดือน " + pData.month_name + " " + pData.year_thai);
    }

    function modal_bind_remark_table(pBudget_operateID, pShowMSG)
    {
        //bind table
        var url_get_table = "<?php echo site_url("budget_operation_admin/ajax_remark_table"); ?>";
        $.post(url_get_table,
                {"budget_operateID": pBudget_operateID, "show_msg": pShowMSG},
                function (data)
                {
                    $("#mdl_remark_detail #res_canvas").html(data.data);
                }, "json");

    }

    function modal_bind_remark_edit(pBudget_operate_remarkID)
    {
        var url_get_edit_form = "<?php echo site_url("budget_operation_admin/ajax_remark_edit"); ?>";
        $.post(url_get_edit_form,
                {"budget_operate_remarkID": pBudget_operate_remarkID},
                function (data)
                {
                    $("#mdl_remark_detail #res_canvas").html("").html(data.data);
                }, "json");
    }

    function modal_bind_remark_create(pBudget_operateID)
    {
        var url_get_edit_form = "<?php echo site_url("budget_operation_admin/ajax_remark_create"); ?>";
        $.post(url_get_edit_form,
                {"budget_operateID": pBudget_operateID},
                function (data)
                {
                    $("#mdl_remark_detail #res_canvas").html("").html(data.data);
                }, "json");
    }
</script>
