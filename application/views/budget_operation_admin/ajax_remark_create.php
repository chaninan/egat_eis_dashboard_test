<?php msgbox("mdl_remark_msgbox"); ?>

<div class="text-right">
    <button type="button" class="btn btn-default" onclick="modal_bind_remark_table('<?php echo $budget_operateID; ?>');"><i class="glyphicon glyphicon-chevron-left"></i> กลับ</button>
</div>
<form id="mdl_remark_create">
    <div class="form-group">
        <label for="remark">เหตุผล</label>
        <textarea class="form-control" rows="10" id="remark" name="remark"></textarea>
    </div>
    <input type="hidden" name="mdl_remark_hdd_budget_operateID" value="<?php echo $budget_operateID; ?>" />
    <button type="button" class="btn btn-success" onclick="mdl_remark_create_save();"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>
</form>

<script>
    var budget_operateID = '<?php echo $budget_operateID; ?>';
    function mdl_remark_create_save()
    {
        var url = "<?php echo site_url("budget_operation_admin/ajax_do_create_remark"); ?>";
        $.post(url,
                $("#mdl_remark_create").serialize(),
                function (data)
                {
                    if (data.status == "1")
                    {//create complete
                        //refresh table (delete tr)
                        modal_bind_remark_table(budget_operateID, "save_ok");
                    } else
                    {
                        $("#mdl_remark_msgbox").addClass("alert alert-danger").html(data.msg);
                    }
                }, "json");
    }
</script>