<div class="wrap-scroll-table fs14" id="tbl_list_plan">                

    <?php if (!empty($dt_list_plan)) : ?>
        ทั้งหมด <?php echo $dt_cnt_plan ?> แผนงาน         

        <table class="fs14 table table-condensed table-hover table-striped table-bordered table-responsive">
            <thead>
                <tr>
                    <th class="text-center" style="vertical-align:middle">#</th>
                    <th class="text-center" style="width:30%;vertical-align:middle">ชื่อแผนงาน</th>
                    <th class="text-center" style="vertical-align:middle">ผู้รับผิดชอบ</th>
                    <th class="text-center rotate"><div>PA<sup>1</sup></div></th>
            <th class="text-center rotate"><div>แผนงาน<sup>1</sup></div></th>
            <th class="text-center rotate"><div>PA<sup>2</sup></div></th>
            <th class="text-center rotate"><div>แผนงาน<sup>2</sup></div></th>
            <th class="text-center rotate"><div>PA<sup>3</sup></div></th>
            <th class="text-center rotate"><div>แผนงาน<sup>3</sup></div></th>
            <th class="text-center" style="vertical-align:middle">เป้าหมายสะสม</th>
            <th class="text-center" style="vertical-align:middle">ผลการดำเนินงาน</th>                
            <th class="text-center" style="vertical-align:middle"><?php echo heading_sortable("สถานะ", "status"); ?></th>                
            </tr>
            </thead>
            <tbody class="fs12">        
                <?php
                $i = 1;
                $cnt = $cnt_start_page;
                foreach ($dt_list_plan as $row) :
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $cnt ?></td>
                        <td><?php echo $row["plan_name"] ?></td>
                        <td><?php echo str_replace(",", "<br/>" . "- ", "- " . $row["full_owner_abb"]) ?></td>
                        <?php
                        foreach ($dt_priority as $item):
                            $arr_priority = explode(",", $row["priority"]);
                            ?>                        
                            <td class="text-center td-color-priority"><?php echo (in_array($item["priorityID"], $arr_priority)) ? "<i class='fa fa-check text-success'</i>" : "" ?></td>
                        <?php endforeach; ?>
                        <td class="text-center"><?php echo ($row["target_percent"] != 0) ? $row["target_percent"] . " %" : "-" ?></td>
                        <?php
                        if ($row["status"] == 4 || $row["status"] == 5)
                        {
                            $actual_percent = $this->utils_plan->get_actual_percent($row["planID"]);
                            $data_info = $this->utils_plan->get_data_infomation($row["planID"]);
                        }
                        else
                        {
                            $actual_percent = $row["actual_percent"];
                            $data_info = $this->utils->get_months_abb($row["month"]);
                        }

                        if ($data_info == $this->utils->get_months_abb($row["month"]))
                        {
                            unset($data_info);
                        }
                        ?> 
                        <td class="text-center"><b><?php echo (!empty($actual_percent)) ? $actual_percent . " %" : "-"; ?></b>
                            <?php echo (!empty($actual_percent) && isset($data_info)) ? '<p class="text-muted fs12">(ข้อมูลเมื่อ : ' . $data_info . ')</p>' : "" ?>
                        </td>
                        
                        <td><b><span class="chart_color_preview" style="margin-top:3px; background-color: <?php echo $status_color[$row["status"]]; ?>"></span><?php echo $row["status_name"]  ?></b></td>
                    </tr>
                    <?php
                    $cnt++;
                endforeach;
                ?>
            </tbody>
        </table>

        <div class="text-center">
            <?php echo $this->pagination->create_links(); ?>
        </div>

        <div class="desc-abb col-sm-8 fs12"> 
            <label class="control-label">คำอธิบาย</label><br>
            <div class="col-sm-4">
                <label class="control-label">PA<sup>1</sup> : PA สายงาน รวห.</label><br/>
                <label class="control-label">PA<sup>2</sup> : PA ระดับฝ่าย</label><br/>
                <label class="control-label">PA<sup>3</sup> : PA ระดับกอง</label><br/>
            </div>
            <div class="col-sm-8">
                <label class="control-label">แผนงาน<sup>1</sup> : แผนงานและโครงการสายงาน รวห.</label><br/>
                <label class="control-label">แผนงาน<sup>2</sup> : แผนงานระดับฝ่าย</label><br/>
                <label class="control-label">แผนงาน<sup>3</sup> : แผนงานระดับกอง</label><br/>
            </div>
        </div>       


    <?php else : ?>
        <?php echo display_no_record_msg() ?>
    <?php endif; ?>
</div>

<script>
    function get_employee_by_type(pType) {
        // remove default selected 
        $(".btn-emp-type").removeClass("active");
        var type = pType;
        var year = $(".btn-year.active").text();
        get_employee(type, year);
    }

    function get_employee_by_year(pYear) {
        var type = $(".btn-emp-type.active").prop("id");
        var year = pYear.text();
        get_employee(type, year);
    }

    function get_employee(type, year) {
        // remove default selected        
        $("#tbl_list_plan").empty();
        $(".btn-year").removeClass("active");
        $(".btn-year").prop("disabled", true);
        $(".btn-emp-type").prop("disabled", true);
        $("body,.btn-custom").css({'cursor': 'wait'});

        var unit = $("#unit").val();
        var vut = $("#vut").val();
        var is_unit;
        if ($("#unit :selected").hasClass("unit")) { // เช็คว่าเป็น unit
            is_unit = "1";
        } else
            is_unit = "0";

        var url = "<?php echo site_url("employee/ajax_get_employee_by_year"); ?>";

        $.post(url, {type: type, year: year, unit: unit, vut: vut, is_unit: is_unit}, function (data) {
            if (data) {
                // set active to clicked elm 
                $("." + "btn-year-" + year).addClass("active");
                $("#" + type).addClass("active");
                // show
                $("#tbl_list_plan").html(data);
            }
        }, "html").always(function () {
            $(".btn-year").prop("disabled", false);
            $(".btn-emp-type").prop("disabled", false);
            $(".btn-custom").css({'cursor': 'pointer'});
            $("body").css({'cursor': 'default'});
        });
    }



    function get_active_employee() {
        get_active_employee_by_year($(".btn-year.active"));
    }

</script>
<style>
    .desc-abb{
        background-color : #fcf8e3;
        border-color : #faebcc;
        color : #8a6d3b;
    }

    .btn-custom {
        color:#fff;
        background-color :#286090;
        border-color : #204d74;
    }

    .btn-custom:active, .btn-custom.active {
        color:#fff;
        background-color:#3c8dbc;

    }

    .btn-custom:hover, .btn-custom.hover {
        color:#fff;
        background-color:#204d74;        
    }

    .btn-custom:focus, .btn-custom.focus {
        color:#fff;
    }
    .table>thead>tr>th {
        /*vertical-align: middle;*/
    }

    th.rotate {
        /* Something you can count on */        
        height: 80px;
        white-space: nowrap;         
    }

    th.rotate > div {
        transform: 
            /* Magic Numbers */
            /*translate(10px, 10px)*/
            /* 45 is really 360 - 45 */
            rotate(-90deg);
        width: 20px;
    } 
    
    .td-color-priority{
        background-color: rgba(63,188,146,0.13);
    }

</style>


