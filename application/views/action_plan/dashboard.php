<style>    
    .btn {
        border-radius:0px;
    }

    .no-padding-width{
        padding-left:0px !important; 
        padding-right:0px !important;
        width:0px
    }

    .wrap-scroll-table .table>thead>tr>th,
    .wrap-scroll-table .table>tbody>tr>td {
        /*border-top: 1px #000 solid !important;*/
        border: 1px solid #FFF !important;
    }
    .wrap-scroll-table .table>tbody>tr:hover{
        background-color: #FBF9A2 !important;
    }

    .wrap-scroll-table tbody tr:nth-child(odd) {
        background-color: rgba(77, 199, 186, 0.1) !important;
    }
    .wrap-scroll-table tbody tr:nth-child(even) {
        background-color: #F7FBFF !important;
    }
    .wrap-scroll-table thead tr{
        background-color: rgba(118, 202, 212, 0.6) !important;
    }   
    .unit {
        font-weight: bold;
    }

</style>
<div class="row"> 
    <div class="col-md-3">     
        <form class="form-horizontal" method="GET">
            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label">ปี</label>
                <div class="col-xs-10 col-sm-8">
                    <select class="form-control" id="year" name="year">
                        <?php $cur_year = date("Y") + 543; ?>
                        <?php foreach ($dt_year as $item): ?>
                            <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("year", $this->utils->year_buddha_convert($item), ($this->utils->year_buddha_convert($item) == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                        <?php endforeach; ?>
                    </select>                           
                </div>                
            </div>
            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label">เดือน</label>
                <div class="col-xs-10 col-sm-8">
                    <select class="form-control" id="month" name="month">
                        <?php $cur_month = date("m"); ?>
                        <?php foreach ($dt_month as $mKey => $mVal): ?>
                            <option value="<?php echo $mKey; ?>" <?php echo set_select("month", $mKey, ($mKey == $cur_month)); ?> ><?php echo $mVal ?></option>
                        <?php endforeach; ?>
                    </select>                           
                </div>                
            </div>
            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label">หน่วยงาน</label>
                <div class="col-xs-10 col-sm-8">
                    <select class="form-control" id="owner" name="owner" >
                        <option value="">ทั้งหมด</option>
                        <?php
                        $last_owner = "";
                        foreach ($dt_owner as $item):
                            if ($last_owner != $item["owner_type"]):
                                if ($item["owner_type"] == "1") :
                                    ?>
                                    <optgroup label="กอง">
                                    <?php else: ?>
                                    <optgroup label="กลุ่มงาน">
                                    <?php endif; ?>
                                <?php endif; ?>
                                <option value="<?php echo $item["plan_ownerID"] ?>" <?php echo set_select("owner", $item["plan_ownerID"]) ?>><?php echo $item["owner_name"] ?> </option>
                                <?php
                                $last_owner = $item["owner_type"];
                            endforeach;
                            ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="text-center">
                    <button type="submit" id="btn_submit" class="btn btn-primary">ตกลง</button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-9">        
        <div id="div_chart_result fs12">
            <?php $this->load->view("action_plan/div_pa") ?>
        </div>
    </div>
</div>

<div class="row"> 
    <div class="col-md-3"> 
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">สถานะแผนปฏิบัติการ : <br><br><span class="fs14 text-muted">ประจำปี <?php echo $year ?> เดือน <?php echo $month ?></span></h3>            
            </div>
            <div class="box-body">
                <?php if (!empty($data_plan_chart)): ?>
                    <canvas id="plan_chart" height="140"></canvas>  
                    <!--<div class="information-date text-right">ข้อมูล ณ วันที่ : <?php // echo date_thai_print($update_date_overall);     ?> </div>-->

                    <div class="wrap-scroll-table" style="padding:20px 20px 0px 20px">
                        <table class="fs14 table table-condensed table-hover table-striped table-bordered table-responsive">
                            <thead>
                                <tr style="background-color:rgba(118, 202, 212, 0.6)">
                                    <th class="text-center">สถานะรายการ</th>
                                    <th class="text-center">จำนวนแผน</th>
                                </tr>
                            </thead>
                            <tbody>  
                                <?php
                                $ind_color = 0;
                                foreach ($dt_summary_plan_status as $item):
                                    ?>
                                    <tr>
                                        <td>
                                            <span class="chart_color_preview" style="background-color: <?php echo $status_color[$ind_color]; ?>"></span>
                                            <?php echo $item["status_name"]; ?>
                                        </td>
                                        <td class="text-center"><?php echo (!empty($item["count"])) ? $item["count"] : "-"; ?></td>
                                    </tr>
                                    <?php
                                    $ind_color ++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php else: ?>
                    <?php echo display_no_record_msg(); ?>
                <?php endif; ?>
            </div>            
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title">แสดงรายชื่อแผนปฏิบัติการ : <br><br><span class="fs14 text-muted">ประจำปี <?php echo $year ?> เดือน <?php echo $month ?></span></h3>
            </div>
            <div class="box-body">
                <?php $this->load->view("action_plan/div_list_plan") ?>
            </div>
        </div>
    </div>
</div>
