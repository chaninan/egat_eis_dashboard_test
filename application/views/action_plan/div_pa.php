<div class="col-md-6"> 
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">คะแนน PA ฝ่ายบริการ</h3>
        </div>
        <div class="box-body">
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-2">ปี</label>
                    <div class="col-xs-10 col-sm-4">
                        <select class="form-control" id="pa_fay_year" name="pa_fay_year" onchange="pa_fay_onchange()" >
                            <?php $cur_year = date("Y") + 543; ?>
                            <?php foreach ($dt_year as $item): ?>
                                <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("year", $this->utils->year_buddha_convert($item), ($this->utils->year_buddha_convert($item) == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                            <?php endforeach; ?>
                        </select>                     
                    </div>
                    <label class="control-label col-xs-12 col-sm-1">งวด</label>
                    <div class="col-xs-10 col-sm-4">
                        <select class="form-control" id="pa_fay_period" name="pa_fay_period" onchange="pa_fay_onchange()" >
                            <option value="F">6 เดือนแรก</option>
                            <option value="L">สิ้นปี</option>                                    
                        </select>
                    </div>
                </div>
                <?php if (!empty($pa_point)) : ?>
                    <div class="form-group">
                        <div class="text-center div-point">
                            <span class="pa-point point-good" id="pa_fay_point"><?php echo $pa_point ?></span>
                        </div>
                    </div>
                <?php else : ?>
                    <?php echo display_no_record_msg() ?>
                <?php endif; ?>
            </form>            
        </div>
    </div>
</div>
<div class="col-md-6"> 
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">คะแนน PA ระดับกอง</h3>
        </div>
        <div class="box-body">
            <div class="col-md-6">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3">ปี</label>
                        <div class="col-xs-10 col-sm-9">
                            <select class="form-control" id="pa_gong_year" name="pa_gong_year" onchange="pa_gong_onchange()" >
                                <?php foreach ($dt_year as $item): ?>
                                    <option value="<?php echo $this->utils->year_buddha_convert($item); ?>" <?php echo set_select("year", $this->utils->year_buddha_convert($item), ($this->utils->year_buddha_convert($item) == $cur_year)); ?> ><?php echo $this->utils->year_buddha_convert($item); ?></option>
                                <?php endforeach; ?>
                            </select>                     
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3">งวด</label>
                        <div class="col-xs-10 col-sm-9">
                            <select class="form-control" id="pa_gong_period" name="pa_gong_period" onchange="pa_gong_onchange()" >
                                <option value="F">6 เดือนแรก</option>
                                <option value="L">สิ้นปี</option>                                    
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-3">กอง</label>
                        <div class="col-xs-10 col-sm-9">
                            <select class="form-control" id="priority" name="priority" onchange="pa_gong_onchange()">
                                <option value=""><?php echo CON_SELECT_ALL ?></option>
                                <option value="8">กบท-ห.</option>
                                <option value="9">กพน-ห.</option>
                                <option value="10">กอค-ห.</option>
                                <option value="11">กยธ-ห.</option>
                            </select>
                        </div>
                    </div>
            </div>
            <?php if (!empty($pa_point)) : ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="text-center div-point">
                            <span class="pa-point point-good" id="pa_gong_point"><?php echo $pa_point ?></span>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <?php echo display_no_record_msg() ?>
            <?php endif; ?>
            </form>
        </div>
    </div>
</div>



<script>
    $(function ()
    {
        //pie chart render        
        var data_plan_chart = <?php echo $data_plan_chart ?>;
        if (data_plan_chart !== 0) {
            plan_chart_render("#plan_chart", data_plan_chart);
        }

    });

    function plan_chart_render(pChartID, pData) {
        var ctx = $(pChartID);
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: pData,
            options: {
                legend: {
                    display: true,
                    position: "right"
                },
                pieceLabel: {
                    mode: 'label'
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data)
                        {
                            var title = data.labels[tooltipItem.index];
                            var color = data.datasets[0].backgroundColor[tooltipItem.index];
                            var value = data.datasets[0].data[tooltipItem.index];
                            return  title + " : " + value + " แผน";
                        }
                    }
                }
            }
        });

        return myChart;
    }

    function pa_fay_onchange()
    {
        var year = $("#pa_fay_year").val();
        var period = $("#pa_fay_period").val();
        var url = "<?php echo site_url("action_plan/ajax_get_pa_fay_point") ?>";
        $.post(url, {year: year, period: period}, function (data) {
            if (data) {
                $("#pa_fay_point").text(data);
            } else {
                $("#pa_fay_point").html("<span class='fs20 text-danger'>ไม่มีข้อมูล</span>");
            }
        }, "html");
    }

    function pa_gong_onchange()
    {
        var year = $("#pa_gong_year").val();
        var period = $("#pa_gong_period").val();
        var priority = $("#priority").val();
        var url = "<?php echo site_url("action_plan/ajax_get_pa_fay_point") ?>";
        $.post(url, {year: year, period: period, priority: priority}, function (data) {
            if (data) {
                $("#pa_gong_point").text(data);
            } else {
                $("#pa_gong_point").html("<span class='fs20 text-danger'>ไม่มีข้อมูล</span>");
            }
        }, "html");

    }
</script>
<style>
    .chart_color_preview
    {
        display: block;
        float: left;
        margin-top: 5px;
        margin-right: 6px;
        width: 10px;
        height: 10px;
    }
    .div-point{
        margin-top: 15px;        

    }
    .pa-point{
        font-size:50px; 
        font-weight:700;         
    }
    .pa-point.point-good { 
        color:#00C1C0;
        /*border-bottom: 2px solid #EC4938;*/        
    }
    .pa-point.point-poor { 
        color:#EC4938
    }

</style>
