<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $con_title; ?></title>

        <?php $this->load->view("templates/favicon"); ?>

        <!-- Bootstrap -->
        <link href="<?php echo base_url("css/bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("css/admin.css"); ?>" rel="stylesheet">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("css/font-awesome.min.css"); ?>">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url("js/jquery-1.12.3.min.js"); ?>"></script>
        <!-- JS Utils -->
        <script src="<?php echo base_url("js/utils.js"); ?>" ></script>
    </head>
    <body>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <?php if(already_login()): ?>
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    <?php endif; ?>
                    <a class="navbar-brand" href="<?php echo site_url("admin"); ?>"> 
                        <img src="<?php echo base_url("images/logo_gsd.png"); ?>" width="30"/> 
                        <span class="hidden-xs">ระบบ</span> Dashboard <span class="hidden-xs">ฝ่ายบริการ</span> (admin) 
                    </a>
                    <?php if(CON_APP_IS_TEST == TRUE): ?>
                        <label class="label label-danger pull-left" id="banner-test">ระบบทดสอบ</label>
                    <?php endif; ?>
                </div>
                <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
                <div id="navbar" class="navbar-collapse collapse">
                    <?php if(already_login()): ?>
                        <ul class="nav navbar-nav">
<!--                            <li class="active"><a href="<?php echo site_url("admin"); ?>"><i class="glyphicon glyphicon-home"></i> หน้าหลัก</a></li>                        -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">งบประมาณ <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url("budget_operation_admin/view_all"); ?>"> <i class="fa fa-money"></i> งบประมาณทำการ</a></li>
                                    <li><a href="<?php echo site_url("budget_investment_admin/view_all"); ?>"><i class="fa fa-bitcoin"></i> งบลงทุน</a></li>
                                    <!--<li role="separator" class="divider"></li>
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                    <li><a href="#">One more separated link</a></li>-->
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">แผนปฏิบัติการ <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo site_url("action_plan_admin/view_all"); ?>"> <i class="fa fa-list-ol"></i> แผนปฏิบัติการ</a></li>
                                    <li><a href="<?php echo site_url("action_plan_report_admin/view_all"); ?>"> <i class="fa fa-check-square-o"></i> รายงานแผนปฏิบัติการ</a></li>
                                    <li><a href="<?php echo site_url("pa_report_admin/view_all"); ?>"><i class="fa fa-star-o"></i> อัพเดทข้อมูล PA</a></li>
                                    <li role="separator" class="divider"></li>
                                    <!--<li class="dropdown-header">Nav header</li>-->
                                    <li><a href="<?php echo base_url("images/document/manual_admin_action_plan.pdf"); ?>" target="_blank"><i class="fa fa-book"></i> คู่มือการใช้งาน</a></li>                                    
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">                            
                            <li>
                                <a href="javascript:void(0);" class="fs14">
                                    <i class="glyphicon glyphicon-user"></i> คุณ, <?php echo get_name_who_login(); ?>
                                </a>
                            </li>
                            <li><a href="<?php echo site_url("authen_admin/logout"); ?>" class="text-danger fs12"><i class="glyphicon glyphicon-log-out"></i> ออกจากระบบ</a></li>
                        </ul>
                    <?php endif; ?>
                </div><!--/.nav-collapse -->
            </div>
        </nav>

        <div class="<?php echo $con_template_container; ?>" id="main-body">
            <div class="page-header">
                <h1><?php echo $con_title ?></h1>                
            </div>
            <?php $this->load->view($con_right_content); ?>
        </div> 

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url("js/bootstrap.min.js"); ?>"></script>
        <!-- Boot box -->
        <script src="<?php echo base_url("js/bootbox.min.js"); ?>"></script>

        <footer class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <b>Version</b> <?php echo CON_APP_VERSION; ?>       
                        </div>
                        <strong>พัฒนาโดย : </strong> กลุ่มงานสารสนเทศ ฝ่ายบริการ
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>