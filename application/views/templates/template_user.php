<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | <?php echo $con_title . " " . $con_sub_title; ?></title>
        <!-- Tell the browser to be responsive to screen width -->        

        <?php $this->load->view("templates/favicon"); ?>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url("css/bootstrap.min.css"); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url("css/font-awesome.min.css"); ?>">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url("css/ionicons.min.css"); ?> ">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url("css/AdminLTE_theme/AdminLTE.css"); ?>">
        <!-- AdminLTE Skins. Choose a skin from the css/skins 
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url("css/AdminLTE_theme/skins/skin-yellow.min.css"); ?>">

        <!-- Custom style -->        
        <link rel="stylesheet" href="<?php echo base_url("css/user.css"); ?> ">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url("js/jquery-1.12.3.min.js"); ?>"></script>
        <!-- JS Utils -->
        <script src="<?php echo base_url("js/utils.js"); ?>" ></script>
        <!-- ChartJS 1.0.1 -->
        <script src="<?php echo base_url("js/Chart.min.js"); ?>"></script>
        <script src="<?php echo base_url("js/Chart.bundle.min.js"); ?>"></script>
        <script src="<?php echo base_url("js/Chart.PieceLabel.js"); ?>"></script>
        
         <!-- Data table -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("css/datatables.min.css"); ?>">
        <script type="text/javascript" src="<?php echo base_url("js/datatables.min.js"); ?>"></script>
    </head>
    <?php
    /* =========================
     *  Essential data for THEME
     * ======================= */
    //load all unit
    $ci = & get_instance();
    $ci->load->model("munit");
    $unit_all = $ci->munit->get_all();

    /* End Essential data for THEME */
    ?>
    <body class="hold-transition skin-yellow sidebar-mini">      
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo site_url("member"); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini">
                        <img src="<?php echo base_url("images/logo_gsd.png"); ?>" height="32px" style="height: 32px;" /> 
                    </span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg">
                        <img src="<?php echo base_url("images/logo_gsd.png"); ?>" height="32px" style="height: 32px;" /> 
                        Dashboard (อบก.)
                    </span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <div class="navbar-custom-menu">
                        <?php if (CON_APP_IS_TEST == TRUE): ?>
                            <label class="label label-danger pull-left" id="banner-test">ระบบทดสอบ</label>
                        <?php endif; ?>
                        <ul class="nav navbar-nav">
                            <?php /*
                              <!-- Messages: style can be found in dropdown.less-->
                              <li class="dropdown messages-menu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-envelope-o"></i>
                              <span class="label label-success">4</span>
                              </a>
                              <ul class="dropdown-menu">
                              <li class="header">You have 4 messages</li>
                              <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                              <li><!-- start message -->
                              <a href="#">
                              <div class="pull-left">
                              <img src="<?php echo base_url("images/user_pic_defualt.png"); ?>" class="img-circle" alt="User Image">
                              </div>
                              <h4>
                              Support Team
                              <small><i class="fa fa-clock-o"></i> 5 mins</small>
                              </h4>
                              <p>Why not buy a new awesome theme?</p>
                              </a>
                              </li>
                              <!-- end message -->
                              </ul>
                              </li>
                              <li class="footer"><a href="#">See All Messages</a></li>
                              </ul>
                              </li>
                              <!-- Notifications: style can be found in dropdown.less -->
                              <li class="dropdown notifications-menu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-bell-o"></i>
                              <span class="label label-warning">10</span>
                              </a>
                              <ul class="dropdown-menu">
                              <li class="header">You have 10 notifications</li>
                              <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                              <li>
                              <a href="#">
                              <i class="fa fa-users text-aqua"></i> 5 new members joined today
                              </a>
                              </li>
                              </ul>
                              </li>
                              <li class="footer"><a href="#">View all</a></li>
                              </ul>
                              </li>
                              <!-- Tasks: style can be found in dropdown.less -->
                              <li class="dropdown tasks-menu">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="fa fa-flag-o"></i>
                              <span class="label label-danger">9</span>
                              </a>
                              <ul class="dropdown-menu">
                              <li class="header">You have 9 tasks</li>
                              <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                              <li><!-- Task item -->
                              <a href="#">
                              <h3>
                              Design some buttons
                              <small class="pull-right">20%</small>
                              </h3>
                              <div class="progress xs">
                              <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                              </div>
                              </div>
                              </a>
                              </li>
                              <!-- end task item -->
                              </ul>
                              </li>
                              <li class="footer">
                              <a href="#">View all tasks</a>
                              </li>
                              </ul>
                              </li>
                             */ ?>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="<?php echo base_url("images/user_pic_defualt.png"); ?>" class="user-image" alt="User Image">
                                    <span class="hidden-xs"><?php echo $login_member["name"]; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="<?php echo base_url("images/user_pic_defualt.png"); ?>" class="img-circle" alt="User Image">
                                        <p>
                                            <?php echo $login_member["name"]; ?> - <?php echo $login_member["d_name"]; ?>
                                            <small><?php echo $login_member["p_nname"]; ?></small>
                                        </p>
                                    </li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <!--<a href="#" class="btn btn-default btn-flat">Profile</a>-->
                                        </div>
                                        <div class="pull-right">
                                            <a href="<?php echo site_url("authen_member/logout"); ?>" class="btn btn-default btn-flat">ออกจากระบบ</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <!-- Control Sidebar Toggle Button -->
                            <!--                            <li>
                                                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                                                        </li>-->
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo base_url("images/user_pic_defualt.png"); ?>" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p><?php echo $login_member["name"]; ?></p>
                            <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">เมนูหลัก</li>
                        <!--                        <li class="treeview">
                                                    <a href="#">
                                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                                        <span class="pull-right-container">
                                                            <i class="fa fa-angle-left pull-right"></i>
                                                        </span>
                                                    </a>
                                                    <ul class="treeview-menu">
                                                        <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                                                        <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                                                    </ul>
                                                </li>-->
                        <li class="treeview <?php menu_user_active(array("b_all", "b_1", "b_2", "b_3", "b_4", "b_5"), $con_menu_user_selected); ?>">
                            <a href="#">
                                <i class="fa fa-money"></i>
                                <span>งบประมาณทำการ</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php menu_user_active("b_all", $con_menu_user_selected) ?>"><a href="<?php echo site_url("budget_operation/all"); ?>"><i class="fa fa-circle-o"></i> ภาพรวม</a></li>
                                <?php foreach ($unit_all as $unit): ?>
                                    <li class="<?php menu_user_active("b_{$unit["unitID"]}", $con_menu_user_selected) ?>"><a href="<?php echo site_url("budget_operation/unit/{$unit["unitID"]}"); ?>"><i class="fa fa-circle-o"></i> <?php echo $unit["unit_name"]; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>                        
                        <li class="treeview <?php menu_user_active(array("bi_all", "bi_1", "bi_2", "bi_3", "bi_4", "bi_5"), $con_menu_user_selected); ?>">
                            <a href="#">
                                <i class="fa fa-bitcoin"></i>
                                <span>งบลงทุน</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php menu_user_active("bi_all", $con_menu_user_selected) ?>"><a href="<?php echo site_url("budget_investment/all"); ?>"><i class="fa fa-circle-o"></i> ภาพรวม</a></li>
                                <?php foreach ($unit_all as $unit): ?>
                                    <li class="<?php menu_user_active("bi_{$unit["unitID"]}", $con_menu_user_selected) ?>"><a href="<?php echo site_url("budget_investment/unit/{$unit["unitID"]}"); ?>"><i class="fa fa-circle-o"></i> <?php echo $unit["unit_name"]; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>

                        <li class="treeview <?php menu_user_active(array("emp_all","emp_1"), $con_menu_user_selected); ?>">
                            <a href="#">
                                <i class="fa fa-male"></i>
                                <span>อัตรากำลัง</span>
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php menu_user_active("emp_all", $con_menu_user_selected) ?>"><a href="<?php echo site_url("employee/"); ?>"><i class="fa fa-circle-o"></i> ภาพรวม</a></li>                                
                            </ul>
                            <ul class="treeview-menu">
                                <li class="<?php menu_user_active("emp_1", $con_menu_user_selected) ?>"><a href="<?php echo site_url("employee/list_employee"); ?>"><i class="fa fa-circle-o"></i> รายชื่อ</a></li>                                
                            </ul>
                        </li>
                        <li class="treeview <?php menu_user_active("p_all", $con_menu_user_selected); ?>">
                            <a href="<?php echo site_url("action_plan"); ?>">
                                <i class="fa fa-list-ol"></i>
                                <span>แผนปฏิบัติการ</span>                                
                            </a>
                        </li>
                        
                        <li class="header">ช่วยเหลือ</li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-book"></i> <span>คู่มือการใช้งาน</span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url("images/document/manual_user_action_plan.pdf") ?>" target="_blank"><i class="fa fa-circle-o"></i> <span>ส่วนแผนปฏิบัติการ</span></a></li>
                            </ul>                            
                            <ul class="treeview-menu">
                                <li><a href="<?php echo base_url("images/document/manual_user_employee.pdf") ?>" target="_blank"><i class="fa fa-circle-o"></i> <span>ส่วนอัตรากำลัง</span></a></li>
                            </ul>                            
                        </li>                        
                        <!--<li><a href="../../documentation/index.html"><i class="fa fa-book"></i> <span>คู่มือการใช้งาน</span></a></li>-->
                        
                        <!--<li class="header">LABELS</li>
                        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
                        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1><?php echo $con_title; ?><small><?php echo $con_sub_title; ?></small></h1>
                    <?php
                    if (isset($this->breadcrumb))
                    {
                        echo $this->breadcrumb->output();
                    }
                    ?>
                </section>

                <!-- Main content -->
                <section class="content">
                    <?php $this->load->view($con_right_content); ?>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right">
                    <b>Version</b> <?php echo CON_APP_VERSION; ?>
                </div>
                <strong>พัฒนาโดย : </strong> กลุ่มงานสารสนเทศ ฝ่ายบริการ
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Create the tabs -->
                <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <!-- Home tab content -->
                    <div class="tab-pane active" id="control-sidebar-home-tab">
                        <h3 class="control-sidebar-heading">Recent Activity</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                                        <p>Will be 23 on April 24th</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-user bg-yellow"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                                        <p>New phone +1(800)555-1234</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                                        <p>nora@example.com</p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon fa fa-file-code-o bg-green"></i>

                                    <div class="menu-info">
                                        <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                                        <p>Execution time 5 seconds</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                        <h3 class="control-sidebar-heading">Tasks Progress</h3>
                        <ul class="control-sidebar-menu">
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Custom Template Design
                                        <span class="label label-danger pull-right">70%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Update Resume
                                        <span class="label label-success pull-right">95%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Laravel Integration
                                        <span class="label label-warning pull-right">50%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <h4 class="control-sidebar-subheading">
                                        Back End Framework
                                        <span class="label label-primary pull-right">68%</span>
                                    </h4>

                                    <div class="progress progress-xxs">
                                        <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- /.control-sidebar-menu -->

                    </div>
                    <!-- /.tab-pane -->
                    <!-- Stats tab content -->
                    <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                    <!-- /.tab-pane -->
                    <!-- Settings tab content -->
                    <div class="tab-pane" id="control-sidebar-settings-tab">
                        <form method="post">
                            <h3 class="control-sidebar-heading">General Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Report panel usage
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Some information about this general settings option
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Allow mail redirect
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Other sets of options are available
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Expose author name in posts
                                    <input type="checkbox" class="pull-right" checked>
                                </label>

                                <p>
                                    Allow the user to show his name in blog posts
                                </p>
                            </div>
                            <!-- /.form-group -->

                            <h3 class="control-sidebar-heading">Chat Settings</h3>

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Show me as online
                                    <input type="checkbox" class="pull-right" checked>
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Turn off notifications
                                    <input type="checkbox" class="pull-right">
                                </label>
                            </div>
                            <!-- /.form-group -->

                            <div class="form-group">
                                <label class="control-sidebar-subheading">
                                    Delete chat history
                                    <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                                </label>
                            </div>
                            <!-- /.form-group -->
                        </form>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </aside>
            <!-- /.control-sidebar -->
            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url("js/bootstrap.min.js"); ?>"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url("js/fastclick.min.js"); ?> "></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url("js/AdminLTE_app.min.js"); ?>"></script>       
    </body>
</html>