<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url("images/favicons/apple-touch-icon.png");?>">
<link rel="icon" type="image/png" href="<?php echo base_url("images/favicons/favicon-32x32.png");?>" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo base_url("images/favicons/favicon-32x32.png");?>" sizes="16x16">
<link rel="manifest" href="<?php echo base_url("images/favicons/manifest.json");?>">
<link rel="mask-icon" href="<?php echo base_url("images/favicons/safari-pinned-tab.svg");?>" color="#5bbad5">
<link rel="shortcut icon" href="<?php echo base_url("images/favicons/images/favicon/favicon.ico");?>">
<meta name="msapplication-config" content="<?php echo base_url("images/favicons/browserconfig.xml");?>">
<meta name="theme-color" content="#ffffff">