<div class="modal fade" id="mdl_add_report" tabindex="-1" role="dialog" data-backdrop="true" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><span id="title"></span>รายงานแผน ประจำเดือน <?php echo $this->utils->get_months(set_value("month")) ?></h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">
                    <form id="frm_save_plan_report" class="form-horizontal">
                        <?php msgbox("mdl_msgbox") ?>
                        <h5>ความก้าวหน้างานสะสม</h5>
                        <div class="form-group">                            
                            <label class="col-sm-4 control-label">ตามเป้าหมาย (%)</label>
                            <label class="col-sm-6 control-label-normal-value" name="target_percent"><?php echo set_value("target_percent") ?></label>
                        </div>  
                        <div class="form-group form-group-little-padding">                            
                            <label class="col-sm-4 control-label"><span class="text-danger fs18"> *</span> ที่ดำเนินการได้ (%)</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control input-percent" id="actual_percent" name="actual_percent" value="<?php echo set_value("actual_percent") ?>">
                            </div>
                        </div>
                        <h5>เอกสารประกอบการรายงานแผน</h5>
                        <div class="form-group form-group-little-padding">
                            <label class="col-sm-4 control-label">เอกสาร pn-2</label>
                            <?php if (!empty($dr_plan_report["attach_file"])): ?> 
                                <label id="cur_attach" class="col-sm-6 control-label-normal-value" style="display:block">
                                    <a href="<?php echo site_url($dr_plan_report["attach_file"]); ?>" target="_blank"><span class="fa fa-file-excel-o"></span> เอกสาร </a>
                                    <a href="#" class="text-danger" onclick="delete_attach()">| <i class="fa fa-remove"></i> ลบ</a>
                                </label>
                            <?php endif; ?>

                            <div id="add_attach" class="col-sm-6" style="<?php echo empty($dr_plan_report["attach_file"]) ? "display:block" : "display:none" ?>">
                                <input type="file" name="attach_file"/>
                                <p class="help-block">รองรับเฉพาะไฟล์นามสกุล .xls และมีขนาดไฟล์ไม่เกิน 2 MB เท่านั้น</p>
                            </div>
                        </div>
                        <input type="hidden" id="planID" name="planID" value="<?php echo $this->utils->encID(set_value("planID")) ?>">
                        <input type="hidden" id="month" name="month" value="<?php echo set_value("month") ?>">
                    </form>                    
                </div>
            </div>

            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-primary" id="btn_save_report" onclick="pre_save_report()">บันทึก</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>                
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $(".input-percent").only_numeric("1");
        $(".input-percent").percent_pattern();
    });

    // เช็คกรณีแก้ไข แล้วเพิ่มเป็น 100% หรือลดลงจากเดิมที่เป็น 100%
    function pre_save_report()
    {
        var url = "<?php echo site_url("action_plan_report_admin/ajax_pre_save_report") ?>";
        var planID = $("#planID").val();
        var month = $("#month").val();
        var actual_percent = $("#actual_percent").val();
        $.post(url, {planID: planID, month: month, actual_percent: actual_percent}, function (data) {
            if (data) {
                if (data.status === "0") {
                    confirm_pre_save(data.msg);
                } else {
                    save_report();
                }
            }
        }, "json");
    }

    function confirm_pre_save(msg) {
        bootbox.dialog({
            title: "ยืนยันการแก้ไข",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i> " + msg + "</div>",
            buttons: {
                confirm: {
                    label: "ยืนยัน",
                    className: 'btn-success',
                    callback: function () {
                        save_report();
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-danger'
                }
            }
        });
    }

    function save_report()
    {
        var url = "<?php echo site_url("action_plan_report_admin/ajax_save_report") ?>";
        var data = new FormData($("#frm_save_plan_report")[0]);
        ajax_disable_btn("btn_save_report");
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            async: true,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false
        }).done(function (data) {
            if (data)
            {
                if (data.status === "0")
                {
                    ajax_enable_btn("btn_save_report");
                    show_msgbox(data.msg, data.status, "mdl_msgbox");
                    gotop_modal();
                } else
                {
                    window.location = data.next_url;
                }
            }
        });
    }

    function delete_attach()
    {
        bootbox.dialog({
            title: "ยืนยันต้องการลบข้อมูล",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i>  คุณต้องการลบไฟล์แนบนี้ใช่หรือไม่</div>",
            buttons: {
                confirm: {
                    label: "ลบข้อมูล",
                    className: 'btn-danger glyphicon glyphicon-trash',
                    callback: function () {
                        $("#add_attach").show();
                        $("#cur_attach").hide();                        
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-default '
                }
            }
        });
    }

    /*    function reset_form()
     {
     var mdl = $("#frm_add_plan");
     mdl.find("#plan_name").val('');
     mdl.find('input:checkbox').prop("checked", false);
     mdl.find("input[name='target_month[]']").val('');
     mdl.find("input[type=file]").val('');
     var cur_year = "<?php // echo $this->utils->year_buddha_convert(date("Y"));                       ?>";
     mdl.find("#year").val(cur_year);
     
     $(".input-percent").css("background-color", "");
     $(".input-percent").css("border-color", "");
     }
     */


</script>