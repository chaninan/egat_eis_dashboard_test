<!-- Modal View Report plan-->
<div class="modal fade" id="mdl_view_report_plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">รายละเอียดแผนปฏิบัติการ : <?php echo $dr_plan["plan_name"] ?></h4>
            </div>
            <div class="modal-body admin_detail_default">                                 
                <div class="container-fluid">
                    <table class="table table-hover table-bordered table-striped">
                        <thead>
                            <tr>
                                <th rowspan="2">เดือน</th>
                                <th colspan="13">รายงานแผน</th>
                            </tr>
                            <tr>
                                <?php
                                $cur_month = date('m');
                                foreach ($dt_plan_report as $row):
                                    ?>                                    
                                    <td class="text-center fs14 <?php echo ($row["month"] == $cur_month) ? 'success' : '' ?>"><?php echo $this->utils->get_months_abb($row["month"]) ?></td>
                                <?php endforeach; ?>                                
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td class="fs14">ความก้าวหน้างานสะสมตามเป้าหมาย (%)</td>
                                <?php foreach ($dt_plan_report as $row): ?>
                                    <td class="text-center fs14 <?php echo ($row["month"] == $cur_month) ? 'success' : '' ?>"><?php echo $row["target_percent"] ?></td>
                                <?php endforeach; ?>
                            </tr>
                            <tr>
                                <td class="fs14">ความก้าวหน้างานสะสมที่ดำเนินการได้ (%)</td>
                                <?php
                                $cur_year = date('Y') + 543;
                                $cnt_cancel = 0;
                                $is_cancel = $this->utils_plan->check_is_plan_cancel($dr_plan["planID"]);

                                foreach ($dt_plan_report as $row):
                                    if ($row["actual_percent"] == 100 || ($is_cancel == true && $row["actual_percent"] == 999))
                                    {
                                        $cnt_cancel++;
                                    }

                                    $can_action = false;
                                    if (($dr_plan["year"] == $cur_year) && ($row["month"] <= $cur_month))
                                    {
                                        $can_action = true;
                                        if ($cnt_cancel > 0 && $row["actual_percent"] != 100)
                                        {
                                            $can_action = false;
                                        }
                                    }
                                    ?>
                                    <td class="text-center <?php echo ($row["month"] == $cur_month) ? 'success' : '' ?>">
                                        <?php if (!empty($row["actual_percent"])): ?>
                                            <?php if ($row["actual_percent"] == "999" && $is_cancel== true): ?>
                                                <span class="fs12 text-muted">ถูกยกเลิก</span>
                                            <?php else: ?>
                                                <span><?php echo $row["actual_percent"]; ?></span>
                                            <?php
                                            endif;
                                        else:
                                            if ($can_action == false) :
                                                ?>
                                                <span>-</span>
                                            <?php else: ?>
                                                <span class="fs12 text-muted">ยังไม่ได้รายงาน</span>
                                            <?php
                                            endif;
                                        endif;
                                        ?>
                                    </td>
                                <?php endforeach; ?>   
                            </tr>
                        </tbody>
                    </table>
                    <div class="col-sm-3 pull-right alert-warning thumb" style="text-align:center; padding:5px 5px 2px 5px;border: 1px solid #8a6d3b">
                        <label class="control-label">สถานะ : </label>
                        <label class="control-label"><?php echo get_plan_status($dr_plan["planID"]) ?></label>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal View Report plan-->