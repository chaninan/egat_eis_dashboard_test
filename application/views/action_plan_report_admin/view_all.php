<?php $this->load->view("action_plan_admin/pnl_search") ?>

<?php if (!empty($dt_plan)) : ?>
    <div class="pull-left">
        รวมทั้งหมด <b><?php echo $dt_plan_cnt ?></b> รายการ
    </div>
    <table class="fs14 table table-hover table-bordered table-responsive table-condensed table-striped">
        <thead>
            <tr>
                <th>#</th>
                <th style="width:10%"><?php echo heading_sortable("ประจำปี", "year"); ?></th>
                <th style="width:20%"><?php echo heading_sortable("ชื่อแผน", "plan_name"); ?></th>
                <th style="width:20%">ผู้รับผิดชอบ</th>
                <th style="width:10%">ความสำคัญ</th>
                <th><?php echo heading_sortable("วันที่สร้างรายการ", "createDate"); ?></th>
                <th>รายงานเดือนล่าสุด</th>
                <th>สถานะโดยรวม</th>
                <th>แสดง</th>
                <th>รายงานแผน</th>
            </tr>
        </thead>
        <tbody> 
            <?php $cnt = 1; ?>
            <?php foreach ($dt_plan as $row) : ?>
                <tr>
                    <td class="text-center"><?php echo $cnt; ?></td>
                    <td class="text-center"><?php echo $row["year"]; ?></td>
                    <td><a href="#" onclick="show_mdl_view_plan('<?php echo $this->utils->encID($row["planID"]) ?>')"> <?php echo $row["plan_name"]; ?></a></td>
                    <td><?php echo str_replace(",", "<br/>" . "- ", "- " . $row["full_owner"]) ?></td>
                    <td><?php echo str_replace(",", "<br/>" . "- ", "- " . $row["full_priority"]) ?></td>
                    <td class="text-center"><?php echo $this->utils->datetime_to_thai($row["createDate"]) ?></td>
                    <td><?php
                        $lastest_plan = $this->utils_plan->get_lastest_plan_report($row["planID"]);
                        echo (count($lastest_plan) > 0) ? $this->utils->get_months($lastest_plan["month"]) : "-";
                        ?> 
                        <p class="help-block"><?php echo (count($lastest_plan) > 0) ? "เมื่อ " . $this->utils->datetime_to_thai($lastest_plan["createDate"]) : "" ?></p></td>
                    <td class="text-center fs16"><?php echo get_status_label($row["planID"]) ?></td>
                    <td class="text-center"><a href="#" onclick="show_mdl_view_report_plan('<?php echo $this->utils->encID($row["planID"]) ?>')"><i class="fa fa-eye text-success"></i></a></td>
                    <td class="text-center"><a href="<?php echo site_url("action_plan_report_admin/report/" . $this->utils->encID($row["planID"])) ?>" class="btn btn-xs btn-info"><i class="fa fa-calendar-check-o"></i> รายงานแผน</a></td>
                </tr>
                <?php $cnt++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="text-center">
        <?php echo $this->pagination->create_links(); ?>
    </div>
<?php else : ?>
    <?php echo display_no_record_msg(); ?>
<?php endif; ?>


<div id="div_plan_detail"></div>
<div id="div_plan_report_detail"></div>


<script>
    function show_search_body()
    {
        $("#wrap-panel-body").slideToggle("slow", "linear", function () {
            if ($("#link_show_search i").hasClass("fa fa-caret-up"))
            {
                $("#link_show_search").html("(แสดง <i class='fa fa-caret-down'></i>)");

            } else
            {
                $("#link_show_search").html("(ซ่อน <i class='fa fa-caret-up'></i>)");
            }

        });
    }

    function show_mdl_view_plan(planID) {
        var url = "<?php echo site_url("action_plan_admin/ajax_view_plan") ?>";
        $.post(url, {planID: planID}, function (data) {
            $("#div_plan_detail").empty().html(data);
            $("#mdl_view_plan").modal('show');
        }, "html");
    }

    function show_mdl_view_report_plan(planID)
    {
        var url = "<?php echo site_url("action_plan_report_admin/ajax_view_report_plan") ?>";
        $.post(url, {planID: planID}, function (data) {
            $("#div_plan_report_detail").empty().html(data);
            $("#mdl_view_report_plan").modal("show");
        }, "html");
    }



</script>
<style>
    .label-brown{
        background-color: #4D5F78;
    }
    .text-subscript{
        margin-bottom: 0px;
    }

</style>
