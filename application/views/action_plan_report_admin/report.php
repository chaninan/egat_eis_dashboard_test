<div class="admin_detail_default">
    <?php echo msgbox(); ?>
    <div class="alert alert-info">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label">ชื่อแผนปฏิบัติการ</label>
                <label class="col-sm-3 control-label-normal-value"><?php echo $dr_plan["plan_name"] ?></label>                
                <label class="col-sm-3 control-label">ประจำปี</label>
                <label class="col-sm-3 control-label-normal-value"><?php echo $dr_plan["year"] ?></label>
            </div>                           
            <div class="form-group">
                <label class="col-sm-3 control-label">ผู้รับผิดชอบ</label>
                <label class="col-sm-3 control-label-normal-value"><?php echo str_replace(",", "<br/>", $dr_plan["full_owner"]) ?></label>                
                <label class="col-sm-3 control-label">ความสำคัญ</label>
                <label class="col-sm-3 control-label-normal-value"><?php echo (!empty($dr_plan["full_priority"])) ? str_replace(",", "<br/>", $dr_plan["full_priority"]) : "-" ?></label>                                                      
            </div>
        </form>        
    </div>

    <div class="form-group" style="padding-bottom:50px">       
        <!--        <div class="pull-left">
                    <span class="text-danger fs14">หมายเหตุ กรุณารายงานแผนรายเดือนตามลำดับ</span> 
                </div>-->
        <div class="pull-right col-sm-2 alert-warning thumb" style="text-align:center; padding:5px 5px 2px 5px;border: 1px solid #8a6d3b">
            <label class="control-label">สถานะ : </label>
            <label class="control-label"><?php echo get_plan_status($dr_plan["planID"]) ?></label>
        </div>
    </div>
    <div class="form-group">
        <table class="fs14 table table-hover table-bordered table-striped table-responsive">
            <thead>
                <tr>
                    <th rowspan="2"  style="width:2%">#</th>
                    <th rowspan="2" style="width:15%">ประจำเดือน</th>
                    <th colspan="2" style="width:20%">ความก้าวหน้างานสะสม (%)</th>                
                    <th rowspan="2" style="width:10%">เอกสารแนบ</th>
                    <th rowspan="2" style="width:15%">วันที่รายงาน</th>
                    <th rowspan="2" style="width:15%">การดำเนินการ</th>
                </tr>
                <tr>
                    <th style="width:15%">ตามเป้าหมาย</th>
                    <th style="width:15%">ที่ดำเนินการได้</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                $cur_month = date('m');
                $cur_year = date('Y') + 543;
//                $cnt_pending = 0;
                $cnt_cancel = 0;
                $plan_encID = $this->utils->encID($dr_plan["planID"]);
                $is_cancel = $this->utils_plan->check_is_plan_cancel($dr_plan["planID"]);
                $is_complete = $this->utils_plan->check_is_plan_report_complete($dr_plan["planID"]);

                foreach ($dt_plan_report as $row) :
                    if ($row["actual_percent"] == 100 || ($is_cancel == true && $row["actual_percent"] == 999))
                    {
                        $cnt_cancel++;
                    }

                    $can_action = false;
                    if (($dr_plan["year"] == $cur_year) && ($row["month"] <= $cur_month))
                    {
                        $can_action = true;
                        if ($cnt_cancel > 0 && $row["actual_percent"] != 100)
                        {
                            $can_action = false;
                        }
                    }
                    ?>
                    <tr id="month_<?php echo $i; ?>" class="<?php echo ($row["month"] == $cur_month) ? 'success' : '' ?>">
                        <td class="text-center" ><?php echo $i; ?></td>
                        <td><?php echo $this->utils->get_months($row["month"]) ?></td>
                        <td class="text-center"><?php echo $row["target_percent"] ?></td>
                        <td class="text-center input-text">
                            <?php if (!empty($row["actual_percent"])): ?>
                                <?php if ($is_complete == true && $row["actual_percent"] == 100) : ?>
                                    <label class="label label-success fs14"><?php echo $row["actual_percent"] ?></label>
                                <?php elseif ($row["actual_percent"] == "999" && $is_cancel == true): ?>
                                    <label class="label label-default fs12">ถูกยกเลิก</label>
                                <?php else: ?>
                                    <label class="label-control"><?php echo $row["actual_percent"]; ?></label>
                                <?php
                                endif;
                            else:
                                if ($can_action == false) :
                                    ?>
                                    <span>-</span>
                                <?php else: ?>
                                    <label class='label label-default'>ยังไม่ได้รายงาน</label>
                                <?php
                                endif;
                            endif;
                            ?></td>                        
                        <td class="text-center">
                            <?php if (!empty($row["attach_file"])): ?>
                                <a href="<?php echo site_url($row["attach_file"]); ?>" target="_blank"><img src="<?php echo base_url("images/file_extension/xls.png") ?>"/></a>                                
                            <?php else: ?>
                                <span class="fs12"><?php echo ($can_action == false) ? "-" : "n/a" ?> </span>                               
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <?php if (!empty($row["createDate"])) : ?>
                                <span> <?php echo (!empty($row["updateDate"])) ? $this->utils->datetime_to_thai($row["updateDate"]) : $this->utils->datetime_to_thai($row["createDate"]); ?> </span>                              
                            <?php else: ?>
                                <span><?php echo ($can_action == false) ? "-" : "n/a" ?> </span>                               
                            <?php endif; ?>
                        </td>

                        <!--  button  -->
                        <td class="text-center">
                            <?php if (!empty($row["actual_percent"]) && $row["actual_percent"] != "999"): ?>
                                <button type="button" class="btn btn-xs btn-warning" onclick="show_mdl_add_report('แก้ไข', '<?php echo $row["month"]; ?>')"><i class="fa fa-pencil"></i> แก้ไข</button>
                                <button type="button" class="btn btn-xs btn-danger" onclick="confirm_delete_plan_report('<?php echo $this->utils->get_months($row["month"]) ?>', '<?php echo $row["month"]; ?>')"><i class="fa fa-trash"></i> ลบ</button>
                                <?php
                            else:
//                                $cnt_pending += 1;
                                if ($can_action == false) :
                                    ?>
                                    <span>-</span>
                                <?php else : ?>
                                    <button type="button" class="btn btn-xs btn-primary" 
                                            onclick="show_mdl_add_report('เพิ่ม', '<?php echo $row["month"]; ?>')" 
                                            <?php // echo ($cnt_pending > 1) ? "disabled" : ""       ?>>รายงาน
                                    </button>
                                <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php $i++ ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php if (isset($is_plan_cancel) && $is_plan_cancel == false): ?>
        <div class="form-group">
            <div class="alert alert-danger">
                <label class="col-sm-6 control-label control-label-normal-value">หากต้องการยกเลิกแผนปฏิบัติการนี้ กรุณาเลือกเดือนที่ต้องการให้มีผลการยกเลิก</label>
                <div class="col-sm-2">
                    <select class="form-control" id="cancel_month" name="cancel_month">
                        <?php
                        foreach ($dt_plan_report as $row):
                            if ($row["month"] > $cur_month):
                                continue;
                            else :
                                ?>
                                <option value="<?php echo $row["month"] ?>"><?php echo $this->utils->get_months($row["month"]) ?></option>
                            <?php
                            endif;
                        endforeach;
                        ?>
                    </select>
                </div>
                <button class="btn btn-danger" id="btn_cancel_plan" onclick="cancel_plan()">ตกลง</button>
            </div>
        </div>
    <?php endif; ?>

    <?php if (isset($show_confirm_complete) && $show_confirm_complete == TRUE) : ?>
        <div class="alert alert-info" style="text-align:center">
            <div class="form-group">
                <button type="button" class="btn btn-md btn-success" id="btn_confirm_complete" onclick="confirm_is_complete()">ยืนยันสิ้นสุดรายงานแผน คลิกที่นี่</button>
            </div>
        </div>
    <?php endif; ?>


</div>

<div id="div_report_detail"></div>

<script>

    // ทั้ง รายงานใหม่ และ แก้ไข
    function show_mdl_add_report(title, month)
    {
        var planID = "<?php echo $plan_encID; ?>";
        var url = "<?php echo site_url("action_plan_report_admin/ajax_add_report"); ?>";
        $.post(url, {planID: planID, month: month}, function (data) {
            $("#div_report_detail").empty().html(data);
            $("#title").html(title);
            $("#mdl_add_report").modal('show');
        }, "html");
    }

    // แผนถูกยกเลิก
    function cancel_plan()
    {
        bootbox.dialog({
            title: "ยืนยันยกเลิกแผนปฏิบัติการ",
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i> \n\
                        คุณยืนยันต้องการยกเลิกแผนปฏิบัติการให้มีผลเดือน <b>" + $("#cancel_month option:selected").text() + "</b> ใช่หรือไม่ ?</div>",
            buttons: {
                confirm: {
                    label: "ยืนยัน",
                    className: 'btn-success',
                    callback: function () {
                        var planID = "<?php echo $plan_encID ?> ";
                        var cancel_month = $("#cancel_month").val();
                        var url = "<?php echo site_url("action_plan_report_admin/ajax_cancel_plan"); ?>";
                        ajax_disable_btn("btn_cancel_plan");
                        $.post(url, {planID: planID, cancel_month: cancel_month}, function (data) {
                            if (data) {
                                if (data.status === "0") {
                                    ajax_enable_btn("btn_cancel_plan");
                                    show_msgbox(data.msg, data.status);
                                } else {
                                    window.location = data.next_url;
                                }
                            }
                        }, "json");
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-danger'
                }
            }
        });
    }

    function confirm_is_complete() {
        var planID = "<?php echo $plan_encID; ?>";
        bootbox.dialog({
            title: "ยืนยันสิ้นสุดการรายงานแผน",
            message: "<div class='alert alert-success'><i class='fa fa-exclamation-triangle fa-2x'></i> \n\
                        เมื่อยืนยันแล้วแผนปฏิบัติการนี้จะเปลี่ยนสถานะเป็น <b>แล้วเสร็จ</b> คุณต้องการยืนยันสิ้นสุดการรายงานแผนนี้ใช่หรือไม่ ?</div>",
            buttons: {
                confirm: {
                    label: "ยืนยัน",
                    className: 'btn-success',
                    callback: function () {
                        var url = "<?php echo site_url("action_plan_report_admin/ajax_mark_is_complete") ?>";
                        ajax_disable_btn("btn_confirm_complete");
                        $.post(url, {planID: planID}, function (data) {
                            if (data) {
                                if (data.status === "0") {
                                    ajax_enable_btn("btn_confirm_complete");
                                    show_msgbox(data.msg, data.status);
                                } else {
                                    window.location = data.next_url;
                                }
                            }
                        }, "json");
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-danger'
                }
            }
        });
    }

    function confirm_delete_plan_report(month_name, month)
    {
        var planID = "<?php echo $plan_encID ?>";
        bootbox.dialog({
            title: "ยืนยันลบการรายงานแผน ประจำเดือน " + month_name,
            message: "<div class='alert alert-danger'><i class='fa fa-exclamation-triangle fa-2x'></i> \n\
                        คุณต้องการลบการรายงานแผนประจำเดือน <b>" + month_name + "</b> ใช่หรือไม่ ?</div>",
            buttons: {
                confirm: {
                    label: "ยืนยัน",
                    className: 'btn-success',
                    callback: function () {
                        var url = "<?php echo site_url("action_plan_report_admin/ajax_delete_plan_report") ?>";
                        $.post(url, {planID: planID, month: month}, function (data) {
                            if (data) {
                                if (data.status === "0") {
                                    show_msgbox(data.msg, data.status);
                                } else {
                                    window.location = data.next_url;
                                }
                            }
                        }, "json");
                    }
                },
                cancel: {
                    label: "ยกเลิก",
                    className: 'btn-danger'
                }
            }
        });
    }


</script>

