<div class="page-header">
    <h1>การสร้างรายงาน Highlight CI/FC Item (Step 1/2)</h1>
</div>
<form>
    <div class="row">   
        <div class="col-sm-12">
            <h4 class="text-primary">รายละเอียดหน่วยงาน</h4>
        </div>    
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label for="exampleInputEmail1">หน่วยงาน</label>
                <select class="form-control">
                    <option>กองบริการทั่วไป</option>
                </select>
            </div>
        </div>        
    </div>
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">หน่วยงาน</th>
                        <th class="text-center">ไฟล์ RP71 (SAP)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center"><input type="checkbox" checked="" /></td>
                        <td>ส่วนกลางบริการทั่วไป</td>
                        <td><input type="file"/></td>
                    </tr>
                    <tr>
                        <td class="text-center"><input type="checkbox" checked=""/></td>
                        <td>แผนกบริการห้องประชุม</td>
                        <td><input type="file"/></td>
                    </tr>
                    <tr>
                        <td class="text-center"><input type="checkbox" /></td>
                        <td>แผนกบริการกลาง</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-center"><input type="checkbox" /></td>
                        <td>แผนกบำรุงรักษาบริเวณ</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-center"><input type="checkbox" /></td>
                        <td>แผนกรับส่งเอกสาร</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">   
        <div class="col-sm-12">
            <h4 class="text-primary">รายละเอียดรายงาน</h4>
        </div>    
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label for="exampleInputEmail1">ประเภท</label>
                <select class="form-control">
                    <option>รายงานประจำเดือน</option>
                    <option>รายงานประจำไตรมาส (EVM)</option>
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="exampleInputEmail1">เดือนที่</label>
                <select class="form-control">
                    <option>มีนาคม</option>
                </select>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <label for="exampleInputEmail1">ปี</label>
                <select class="form-control">
                    <option>2559</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label for="exampleInputEmail1">เพื่อโปรดดำเนินการภายในวันที่</label>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
                    </span>
                </div><!-- /input-group -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <a class="btn btn-success" href="<?php echo site_url("mockup/create_ci2"); ?>">ต่อไป <span class="glyphicon glyphicon-arrow-right"></span></a>
        </div>
    </div>
</form>

