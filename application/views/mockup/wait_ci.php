<div class="page-header">
    <h1>รายการที่รอการชี้แจง CI/FC</h1>
</div>

<div>    
    <div>
        <h3>กองบริการทั่วไป</h3>
    </div>    
    <div>
        <h4>จำนวนรายการ <b>2</b> รายการ </h4>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th class="text-center" rowspan="2">#</th>
                <th class="text-center" rowspan="2">ประเภทรายงาน</th>
                <th class="text-center" rowspan="2">เดือน/ไตรมาส</th>
                <th class="text-center" rowspan="2">สถานะ</th>
                <th class="text-center" rowspan="2">หน่วยงาน</th>
                <th class="text-center" rowspan="2">โปรดดำเนินการภายในวันที่</th>
                <th class="text-center" rowspan="2">วันที่สร้างรายการ</th>
                <th class="text-center" colspan="2">จำนวนรายการ</th>
                <th class="text-center" rowspan="2">รายงาน</th>
            </tr>
            <tr>
                <th class="text-center">
                    เสร็จสิ้น
                </th>
                <th class="text-center">
                    ทั้งหมด
                </th>
            </tr>            
        </thead>
        <tbody>
            <tr>
                <td class="text-center">1</td>
                <td class="text-center">รายงานประจำเดือน</td>
                <td class="text-center">มีนาคม</td>
                <td class="text-center">รอชี้แจง</td>
                <td class="text-center">ส่วนกลางกอง</td>
                <td class="text-center">15/03/2559</td>
                <td class="text-center">05/03/2559</td>
                <td class="text-center text-success">2</td>
                <td class="text-center">5</td>
                <td class="text-center"> <button class="btn btn-primary btn-sm">รายงาน</button></td>               
            </tr>
            <tr>
                <td class="text-center">2</td>
                <td class="text-center">รายงานประจำไตรมาส</td>
                <td class="text-center">1</td>
                <td class="text-center">รอชี้แจง</td>
                <td class="text-center">ส่วนกลางกอง</td>
                <td class="text-center">15/03/2559</td>
                <td class="text-center">05/03/2559</td>
                <td class="text-center  text-success">3</td>
                <td class="text-center">5</td>
                <td class="text-center"> <button class="btn btn-primary btn-sm">รายงาน</button></td>               
            </tr>
            
        </tbody>
    </table>
    
    <div class="text-center">
        <a class="btn btn-default" href="<?php echo site_url("mockup/create_ci2"); ?>">ดูประวัติทั้งหมด <span class="glyphicon glyphicon-list"></span></a>
    </div>

</div>