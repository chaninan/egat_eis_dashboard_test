<div class="page-header">
    <h1>การสร้างรายงาน Highlight CI/FC Item (Step 2/2)</h1>
</div>

<div>
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#">ส่วนกลางบริการทั่วไป</a></li>
        <li role="presentation"><a href="#"> <span class="text-success glyphicon glyphicon-ok-sign"></span> แผนกบริการห้องประชุม</a></li>
        <li role="presentation"><a href="#"> <span class="text-success glyphicon glyphicon-ok-sign"></span> แผนกบริการกลาง</a></li>
        <li role="presentation"><a href="#"> <span class="text-success glyphicon glyphicon-ok-sign"></span> แผนกบำรุงรักษาบริเวณ</a></li>
        <li role="presentation"><a href="#">แผนกรับส่งเอกสาร</a></li>
    </ul>
    <div>
        <h3>กองบริการทั่วไป (ส่วนกลางบริการทั่วไป)</h3>
        <h4>รายงานประจำเดือน มีนาคม 2559 <small> (โปรดดำเนินการภายในวันที่ : 25 มีนาคม 2559)</small></h4>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <span class="text-warning">แก้ไขไฟล์ RP71 : </span>
        </div>
        <div class="col-sm-7">
            <input type="file"/>            
        </div>
    </div>
    <hr/>
    <div>
        <h4>จำนวนรายการ CI/FC ที่เลือก <span class="text-success">4</span> รายการ </h4>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">เลือก</th>
                <th class="text-center">CI / FC</th>
                <th class="text-center">งบประมาณทั้งปี</th>
                <th class="text-center">งบประมาณเดือน 3</th>
                <th class="text-center">เบิกจ่ายเดือน 3</th>
                <th class="text-center">งบคงเหลือเดือน 3</th>
                <th class="text-center">%เบิกจ่ายเดือน 3</th>
                <th class="text-center">งบประมาณสะสม 1-3</th>
                <th class="text-center">เบิกจ่ายสะสม 1-3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td><button class="btn btn-primary btn-sm">เลือก</button></td>
                <td>*  B512000  ค่าใช้จ่ายเดินทาง</td>
                <td>318,700</td>
                <td>31,000</td>
                <td>0</td>
                <td>31,000</td>
                <td>0</td>
                <td>741,000</td>
                <td>45,000</td>
            </tr>
            <tr class="tr-selected">
                <td>2</td>
                <td><button class="btn btn-warning btn-sm">ยกเลิก</button></td>
                <td>*  B560200  ค่าใช้จ่ายเบ็ดเตล็</td>
                <td>318,700</td>
                <td>31,000</td>
                <td>0</td>
                <td>31,000</td>
                <td>0</td>
                <td>741,000</td>
                <td>45,000</td>
            </tr>
            <tr>
                <td>3</td>
                <td><button class="btn btn-primary btn-sm">เลือก</button></td>
                <td>*  5120003  ค่าพาหนะ</td>
                <td>318,700</td>
                <td>31,000</td>
                <td>0</td>
                <td>31,000</td>
                <td>0</td>
                <td>741,000</td>
                <td>45,000</td>
            </tr>
            <tr>
                <td>4</td>
                <td><button class="btn btn-primary btn-sm">เลือก</button></td>
                <td>*  5210005  ค่าวัสดุสำนักงานฯ</td>
                <td>318,700</td>
                <td>31,000</td>
                <td>0</td>
                <td>31,000</td>
                <td>0</td>
                <td>741,000</td>
                <td>45,000</td>
            </tr>
        </tbody>
    </table>
    
    <div class="text-center">
        <a class="btn btn-primary" href="<?php echo site_url("mockup/create_ci2"); ?>">บันทึก  <span class="glyphicon glyphicon-floppy-disk"></span></a>
    </div>
    
    <div class="alert alert-success" style="margin-top: 20px;">
        <p>ดำเนินการเลือกรายการครบทุกหน่วยงานแล้ว ต้องการส่งข้อมูลไปให้ผู้รับผิดขอบหรือไม่ ?</p>
        <button class="btn btn-success">ส่งรายการให้ผู้รับผิดชอบ <span class="glyphicon glyphicon-send"></span></button>
    </div>
</div>

<style>
    tr.tr-selected td
    {
        background-color: #f5e79e;
    }
</style>