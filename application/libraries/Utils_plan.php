<?php

class Utils_plan
{

    function get_owner_name($ownerID)
    {
        $ci = & get_instance();

        $ci->load->model("mplan");
        $dr_owner = $ci->mplan->get_owner($ownerID);
        return $dr_owner["owner_name"];
    }

    function get_priority_name($priorityID)
    {
        $ci = & get_instance();

        $ci->load->model("mplan");
        $dr_priority = $ci->mplan->get_priority($priorityID);
        return $dr_priority["priority_name"];
    }

    function get_status_plan_report($planID)
    {
        $ci = & get_instance();

        $ci->load->model("mplan");
        $dr_plan = $ci->mplan->get_plan_by_id($planID);
        if ($dr_plan["is_complete"] == "0")
        {
            $dr_lastest = $this->get_lastest_plan_report($planID);
            if (empty($dr_lastest))
            {
                $res["status"] = "0";
                $res["text"] = "ยังไม่ได้รายงาน";
            }
            else
            {
                if ($dr_lastest["actual_percent"] < $dr_lastest["target_percent"])
                {
                    $res["status"] = "1";
                    $res["text"] = "ช้ากว่าแผน";
                }
                elseif ($dr_lastest["actual_percent"] == $dr_lastest["target_percent"])
                {
                    $res["status"] = "2";
                    $res["text"] = "ตามแผน";
                }
                elseif ($dr_lastest["actual_percent"] > $dr_lastest["target_percent"])
                {
                    $res["status"] = "3";
                    $res["text"] = "เร็วกว่าแผน";
                }
            }
        }
        elseif ($dr_plan["is_complete"] == "1")
        {
            $res["status"] = "4";
            $res["text"] = "แล้วเสร็จ";
        }
        elseif ($dr_plan["is_complete"] == "2")
        {
            $res["status"] = "5";
            $res["text"] = "ถูกยกเลิก";
        }
        return $res;
    }

    function get_lastest_plan_report($planID)
    {
        $ci = & get_instance();
        $ci->load->model("mplan_report");
        $dr_lastest = $ci->mplan_report->get_lastest_plan_report($planID);
        return $dr_lastest;
    }

    function check_is_plan_report_complete($planID)
    {
        // is_complete ต้องเท่ากับ 1
        $res = FALSE;
        $ci = & get_instance();
        $ci->load->model("mplan");
        $dr_plan = $ci->mplan->get_plan_by_id($planID);
        if ($dr_plan["is_complete"] == "1")
        {
            $res = TRUE;
        }

        return $res;
    }

    function check_is_plan_cancel($planID)
    {
        // is_complete ต้องเท่ากับ2
        $res = FALSE;
        $ci = & get_instance();
        $ci->load->model("mplan");
        $dr_plan = $ci->mplan->get_plan_by_id($planID);
        if ($dr_plan["is_complete"] == "2")
        {
            $res = TRUE;
        }

        return $res;
    }

    function check_month_is_last_report($planID, $month)
    {
        $res = FALSE;
        $ci = & get_instance();
        $ci->load->model("mplan_report");
        $dr_last_report = $this->get_lastest_plan_report($planID);
        $last_month = $dr_last_report["month"];
        if ($last_month == $month)
        {
            $res = TRUE;
        }
        return $res;
    }

    function get_data_lastest_report($planID)
    {
        $ci = & get_instance();
        $ci->load->model("mplan_report");
        $dr_plan_report = "";
        $dr_last = $this->get_lastest_plan_report($planID);
        if (!empty($dr_last))
        {
            if ($dr_last["actual_percent"] == "999")
            {
                $last_month = $dr_last["month"] - 1;
            }
            else
            {
                $last_month = $dr_last["month"];
            }
            $dr_plan_report = $ci->mplan_report->get_one_plan_report(array("planID" => $planID, "month" => $last_month));
        }
        return $dr_plan_report;
    }

    function get_data_infomation($planID)
    {
        $ci = & get_instance();
        $dr_plan_report = $this->get_data_lastest_report($planID);
        return (!empty($dr_plan_report)) ? $ci->utils->get_months_abb($dr_plan_report["month"]) : null;
    }

    function get_actual_percent($planID)
    {
        $ci = & get_instance();
        $dr_plan_report = $this->get_data_lastest_report($planID);
        return (!empty($dr_plan_report)) ? $dr_plan_report["actual_percent"] : null;
    }

}
