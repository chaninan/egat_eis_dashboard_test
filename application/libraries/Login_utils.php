<?php

/**
 * หากต้องการปลดล๊อครหัสผ่านผิดแบบทันทีให้ไป set User_egat.try_login = 1 
 */
class Login_utils
{
    private $msg_over_login_quota = "คุณได้กรอกรหัสผ่านผิดพลาดเกินระบบกำหนด สามารถเข้าสู่ระบบอีกครั้งหลังเวลา ";
    private $msg_pwd_user_incorrect = "รหัสพนักงาน หรือรหัสผ่าน ไม่ถูกต้อง !";
    private $msg_user_not_exist = "ไม่มีรหัสพนักงานนี้ในระบบฯ หรือรหัสพนักงานนี้ไม่มีสิทธิ์เข้าระบบฯ !";
    private $_CI;
    private $session_user_egat_name = "sess_user_dashboard";
//    private $wait_time = (30*60);
    private $wait_time = 1800;
    private $login_max_try = 3;

    function __construct()
    {
        $this->_CI = & get_instance();
        $this->_CI->load->model("muser_login", "muser");
    }

    /* =========================
     *        Login
     * ======================= */
    /**
     * ตรวจสอบในการเข้าใช้งานของ User มีการตรวจสอบเมื่อมีการ Login ซ้อน <br/>
     * กรณีที่ต้องการ Login ซ้อน ให้กำหนด Override = true
     * @param String $pEmpID รหัสพนักงาน
     * @param String $pPassword Password (ไม่ต้อง encode)
     * @param Boolean $pOverride ให้ทำการ Override กรณีที่เป็น login ซ้อนหรือไม่ ?
     * @return type
     */
    function can_login($pEmpID, $pPassword, $pOverride = FALSE)
    {
        $res = array("status" => FALSE, "msg" => "", "is_second_login" => FALSE);
        /**
         * 1.ดึงข้อมูลจาก user_egat
         * 2.ตรวจสอบว่า login เกิน 3 ครั้ง ?
         * 3.ตรวจสอบ user pwd ถูกหรือไม่ ?
         * 4.ตรวจสอบว่า login ซ้อน หรือไม่ ?
         * 5.เก็บค่าลงใน sess_user_egat + ค่า generate key + is_admin
         */
        //1.
        $dr_emp = $this->_CI->muser->get_by_empID($pEmpID);
        if(!empty($dr_emp))
        {//have employee data
            //2.
            $res_try_login = $this->_check_try_login($dr_emp);
            if($res_try_login["status"])
            {//valid check try login
                //3.
                $res_check_authen = $this->_check_authen($pEmpID, $pPassword);
                if($res_check_authen["status"])
                {//password correct
                    //4.
                    $res_is_multiple_login = $this->_check_multiple_login($pEmpID, $pOverride);
                    if($res_is_multiple_login["status"])
                    {//sucess Login 
                        //5.
                        $this->_CI->session->unset_userdata($this->session_user_egat_name); //unset first
                        //add generate_key
                        $dr_emp["generate_key"] = $res_is_multiple_login["generate_key"];
                        
                        $this->_CI->session->set_userdata($this->session_user_egat_name, $dr_emp);
                        $res["status"] = TRUE;
                    }
                    else
                    {//It is a second login
                        $res["msg"] = $res_is_multiple_login["msg"];
                        $res["is_second_login"] = TRUE;
                    }
                }
                else
                {//password Incorrect 
                    $res["msg"] = $res_check_authen["msg"];
                }
            }
            else
            {//invalid check try login
                $res["msg"] = $this->msg_over_login_quota . $res_try_login["next_time"];
            }
        }
        else
        {//not have empID
            $res["msg"] = $this->msg_user_not_exist;
            //Insert log login
            $this->_CI->muser->insert_log_login($pEmpID, "fail");
        }

        return $res;
    }

    //ตรวจสอบจำนวนครั้งที่พยายาม Login ถูกต้อง ?
    private function _check_try_login(&$pDr_emp)
    {
        $res = array("status" => FALSE, "next_time" => "");
        /*
         * 1. ตรวจสอบว่า user_egat.try_login >= $config["max_try_login"]
         * --> Yes = 2. user_egat.try_login_date > $config["wait_time_try_login"] หรือไม่ 
         * ------->Yes = True และทำการ Update try_login = 1
         * ------->No = False
         * --> No = True
         */

        $_max_try_login = $this->login_max_try;

        //1.
        if(intval($pDr_emp["try_login"]) > $_max_try_login)
        {
            $last_try = strtotime($pDr_emp["try_login_date"]);
            $cal_last_sec = (time() - $last_try);

            $wait_time = $this->wait_time;
            if($cal_last_sec > $wait_time)
            {
                $res["status"] = TRUE;
                //Update try_login = 1
                $empID = $pDr_emp["empn"];
                $this->_CI->muser->reset_try_login($empID);
            }
            else
            {//too early
                $res["status"] = FALSE;
                $next_time = ($last_try + $wait_time);
                $res["next_time"] = date("d/m/Y H:i:s", $next_time);
            }
        }
        else
        {
            $res["status"] = TRUE;
        }

        return $res;
    }

    //ตรวจสอบ User/password ถูกต้อง ? พร้อม log
    private function _check_authen($pEmpID, $pPassword)
    {
        $res = array("status" => FALSE, "msg" => "");

        //check with  Webservice
        $client = new SoapClient("http://webservices.egat.co.th/authentication/au_provi.php?wsdl");
        $res_login = $client->validate_user($pEmpID, $pPassword);

        if($res_login)
        {//login pass
            $res["status"] = TRUE;
            //Insert log login
            $this->_CI->muser->insert_log_login($pEmpID, "success");
        }
        else
        {//login fail
            //Insert log login
            $this->_CI->muser->insert_log_login($pEmpID, "fail");
            $res["status"] = FALSE;
            $res["msg"] = $this->msg_pwd_user_incorrect;
        }
        return $res;
    }

    //ตรวจสอบ ว่าเป็น Login ซ้อนหรือไม่ ? 
    private function _check_multiple_login($pEmpID, $pOverride = FALSE)
    {
        $res = array("status" => FALSE, "msg" => "", "generate_key" => "");
        /* 1.ตรวจสอบว่าใน #user_egat_session มี empID นี้หรือไม่ ?
         * a----> True = เช็คต่อว่า (now() - last_update) > ค่า session_timeout ? หรือ กำหนดให้ Login ซ้อนได้
         * b------------> True = OK ! แปลว่า ก่อนหน้าไม่ได้ log off และไม่ใช่ login ซ้อน Update #user_egat_session
         * c------------> False = แปลว่าเป็น Login ซ้อน 
         * d----> False = OK ! ไม่ใช่ login ซ้อน
         * 2.กรณีที่ไม่ใช่ Login ซ้อน ให้ทำการ Insert #user_egat_session
         */

        //prepare for insert_session
        $tbl_user_egat_session["empn"] = $pEmpID;
        $tbl_user_egat_session["generate_key"] = $res["generate_key"] = sha1(time() . $pEmpID);
        $tbl_user_egat_session["last_update"] = date("Y-m-d H:i:s");
        $tbl_user_egat_session["ip_address"] = $this->_CI->input->ip_address();

        //1.
        $dr_user_session = $this->_CI->muser->get_user_session_by_empID($pEmpID);

        if(!empty($dr_user_session))
        {//a
            $last_update = strtotime($dr_user_session["last_update"]);
            $session_exp = $this->_CI->config->item("sess_expiration");
            if(((time() - $last_update) > $session_exp) || ($pOverride == TRUE))
            {//b
                $res["status"] = TRUE;
                //2. ----> update @user_egat_session
                $res_update_session = $this->_CI->muser->update_user_session($tbl_user_egat_session);
                if(!$res_update_session["status"])
                {//can not insert to table
                    $res["status"] = FALSE;
                    $res["msg"] = $res_update_session["msg"];
                }
            }
            else
            {//c
                $res["status"] = FALSE;
                $res["msg"] = "ระบบตรวจสอบพบว่ามีการเข้าใช้งานก่อนหน้าจากรหัสพนักงานนี้ จาก IP Address : " . $dr_user_session["ip_address"] . " เมื่อเวลา : " . date("d/m/Y H:i", strtotime($dr_user_session["last_update"]));
            }
        }
        else
        {//d
            $res["status"] = TRUE;

            //2. --->insert new record table @user_egat_session
            $res_insert_session = $this->_CI->muser->insert_user_session($tbl_user_egat_session);
            if(!$res_insert_session["status"])
            {//can not insert to table
                $res["status"] = FALSE;
                $res["msg"] = $res_insert_session["msg"];
            }
        }
        return $res;
    }

    //END Login

    /* =========================
     *        Log out
     * ======================= */
    function logout()
    {
        /**
         * 1. delete from table #user_egat_session
         * 2. clear Session, Post Data
         */
        //1.
        $empID = $this->get_data("empn");
        if(!empty($empID))
        {
            $this->_CI->muser->delete_user_session($empID);
        }
        //2.
        $this->_CI->session->unset_userdata($this->session_user_egat_name);
        UNSET($_POST);
        UNSET($_SESSION);
    }

    //END Log out

    /* =========================
     *       Check Login
     * ======================= */
    /**
     * ตรวจสอบว่าได้ทำการ Login แล้วหรือยัง ? 
     * 1. ดูว่ามี Session "session_user_egat_name" หรือไม่ ? 
     * 2. ต้อง check ว่า user_egat_session.generate_key เหมือนที่เก็บไว้ใน Session ? (พร้อม Update last_update ถ้ามี)
     * @param Boolean $pRedirect ให้ทำการ redirect ไปยังหน้า Login หรือไม่ ? 
     */
    function have_login($pRedirect = FALSE, $pUrl_redirect = "")
    {
        $res = FALSE;
        //1.
        if($this->_CI->session->userdata($this->session_user_egat_name))
        {//have session login
            //2.
            $sess_emp = $this->_CI->session->userdata($this->session_user_egat_name);
            $empID = $sess_emp["empn"];
            $generate_key_ses = $sess_emp["generate_key"];
            $dr_user_session = $this->_CI->muser->get_generate_key_update_time_visit($empID);
            if(!empty($dr_user_session))
            {//have data #user_egat_session
                $generate_key_db = $dr_user_session["generate_key"];
                //compare key in session and database
                if($generate_key_ses == $generate_key_db)
                {//final answer
                    $res = TRUE;
                }
                else
                {//ถ้าไม่เหมือนให้clear ค่า session ทันที (จะไม่ได้ต้อง check กับ database อีก)
                    $this->_CI->session->unset_userdata($this->session_user_egat_name);
                }
            }
        }
        
        //check ว่าถ้าไม่ได้ login แล้วบังคับให้ไปหน้า login ? 
        if(($res == FALSE) && ($pRedirect == TRUE))
        {//force redirect to login page
            //check have assign redirect URL ? 
            $url = "authen_member/login"; 
            if($pUrl_redirect != "")
            {
                $url = $pUrl_redirect;
            }
            redirect($url . "?backurl=" . urlencode($this->_CI->utils->full_url()));
        }

        return $res;
    }

    //END Check Login
    /**
     * get ค่าจาก session โดยส่ง Key มา
     * @param String $pKey column ไหน
     * @return boolean 
     */
    function get_data($pKey = "")
    {
        if($this->have_login())
        {
            $dr_data = $this->_CI->session->userdata($this->session_user_egat_name);
            if($pKey != "")
            {
                if(array_key_exists($pKey, $dr_data))
                {
                    return $dr_data[$pKey];
                }
            }
            else
            {
                return $dr_data;
            }
        }

        return FALSE;
    }

    function get_member_basic_data()
    {
        $member_data["employee_data"] = $this->get_data();

        //ตรวจสอบว่าเป็น Admin ? 
        /* $is_admin = $this->get_data("is_admin");
          $member_data["show_menu_backoffice"] = $is_admin; */

        return $member_data;
    }

}
