<?php

class Utils {

    public function get_basic_data()
    {
        $data_page[CON_TITLE] = "";
        $data_page[CON_SUB_TITLE] = "";
        $data_page[CON_META_AUTHOR] = "GSD IT";
        $data_page[CON_META_DESCRIPTION] = "ระบบ Dashboard ฝ่ายบริการ ";
        $data_page[CON_META_KEYWORDS] = "ระบบ Dashboard ฝ่ายบริการ ";
        $data_page[CON_TEMPLATE_CONTAINER] = "container";
        $data_page[CON_MENU_USER_SELECTED] = "";

        return $data_page;
    }

    public function encID($pID)
    {
        //return urlencode(base64_encode($pID));
        return rtrim(strtr(base64_encode($pID), '+/', '-_'), '=');
    }

    public function decID($pID)
    {
        //return base64_decode(urldecode($pID));        
        return base64_decode(str_pad(strtr($pID, '-_', '+/'), strlen($pID) % 4, '=', STR_PAD_RIGHT));
    }

    public function is_postback()
    {
        return (strtoupper($_SERVER["REQUEST_METHOD"]) == "POST" );
    }
    
    function datetime_to_thai($datetime = "")
    {
        $explode = explode(" ", $datetime);
        $date = $this->date_to_thai($explode[0]);
        $time = date("H:i", trim(strtotime($explode[1])));
        return $date . " " . $time . " น.";
    }
    
    ///convert date from DB to view "Y-m-d => DD/MM/YYYY (พ.ศ.)
    function date_to_thai($date = "")
    {
        $new_format = "";
        if ($date != "")
        {
            // year over 2037 can't convert with strtotime , use date_create instead
            if (!empty($this->dmy_convert_strtotime($date)))
            {
                $new_format = $this->getThaiYear(date("d/m/Y", $this->dmy_convert_strtotime($date)));
            }
            else
            {
                $new_format = $this->getThaiYear(date_format(date_create($date), "d/m/Y"));
            }
        }
        return $new_format;
    }
    
    /* dmy_convert_strtotime()
     * covert from d/m/y -> time()
     */
    public function dmy_convert_strtotime($pDate)
    {
        return strtotime(str_replace('/', '-', $pDate));
    }
    
    /*
     * Convert d/m/ค.ศ => d/m/พ.ศ.
     */
    public function getThaiYear($pDate)
    {
        if ($pDate !== "")
        {
            $arr = explode("/", $pDate);
            if (count($arr) == 3)
            {
                //get year +543
                $year = intval($arr[2]) + 543;
                return $arr[0] . "/" . $arr[1] . "/" . $year;
            }
        }
    }

    function get_months($month = 0)
    {
        $arr_month = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");

        if ($month != 0)
        {
            return $arr_month[$month];
        }
        else
        {
            //remove dummy index 0
            unset($arr_month[0]);
            return $arr_month;
        }
    }

    function get_months_abb($month = 0)
    {
        $arr_month = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        if ($month != 0)
        {
            return $arr_month[$month];
        }
        else
        {
            //remove dummy index 0
            unset($arr_month[0]);
            return $arr_month;
        }
    }

    function get_year_valid_range($back = 5, $next = 3)
    {
        $this_year = date("Y");

        for ($i = ($this_year - $back); $i <= ($this_year + $next); $i++)
        {
            $arr_year[] = $i;
        }

        return $arr_year;
    }

    /**
     * Convert จาก ค.ศ ---> พ.ศ. 
     * @param type $pAD
     */
    function year_buddha_convert($pAD = "")
    {
        if (!empty($pAD))
        {
            return ($pAD + 543);
        }
        else
        {
            return "-";
        }
    }

    /**
     * Convert จาก พ.ศ.---> ค.ศ  
     * @param type $pBE
     */
    function year_christ_convert($pBE = "")
    {
        if (!empty($pBE))
        {
            return ($pBE - 543);
        }
        else
        {
            return "-";
        }
    }

    function full_url()
    {
        $CI = & get_instance();

        $url = $CI->config->site_url($CI->uri->uri_string());
        return $_SERVER['QUERY_STRING'] ? $url . '?' . $_SERVER['QUERY_STRING'] : $url;
    }

    /* =========================
     *        Message box
     * ======================= */

    /**
     * เรียกใช้ Messagebox ที่นี่ แต่การ render ให้ไปดูที่ Helper/Msgbox_helper
     */
    private $msg_box_value = array("is_show" => FALSE, "msg" => "", "class" => "", "target_id" => "");

    function msgbox($pMSg, $pClass, $pMsgboxID = "")
    {
        $this->msgbox_set_data($pMSg, $pClass, $pMsgboxID);
    }

    function msgbox_flash($pMSg, $pClass, $pMsgboxID = "")
    {
        $this->msgbox_set_data($pMSg, $pClass, $pMsgboxID);
        //เก็บค่าลง flash_session
        $ci = & get_instance();
        $ci->session->set_flashdata("msgbox_data", $this->msg_box_value);
    }

    //Set Msgbox data กรณีที่มีการ set ค่ามา, ทุกกรณีไม่ว่าจะเป็น msgbox_flash หรือไม่ ?
    private function msgbox_set_data($pMSg, $pClass, $pMsgboxID = "")
    {
        $msg_val = & $this->msg_box_value;
        if ($pMSg != "")
        {//check have message ? 
            $msg_val["is_show"] = TRUE;
            $msg_val["msg"] = $pMSg;
            $msg_val["class"] = $pClass;
            //check specific target msgbox_id 
            if ($pMsgboxID != "")
            {
                $msg_val["target_id"] = $pMsgboxID;
            }
        }
    }

    function get_msgbox_data($pValue = "")
    {
        //*** ต้อง check ที่ flash data ก่อนเสมอ
        $ci = & get_instance();
        if ($ci->session->flashdata("msgbox_data"))
        {//usedata form session_data
            $this->msg_box_value = $ci->session->flashdata("msgbox_data");
        }

        if ($pValue != "" && array_key_exists($pValue, $this->msg_box_value))
        {//return 1 value
            return $this->msg_box_value[$pValue];
        }
        else
        {//return all values
            return $this->msg_box_value;
        }
    }

    // -- End Message box

    /* =========================
     *        Pagination
     * ======================= */

    function config_pagination()
    {
        $config_pagination["full_tag_open"] = "<ul class='pagination'>";
        $config_pagination["full_tag_close"] = "</ul>";
        $config_pagination["num_tag_open"] = "<li>";
        $config_pagination["num_tag_close"] = "</li>";
        $config_pagination["prev_tag_open"] = "<li>";
        $config_pagination["prev_tag_close"] = "</li>";
        $config_pagination["next_tag_open"] = "<li>";
        $config_pagination["next_tag_close"] = "</li>";
        $config_pagination["first_tag_open"] = "<li>";
        $config_pagination["first_tag_close"] = "</li>";
        $config_pagination["last_tag_open"] = "<li>";
        $config_pagination["last_tag_close"] = "</li>";
        $config_pagination["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config_pagination["cur_tag_close"] = "</a></li>";

        $config_pagination["reuse_query_string"] = TRUE;

        return $config_pagination;
    }

    function get_sortBy($pDefualt)
    {
        $res = "";
        $ci = & get_instance();
        if ($ci->input->get("sortBy"))
        {
            $res = $ci->input->get("sortBy");
        }
        else
        {
            $res = $pDefualt;
        }

        return $res;
    }

    function get_sortDi($pDefualt)
    {
        $res = "";
        $ci = & get_instance();
        if ($ci->input->get("sortDi"))
        {
            $res = $ci->input->get("sortDi");
        }
        else
        {
            $res = $pDefualt;
        }

        return $res;
    }

    // -- End Pagination

    function get_array_range_year($start_year, $end_year)
    {
        $year_range = array();
        for ($i = $start_year; $i <= $end_year; $i++)
        {
            $year_range[] = $i;
        }
        return $year_range;
    }

    /* =========================
     *        Upload File
     * ======================= */

    // ใช้ upload file กรอก 
    // $input_name =ชื่อ input name
    // $folder_name ="ชื่อ Folder"
    function upload_file($input_name, $folder_name, $file_name = "", $pArr_option_cfg = array())
    {
        $result = array("is_success_upload" => FALSE, "have_upload_file" => TRUE, "file_path" => NULL, "msg" => NULL);
        $ci = & get_instance();

        if (!isset($_FILES[$input_name]) || $_FILES[$input_name]['error'] == UPLOAD_ERR_NO_FILE)
        {
            $result["have_upload_file"] = FALSE;   // no file selected
        }
        else
        {
            $cfg_upload = $this->get_validation_file($folder_name);
//            $cfg_upload['encrypt_name'] = FALSE;          

            if ($file_name != "")
            {
                $cfg_upload["file_name"] = $file_name;
            }

            if (!empty($pArr_option_cfg))
            {
                $cfg_upload = $this->get_option_cfg_upload($pArr_option_cfg, $cfg_upload);
            }

            $ci->load->library("upload");
            $ci->upload->initialize($cfg_upload);

            //do upload
            if ($ci->upload->do_upload($input_name))
            {//success  
                $file_data = $ci->upload->data();
                $result["file_path"] = $cfg_upload["upload_path"] . $file_data["file_name"];
                $result["is_success_upload"] = TRUE;
            }
            else
            {//fail upload  
                $result["msg"] = $ci->upload->display_errors();
                $result["is_success_upload"] = FALSE;
            }
        }

        return $result;
    }

    /**
     * เลือกหลายไฟล์ ctrl + เลือกไฟล์มากกว่า 1
     * @param type $input_name
     * @param type $folder_name
     * @param type $file_name
     * @param type $pArr_option_cfg
     * @return boolean
     */
    function upload_multiple_file($input_name, $folder_name, $file_name = "", $pArr_option_cfg = array())
    {
        $result = array("is_success_upload" => FALSE, "have_upload_file" => TRUE, "file_path" => NULL, "msg" => NULL);
        $ci = & get_instance();

        $files = $_FILES[$input_name];
        $cpt = count($files['name']);
        for ($i = 0; $i < $cpt; $i++)
        {
            $_FILES[$input_name]['name'] = $files['name'][$i];
            $_FILES[$input_name]['type'] = $files['type'][$i];
            $_FILES[$input_name]['tmp_name'] = $files['tmp_name'][$i];
            $_FILES[$input_name]['error'] = $files['error'][$i];
            $_FILES[$input_name]['size'] = $files['size'][$i];

            $cfg_upload = $this->get_validation_file($folder_name);
//            $cfg_upload['encrypt_name'] = FALSE;

            if ($file_name != "")
            {
                $cfg_upload["file_name"] = $file_name;
            }

            if (!empty($pArr_option_cfg))
            {
                $cfg_upload = $this->get_option_cfg_upload($pArr_option_cfg, $cfg_upload);
            }

            $ci->load->library("upload");
            $ci->upload->initialize($cfg_upload);

            //do upload
            if ($ci->upload->do_upload($input_name))
            {//success  
                $file_data = $ci->upload->data();
                $result["file_path"] = $cfg_upload["upload_path"] . $file_data["file_name"];
                $result["is_success_upload"] = TRUE;
            }
            else
            {//fail upload  
                $result["msg"] = $ci->upload->display_errors();
                $result["is_success_upload"] = FALSE;
            }
        }
        return $result;
    }

    /* get_validation_file()
     * 
     * prepare $config สำหรับ Upload file
     */

    public function get_validation_file($pPath = "")
    {
        if ($pPath === "")
        {
            $con_upload['upload_path'] = './medias/';
        }
        else
        {
            $con_upload['upload_path'] = './medias/' . $pPath . "/";
        }
        //check destination has folder ?
        if (!is_dir($con_upload['upload_path']))
        {
            //no exist create 
            mkdir($con_upload["upload_path"]);
        }

        $con_upload['allowed_types'] = '*';
        $con_upload['max_size'] = '2048'; //2MB
        $con_upload['encrypt_name'] = TRUE;

        return $con_upload;
    }

    function get_option_cfg_upload($pArr_option_cfg, $cfg_upload)
    {

        if (isset($pArr_option_cfg["file_type"]) && !empty($pArr_option_cfg["file_type"]))
        {
            $cfg_upload["allowed_types"] = $pArr_option_cfg["file_type"];
        }

        if (isset($pArr_option_cfg["max_size"]) && !empty($pArr_option_cfg["max_size"]))
        {
            $cfg_upload["max_size"] = $pArr_option_cfg["max_size"];
        }
        
        if (isset($pArr_option_cfg["encrypt_name"]) && !empty($pArr_option_cfg["encrypt_name"]))
        {
            $cfg_upload["encrypt_name"] = $pArr_option_cfg["encrypt_name"];
        }

        return $cfg_upload;
    }

    // -- End Upload File
    
    /* =====================
     *      Thai Date Year
     * =================== */
    
    function convert_thai_date_abbr($date = "")
    {
        if ($this->is_yyyymmdd_format($date) == "1")
        {
            $idx_day = "2";
            $idx_month = "1";
            $idx_year = "0";
        }

        if ($this->is_ddmmyyyy_format($date) == "1")
        {
            $idx_day = "0";
            $idx_month = "1";
            $idx_year = "2";
        }
        if (strpos($date, "/") !== false)
        {
            $explode = explode("/", $date);
        }
        if (strpos($date, "-") !== false)
        {
            $explode = explode("-", $date);
        }
        $day = ltrim($explode[$idx_day], "0");
        $month = $this->get_abbr_month_name($explode[$idx_month]);
        $year = $this->get_year_thai(intval($explode[$idx_year]));
//        $year =intval($explode[$idx_year]);
        return $day . " " . $month . " " . $year;
    }
    
    function get_abbr_month_name($month)
    {
        if ($month == "01"):
            $name = "ม.ค.";
        elseif ($month == "02"):
            $name = "ก.พ.";
        elseif ($month == "03"):
            $name = "มี.ค.";
        elseif ($month == "04"):
            $name = "เม.ย.";
        elseif ($month == "05"):
            $name = "พ.ค.";
        elseif ($month == "06"):
            $name = "มิ.ย.";
        elseif ($month == "07"):
            $name = "ก.ค.";
        elseif ($month == "08"):
            $name = "ส.ค.";
        elseif ($month == "09"):
            $name = "ก.ย.";
        elseif ($month == "10"):
            $name = "ต.ค.";
        elseif ($month == "11"):
            $name = "พ.ย.";
        else:
            $name = "ธ.ค.";
        endif;
        return $name;
    }
    
    function get_year_thai($year)
    {
        return intval($year) + 543;
    }
    
    function is_yyyymmdd_format($date = "")
    {
        return preg_match('/^[0-9]{4}(-|\/)(0[1-9]|1[0-2])(-|\/)(0[1-9]|[1-2][0-9]|3[0-1])$/', $date);
    }

    function is_ddmmyyyy_format($date = "")
    {
        return preg_match('/^(0[1-9]|[1-2][0-9]|3[0-1])(-|\/)(0[1-9]|1[0-2])(-|\/)[0-9]{4}$/', $date);
    }
}
