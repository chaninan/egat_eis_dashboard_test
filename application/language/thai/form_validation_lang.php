<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']        = 'กรุณาระบุ {field}';
$lang['form_validation_isset']            = 'โปรดระบุ {field}';
$lang['form_validation_valid_email']        = '{field} กรุณาระบุเป็น E-mail เท่านั้น';
$lang['form_validation_valid_emails']        = '{field} กรุณาระบุเป็น E-mail เท่านั้น';
$lang['form_validation_valid_url']        = '{field} กรุณาระบุเป็น URL เท่านั้น';
$lang['form_validation_valid_ip']        = '{field} กรุณาระบุเป็น IP. เท่านั้น';
$lang['form_validation_min_length']        = '{field} กรุณาระบุอย่างน้อย {param} ตัวอักษร';
$lang['form_validation_max_length']        = '{field} กรุณาระบุไม่เกิน {param} ตัวอักษร';
$lang['form_validation_exact_length']        = '{field} กรุณาระบุขนาด {param} cตัวอักษร';
$lang['form_validation_alpha']            = '{field} กรุณาระบุเฉพาะตัวอักษร';
$lang['form_validation_alpha_numeric']        = '{field} กรุณาระบุเฉพาะตัวอักษร หรือตัวเลข';
$lang['form_validation_alpha_numeric_spaces']    = '{field} กรุณาระบุเฉพาะตัวอักษร ตัวเลข หรือเว้นวรรค';
$lang['form_validation_alpha_dash']        = '{field} กรุณาระบุเฉพาะตัวอักษร ตัวเลข ขีดล่าง หรือขีดกลาง';
$lang['form_validation_numeric']        = '{field} กรุณาระบุเฉพาะตัวเลข';
$lang['form_validation_is_numeric']        = '{field} กรุณาระบุเฉพาะตัวเลข';
$lang['form_validation_integer']        = '{field} กรุณาระบุเฉพาะตัวเลข';
$lang['form_validation_regex_match']        = '{field} ระบุไม่ถูก Format';
$lang['form_validation_matches']        = '{field} กรุณาระบุให้ตรงกับ {param}';
$lang['form_validation_differs']        = '{field} กรุณาระบุให้แตกต่างจาก {param}';
$lang['form_validation_is_unique']         = '{field} ไม่สามารถใช้ซ้ำได้';
$lang['form_validation_is_natural']        = '{field} กรุณาระบุจำนวนเต็ม';
$lang['form_validation_is_natural_no_zero']    = '{field} กรุณาระบุจำนวนเต็ม และมากกว่า 0';
$lang['form_validation_decimal']        = '{field} กรุณาระบุเฉพาะค่าทศนิยม';
$lang['form_validation_less_than']        = '{field} กรุณาระบุค่าน้อยกว่า {param}';
$lang['form_validation_less_than_equal_to']    = '{field} กรุณาระบุค่าน้อยกว่า หรือเท่ากับ {param}';
$lang['form_validation_greater_than']        = '{field} กรุณาระบุค่ามากกว่า {param}';
$lang['form_validation_greater_than_equal_to']    = '{field} กรุณาระบุค่ามากกว่า หรือเท่ากับ {param}';
$lang['form_validation_error_message_not_set']    = 'เกิดข้อผิดพลาด ไม่สามารถแสดงข้อมูลที่ผิดพลาดของ {field} ได้';
$lang['form_validation_in_list']        = '{field} ต้องมีค่าใน {param}';

