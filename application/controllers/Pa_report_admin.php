<?php

Class Pa_report_admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        //Permission check
        if ($this->login_utils->have_login(TRUE, "authen_admin/login"))
        {
            $this->adminID = $this->login_utils->get_data("empn");
        }

        $this->data = $this->utils->get_basic_data();

        // load pa
        $this->load->model("mpa");
    }

    function index()
    {
        $this->view_all();
    }

    function view_all()
    {
        $this->data[CON_TITLE] = "รายการคะแนน PA";
        $this->data[CON_RIGHT_CONTENT] = "pa_report_admin/view_all";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fit";
        $pArrWhere = null;
        $result = $this->mpa->view_all($pArrWhere);
        $this->data["dt_pa"] = $result["data"];
        $this->data["dt_pa_cnt"] = $result["found_rows"];

        $this->load->view("templates/template_admin", $this->data);
    }

    function ajax_add_pa()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        if ($this->input->post())
        {
            //validation
            $form_validate = $this->_validation_add_pa();
            $attach_validate = $this->_validation_attachment("attach_file", "PA");

            //validation success
            if ($form_validate["status"] == TRUE)
            {
                if ($attach_validate["status"] == TRUE)
                {
                    // data for save
                    $pa["year"] = $this->input->post("year");
                    $pa["period"] = $this->input->post("period");
                    $pa["priorityID"] = $this->input->post("priority");
                    $pa["point"] = $this->input->post("point_pa");
                    $pa["attach_file"] = !empty($attach_validate["file_path"]) ? $attach_validate["file_path"] : NULL;
                    $pa["createDate"] = CON_DATE_NOW;
                    $pa["createBy"] = $this->adminID;

                    $result = $this->mpa->insert($pa);
                    if ($result["status"])
                    {
                        $res["status"] = "1";
                        $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                        $res["next_url"] = site_url("pa_report_admin/view_all");
                    }
                    else
                    {
                        $res["msg"] = $result["msg"];
                    }
                }
                else
                {
                    $res["msg"] = $attach_validate["msg"];
                }
            }
            else
            {
                $res["msg"] = $form_validate["msg"];
            }
        }
        echo json_encode($res);
    }

    private function _validation_add_pa()
    {
        $res = array("status" => FALSE, "msg" => "");
        if ($this->input->post())
        {
            $this->form_validation->set_rules("priority", "ความสำคัญ", "trim|required|callback_check_duplicate_pa");
            $this->form_validation->set_rules("point_pa", "คะแนน PA", "trim|required");

            if ($this->form_validation->run())
            {
                $res["status"] = TRUE;
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        return $res;
    }

    private function _validation_attachment($file, $folder = "")
    {
        $res = array("file" => "", "status" => TRUE, "msg" => "", "file_path" => "");

        $option_config = array("file_type" => "xls|xlsx");
        $res_upload = $this->utils->upload_file($file, $folder, "", $option_config);
        $res["file"] = $file;
        if ($res_upload["have_upload_file"])
        {
            if ($res_upload["is_success_upload"] == TRUE) // upload success
            {
                $res["file_path"] = $res_upload["file_path"];
            }
            else // upload fail
            {
                $res["msg"] = $res_upload["msg"];
                $res["status"] = FALSE;
            }
        }
        else
        {
            // ไม่ได้เลือกไฟล์
            $res["file_path"] = "";
        }
        return $res;
    }

    function check_duplicate_pa()
    {
        $year = $this->input->post("year");
        $period = $this->input->post("period");
        $priority = $this->input->post("priority");
        $cond = array("year" => $year, "period" => $period, "priorityID" => $priority);
        $dt_pa = $this->mpa->get_one_pa($cond);
        if (!empty($dt_pa))
        {
            $this->form_validation->set_message("check_duplicate_pa", "คะแนน " . $dt_pa["priority_name"] . " ประจำปี " . $year . " งวด " . $dt_pa["period_name"] . " มีแล้วในระบบ");
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function ajax_get_detail_edit_pa()
    {
        $paID = $this->input->post("paID");
        $dec_paID = $this->utils->decID($paID);
        $_POST = $this->data["dr_pa"] = $dr_pa = $this->mpa->get_one_pa(array("paID" => $dec_paID));
        $_POST["point_pa"] = $dr_pa["point"];
        $html = $this->load->view("pa_report_admin/mdl_edit_pa", $this->data, TRUE);
        echo $html;
    }

    function ajax_edit_pa()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        if ($this->input->post())
        {
            //validation
            $this->form_validation->set_rules("point_pa", "คะแนน PA", "trim|required");
            $attach_validate = $this->_validation_attachment("attach_file", "PA");

            //validation success
            if ($this->form_validation->run())
            {
                if ($attach_validate["status"] == TRUE)
                {
                    // data for save
                    $pa["paID"] = $this->utils->decID($this->input->post("paID"));
                    $pa["point"] = $this->input->post("point_pa");
                    $pa["attach_file"] = !empty($attach_validate["file_path"]) ? $attach_validate["file_path"] : NULL;
                    $pa["updateDate"] = CON_DATE_NOW;
                    $pa["updateBy"] = $this->adminID;

                    $result = $this->mpa->update($pa);
                    if ($result["status"])
                    {
                        $res["status"] = "1";
                        $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                        $res["next_url"] = site_url("pa_report_admin/view_all");
                    }
                    else
                    {
                        $res["msg"] = $result["msg"];
                    }
                }
                else
                {
                    $res["msg"] = $attach_validate["msg"];
                }
            }
            else
            {
                 $res["msg"] = validation_errors();
            }
        }
        echo json_encode($res);
    }
    
    function ajax_delete_pa()
    {
        $res = array("status" => "0", "msg" => "");
        $paID = $this->input->post("paID");
        $dec_paID = $this->utils->decID($paID);
        $res_delete = $this->mpa->delete($dec_paID);
        if ($res_delete["status"])
        {
            $res["status"] = "1";
            $this->utils->msgbox_flash(CON_MSG_DEL_COMPLETE, "alert alert-success");
            $res["next_url"] = site_url("pa_report_admin/view_all");
        }
        else
        {
            $res["msg"] = $res_delete["msg"];
        }

        echo json_encode($res);
    }

}
