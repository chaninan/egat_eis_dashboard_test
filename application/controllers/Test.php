<?php

class Test extends CI_Controller {

    function subview()
    {
        $this->load->view("test/subview");
    }

    function graph()
    {
        $this->load->model("mbudget_operate");
        $dt = $this->mbudget_operate->get_graph_line_plan_accumulate(2013);
        //$dt = $this->mbudget_operate->get_graph_bar_plan_actual(2016);

        echo "<pre>";
        var_dump($dt);
        echo "</pre>";
    }
}
