<?php

class Authen_admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->data = $this->utils->get_basic_data();
    }

    function index()
    {
        $this->login();
    }

    function login()
    {
        $this->data[CON_TITLE] = "Admin Login";
        $this->data[CON_RIGHT_CONTENT] = "authen_admin/login";
        $this->data[CON_TEMPLATE_CONTAINER] = "container";

        if($this->input->get("backurl"))
        {
            $this->data["backurl"] = $this->input->get("backurl");
        }
        if($this->input->get("empn"))
        {
            $_POST["empn"] = $this->input->get("empn");
        }

        if($this->utils->is_postback())
        {//postback 
            $this->load->library("form_validation");
            $this->form_validation->set_rules("empn", "รหัสพนักงาน", "trim|required");
            $this->form_validation->set_rules("pwd", "รหัสผ่าน", "trim|required");

            if($this->form_validation->run())
            {
                $empn = $this->input->post("empn");
                $pwd = $this->input->post("pwd");

                $is_override = FALSE;
                if(($this->input->get("override") == "true"))
                {
                    $is_override = TRUE;
                }
                $res_login = $this->login_utils->can_login($empn, $pwd, $is_override);

                if($res_login["status"])
                {
                    //check have backurl ? 
                    if($this->input->post("backurl"))
                    {
                        redirect($this->input->post("backurl"));
                    }
                    else
                    {
                        redirect("admin");
                    }
                }
                else if($res_login["is_second_login"] == TRUE)
                {//duplicate login
                    $this->utils->msgbox_flash($res_login["msg"] . "<br/>กรุณากรอกรหัสผ่านอีกครั้ง เพื่อยืนยันการเข้าระบบ", "alert alert-danger");

                    //prepare url for redirect 
                    $redirect_url = "authen_admin/login?override=true&empn=" . $empn;
                    
                    //check have backurl ? 
                    if($this->input->post("backurl"))
                    {
                        $redirect_url .=  "&backurl=" . $this->input->post("backurl");
                    }                    
                    
                    redirect($redirect_url);
                }
                else
                {
                    $this->utils->msgbox($res_login["msg"], "alert alert-danger");
                }
            }
            else
            {
                $this->utils->msgbox(validation_errors(), "alert alert-danger");
            }
        }

        $this->load->view("templates/template_admin", $this->data);
    }

    function logout()
    {
        $this->login_utils->logout();
        redirect("authen_admin/login");
    }

}
