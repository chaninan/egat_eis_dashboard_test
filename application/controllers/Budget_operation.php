<?php

class Budget_operation extends CI_Controller
{
    private $memberID = "";

    function __construct()
    {
        parent::__construct();
        $this->data = $this->utils->get_basic_data();

        //Permission check
        if($this->login_utils->have_login(TRUE))
        {
            $this->memberID = $this->login_utils->get_data("empn");
            //load member data
            $this->load->model("muser_gsd");
            $this->data["login_member"] = $this->muser_gsd->get_by_empID($this->memberID);
        }

        $this->load->library("breadcrumb");
        $this->breadcrumb->add("<i class='fa fa-home'></i>หน้าแรก", site_url());
        $this->breadcrumb->add("งบประมาณทำการ (ภาพรวม)", site_url("budget_operation/all"));
    }

    function index()
    {
        $this->all();
    }

    //ภาพรวม
    function all()
    {
        $this->data[CON_RIGHT_CONTENT] = "budget_operation/all";
        $this->data[CON_TITLE] = "งบประมาณทำการ";
        $this->data[CON_SUB_TITLE] = "ภาพรวม";
        $this->data[CON_MENU_USER_SELECTED] = "b_all";

        $this->data["dt_year"] = $this->utils->get_year_valid_range();

        $this->load->model("mbudget_operate");

        //check have $_GET 
        $year = date("Y");
        if($this->input->get("year"))
        {//set year for query and display
            $_POST["year"] = $year = $this->input->get("year");
        }
        $this->data[CON_TITLE] .= " ปี " . $this->utils->year_buddha_convert($year);

        //ภาพรวมฝ่าย
        $dt_graph_overall = $this->mbudget_operate->get_graph_bar_plan_actual_overall($year);
        $this->data["data_overall"] = $this->_generate_bar_chart_budget_operate($dt_graph_overall);
        $this->data["update_date_overall"] = $this->mbudget_operate->get_latest_update($year);

        //ส่วนกลาง อบก.
        $dt_graph_gsd_center = $this->mbudget_operate->get_graph_bar_plan_actual($year, CON_UNITID_GSD_CENTER);
        $this->data["data_gsd_center"] = $this->_generate_bar_chart_budget_operate($dt_graph_gsd_center);
        $this->data["update_date_gsd_center"] = $this->mbudget_operate->get_latest_update($year, CON_UNITID_GSD_CENTER);

        //กบท.
        $dt_graph_general = $this->mbudget_operate->get_graph_bar_plan_actual($year, CON_UNITID_GENERAL);
        $this->data["data_general"] = $this->_generate_bar_chart_budget_operate($dt_graph_general);
        $this->data["update_date_general"] = $this->mbudget_operate->get_latest_update($year, CON_UNITID_GENERAL);

        //กพน.
        $dt_graph_vehicle = $this->mbudget_operate->get_graph_bar_plan_actual($year, CON_UNITID_VEHICLE);
        $this->data["data_vehicle"] = $this->_generate_bar_chart_budget_operate($dt_graph_vehicle);
        $this->data["update_date_vehicle"] = $this->mbudget_operate->get_latest_update($year, CON_UNITID_VEHICLE);

        //กยธ.
        $dt_graph_civil = $this->mbudget_operate->get_graph_bar_plan_actual($year, CON_UNITID_CIVIL);
        $this->data["data_civil"] = $this->_generate_bar_chart_budget_operate($dt_graph_civil);
        $this->data["update_date_civil"] = $this->mbudget_operate->get_latest_update($year, CON_UNITID_CIVIL);

        //กอค.
        $dt_graph_building = $this->mbudget_operate->get_graph_bar_plan_actual($year, CON_UNITID_BUILDING);
        $this->data["data_building"] = $this->_generate_bar_chart_budget_operate($dt_graph_building);
        $this->data["update_date_building"] = $this->mbudget_operate->get_latest_update($year, CON_UNITID_BUILDING);

        //Accumulate
        $arr_dt_budget = $this->mbudget_operate->get_graph_line_plan_accumulate($year);
        $this->data["data_line_accummulate"] = $this->_generate_line_chart_budget_operate($arr_dt_budget);

        $this->load->view("templates/template_user", $this->data);
    }

    function unit($pUnitID = "")
    {
        if($pUnitID != "")
        {
            $this->data[CON_RIGHT_CONTENT] = "budget_operation/unit";
            $this->data[CON_TITLE] = "งบประมาณทำการ";

            //get unit data
            $this->load->model("munit");
            $dr_unit = $this->munit->get($pUnitID);
            if(!empty($dr_unit))
            {
                $this->data[CON_SUB_TITLE] = $unit_name = $dr_unit["unit_name"];
                $this->data[CON_MENU_USER_SELECTED] = "b_" . $pUnitID;

                $this->breadcrumb->add("งบประมาณทำการ ({$unit_name})", current_url());

                $this->data["dt_year"] = $this->utils->get_year_valid_range();

                //check have $_GET 
                $year = date("Y");
                if($this->input->get("year"))
                {//set year for query and display
                    $_POST["year"] = $year = $this->input->get("year");
                }
                $this->data[CON_TITLE] .= " ปี " . $this->utils->year_buddha_convert($year);

                $this->load->model("mbudget_operate");
                //load budget data
                $dt_graph_bar = $this->mbudget_operate->get_graph_bar_plan_actual($year, $pUnitID);
                $this->data["data_graph_bar"] = $this->_generate_bar_chart_budget_operate($dt_graph_bar);
                $this->data["update_date_all"] = $this->mbudget_operate->get_latest_update($year, $pUnitID);

                //Accumulate
                $arr_dt_budget = $this->mbudget_operate->get_graph_line_plan_accumulate($year, $pUnitID);
                $this->data["data_line_accummulate"] = $this->_generate_line_chart_budget_operate($arr_dt_budget);

                //Prepare data for each month
                if(!empty($dt_graph_bar))
                {
                    foreach($dt_graph_bar as $i)
                    {
                        $arr_dt_chart_month[] = array("graph_data" => $this->_generate_bar_chart_month_budget_operate($i)
                            , "budget_operateID" => $i["budget_operateID"]
                            , "month" => $i["month"]
                            , "month_name" => $i["month_name"]
                            , "dt_remark" => $this->mbudget_operate->get_all_budget_remark($i["budget_operateID"])
                            , "show_sign" => $this->_calculate_color_each_month($i["plan"], $i["actual"])
                            , "count_remark" => $this->mbudget_operate->get_count_remark_by_budgetID($i["budget_operateID"])
                        );
                    }
                    $this->data["arr_dt_chart_month"] = $arr_dt_chart_month;
                }

                $this->load->view("templates/template_user", $this->data);
            }
            else
            {
                //TODO:: Redirect to no item !
            }
        }
        else
        {
            //TODO:: Redirect to no item !
        }
    }

    /**
     * Data format Chart data
     * -->labels[]
     * -->datasets[] 
     * ----->label
     * ----->backgroundColor
     * ----->data[]
     * @param array $pData เรืยกจาก $this->mbudget_operate->get_graph_bar_plan_actual
     * @return array
     */
    private function _generate_bar_chart_budget_operate($pData)
    {
        if(!empty($pData))
        {
            foreach($pData as $item)
            {
                $labels[] = $item["month_name"];
                $dt_plan[] = $item["plan"];
                $dt_actual[] = $item["actual"];
            }
            //Prepare Dataset Plan
            //Overall data (ภาพรวมฝ่าย)
            $dts_plan = array("label" => "งบที่ตั้ง", "backgroundColor" => CON_COLOR_PLAN, "data" => $dt_plan);
            $dts_actual = array("label" => "ใช้จริง", "backgroundColor" => CON_COLOR_ACTUAL, "data" => $dt_actual);
            $res = array("labels" => $labels, "datasets" => array($dts_plan, $dts_actual));
            return json_encode($res);
        }
        else
        {
            return "";
        }
    }

    private function _generate_bar_chart_month_budget_operate($pData)
    {
        if(!empty($pData))
        {
            $labels[] = $pData["month_name"];
            $dt_plan[] = $pData["plan"];
            $dt_actual[] = $pData["actual"];

            //Prepare Dataset Plan
            //Overall data (ภาพรวมฝ่าย)
            $dts_plan = array("label" => "งบที่ตั้ง", "backgroundColor" => CON_COLOR_PLAN, "data" => $dt_plan);
            $dts_actual = array("label" => "ใช้จริง", "backgroundColor" => CON_COLOR_ACTUAL, "data" => $dt_actual);
            $res = array("labels" => $labels, "datasets" => array($dts_plan, $dts_actual));
            return json_encode($res);
        }
        else
        {
            return "";
        }
    }

    private function _generate_line_chart_budget_operate($pData)
    {
        if(!empty($pData))
        {
            //label 
            $arr_month = $this->utils->get_months();
            foreach($arr_month as $month)
            {
                $labels[] = $month;
            }

            //dt_accumulate_actual
            $dt_accumulate_actual = $pData["dt_accumulate_actual"];
            $dt_actual = array();
            foreach($dt_accumulate_actual as $item)
            {
                $dt_actual[] = $item["accumulate"];
            }

            //dt_accumulate_plan
            $dt_accumulate_plan = $pData["dt_accumulate_plan"];
            $dt_plan = array();
            foreach($dt_accumulate_plan as $item)
            {
                $dt_plan[] = $item["accumulate"];
            }

            //calculate PA 
            $PA = intval(CON_PA);
            $sum_plan = doubleval($pData["sum_plan"]);
            $limit_pa = ($sum_plan * $PA) / 100;
            $dt_pa = array();
            foreach($arr_month as $month)
            {
                $dt_pa[] = $limit_pa;
            }

            //Prepare Dataset Plan
            //Overall data (ภาพรวมฝ่าย)
            $dts_plan = array("label" => "สะสม งบที่ตั้ง", "backgroundColor" => CON_COLOR_PLAN, "borderColor" => CON_COLOR_PLAN, "data" => $dt_plan, "fill" => false);
            $dts_actual = array("label" => "สะสม ใช้จริง", "backgroundColor" => CON_COLOR_ACTUAL, "data" => $dt_actual);
            $dts_pa = array("label" => "PA", "backgroundColor" => CON_COLOR_PA_GREEN, "pointStyle" => "line", "data" => $dt_pa);
            $res = array("labels" => $labels, "datasets" => array($dts_plan, $dts_actual, $dts_pa));

            return json_encode($res);
        }
        else
        {
            return "";
        }
    }

    function _calculate_color_each_month($pPlan, $pActual)
    {
        $res = "";
        if($pActual != 0)
        {
            if($pActual > $pPlan)
            {
                $res = "danger";
            }
            elseif($pActual < $pPlan)
            {
                $res = "success";
            }
        }

        return $res;
    }

    function ajax_render_remark_table()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_operateID"))
        {
            $budget_operateID = $this->input->post("budget_operateID");
            $this->load->model("mbudget_operate");
            //remark
            $data_render["dt"] = $this->mbudget_operate->get_all_budget_remark($budget_operateID);
            //budget detail
            $budget_detail = $this->mbudget_operate->get($budget_operateID);
            $res["data"] = $this->load->view("budget_operation/ajax_remark_table", $data_render, TRUE);
            $res["budget_detail"] = json_encode($budget_detail);
            $res["status"] = "1";
        }

        echo json_encode($res);
    }

}
