<?php

class Mockup extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }
    
    function index()
    {
        $this->data["body"] = "mockup/index";
        
        $this->load->view("mockup/templates", $this->data);
    }
    
    function create_ci()
    {
        $this->data["body"] = "mockup/create_ci";
        
        $this->load->view("mockup/templates", $this->data);
    }
    
    function create_ci2()
    {
        $this->data["body"] = "mockup/create_ci2";
        
        $this->load->view("mockup/templates", $this->data);
    }
    
    function wait_ci()
    {
        $this->data["body"] = "mockup/wait_ci";
        
        $this->load->view("mockup/templates", $this->data);
    }
    
    function report_ci()
    {
        $this->data["body"] = "mockup/report_ci";
        
        $this->load->view("mockup/templates", $this->data);
    }

}
