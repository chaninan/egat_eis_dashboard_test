<?php

Class Action_plan_admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();

        //Permission check
        if ($this->login_utils->have_login(TRUE, "authen_admin/login"))
        {
            $this->adminID = $this->login_utils->get_data("empn");
        }

        $this->data = $this->utils->get_basic_data();

        // load owner
        $this->load->model("mplan");
        // load unit
        $this->load->model("munit");
    }

    function index()
    {
        $this->view_all();
    }

    function view_all($pOffset = "0")
    {
        $this->data[CON_TITLE] = "ข้อมูลแผนปฏิบัติการ";
        $this->data[CON_RIGHT_CONTENT] = "action_plan_admin/view_all";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fit";

        $this->data["dt_month"] = $this->utils->get_months_abb();

        $pArrWhere = null;
        if ($this->input->post())
        {
            //  get condition when search
            $pArrWhere = $this->input->post();
            $_POST = $this->input->post();
            $this->data["have_search"] = true;
        }

        //pagination
        $sortBy = $this->utils->get_sortBy("createDate");
        $sortDi = $this->utils->get_sortDi("DESC");

        $result = $this->mplan->view_all($pArrWhere, $sortBy, $sortDi, $pOffset, CON_PER_PAGE);
        $this->data["dt_plan"] = $result["data"];
        $this->data["dt_plan_cnt"] = $result["found_rows"];

        $this->load->library("pagination");
        $config_pagination = $this->utils->config_pagination();
        $config_pagination["base_url"] = site_url("action_plan_admin/view_all");
        $config_pagination["total_rows"] = $result["found_rows"];
        $config_pagination["per_page"] = CON_PER_PAGE;
        $config_pagination["uri_segment"] = 3;
        $this->pagination->initialize($config_pagination);
        //End pagination

        $this->load->view("templates/template_admin", $this->data);
    }

    function ajax_add_plan()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        if ($this->input->post())
        {
            //validation
            $form_validate = $this->_validation_add_plan();
            $attach_validate = $this->_validation_attachment();

            //validation success
            if ($form_validate["status"] == TRUE)
            {
                if ($attach_validate["status"] == TRUE)
                {
                    $post_data = $this->input->post();
                    if (isset($attach_validate["data"]))
                    {
                        foreach ($attach_validate["data"] as $item)
                        {
                            $post_data[$item["file"]] = $item["file_path"];
                        }
                    }

                    $res_insert = $this->insert_plan($post_data);
                    if ($res_insert["status"] == "1")
                    {
                        $res["status"] = "1";
                        $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                        $res["next_url"] = site_url("action_plan_admin/view_all");
                    }
                    else
                    {
                        $res["msg"] = $res_insert["msg"];
                    }
                }
                else
                {
                    $res["msg"] = $attach_validate["msg"];
                }
            }
            else
            {
                $res["msg"] = $form_validate["msg"];
            }
        }
//        var_dump($post_data);
        echo json_encode($res);
    }

    function insert_plan($post_data)
    {
        $res = array("status" => "0", "msg" => "");

        $plan = $this->prepare_plan($post_data);
        $plan["createDate"] = CON_DATE_NOW;
        $plan["createBy"] = $this->adminID;
        $plan_target = $post_data["target_month"];
        $res_insert = $this->mplan->insert($plan, $plan_target);
        if ($res_insert["status"])
        {
            $res["status"] = "1";
        }
        else
        {
            $res["msg"] = $res_insert["msg"];
        }

        return $res;
    }

    function prepare_target_month($post_data)
    {
        // ใช้ target ที่มีค่าเท่านั้น
        foreach ($post_data as $key => $val)
        {
            if ($val == "")
            {
                continue;
            }
            else
            {
                $target_month[$key] = $val;
            }
        }

        if (isset($target_month))
        {
            return $target_month;
        }
        else
        {
            return FALSE;
        }
    }

    function ajax_edit_plan()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        if ($this->input->post())
        {
            //validation
            $form_validate = $this->_validation_add_plan();
            $attach_validate = $this->_validation_attachment();

            //validation success
            if ($form_validate["status"] == TRUE)
            {
                if ($attach_validate["status"] == TRUE)
                {
                    $post_data = $this->input->post();
                    if (isset($attach_validate["data"]))
                    {
                        foreach ($attach_validate["data"] as $item)
                        {
                            $post_data[$item["file"]] = $item["file_path"];
                        }
                    }

                    $res_update = $this->update_plan($post_data);
                    if ($res_update["status"])
                    {
                        $res["status"] = "1";
                        $this->utils->msgbox_flash(CON_MSG_EDIT_COMPLETE, "alert alert-success");
                        $res["next_url"] = site_url("action_plan_admin/view_all");
                    }
                    else
                    {
                        $res["msg"] = $res_update["msg"];
                    }
                }
                else
                {
                    $res["msg"] = $attach_validate["msg"];
                }
            }
            else
            {
                $res["msg"] = $form_validate["msg"];
            }
        }
//        var_dump($post_data);
        echo json_encode($res);
    }

    function update_plan($post_data)
    {
        $res = array("status" => false, "msg" => "");
        $planID = $this->utils->decID($post_data["planID"]);
        $plan = $this->prepare_plan($post_data);
        $plan["updateDate"] = CON_DATE_NOW;
        $plan["updateBy"] = $this->adminID;
        $plan_target = $post_data["target_month"];
        $res_update = $this->mplan->update($planID, $plan, $plan_target);
        if ($res_update["status"])
        {
            $res["status"] = true;
        }
        else
        {
            $res["msg"] = $res_update["msg"];
        }

        return $res;
    }

    function prepare_plan($post_data)
    {
        $plan = array(
            "plan_name" => $post_data["plan_name"],
            "year" => $post_data["year"],
            "owner" => (isset($post_data["owner"])) ? implode(",", $post_data["owner"]) : NULL,
            "priority" => (isset($post_data["priority"])) ? implode(",", $post_data["priority"]) : NULL,
            "file_bcg1" => (isset($post_data["file_bcg1"])) ? $post_data["file_bcg1"] : NULL,
            "file_bcg2" => (isset($post_data["file_bcg2"])) ? $post_data["file_bcg2"] : NULL,
            "file_bcg3-5" => (isset($post_data["file_bcg3-5"])) ? $post_data["file_bcg3-5"] : NULL
        );

        return $plan;
    }

    private function _validation_add_plan()
    {
        $res = array("status" => FALSE, "msg" => "");
        if ($this->input->post())
        {
            $this->form_validation->set_rules("plan_name", "ชื่อแผนปฏิบัติการ", "trim|required|callback_check_duplicate_plan");
            $this->form_validation->set_rules("year", "ปี", "trim|required");
            $this->form_validation->set_rules("owner[]", "ผู้รับผิดชอบ", "trim|required|callback_check_owner");
            $this->form_validation->set_rules("priority[]", "ความสำคัญ", "trim");
            $this->form_validation->set_rules("target_month[]", "", "trim|callback_check_target_month");

            if ($this->form_validation->run())
            {
                $res["status"] = TRUE;
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        return $res;
    }

    private function _validation_attachment()
    {
        $attach_validate = array("status" => TRUE, "msg" => "");

        $file_attach = array(
            array("file" => "file_bcg1", "folder" => "BCG1"),
            array("file" => "file_bcg2", "folder" => "BCG2"),
            array("file" => "file_bcg3-5", "folder" => "BCG3-5"));

        foreach ($file_attach as $file)
        {
            $result[] = $this->_upload_attachment($file["file"], $file["folder"]);
        }

        // 1. เคสปกติ จะ insert/update ใหม่ถ้ามีการเลือกไฟล์ใหม่
        // 2. เคสเปลี่ยนแปลงไฟล์
        // ==> กรณี 1) ลบไฟล์ = NULL , 2) ไม่ได้ลบไฟล์ = ใช้ไฟล์เดิม (skip)
        foreach ($result as $item)
        {
            if ($item["status"])
            {
                $tmp_file["file"] = $item["file"];
                if ($item["file_path"] != "")
                {
                    $tmp_file["file_path"] = $item["file_path"];
                }
                else
                {
                    // เช็คจาก flag is_delete
                    $post_elm = "is_delete_" . $item["file"];
                    $is_delete = $this->input->post($post_elm);
                    if (isset($is_delete))
                    {
                        if ($is_delete == "1")
                        {
                            $tmp_file["file_path"] = NULL;
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        $tmp_file["file_path"] = NULL;
                    }
                }
                $res[] = $tmp_file;
                $attach_validate["data"] = $res;
            }
            elseif ($item["status"] == FALSE)
            {
                $attach_validate["status"] = FALSE;
                $attach_validate["msg"] = $item["msg"];
                unset($attach_validate["data"]);
            }
        }
//        var_dump($attach_validate);
        return $attach_validate;
    }

    private function _upload_attachment($file, $folder = "")
    {
        $res = array("file" => "", "status" => TRUE, "msg" => "", "file_path" => "");

        $option_config = array("file_type" => "pdf");
        $res_upload = $this->utils->upload_file($file, $folder, "", $option_config);
        $res["file"] = $file;
        if ($res_upload["have_upload_file"])
        {
            if ($res_upload["is_success_upload"] == TRUE) // upload success
            {
                $res["file_path"] = $res_upload["file_path"];
            }
            else // upload fail
            {
                $res["msg"] = $res_upload["msg"];
                $res["status"] = FALSE;
            }
        }
        else
        {
            // ไม่ได้เลือกไฟล์
            $res["file_path"] = "";
        }
        return $res;
    }

    function check_owner()
    {
        if (empty($this->input->post("owner[]")))
        {
            $this->form_validation->set_message("check_owner", "กรุณาระบุ ผู้รับผิดชอบ");
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    function check_target_month()
    {
        // ใช้ target ที่มีค่าเท่านั้น
        $post_data = $this->input->post("target_month");

        $target_month = $this->prepare_target_month($post_data);
        if ($target_month == FALSE)
        {
            $this->form_validation->set_message("check_target_month", "กรุณาระบุ % ความก้าวหน้างานตามเป้าหมายให้ครบ 100%");
            return FALSE;
        }
        else
        {
            // เช็คว่าระหว่าง target แรก กับ target สุดท้าย ใส่ครบหรือไม่?
            $cnt_empty_val = 0;
            for ($i = min(array_keys($target_month)); $i <= max(array_keys($target_month)); $i++)
            {
                $target_month_val = $this->input->post("target_month[" . $i . "]");
                if ($target_month_val == "")
                {
                    $cnt_empty_val++;
                }
            }

            if ($cnt_empty_val > 0)
            {
                $this->form_validation->set_message("check_target_month", "กรุณาระบุ % ความก้าวหน้างานตามเป้าหมายให้ครบถ้วน");
                return FALSE;
            }
            else
            {
                $previous = 0;
                $total = 0;
                foreach ($target_month as $percent)
                {
                    $total = floatval($total) + (floatval($percent) - floatval($previous));
                    $previous = $percent;
                }

                if ($total != 100)
                {
                    $this->form_validation->set_message("check_target_month", "กรุณาระบุ % ความก้าวหน้างานตามเป้าหมายให้ครบ 100%");
                    return FALSE;
                }
                else
                {
                    return TRUE;
                }
            }
        }
    }

    function check_duplicate_plan()
    {
        $plan_name = trim($this->input->post("plan_name"));
        $year = $this->input->post("year");
        $cond = array("plan_name" => $plan_name, "year" => $year);
        $dt_plan = $this->mplan->get_plan($cond);

        if (!empty($dt_plan))
        {
            if ($this->input->post("planID") == NULL) // case : add
            {
                $this->form_validation->set_message("check_duplicate_plan", "แผนปฏิบัติการ \"" . $plan_name . " ประจำปี " . $year . "\" มีแล้วในระบบ");
                return FALSE;
            }
            else
            {   // case : edit
                $plan_decID = $this->utils->decID($this->input->post("planID"));
                if ($plan_decID == $dt_plan[0]["planID"])
                {
                    return TRUE;
                }
                else
                {
                    $this->form_validation->set_message("check_duplicate_plan", "แผนปฏิบัติการ \"" . $plan_name . " ประจำปี " . $year . "\" มีแล้วในระบบ");
                    return FALSE;
                }
            }
        }
        else
        {
            return TRUE;
        }
    }

    function ajax_view_plan()
    {
        $planID = $this->input->post("planID");
        $dec_planID = $this->utils->decID($planID);

        $this->data["dr_plan"] = $this->mplan->get_plan_by_id($dec_planID);
        $this->data["dt_month"] = $this->utils->get_months_abb();
        $this->data["dt_plan_target"] = $this->mplan->get_plan_target_by_planID($dec_planID);
        $html = $this->load->view("action_plan_admin/mdl_view_plan", $this->data, TRUE);
        echo $html;
    }

    function ajax_delete_plan()
    {
        $res = array("status" => "0", "msg" => "");
        $planID = $this->input->post("planID");
        $dec_planID = $this->utils->decID($planID);
        $res_delete = $this->mplan->delete($dec_planID);
        if ($res_delete["status"])
        {
            $res["status"] = "1";
            $this->utils->msgbox_flash(CON_MSG_DEL_COMPLETE, "alert alert-success");
            $res["next_url"] = site_url("action_plan_admin/view_all");
        }
        else
        {
            $res["msg"] = $res_delete["msg"];
        }

        echo json_encode($res);
    }

    function ajax_get_detail_edit_plan()
    {
        $planID = $this->input->post("planID");
        $dec_planID = $this->utils->decID($planID);

        $_POST = $this->data["dr_plan"] = $this->mplan->get_plan_by_id($dec_planID);
        $this->data["dt_owner"] = $this->get_owner_in_plan($dec_planID);
        $this->data["dt_priority"] = $this->get_priority_in_plan($dec_planID);
        $this->data["dt_month"] = $this->utils->get_months_abb();
        $this->data["dt_plan_target"] = $this->mplan->get_plan_target_by_planID($dec_planID);
        $html = $this->load->view("action_plan_admin/mdl_edit_plan", $this->data, TRUE);
        echo $html;
    }

    function get_owner_in_plan($dec_planID)
    {
        $dt_all_owner = $this->mplan->get_owner();

        $dt_plan = $this->mplan->get_plan_by_id($dec_planID);
        $dt_owner = explode(",", $dt_plan["owner"]);
        foreach ($dt_owner as $item)
        {
            $key = array_search($item, array_column($dt_all_owner, 'plan_ownerID'));
            $dt_all_owner[$key]["is_checked"] = true;
        }
        return $dt_all_owner;
    }

    function get_priority_in_plan($dec_planID)
    {
        $dt_all_priority = $this->mplan->get_priority();

        $dt_plan = $this->mplan->get_plan_by_id($dec_planID);
        $dt_priority = explode(",", $dt_plan["priority"]);
        foreach ($dt_priority as $item)
        {
            $key = array_search($item, array_column($dt_all_priority, 'priorityID'));
            $dt_all_priority[$key]["is_checked"] = true;
        }
        return $dt_all_priority;
    }

}
