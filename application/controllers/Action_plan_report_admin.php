<?php

Class Action_plan_report_admin extends CI_Controller {

    function __construct()
    {
        parent::__construct();

//Permission check
        if ($this->login_utils->have_login(TRUE, "authen_admin/login"))
        {
            $this->adminID = $this->login_utils->get_data("empn");
        }

        $this->data = $this->utils->get_basic_data();
// load owner
        $this->load->model("mplan");
// load unit
        $this->load->model("munit");
        $this->load->model("mplan_report");
    }

    function index()
    {
        $this->view_all();
    }

    function view_all($pOffset = "0")
    {
        $this->data[CON_TITLE] = "รายงานแผนปฏิบัติการ";
        $this->data[CON_RIGHT_CONTENT] = "action_plan_report_admin/view_all";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fit";

        $pArrWhere = null;
        if ($this->input->post())
        {
            //  get condition when search
            $pArrWhere = $this->input->post();
            $_POST = $this->input->post(); 
            $this->data["have_search"] = true;
        }

        //pagination
        $sortBy = $this->utils->get_sortBy("createDate");
        $sortDi = $this->utils->get_sortDi("desc");

        $result = $this->mplan->view_all($pArrWhere, $sortBy, $sortDi, $pOffset, CON_PER_PAGE);
        $this->data["dt_plan"] = $result["data"];
        $this->data["dt_plan_cnt"] = $result["found_rows"];

        $this->load->library("pagination");
        $config_pagination = $this->utils->config_pagination();
        $config_pagination["base_url"] = site_url("action_plan_report_admin/view_all/");
        $config_pagination["total_rows"] = $result["found_rows"];
        $config_pagination["per_page"] = CON_PER_PAGE;
        $config_pagination["uri_segment"] = 3;
        $this->pagination->initialize($config_pagination);
        //End pagination

        $this->load->view("templates/template_admin", $this->data);
    }

    function report($planID)
    {
        $this->data[CON_TITLE] = "รายงานแผนปฏิบัติการ";
        $this->data[CON_RIGHT_CONTENT] = "action_plan_report_admin/report";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fit";

        $plan_decID = $this->utils->decID($planID);
        $this->data["dr_plan"] = $this->mplan->get_plan_by_id($plan_decID);
        $this->data["dt_month"] = $this->utils->get_months();
        $this->data["dt_plan_report"] = $dt_plan_report = $this->mplan_report->get_plan_report(array("planID" => $plan_decID, "target_percent<>" => 0));

        // เช็คว่า รอ confirm แล้วเสร็จ?
        $dr_last = $this->mplan_report->get_lastest_plan_report($plan_decID);
        if (!empty($dr_last))
        {
            $show_confirm_complete = $this->show_confirm_is_complete($plan_decID, $dr_last["month"]);
            $this->data["show_confirm_complete"] = $show_confirm_complete;
            $this->data["is_plan_cancel"] = $this->utils_plan->check_is_plan_cancel($plan_decID);
//            var_dump($this->data["is_plan_cancel"]);
        }
        $this->load->view("templates/template_admin", $this->data);
    }

    function ajax_add_report()
    {
        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $month = $this->input->post("month");

        $cond = array("planID" => $plan_decID, "month" => $month);
        $_POST = $this->data["dr_plan_report"] = $this->mplan_report->get_one_plan_report($cond);
        $html = $this->load->view("action_plan_report_admin/mdl_add_report", $this->data, TRUE);
        echo $html;
    }

    function ajax_view_report_plan()
    {
        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $this->data["dt_plan_report"] = $this->mplan_report->get_plan_report(array("planID" => $plan_decID, "target_percent<>" => 0));
        $this->data["dr_plan"] = $this->mplan->get_plan_by_id($plan_decID);
        $html = $this->load->view("action_plan_report_admin/mdl_view_report_plan", $this->data, TRUE);
        echo $html;
    }

    // ปจบ ไม่ได้ใช้
    function get_count_pending_report()
    {
        $res = array("status" => "0", "msg" => "");
        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $month = $this->input->post("month");

        // เช็คมีรายการก่อนหน้าที่ยังไม่ได้รายงานหรือไม่?
        $cond = array("planID" => $plan_decID, "month<" => $month);
        $cnt_pending = $this->mplan_report->get_count_pending_report($cond);
        if ($cnt_pending == 0)
        {
            $res["status"] = "1";
        }
        else
        {
            $res["msg"] = "มีรายการค้างของเดือนก่อนหน้า กรุณารายงานแผนของเดือนก่อนหน้าให้ครบก่อน";
        }
        echo json_encode($res);
    }

    // เช็คเฉพาะกรณีแก้ไข แล้วค่าใหม่เพิ่มเป็น 100% หรือ เดิมเป็น 100% แต่ค่าใหม่ลดลง
    function ajax_pre_save_report()
    {
        $res = array("status" => "1");

        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $month = $this->input->post("month");
        $new_actual_percent = $this->input->post("actual_percent");

        $cond = array("planID" => $plan_decID, "month" => $month);
        $dr_result = $this->mplan_report->get_one_plan_report($cond);
        if (!empty($dr_result) && !empty($dr_result["actual_percent"])) // exist = edit case
        {
            if ($new_actual_percent == 100)
            {
                if (!$this->utils_plan->check_month_is_last_report($plan_decID, $month))
                {
                    $res["status"] = "0";
                    $res["msg"] = "ข้อมูลการรายงานแผนของเดือนถัดไปที่เคยบันทึกไว้จะถูกลบออกจากระบบ คุณยืนยันต้องการดำเนินการใช่หรือไม่ ?";
                }
            }

            $curr_actual_percent = $dr_result["actual_percent"];
            if (($curr_actual_percent == 100) && ($new_actual_percent < 100))
            {
                if ($this->utils_plan->check_is_plan_report_complete($plan_decID))
                {
                    $res["status"] = "0";
                    $res["msg"] = "แผนปฏิบัติการนี้ถูกยืนยันสิ้นสุดการรายงานแผนก่อนหน้านี้แล้ว หากท่านยืนยันแก้ไขครั้งนี้ สถานะ <b>แล้วเสร็จ</b> จะถูกยกเลิกทันที คุณยืนยันต้องการดำเนินการใช่หรือไม่ ?";
                }
            }
        }

        echo json_encode($res);
    }

    function ajax_save_report()
    {
        $res = array("status" => "0", "msg" => "");

        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $month = $this->input->post("month");
        $actual_percent = $this->input->post("actual_percent");

        // validation
//        $this->form_validation->set_rules("actual_percent", "% สะสมตามแผน", "trim|required|callback_check_actual_percent");
        $this->form_validation->set_rules("actual_percent", "% ความก้าวหน้างานสะสมที่ดำเนินการได้", "trim|required");
        $attach_validate = $this->_validation_attachment("attach_file", "PN-2");

        if ($this->form_validation->run())
        {
            if ($attach_validate["status"] == TRUE)
            {
                // data for save
                $plan_report["planID"] = $plan_decID;
                $plan_report["month"] = $month;
                $plan_report["percentage"] = $actual_percent;
                $plan_report["createDate"] = CON_DATE_NOW;
                $plan_report["createBy"] = $this->adminID;
                $plan_report["updateDate"] = CON_DATE_NOW;
                $plan_report["updateBy"] = $this->adminID;
                $plan_report["attach_file"] = !empty($attach_validate["file_path"]) ? $attach_validate["file_path"] : NULL;
                $result = $this->mplan_report->save_report($plan_report);
                if ($result["status"])
                {
                    $res["status"] = "1";
                    $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                    $res["next_url"] = site_url("action_plan_report_admin/report/" . $planID);
                }
                else
                {
                    $res["msg"] = $result["msg"];
                }
            }
            else
            {
                $res["msg"] = $attach_validate["msg"];
            }
        }
        else
        {
            $res["msg"] = validation_errors();
        }

        echo json_encode($res);
    }

    private function _validation_attachment($file, $folder = "")
    {
        $res = array("file" => "", "status" => TRUE, "msg" => "", "file_path" => "");

        $option_config = array("file_type" => "xls|xlsx");
        $res_upload = $this->utils->upload_file($file, $folder, "", $option_config);
        $res["file"] = $file;
        if ($res_upload["have_upload_file"])
        {
            if ($res_upload["is_success_upload"] == TRUE) // upload success
            {
                $res["file_path"] = $res_upload["file_path"];
            }
            else // upload fail
            {
                $res["msg"] = $res_upload["msg"];
                $res["status"] = FALSE;
            }
        }
        else
        {
            // ไม่ได้เลือกไฟล์
            $res["file_path"] = "";
        }
        return $res;
    }

    /*  เดิม
      function check_actual_percent()
      {
      $planID = $this->utils->decID($this->input->post("planID"));
      $actual_percent = $this->input->post("actual_percent");
      $month = $this->input->post("month");
      // get current actual percent
      $dt_plan_report = $this->mplan_report->get_plan_report_by_planID($planID);
      // check is over 100%
      $previous = 0;
      $total = 0;
      // กรณีแก้ไข
      foreach ($dt_plan_report as $row)
      {
      if ($row["month"] == $month)
      {
      $total = floatval($total) + ($actual_percent - floatval($previous));
      $previous = $actual_percent;
      }
      else
      {
      $total = floatval($total) + ($row["percentage"] - floatval($previous));
      $previous = $row["percentage"];
      }
      }

      // กรณีรายงานแผน (รายงานเพิ่มใหม่)
      $key = array_search($month, array_column($dt_plan_report, 'month'));
      if ($key == false)
      {
      $total = floatval($total) + ($actual_percent - floatval($previous));
      }

      if ($total > 100)
      {
      $this->form_validation->set_message("check_actual_percent", "ความก้าวหน้างานสะสมตามแผนเกิน 100% กรุณาตรวจสอบ");
      return FALSE;
      }
      else
      {
      return TRUE;
      }
      } */

    function show_confirm_is_complete($planID, $month)
    {
        /*
         * 1. ต้องรายงานครบทุกแผน จนถึงเดือนปัจจุบัน
         * 2. ต้องรายงานเดือนล่าสุดเป็น 100%
         * 3. is_complete ต้องเท่ากับ 0
         */
        $res = TRUE;

        // 1.
        $cond = array("planID" => $planID, "month<" => $month);
        $cnt_pending = $this->mplan_report->get_count_pending_report($cond);
        if ($cnt_pending > 0)
        {
            $res = FALSE;
        }
        else
        {
            // 2.
            $dr_plan_report = $this->mplan_report->get_lastest_plan_report($planID);
            if ($dr_plan_report["actual_percent"] != 100)
            {
                $res = FALSE;
            }
            else
            {
                // 3.
                if ($this->utils_plan->check_is_plan_report_complete($planID))
                {
                    $res = FALSE;
                }
            }
        }
        return $res;
    }

    function ajax_mark_is_complete()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);

        $res_update = $this->mplan->mark_is_complete($plan_decID);
        if ($res_update["status"])
        {
            $res["status"] = "1";
            $res["next_url"] = site_url("action_plan_report_admin/view_all");
            $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
        }
        else
        {
            $res["msg"] = $res_update["msg"];
        }
        echo json_encode($res);
    }

    function ajax_delete_plan_report()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $month = $this->input->post("month");

        $plan_report["planID"] = $plan_decID;
        $plan_report["month"] = $month;
        $plan_report["updateDate"] = CON_DATE_NOW;
        $plan_report["updateBy"] = $this->adminID;
        $res_delete = $this->mplan_report->delete($plan_report);
        if ($res_delete)
        {
            $res["status"] = "1";
            $res["next_url"] = site_url("action_plan_report_admin/report/" . $planID);
            $this->utils->msgbox_flash(CON_MSG_DEL_COMPLETE, "alert alert-success");
        }
        else
        {
            $res["msg"] = $res_delete["msg"];
        }
        echo json_encode($res);
    }

    // กรณียกเลิกแผนกลางคัน (ไม่ใช่ลบแผน)
    function ajax_cancel_plan()
    {
        $res = array("status" => "0", "msg" => "", "next_url" => "");
        $planID = $this->input->post("planID");
        $plan_decID = $this->utils->decID($planID);
        $month = $this->input->post("cancel_month");

        $plan_report["planID"] = $plan_decID;
        $plan_report["month"] = $month;
        $plan_report["percentage"] = 999;
        $plan_report["createBy"] = $this->adminID;
        $res_cancel = $this->mplan_report->cancel_plan($plan_report);
        if ($res_cancel)
        {
            $res["status"] = "1";
            $res["next_url"] = site_url("action_plan_report_admin/report/" . $planID);
            $this->utils->msgbox_flash("ยกเลิกแผนเรียบร้อย", "alert alert-success");
        }
        else
        {
            $res["msg"] = $res_cancel["msg"];
        }
        echo json_encode($res);
    }

}
