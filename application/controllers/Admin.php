<?php

class Admin extends CI_Controller
{
    private $adminID = "";

    function __construct()
    {
        parent::__construct();

        //Permission check
        if($this->login_utils->have_login(true))
        {
            $this->adminID = $this->login_utils->get_data("empn");
        }
        
        $this->data = $this->utils->get_basic_data();
    }

    function index()
    {
        $this->data[CON_TITLE] = "Admin Dashboard";
        $this->data[CON_RIGHT_CONTENT] = "admin/index";
        $this->data[CON_TEMPLATE_CONTAINER] = "container";
        
        
        $this->load->view("templates/template_admin", $this->data);
    }

}
