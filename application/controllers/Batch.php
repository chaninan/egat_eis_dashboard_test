<?php

class Batch extends CI_Controller {

    function update_user_gsd()
    {
        $this->load->model("memployee");
        
        // 1. load data to temp and create or update table user_gsd_xxx
        $this->memployee->update_user_gsd_by_year();
        
        // 2. get all employee (active and retire) in range year
        $this->memployee->get_all_employee_by_range_year(2555, 2570);
    }

}
