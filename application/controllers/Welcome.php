<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function index()
    {
        //Permission check
        if($this->login_utils->have_login(true))
        {
            //already login 
            redirect("budget_operation/all");
        }
        else
        {
            //redirect login page
            redirect("authen_member/");
        }
    }

}
