<?php

class Budget_investment extends CI_Controller
{
    private $memberID = "";
    //1.no action, 2.TOR, 3.PR, 4.PO, 5.Actual (ข้อมูลที่จะแสดง ต้องเรียงตามนี้ด้วย)
    private $status_color = array(CON_COLOR_INV_NO_ACTION, CON_COLOR_INV_TOR, CON_COLOR_INV_PR, CON_COLOR_INV_PO, CON_COLOR_INV_ACTUAL);

    function __construct()
    {
        parent::__construct();
        $this->data = $this->utils->get_basic_data();

        //Permission check
        if($this->login_utils->have_login(TRUE))
        {
            $this->memberID = $this->login_utils->get_data("empn");
            //load member data
            $this->load->model("muser_gsd");
            $this->data["login_member"] = $this->muser_gsd->get_by_empID($this->memberID);
        }
        
        $this->load->library("breadcrumb");
        $this->breadcrumb->add("<i class='fa fa-home'></i>หน้าแรก", site_url());
        $this->breadcrumb->add("งบลงทุน (ภาพรวม)", site_url("budget_operation/all"));
    }

    function index()
    {
        $this->all();
    }

    //ภาพรวม
    function all()
    {
        $this->data[CON_RIGHT_CONTENT] = "budget_investment/all";
        $this->data[CON_TITLE] = "งบลงทุน";
        $this->data[CON_SUB_TITLE] = "ภาพรวม";
        $this->data[CON_MENU_USER_SELECTED] = "bi_all";

        //load all year
        $this->data["dt_year"] = $this->utils->get_year_valid_range();

        //check have $_GET 
        $year = date("Y");
        if($this->input->get("year"))
        {//set year for query and display
            $_POST["year"] = $year = $this->input->get("year");
        }

        //Page Title
        $this->data[CON_TITLE] .= " ปี " . $this->utils->year_buddha_convert($year);

        //for render color of status
        $this->data["data_table_status_color"] = $this->status_color;

        $this->load->model("mbudget_investment");
        $this->load->model("mconfig");
        /* 1. check have record each year & trimester 
         * ----> no : record return ""
         * ----> yes : find data for Pie chart, Bar chart
         */

        //load all trimester
        $dt_trimester = $this->mconfig->get_all("trimester");
        foreach($dt_trimester as $item)
        {
            $json_pie_tri = "";
            $json_bar_tri = "";
            $latest_update_date = NULL;
            $dt_table_pie = array();
            $title_box = "ภาพรวมฝ่าย รายการเบิกจ่าย" . $item["config_display"];

            $cnt_record = $this->mbudget_investment->count_by_year_unit($year, $item["configID"]);
            if($cnt_record > 0)
            {
                //pie chart count status
                $dt_table_pie = $this->mbudget_investment->get_pie_data_overall($year, $item["configID"]);
                $json_pie_tri = $this->_generate_pie_chart_overall($dt_table_pie);
                //bar chart usage
                $dr_bar_usage = $this->mbudget_investment->get_bar_data_usage_overall($year, $item["configID"]);
                $json_bar_tri = $this->_generate_bar_chart_usage_overall($dr_bar_usage);
                $latest_update_date = $this->mbudget_investment->get_latest_update_date($year, $item["configID"]);
            }

            $dt_all_tri[] = array(
                "dt_table_pie" => $dt_table_pie,
                "json_pie_tri" => $json_pie_tri,
                "json_bar_tri" => $json_bar_tri,
                "title" => $title_box,
                "latest_update" => $latest_update_date,
                "trimester" => $item["configID"]);
        }

        $this->data["dt_all_trimester"] = $dt_all_tri;

        $this->load->view("templates/template_user", $this->data);
    }

    function unit($pUnitID = "")
    {
        if($pUnitID != "")
        {
            $this->data[CON_RIGHT_CONTENT] = "budget_investment/unit";
            $this->data[CON_TITLE] = "งบลงทุน";

            //get unit data
            $this->load->model("munit");
            $dr_unit = $this->munit->get($pUnitID);
            if(!empty($dr_unit))
            {
                $this->data[CON_SUB_TITLE] = $unit_name = $dr_unit["unit_name"];
                $this->data[CON_MENU_USER_SELECTED] = "bi_" . $pUnitID;

                $this->breadcrumb->add("งบประมาณทำการ ({$unit_name})", current_url());

                //load all year
                $this->data["dt_year"] = $this->utils->get_year_valid_range();

                //check have $_GET 
                $year = date("Y");
                if($this->input->get("year"))
                {//set year for query and display
                    $_POST["year"] = $year = $this->input->get("year");
                }
                $this->data[CON_TITLE] .= " ปี " . $this->utils->year_buddha_convert($year);

                //for render color of status
                $this->data["data_table_status_color"] = $this->status_color;

                $this->load->model("mbudget_investment");
                $this->load->model("mconfig");
                /* 1. check have record each year & trimester 
                 * ----> no : record return ""
                 * ----> yes : find data for Pie chart, Bar chart
                 */

                //load all trimester
                $dt_trimester = $this->mconfig->get_all("trimester");
                foreach($dt_trimester as $item)
                {
                    $json_pie_tri = "";
                    $json_bar_tri = "";
                    $latest_update_date = NULL;
                    $dt_table_pie = array();
                    $trimester_name = $item["config_display"];
                    $dt_table_detail = array();

                    $cnt_record = $this->mbudget_investment->count_by_year_unit($year, $item["configID"], $pUnitID);
                    if($cnt_record > 0)
                    {
                        //pie chart count status
                        $dt_table_pie = $this->mbudget_investment->get_pie_data_overall($year, $item["configID"], $pUnitID);
                        $json_pie_tri = $this->_generate_pie_chart_overall($dt_table_pie);
                        //bar chart usage
                        $dr_bar_usage = $this->mbudget_investment->get_bar_data_usage_overall($year, $item["configID"], $pUnitID);
                        $json_bar_tri = $this->_generate_bar_chart_usage_overall($dr_bar_usage);
                        $latest_update_date = $this->mbudget_investment->get_latest_update_date($year, $item["configID"], $pUnitID);

                        //get all item each trimester
                        $dt_table_detail = $this->mbudget_investment->get_all_by_unit_year($pUnitID, $year, $item["configID"]);
                    }

                    $dt_all_tri[] = array(
                        "dt_table_pie" => $dt_table_pie,
                        "json_pie_tri" => $json_pie_tri,
                        "json_bar_tri" => $json_bar_tri,
                        "trimester_name" => $trimester_name,
                        "latest_update" => $latest_update_date,
                        "trimester" => $item["configID"],
                        "dt_table_detail" => $dt_table_detail);
                }

                $this->data["dt_all_trimester"] = $dt_all_tri;



                $this->load->view("templates/template_user", $this->data);
            }
            else
            {
                //TODO:: Redirect to no item
            }
        }
        else
        {
            //TODO:: Redirect to no item 
        }
    }

    private function _generate_pie_chart_overall($pDT)
    {
        /* prepare for pie chart 
         * Structure 
         * ----> labels : []
         * ----> datasets : [{data:[], backgroundColor :[]}]
         */
        if(!empty($pDT))
        {
            $grp_bg = $this->status_color;
            $grp_labels = array();
            $grp_data = array();
            foreach($pDT as $item)
            {
                $grp_labels[] = $item["config_display"];
                $grp_data[] = intval($item["cnt_status"]);
            }

            $res["labels"] = $grp_labels;
            $res["datasets"][] = array("data" => $grp_data, "backgroundColor" => $grp_bg);
            return json_encode($res);
        }
        else
        {
            return "";
        }
    }

    private function _generate_bar_chart_usage_overall($pDR)
    {
        /* prepare for bar chart
         * Structure
         * ---> labels : []
         * ---> datasets : [{data:[], backgroundColor :[]}] 
         */
        if(!empty($pDR))
        {
            $grp_bg = array(CON_COLOR_PLAN, CON_COLOR_ACTUAL);
            $grp_labels = array("งบที่ตั้ง", "ใช้จริง");
            $grp_data = array();
            $grp_data[] = $pDR["plan"];
            $grp_data[] = $pDR["actual"];

            //$res["labels"] = $grp_labels;
            //$res["datasets"][] = array("data" => $grp_data, "backgroundColor" => $grp_bg);
            $datasets = array("data" => $grp_data, "backgroundColor" => $grp_bg, "label" => "เป็นเงิน");
            $res = array("labels" => $grp_labels, "datasets" => array($datasets));

            /* $dts_plan = array("label" => "งบที่ตั้ง", "backgroundColor" => CON_COLOR_PLAN, "data" => array($pDR["plan"]));
              $dts_actual = array("label" => "ใช้จริง", "backgroundColor" => CON_COLOR_ACTUAL, "data" => array($pDR["actual"]));
              $res = array("labels" => array("ประเภท"), "datasets" => array($dts_plan, $dts_actual)); */
            return json_encode($res);
        }
        else
        {
            return "";
        }
    }
}
