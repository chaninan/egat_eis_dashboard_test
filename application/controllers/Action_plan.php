<?php

Class Action_plan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->data = $this->utils->get_basic_data();

        //Permission check
        if ($this->login_utils->have_login(TRUE))
        {
            $this->memberID = $this->login_utils->get_data("empn");
            //load member data
            $this->load->model("muser_gsd");
            $this->data["login_member"] = $this->muser_gsd->get_by_empID($this->memberID);
        }
        // load model
        $this->load->model("mplan");
        $this->load->model("mpa");
    }

    function index()
    {
        $this->dashboard();
    }

    function dashboard($pOffset = "0")
    {
        $this->data[CON_RIGHT_CONTENT] = "action_plan/dashboard";
        $this->data[CON_TITLE] = "ข้อมูลแผนปฏิบัติการ";
        $this->data[CON_SUB_TITLE] = "ภาพรวม";
        $this->data[CON_MENU_USER_SELECTED] = "p_all";

        // initial ddl
        $this->data["dt_year"] = $dt_year = $this->utils->get_year_valid_range(1, 3);
        $this->data["dt_month"] = $dt_month = $this->utils->get_months();
        $this->data["dt_owner"] = $this->mplan->get_owner();
        $this->data["dt_priority"] = $this->mplan->get_all_priority(array("priority_group" => 1));
        // status color
        $this->data["status_color"] = array("#5E7E8C", "#EC4938", "#F4C20D", "#3FBC92", "#36A2EB", "#9A9AA2");

        //pagination
        $sortBy = $this->utils->get_sortBy("p.planID, pr.month");
        $sortDi = $this->utils->get_sortDi("ASC");

        if ($this->input->get())
        {
            $_POST = $this->input->get();  // repopulate to input elm

            if (is_null($this->input->get("year")) || $this->input->get("year") == "" || !in_array($this->input->get("year") - 543, $dt_year))
            {
                $year = date("Y") + 543 . "";
            }
            else
            {
                $year = $this->input->get("year");
            }

            if (is_null($this->input->get("month")) || $this->input->get("month") == "" || !in_array($this->input->get("month"), array_keys($dt_month)))
            {
                $month = date("m");
            }
            else
            {
                $month = $this->input->get("month");
            }

            $owner = ($this->input->get("owner") == "" || is_null($this->input->get("owner"))) ? "" : $this->input->get("owner");
        }
        else
        {
            // default list plan with current year
            $year = date("Y") + 543 . "";
            $month = date("m");
            $owner = "";
        }

        $_POST["year"] = $year;
        $_POST["month"] = $month;
        $_POST["owner"] = $owner;
        

        // plan list
        $dt_result = $this->mplan->view_list_plan($year, $month, $owner, $sortBy, $sortDi, $pOffset, CON_PER_PAGE);
        // plan chart data
        $dr_plan_chart = $this->mplan->get_data_plan_for_chart($year, $month, $owner);
        // generate chart
        $this->data["data_plan_chart"] = (!empty($dr_plan_chart)) ? $this->_generate_pie_chart($dr_plan_chart) : 0;

        // summary plan chart
        $this->data["dt_summary_plan_status"] = $this->get_summary_plan_chart($dr_plan_chart);

        $this->data["dt_list_plan"] = $dt_result["data"];
        $this->data["dt_cnt_plan"] = $dt_result["found_rows"];
        $this->data["year"] = $year;
        $this->data["month"] = $this->utils->get_months((int) $month);
        $this->data["cnt_start_page"] = $pOffset + 1;

        $this->load->library("pagination");
        $config_pagination = $this->utils->config_pagination();
        $config_pagination["base_url"] = site_url("action_plan/dashboard");
        $config_pagination["total_rows"] = $dt_result["found_rows"];
        $config_pagination["per_page"] = CON_PER_PAGE;
        $config_pagination["uri_segment"] = 3;
        $this->pagination->initialize($config_pagination);


        // pa point
        $dft_year = date("Y") + 543;
        $dft_period = "F";
        $dft_priority = "7";
        $cond = array("year" => $dft_year, "period" => $dft_period, "priorityID" => $dft_priority);
        $this->data["pa_point"] = $this->mpa->get_pa_point($cond);

        $this->load->view("templates/template_user", $this->data);
    }

    function _generate_pie_chart($pData)
    {
        if (!empty($pData))
        {
            $dt_plan_status = $this->mplan->get_status();

            foreach ($dt_plan_status as $status)
            {
                $key = array_search($status["status_code"], array_column($pData, 'status'));
                if (empty($key) && $key !== 0)
                {
                    $labels[] = $status["status_name"];
                    $dt_plan[] = 0;
                }
                else
                {
                    $labels[] = $pData[$key]["status_name"];
                    $dt_plan[] = (int) $pData[$key]["count"];
                }
            }

            //Prepare Dataset
            $dts_plan = array(
                "backgroundColor" => array("#5E7E8C", "#EC4938", "#F4C20D", "#3FBC92", "#36A2EB", "#9A9AA2"),
                "hoverBackgroundColor" => array("#516f7c", "#C01315", "#FDB632", "#00AAA0", "#228CC0", "#8b8b8e"),
                "data" => $dt_plan);
            $res = array("labels" => $labels, "datasets" => array($dts_plan));

            return json_encode($res);
        }
        else
        {
            return "";
        }
    }

    function get_summary_plan_chart($dr_plan_chart)
    {
        $dt_plan_status = $this->mplan->get_status();
        foreach ($dr_plan_chart as $data)
        {
            $key = array_search($data["status"], array_column($dt_plan_status, "status_code"));
            $dt_plan_status[$key]["count"] = $data["count"];
        }
        return $dt_plan_status;
    }

    function ajax_get_pa_fay_point()
    {
        $year = $this->input->post("year");
        $period = $this->input->post("period");
        $priorityID = ($this->input->post("priority") == null) ? 7 : $this->input->post("priority");
        $cond = array("year" => $year, "period" => $period, "priorityID" => $priorityID);
        $pa_point = $this->mpa->get_pa_point($cond);
//        var_dump($pa_point);
        echo $pa_point;
    }

}
