<?php

class Budget_investment_admin extends CI_Controller
{
    private $adminID = "";
    private $investment_status_show_actual = 8;

    function __construct()
    {
        parent::__construct();

        //Permission check
        if($this->login_utils->have_login(TRUE, "authen_admin/login"))
        {
            $this->adminID = $this->login_utils->get_data("empn");
        }

        $this->data = $this->utils->get_basic_data();

        //Essential in this class
        $this->load->model("mbudget_investment");
    }

    function index()
    {
        $this->view_all();
    }

    function view_all()
    {
        $this->data[CON_TITLE] = "รายการงบลงทุน ทั้งหมด";
        $this->data[CON_RIGHT_CONTENT] = "budget_investment_admin/all";
        $this->data[CON_TEMPLATE_CONTAINER] = "container";

        //load year        
        $this->data["dt_year"] = $this->utils->get_year_valid_range();

        //load unit
        $this->load->model("munit");
        $this->data["dt_unit"] = $dt_unit = $this->munit->get_all();

        //default selected
        $year_selected = date("Y");
        $unit_selected = "";

        if($this->input->get())
        {
            //year selected 
            $year_selected = $this->input->get('search_year');

            //unit_selected 
            if($this->input->get("search_unitID"))
            {
                $unit_selected = $this->input->get("search_unitID");
                $_POST["search_unitID"] = $unit_selected;
            }
        }
        $this->data["search_year"] = $_POST["search_year"] = $year_selected;

        //get budget depend selected unit
        $dt_budget_unit = array();

        if($unit_selected != "")
        {
            //only 1 unit
            $dt_budget_unit[$unit_selected] = array(
                "dt_budget" => $this->mbudget_investment->get_all_by_unit_year($unit_selected, $year_selected),
                "dr_unit" => $this->_find_unit_data($dt_unit, $unit_selected));
            $this->data["dt_budget_unit"] = $dt_budget_unit;
        }
        else
        {
            //get budget all unit
            foreach($dt_unit as $item_unit)
            {
                $dt_budget_unit[$item_unit["unitID"]] = array(
                    "dt_budget" => $this->mbudget_investment->get_all_by_unit_year($item_unit["unitID"], $year_selected),
                    "dr_unit" => $this->_find_unit_data($dt_unit, $item_unit["unitID"]));
            }
            $this->data["dt_budget_unit"] = $dt_budget_unit;
        }

        $this->load->view("templates/template_admin", $this->data);
    }

    private function _find_unit_data(&$pUnit, $pID)
    {
        foreach($pUnit as $data)
        {
            if($data["unitID"] == $pID)
            {
                return $data;
            }
        }
    }

    function create()
    {
        $this->data[CON_TITLE] = "สร้าง/แก้ไข รายการงบลงทุน";
        $this->data[CON_RIGHT_CONTENT] = "budget_investment_admin/create";
        $this->data[CON_TEMPLATE_CONTAINER] = "container";

        //load year        
        $this->data["dt_year"] = $this->utils->get_year_valid_range();

        //load unit
        $this->load->model("munit");
        $this->data["dt_unit"] = $dt_unit = $this->munit->get_all();

        //load trimester
        $this->load->model("mconfig");
        $this->data["dt_trimester"] = $this->mconfig->get_all("trimester");

        //load investment status
        $this->data["dt_invest_status"] = $this->mconfig->get_all("investment_status");

        if($this->input->get())
        {
            $this->load->library("form_validation");
            $this->form_validation->set_data($_GET);
            $this->form_validation->set_rules("search_unitID", "หน่วยงานผู้รับผิดชอบ", "trim|required");
            $this->form_validation->set_rules("search_year", "ปีงบประมาณ", "trim|required");

            //for set_value
            $_POST = $_GET;

            if($this->form_validation->run())
            {
                //bind data for view table 
                $unitID = $this->input->get("search_unitID");
                $year = $this->input->get("search_year");
                $this->data["dt_budget"] = $this->mbudget_investment->get_all_by_unit_year($unitID, $year);

                //bind title 
                $this->load->model("munit");
                $dr_unit = $this->munit->get($unitID);

                $this->data["table_title"] = $dr_unit["unit_name"] . " ปี " . $this->utils->year_buddha_convert($year);
                $this->data["selected_unitID"] = $unitID;
                $this->data["selected_year"] = $year;
            }
            else
            {
                $this->utils->msgbox(validation_errors(), "alert alert-danger");
            }
        }
        else
        {
            $_POST["search_year"] = date("Y");
        }

        $this->load->view("templates/template_admin", $this->data);
    }

    function ajax_create()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post())
        {
            $this->load->model("mbudget_investment");
            $this->load->library("form_validation");
            $this->form_validation->set_rules("item_name", "ชื่อรายการ", "trim|required|callback_check_duplicate_name");
            $this->form_validation->set_rules("trimester", "เบิกจ่ายไตรมาส", "trim|required");
            $this->form_validation->set_rules("latest_statusID", "สถานะรายการ", "trim|required");
            $this->form_validation->set_rules("plan", "วงเงินขอตั้ง", "trim|required|callback_is_numeric");
            $this->form_validation->set_rules("unitID", "รหัสหน่วยงาน", "trim|required");
            $this->form_validation->set_rules("year", "ปี", "trim|required");

            if($this->form_validation->run())
            {
                $budget_investment["item_name"] = $this->input->post('item_name');
                $budget_investment["year"] = $this->input->post('year');
                $budget_investment["unitID"] = $this->input->post('unitID');
                $budget_investment["plan"] = $this->remove_comma($this->input->post('plan'));
                $budget_investment["trimester"] = $this->input->post('trimester');
                $budget_investment["latest_work_statusID"] = 9; // default progress is normal
                $budget_investment["latest_statusID"] = $this->input->post('latest_statusID');
                $budget_investment["createBy"] = $this->adminID;
                $budget_investment["createDate"] = date("Y-m-d H:i:s");

                $res_insert = $this->mbudget_investment->insert($budget_investment);
                if($res_insert["status"])
                {
                    $res["status"] = "1";
                    $res["msg"] = CON_MSG_SAVE_COMPELTE;
                    $res["next_url"] = $this->input->post("next_url");
                }
                else
                {
                    $res["msg"] = $res_insert["msg"];
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }

        echo json_encode($res);
    }

    function check_duplicate_name()
    {
        $res = FALSE;
        $this->form_validation->set_message("check_duplicate_name", "ไม่สามารถใช้ชื่อรายการซ้ำได้");
        if($this->input->post("year") && $this->input->post("unitID") && $this->input->post("item_name"))
        {
            $year = $this->input->post("year");
            $unitID = $this->input->post("unitID");
            $item_name = $this->input->post("item_name");

            $is_dup = $this->mbudget_investment->is_duplicate_item_name($item_name, $unitID, $year);
            if(!$is_dup)
            {
                $res = TRUE;
            }
        }

        return $res;
    }

    /* =========================
     *        Update Status
     * ======================= */
    function ajax_show_update_status()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_investmentID"))
        {
            $budget_investmentID = $this->input->post("budget_investmentID");
            $dr_budget = $this->mbudget_investment->get($budget_investmentID);
            if(!empty($dr_budget))
            {
                //load data budget_investment
                $data["dr_budget"] = $dr_budget;

                //load data investment_status
                $this->load->model("mconfig");
                $data["dt_invest_status"] = $this->mconfig->get_all("investment_status");

                //load data work status 
                $data["dt_work_status"] = $this->mconfig->get_all("work_status");

                //tell what step show actual textbox
                $data["show_actual_investment_statusID"] = $this->investment_status_show_actual;

                $data["current_url"] = $this->input->post("current_url");

                $res["status"] = "1";
                $res["msg"] = $this->load->view("budget_investment_admin/ajax_show_update_status", $data, TRUE);
            }
        }
        echo json_encode($res);
    }

    function ajax_do_update_status()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post())
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("budget_investmentID", "Budget InvestmentID (ติดต่อ Programmer)", "trim|required");
            $this->form_validation->set_rules("latest_work_statusID", "ความก้าวหน้าของแผน", "trim|required");
            $this->form_validation->set_rules("latest_statusID", "สถานะรายการ", "trim|required");
            $this->form_validation->set_rules("actual", "เบิกจ่ายจริง", "trim|callback_check_latest_status_actual");

            if($this->form_validation->run())
            {
                $budget_investmentID = $this->input->post("budget_investmentID");
                $latest_work_statusID = $this->input->post("latest_work_statusID");
                $latest_statusID = $this->input->post("latest_statusID");
                $actual = $this->input->post("actual");

                $dr_investment_update["budget_investmentID"] = $budget_investmentID;
                $dr_investment_update["latest_statusID"] = $latest_statusID;
                $dr_investment_update["latest_work_statusID"] = $latest_work_statusID;
                if($this->input->post("actual"))
                {
                    $dr_investment_update["actual"] = $this->remove_comma($actual);
                }
                if($this->input->post("remark"))
                {
                    $dr_investment_update["remark"] = $this->input->post("remark");
                }

                $dr_investment_update["createBy"] = $this->adminID;
                $dr_investment_update["createDate"] = date("Y-m-d H:i:s");

                $res_insert = $this->mbudget_investment->insert_budget_update($dr_investment_update);
                if($res_insert["status"])
                {//success
                    $res["status"] = "1";
                    $res["next_url"] = $this->input->post("current_url");
                    $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                }
                else
                {
                    $res["msg"] = $res_insert["msg"];
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        echo json_encode($res);
    }

    /* =========================
     *        Edit 
     * ======================= */
    function ajax_show_edit()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_investmentID"))
        {
            $budget_investmentID = $this->input->post("budget_investmentID");
            $dr_budget = $this->mbudget_investment->get($budget_investmentID);
            if(!empty($dr_budget))
            {
                //load data budget_investment
                $data["dr_budget"] = $dr_budget;

                //load trimester
                $this->load->model("mconfig");
                $data["dt_trimester"] = $this->mconfig->get_all("trimester");

                $data["current_url"] = $this->input->post("current_url");

                $res["status"] = "1";
                $res["msg"] = $this->load->view("budget_investment_admin/ajax_show_edit", $data, TRUE);
            }
        }

        echo json_encode($res);
    }

    function ajax_do_edit()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post())
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("budget_investmentID", "Budget InvestmentID (ติดต่อ Programmer)", "trim|required");
            $this->form_validation->set_rules("item_name", "ชื่อรายการ", "trim|required");
            $this->form_validation->set_rules("trimester", "เบิกจ่ายไตรมาส", "trim|required");
            $this->form_validation->set_rules("plan", "วงเงินขอตั้ง", "trim|required|callback_is_numeric");

            if($this->form_validation->run())
            {
                $budget_investmentID = $this->input->post("budget_investmentID");
                $plan = $this->input->post("plan");

                $dr_investment_update["item_name"] = $this->input->post("item_name");
                $dr_investment_update["trimester"] = $this->input->post("trimester");
                $dr_investment_update["plan"] = $this->remove_comma($plan);
                $dr_investment_update["updateBy"] = $this->adminID;
                $dr_investment_update["updateDate"] = date("Y-m-d H:i:s");

                $res_update = $this->mbudget_investment->update($budget_investmentID, $dr_investment_update);
                if($res_update["status"])
                {//success
                    $res["status"] = "1";
                    $res["next_url"] = $this->input->post("current_url");
                    $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                }
                else
                {
                    $res["msg"] = $res_update["msg"];
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        echo json_encode($res);
    }

    /* =========================
     *        Log
     * ======================= */
    function ajax_show_log()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_investmentID"))
        {
            $budget_investmentID = $this->input->post("budget_investmentID");
            $dr_budget = $this->mbudget_investment->get($budget_investmentID);
            if(!empty($dr_budget))
            {
                //load data budget_investment
                $data["dr_budget"] = $dr_budget;

                //get log
                $data["dt_log"] = $this->mbudget_investment->get_all_log($budget_investmentID);

                $res["status"] = "1";
                $res["msg"] = $this->load->view("budget_investment_admin/ajax_show_log", $data, TRUE);
            }
        }

        echo json_encode($res);
    }

    /* =========================
     *        Utils function
     * ======================= */
    //check latest status must input actual value ?
    function check_latest_status_actual()
    {
        $res = FALSE;
        $this->form_validation->set_message("check_latest_status_actual", "กรุณาระบุ เบิกจ่ายจริง");
        $latest_statusID = $this->input->post("latest_statusID");

        if($latest_statusID == $this->investment_status_show_actual)
        {
            if(!empty($this->input->post("actual")))
            {
                $res = TRUE;
            }
        }
        else
        {//another case
            $res = TRUE;
        }

        return $res;
    }

    /**
     * Function สำหรับ check ว่าเป็นตัวเลขหรือไม่ ? แต่จะเอา "," ออก ก่อน
     */
    function is_numeric($pNum)
    {
        $del_comma = $this->remove_comma($pNum);
        if(!is_numeric($del_comma))
        {//not numeric
            $this->form_validation->set_message("is_numeric", "{field} ต้องระบุเป็นตัวเลขเท่านั้น");
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    private function remove_comma($pStr)
    {
        return str_replace(",", "", $pStr);
    }

}
