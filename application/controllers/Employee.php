<?php

Class Employee extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->data = $this->utils->get_basic_data();

//Permission check
        if ($this->login_utils->have_login(TRUE))
        {
            $this->memberID = $this->login_utils->get_data("empn");
//load member data
            $this->load->model("muser_gsd");
            $this->data["login_member"] = $this->muser_gsd->get_by_empID($this->memberID);
        }

        $this->load->model("memployee");
        $this->load->model("munit");
        $this->load->model("mvut");
        $this->load->library("breadcrumb");
        $this->breadcrumb->add("<i class='fa fa-home'></i>หน้าแรก", site_url());
    }

    function index()
    {
        $this->dashboard();
    }

    function dashboard()
    {
        $this->data[CON_RIGHT_CONTENT] = "employee/all";
        $this->data[CON_TITLE] = "อัตรากำลัง";
        $this->data[CON_SUB_TITLE] = "ภาพรวม";
        $this->data[CON_MENU_USER_SELECTED] = "emp_all";
        $this->breadcrumb->add("อัตรากำลัง (ภาพรวม)", site_url("employee/all"));
        // initial ddl
        $this->data["dt_year"] = $this->utils->get_year_valid_range(2, 3);
        $this->data["dt_gsd_unit"] = $this->munit->get_gsd_unit();
        $unit_name = $this->munit->get_gsd_unit(array("unit_sub_dept" => 9324000, "is_unit" => 1));
        $unit = $unit_name["unit_name"];
        $this->data["unit"] = $unit;
        $this->data["info_update_date"] = $this->memployee->get_information_date();

        // default load current year
        $start_year = $this->utils->year_buddha_convert(date("Y"));
        $end_year = $start_year + 3;
        $this->data["start_year"] = $start_year;
        $this->data["end_year"] = $end_year;
        $this->data["default_year"] = $start_year;

        /* ===============  1) Position =================== */
        // bar chart
        $data_pos_chart = $this->memployee->get_data_for_chart("pos", $start_year, $end_year);
        $this->data["data_chart_pos"] = (!empty($data_pos_chart)) ? $this->_generate_bar_chart("pos", $data_pos_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_summary_pos = $this->_generate_summary("pos", $data_pos_chart["result"], $start_year, $end_year);
//        $this->data["label_level"] = $data_level_chart["tbl_header_label"];
        $this->data["label_pos"] = ["วศ.", "สถ.", "วท.", "วก.", "นค.", "ช.", "พช.", "ชก.", "พขก.", "พขร.", "ชง."];
        $this->data["data_summary_pos"] = $data_summary_pos["data"];

        /* ===============  2) Level =================== */
        // bar chart
        $data_level_chart = $this->memployee->get_data_for_chart("level", $start_year, $end_year);
        $this->data["data_chart_level"] = (!empty($data_level_chart)) ? $this->_generate_bar_chart("level", $data_level_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_summary_level = $this->_generate_summary("level", $data_level_chart["result"], $start_year, $end_year);
//        $this->data["label_level"] = $data_level_chart["tbl_header_label"];
        $this->data["label_level"] = ["12", "11", "10", "09", "08", "07", "06", "05", "04", "03", "00"];
        $this->data["data_summary_level"] = $data_summary_level["data"];

        /* ===============  3) Vut =================== */
        // bar chart
        $data_vut_chart = $this->memployee->get_data_for_chart("vut", $start_year, $end_year);
        $this->data["data_vut_level"] = (!empty($data_vut_chart)) ? $this->_generate_bar_chart("vut", $data_vut_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_vut_level = $this->_generate_summary("vut", $data_vut_chart["result"], $start_year, $end_year);
//        $this->data["label_vut"] = $data_vut_chart["tbl_header_label"];
        $this->data["label_vut"] = ["ปริญญาโท","ปริญญาตรี","ปวส.","ปวช.","ต่ำกว่า ปวช."];
        $this->data["data_summary_vut"] = $data_vut_level["data"];

        /* ===============  4) Age =================== */
        // bar chart
        $data_age_chart = $this->memployee->get_data_for_chart("age", $start_year, $end_year);
        $this->data["data_age_level"] = (!empty($data_age_chart)) ? $this->_generate_bar_chart("age", $data_age_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_age_level = $this->_generate_summary("age", $data_age_chart["result"], $start_year, $end_year);
//        $this->data["label_age"] = $data_age_chart["tbl_header_label"];
        $this->data["label_age"] = ["20-25", "26-30", "31-35", "36-40", "41-45", "46-50", "51-55", "56-60"];
        $this->data["data_summary_age"] = $data_age_level["data"];

        $this->load->view("templates/template_user", $this->data);
    }

    function ajax_get_chart()
    {
        $start_year = $this->input->post("start_year");
        $end_year = $this->input->post("end_year");
        $dept = $this->input->post("unit");
        $is_unit = $this->input->post("is_unit");
        $this->data["info_update_date"] = $this->memployee->get_information_date();

        if ($is_unit == 0)
        {
            $unit_sub_name = $this->munit->get_gsd_unit(array("unit_sub_dept" => $dept, "is_unit" => $is_unit));
            $unit_name = $this->munit->get_gsd_unit(array("unit_dept" => $unit_sub_name["unit_dept"], "is_unit" => 1));
            $unit = $unit_sub_name["unit_abb"] . " | " . $unit_name["unit_abb"];
        }
        else
        {
            $unit_name = $this->munit->get_gsd_unit(array("unit_sub_dept" => $dept, "is_unit" => $is_unit));
            $unit = $unit_name["unit_name"];
        }
        $this->data["unit"] = $unit;

        // default load current year
        $this->data["start_year"] = $start_year;
        $this->data["end_year"] = $end_year;
        $this->data["default_year"] = $start_year;

        /* ===============  1) Position =================== */
        // bar chart
        $data_pos_chart = $this->memployee->get_data_for_chart("pos", $start_year, $end_year, $dept, $is_unit);
        $this->data["data_chart_pos"] = (!empty($data_pos_chart)) ? $this->_generate_bar_chart("pos", $data_pos_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_summary_pos = $this->_generate_summary("pos", $data_pos_chart["result"], $start_year, $end_year);
//        $this->data["label_pos"] = $data_pos_chart["tbl_header_label"];
        $this->data["label_pos"] = ["วศ.", "สถ.", "วท.", "วก.", "นค.", "ช.", "พช.", "ชก.", "พขก.", "พขร.", "ชง."];
        $this->data["data_summary_pos"] = $data_summary_pos["data"];

        /* ===============  2) Level =================== */
        // bar chart
        $data_level_chart = $this->memployee->get_data_for_chart("level", $start_year, $end_year, $dept, $is_unit);
        $this->data["data_chart_level"] = (!empty($data_level_chart)) ? $this->_generate_bar_chart("level", $data_level_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_summary_level = $this->_generate_summary("level", $data_level_chart["result"], $start_year, $end_year);
//        $this->data["label_level"] = $data_level_chart["tbl_header_label"];
        $this->data["label_level"] = ["12", "11", "10", "09", "08", "07", "06", "05", "04", "03", "00"];
        $this->data["data_summary_level"] = $data_summary_level["data"];

        /* ===============  3) Vut =================== */
        // bar chart
        $data_vut_chart = $this->memployee->get_data_for_chart("vut", $start_year, $end_year, $dept, $is_unit);
        $this->data["data_vut_level"] = (!empty($data_vut_chart)) ? $this->_generate_bar_chart("vut", $data_vut_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_vut_level = $this->_generate_summary("vut", $data_vut_chart["result"], $start_year, $end_year);
//        $this->data["label_vut"] = $data_vut_chart["tbl_header_label"];
        $this->data["label_vut"] = ["ปริญญาโท","ปริญญาตรี","ปวส.","ปวช.","ต่ำกว่า ปวช."];
        $this->data["data_summary_vut"] = $data_vut_level["data"];

        /* ===============  4) Age =================== */
        // bar chart
        $data_age_chart = $this->memployee->get_data_for_chart("age", $start_year, $end_year, $dept, $is_unit);
        $this->data["data_age_level"] = (!empty($data_age_chart)) ? $this->_generate_bar_chart("age", $data_age_chart["result"], $start_year, $end_year) : 0;
        // summary table
        $data_age_level = $this->_generate_summary("age", $data_age_chart["result"], $start_year, $end_year);
//        $this->data["label_age"] = $data_age_chart["tbl_header_label"];
        $this->data["label_age"] = ["20-25", "26-30", "31-35", "36-40", "41-45", "46-50", "51-55", "56-60"];
        $this->data["data_summary_age"] = $data_age_level["data"];

        // List all active employee from year given as default show
        $dt_result = $this->memployee->view_active_employee($start_year, $dept, "", $is_unit);
        $this->data["dt_list_emp"] = $dt_result["result"];
        $this->data["dt_cnt_emp"] = $dt_result["cnt"];

        $html = $this->load->view("employee/dashboard", $this->data, TRUE);
        echo $html;
    }

    private function _generate_bar_chart($pType, $pData, $start_year, $end_year)
    {
        if (!empty($pData))
        {
            $year_range = $this->utils->get_array_range_year($start_year, $end_year);
            $dt_act = $dt_ret = $dt_rep = $labels = array();

            foreach ($pData as $item)
            {
                $labels[] = $item["year"];
                if ($pType == "pos")
                {
                    $key = $item["postb"];
                }
                elseif ($pType == "level")
                {
                    $key = (String) $item["ladum2"];
                }
                elseif ($pType == "vut")
                {
                    $key = (String) $item["level_name"];
                }
                elseif ($pType == "age")
                {
                    $key = (String) $item["agetow"];
                }
                $dt_act[$key][$item["year"]] = $item["active_emp"];
                $dt_ret[$key][$item["year"]] = $item["retire_emp"];
                // replace_emp
                if ($pType == "pos" || $pType == "vut")
                {
                    $dt_rep[$key][$item["year"]] = $item["replace_emp"];
                }
            }

            $dt_act = $this->prepare_bar_chart_data($dt_act, $pType, $year_range);
            $dt_ret = $this->prepare_bar_chart_data($dt_ret, $pType, $year_range);
            // replace_emp
            if ($pType == "pos" || $pType == "vut")
            {
                $dt_rep = $this->prepare_bar_chart_data($dt_rep, $pType, $year_range);
            }

            //Prepare Dataset
            $i = 0;
            foreach ($dt_act["data"] as $key => $val)
            {
                $dts[] = array("label" => (String) $key, "backgroundColor" => array_values($dt_act["color"])[$i], "borderColor" => "#fff", "borderWidth" => 0, "stack" => "Stack 0", "data" => array_values($val));
                $i++;
            }

            $i = 0;
            foreach ($dt_ret["data"] as $key => $val)
            {
                $dts[] = array("label" => (String) $key, "backgroundColor" => array_values($dt_ret["color"])[$i], "borderColor" => "#fff", "borderWidth" => 0, "stack" => "Stack 1", "data" => array_values($val));
                $i++;
            }

            //replace_emp
            if ($pType == "pos" || $pType == "vut")
            {
                $i = 0;
                foreach ($dt_rep["data"] as $key => $val)
                {
                    $dts[] = array("label" => (String) $key, "backgroundColor" => array_values($dt_rep["color"])[$i], "borderColor" => "#fff", "borderWidth" => 0, "stack" => "Stack 2", "data" => array_values($val));
                    $i++;
                }
            }

            $arr_label = array();
            foreach (array_unique($labels) as $item)
            {
                array_push($arr_label, "ปี " . $item);
            }
//            var_dump($dts);
            $res = array("labels" => $arr_label, "datasets" => $dts);
            return json_encode($res);
        }
        else
        {
            return "";
        }
    }

    private function _generate_summary($pType, $pData, $start_year, $end_year)
    {
        if (!empty($pData))
        {
            $year_range = $this->utils->get_array_range_year($start_year, $end_year);
            $dt_act = $dt_ret = $dt_rep = array();
            foreach ($pData as $item)
            {
                if ($pType == "pos")
                {
                    $key = $item["postID"];
                    $labels[] = $item["postb"];
                }
                elseif ($pType == "level")
                {
                    $key = $item["ladum2"];
                    $labels[] = $item["ladum2"];
                }
                elseif ($pType == "vut")
                {
                    $key = $item["q_level"];
                    $labels[] = $item["level_name"];
                }
                elseif ($pType == "age")
                {
                    $key = $item["agetow"];
                    $labels[] = $item["agetow"];
                }

                $dt_act[$item["year"]][$key] = $item["active_emp"];
                $dt_ret[$item["year"]][$key] = $item["retire_emp"];
                if ($pType == "pos" || $pType == "vut")
                {
                    $dt_rep[$item["year"]][$key] = $item["replace_emp"];
                }
            }

            $dt_act = $this->prepare_summary_data($dt_act, $pType, $year_range);
            $dt_ret = $this->prepare_summary_data($dt_ret, $pType, $year_range);
            $dt_act = $this->get_summary_total($dt_act);
            $dt_ret = $this->get_summary_total($dt_ret);
            //replace_emp
            if ($pType == "pos" || $pType == "vut")
            {
                $dt_rep = $this->prepare_summary_data($dt_rep, $pType, $year_range);
                $dt_rep = $this->get_summary_total($dt_rep);
            }
            $res = array("labels" => array_unique($labels), "data" => array("active" => $dt_act, "retire" => $dt_ret));
            //replace_emp
            if ($pType == "pos" || $pType == "vut")
            {
                $res["data"]["replace"] = $dt_rep;
            }
            return $res;
        }
    }

    private function prepare_bar_chart_data($pData, $pType, $year_range)
    {
        $color = ["#3485BB", "#6DA9CF", "#39A330", "#A4D880", "#EB494A", "#F49095", "#F68721", "#FDAA4B", "#AE77B2", "#E194BC", "#939393", "#4593A8", "#83BE97"];


        foreach ($year_range as $year)
        {
            if ($pType == "pos")
            {
                $def_pos = ["วศ.", "สถ.", "วท.", "วก.", "นค.", "ช.", "พช.", "ชก.", "พขก.", "พขร.", "ชง."];
                $ind_color = 0;
                foreach ($def_pos as $item)
                {
                    if (isset($pData[$item]) && !array_key_exists($year, $pData[$item]))
                    {
                        $pData[$item][$year] = "0";
                    }
                    elseif (!isset($pData[$item]))
                    {
                        unset($color[$ind_color]);
                    }
                    $ind_color++;
                }
            }
            elseif ($pType == "level")
            {
                $def_ladum = ["12", "11", "10", "09", "08", "07", "06", "05", "04", "03", "00"];
                $ind_color = 0;
                foreach ($def_ladum as $item)
                {
                    if (isset($pData[$item]) && !array_key_exists($year, $pData[$item]))
                    {
                        $pData[$item][$year] = "0";
                    }
                    elseif (!isset($pData[$item]))
                    {
                        unset($color[$ind_color]);
                    }
                    $ind_color++;
                }
            }
        }
        $prepare_data["data"] = $pData;
        $prepare_data["color"] = $color;

        return $prepare_data;
    }

    private function prepare_summary_data($pData, $pType, $year_range)
    {
        foreach ($year_range as $year)
        {
            if ($pType == "pos")
            {
                $def_pos = ["01", "02", "04", "15", "16", "20", "21", "30", "31", "33", "37"];
                foreach ($def_pos as $item)
                {
                    if (!array_key_exists($item, $pData[$year]))
                    {
                        $pData[$year][$item] = "0";
//                        continue;
                    }
                }
                ksort($pData[$year]);
            }
            elseif ($pType == "level")
            {
                $def_ladum = ["12", "11", "10", "09", "08", "07", "06", "05", "04", "03", "00"];
                foreach ($def_ladum as $item)
                {
                    if (!array_key_exists($item, $pData[$year]))
                    {
                        $pData[$year][$item] = "0";
//                        continue;
                    }
                }
                krsort($pData[$year]);
            }
            elseif ($pType == "vut")
            {
                $def_vut = ["2", "3", "4", "5", "6"];
                foreach ($def_vut as $item)
                {
                    if (!array_key_exists($item, $pData[$year]))
                    {
                        $pData[$year][$item] = "0";
//                        continue;
                    }
                }
                ksort($pData[$year]);
            }
            elseif ($pType == "age")
            {
                $def_age = ["20-25", "26-30", "31-35", "36-40", "41-45", "46-50", "51-55", "56-60"];
                foreach ($def_age as $item)
                {
                    if (!array_key_exists($item, $pData[$year]))
                    {
                        $pData[$year][$item] = "0";
//                        continue;
                    }
                }
                ksort($pData[$year]);
            }
        }
        return $pData;
    }

    private function get_summary_total($pData)
    {
        $total = 0;
        foreach ($pData as $year => $val)
        {
            foreach ($val as $count)
            {
                $total += $count;
            }
            $pData[$year]["total"] = $total;
            $total = 0;
        }
        return $pData;
    }

    function list_employee()
    {
        $this->data[CON_RIGHT_CONTENT] = "employee/list_employee";
        $this->data[CON_TITLE] = "อัตรากำลัง";
        $this->data[CON_SUB_TITLE] = "รายชื่อผู้ปฏิบัติงาน และผู้เกษียณอายุ";
        $this->data[CON_MENU_USER_SELECTED] = "emp_1";
        $this->breadcrumb->add("อัตรากำลัง (รายชื่อผู้ปฏิบัติงาน และผู้เกษียณอายุ)", site_url("employee/list_employee"));

        // initial ddl
        $this->data["dt_year"] = $this->utils->get_year_valid_range(2, 3);
        $this->data["dt_gsd_unit"] = $this->munit->get_gsd_unit();
        $this->data["dt_vut"] = $this->mvut->get_all();

        if ($this->input->post())
        {
            //postback
            $_POST = $this->input->post();
            $start_year = $this->input->post("start_year");
            $end_year = $this->input->post("end_year");
            $unit = $this->input->post("unit");
            $vut = $this->input->post("vut");
            $is_unit = $this->input->post("is_unit");
            $this->data["ddl_unit"] = $unit;
            $this->data["ddl_is_unit"] = $is_unit;
            $dt_unit = $this->munit->get_gsd_unit(array("unit_sub_dept" => $unit, "is_unit" => $is_unit));
            $title = (empty($unit)) ? "ฝ่ายบริการ" : "";
            if (!empty($unit))
            {
                if ($is_unit == "0")
                {
                    $dt_unit_dept = $this->munit->get_gsd_unit(array("unit_sub_dept" => $dt_unit["unit_dept"], "is_unit" => 1));
                    $title = $dt_unit["unit_name"] . " | " . $dt_unit_dept["unit_name"];
                }
                else
                {
                    $title = $dt_unit["unit_name"];
                }
            }
            $this->data["header_title"] = "ปี " . $start_year . " - " . $end_year . " (" . $title . ")";
            $this->data["title"] = "<< ผู้ปฏิบัติงาน ปี " . $start_year . " >>";

            $dt_result = $this->memployee->view_active_employee($start_year, $unit, $vut, $is_unit);
        }
        else
        {
            // default load current year
            $start_year = $this->utils->year_buddha_convert(date("Y"));
            $end_year = $start_year + 3;
            $this->data["header_title"] = "ปี " . $start_year . " - " . $end_year . " (ฝ่ายบริการ)";
            $this->data["title"] = "<< ผู้ปฏิบัติงาน ปี " . $start_year . " >>";

            $dt_result = $this->memployee->view_active_employee($start_year);
        }
        // btn-group
        $this->data["start_year"] = $start_year;
        $this->data["end_year"] = $end_year;
        $this->data["default_year"] = $start_year;

        $this->data["dt_list_emp"] = $dt_result["result"];
        $this->data["dt_cnt_emp"] = $dt_result["cnt"];
        $this->load->view("templates/template_user", $this->data);
    }

    function ajax_get_employee_by_year()
    {
        $type = $this->input->post("type");
        $year = $this->input->post("year");
        $unit = $this->input->post("unit");
        $vut = $this->input->post("vut");
        $is_unit = $this->input->post("is_unit");

        if ($type == "active") // ผู้ปฏิบัติงาน
        {
            $dt_result = $this->memployee->view_active_employee($year, $unit, $vut, $is_unit);
            $type_text = "ผู้ปฏิบัติงาน";
        }
        elseif ($type == "retire")  // ผู้เกษียณอายุ
        {
            $dt_result = $this->memployee->view_retire_employee($year, $unit, $vut, $is_unit);
            $type_text = "ผู้เกษียณอายุ";
        }

        $this->data["dt_list_emp"] = $dt_result["result"];
        $this->data["dt_cnt_emp"] = $dt_result["cnt"];
        $this->data["title"] = "<< " . $type_text . " ปี " . $year . " >>";

        $html = $this->load->view("employee/tbl_list_employee", $this->data, TRUE);

        echo $html;
    }

}
