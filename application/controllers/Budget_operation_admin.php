<?php

/**
 * @property utils $utils 
 */
class Budget_operation_admin extends CI_Controller
{
    private $adminID = "";

    function __construct()
    {
        parent::__construct();

        //Permission check
        if($this->login_utils->have_login(TRUE, "authen_admin/login"))
        {
            $this->adminID = $this->login_utils->get_data("empn");
        }        
        
        $this->data = $this->utils->get_basic_data();

        //Essential in this class
        $this->load->model("mbudget_operate");
    }

    function index()
    {
        $this->view_all();
    }

    function view_all()
    {
        $this->data[CON_TITLE] = "งบประมาณทำการ ทั้งหมด";
        $this->data[CON_RIGHT_CONTENT] = "budget_operation_admin/view_all";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fluid";
        //get data budget
        $this->data["dt_budget"] = $this->mbudget_operate->get_view_all_admin();
        //get data unit 
        $this->load->model("munit");
        $this->data["dt_unit"] = $this->munit->get_all();

        $this->load->view("templates/template_admin", $this->data);
    }

    function view($pYear = "")
    {
        $thai_year = $this->utils->year_buddha_convert($pYear);
        $this->data[CON_TITLE] = "งบประมาณทำการ ปี " . $thai_year;
        $this->data[CON_RIGHT_CONTENT] = "budget_operation_admin/view";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fluid";

        if($pYear != "")
        {
            //get data budget
            $this->data["dt_budget"] = $this->mbudget_operate->get_view_admin($pYear);

            $this->data["thai_year"] = $thai_year;
            $this->data["year"] = $pYear;

            //all month 
            $this->data["arr_month"] = $this->utils->get_months();

            //get data unit 
            $this->load->model("munit");
            $this->data["dt_unit"] = $this->munit->get_all();
        }

        $this->load->view("templates/template_admin", $this->data);
    }

    function create()
    {
        $this->data[CON_TITLE] = "สร้างรายการ งบประมาณทำการ ";
        $this->data[CON_RIGHT_CONTENT] = "budget_operation_admin/create";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fluid";

        /*
         * 1. get year (with condition)
         * 2. get all unit from #unit (render template)
         * 3. get all month
         */
        $this->data["dt_year"] = $this->_get_year_with_check();

        $this->load->model("munit");
        $this->data["dt_unit"] = $dt_unit = $this->munit->get_all();
        $this->data["dt_month"] = $arr_month = $this->utils->get_months();

        $this->load->library("form_validation");
        if($this->utils->is_postback())
        {//post back
            $this->form_validation->set_rules("year", "ปีงบประมาณ", "trim|required|numeric");

            //set form validation on month and unit
            //$id = txt_unitID_month(index)
            foreach($dt_unit as $unit)
            {//unit
                for($i = 1; $i <= 12; $i++)
                {//month
                    $id = "txt_" . $unit["unitID"] . "_" . $i;
                    $error_display = "งบประมาณที่ตั้ง ของ " . $unit["unit_name"] . " เดือน " . $arr_month[$i];
                    $this->form_validation->set_rules($id, $error_display, "trim|required|callback_is_numeric");
                }
            }

            if($this->form_validation->run())
            {
                //prepare for insert
                $year = $this->input->post("year");
                $dt_budget = $this->_get_prepare_budget_insert($this->input->post(), $year);

                $res_insert = $this->mbudget_operate->insert_budget($dt_budget);
                if($res_insert["status"])
                {//insert complete
                    //redirect
                    $this->utils->msgbox_flash(CON_MSG_SAVE_COMPELTE, "alert alert-success");
                    redirect("budget_operation_admin/view_all");
                }
                else
                {//insert incomplete
                    $this->utils->msgbox($res_insert["msg"], "alert alert-danger");
                }
            }
            else
            {
                $this->utils->msgbox(validation_errors(), "alert alert-danger");
            }
        }

        $this->load->view("templates/template_admin", $this->data);
    }

    /**
     * Function สำหรับ check ว่าเป็นตัวเลขหรือไม่ ? แต่จะเอา "," ออก ก่อน
     */
    function is_numeric($pNum)
    {
        $del_comma = $this->remove_comma($pNum);
        if(!is_numeric($del_comma))
        {//not numeric
            $this->form_validation->set_message("is_numeric", "{field} ต้องระบุเป็นตัวเลขเท่านั้น");
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    /**
     * เอา comma ออก
     * @param type $pStr
     * @return type
     */
    private function remove_comma($pStr)
    {
        return str_replace(",", "", $pStr);
    }

    /**
     * 1. Load Year +3 , -3 From This Year
     * 2. check from database #budget_operate ว่ามีการสร้างของปีนั้นหรือยัง ถ้าสร้างแล้วให้ disable ไม่ให้เลือกปีนั้น 
     * @return boolean
     */
    private function _get_year_with_check()
    {
        $arr_year = $this->utils->get_year_valid_range();

        //find in table #budget_operate
        $dt_year_created = $this->mbudget_operate->check_year_exist($arr_year);

        //make array year created flag [year, created]
        foreach($arr_year as $year)
        {
            $res_year[] = array("year" => $year, "created" => FALSE);
        }

        //marked created flag
        if(!empty($dt_year_created))
        {//have year already exist found
            foreach($dt_year_created as $c)
            {//update flag year_created
                foreach($res_year as &$x)
                {
                    if($c["year"] == $x["year"])
                    {
                        $x["created"] = TRUE;
                    }
                }
            }
        }

        return $res_year;
    }

    /**
     * เตรียม Data table สำหรับ insert #budget_operate
     * @param type $pPost
     * @param type $pYear
     */
    private function _get_prepare_budget_insert($pPost, $pYear)
    {
        if(!empty($pPost))
        {//check not empty
            //1. unset year
            unset($pPost["year"]);

            $dt_budget = array();
            foreach($pPost as $key => $value)
            {
                /*
                 * 1. key is "txt_1_10" will cleansing key to 1_10
                 * 2. get unit = 1
                 * 3. get month = 10
                 */

                //1.
                $cls_key = str_replace("txt_", "", $key);
                $arr_key = explode("_", $cls_key);
                if(count($arr_key) == 2)
                {//2.,3.
                    $unitID = $arr_key[0];
                    $month = $arr_key[1];

                    $tmp_data["month"] = $month;
                    $tmp_data["unitID"] = $unitID;
                    $tmp_data["year"] = $pYear;
                    $tmp_data["plan"] = $this->remove_comma($value);
                    $tmp_data["createBy"] = $this->adminID;
                    $tmp_data["createDate"] = date("Y-m-d H:i:s");
                    $dt_budget[] = $tmp_data;
                    unset($tmp_data);
                }
            }

            return $dt_budget;
        }//end if
    }

    function edit($pYear = "")
    {
        $thai_year = $this->utils->year_buddha_convert($pYear);
        $this->data[CON_TITLE] = "แก้ไขงบประมาณทำการ ปี " . $thai_year;
        $this->data[CON_RIGHT_CONTENT] = "budget_operation_admin/edit";
        $this->data[CON_TEMPLATE_CONTAINER] = "container-fluid";

        if($pYear != "")
        {
            //get data unit 
            $this->load->model("munit");
            $this->data["dt_unit"] = $dt_unit = $this->munit->get_all();

            //get data budget
            $this->data["dt_budget"] = $this->mbudget_operate->get_edit_admin($pYear);

            $this->data["thai_year"] = $thai_year;
            $this->data["year"] = $pYear;

            //all month 
            $this->data["arr_month"] = $this->utils->get_months();
        }

        $this->load->view("templates/template_admin", $this->data);
    }

    function ajax_do_edit()
    {
        $res = array("status" => "0", "msg" => "");

        if($this->utils->is_postback())
        {//post back
            //get data unit 
            $this->load->model("munit");
            $this->data["dt_unit"] = $dt_unit = $this->munit->get_all();

            $this->load->library("form_validation");
            //validate plan and actual all unit 
            foreach($dt_unit as $unit)
            {
                //plan format : plan_x
                $id_plan = "plan_" . $unit["unitID"];
                $error_display_plan = "งบที่ตั้ง ของ " . $unit["unit_name"];
                $this->form_validation->set_rules($id_plan, $error_display_plan, "trim|required|callback_is_numeric");

                //actual format : actual_x
                $id_actual = "actual_" . $unit["unitID"];
                $error_display_actual = "งบใช้จริง ของ " . $unit["unit_name"];
                $this->form_validation->set_rules($id_actual, $error_display_actual, "trim|callback_is_numeric");
            }

            if($this->form_validation->run())
            {//all valid will save
                $dt_budget = $this->_get_prepare_budget_update($this->input->post());
                $month = $this->input->post("month");
                $year = $this->input->post("year");

                $res_update = $this->mbudget_operate->update_batch($dt_budget, $month, $year);

                if($res_update["status"])
                {
                    $res["status"] = "1";
                    $res["next_url"] = site_url("budget_operation_admin/edit/" . $year);
                    $this->utils->msgbox_flash(CON_MSG_EDIT_COMPLETE, "alert alert-success");
                }
                else
                {
                    $res["msg"] = $res_update["msg"];
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        else
        {
            $res["msg"] = CON_MSG_CAN_NOT_RECEIVE_DATA;
        }

        echo json_encode($res);
    }

    function ajax_get_remark()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_operateID"))
        {
            $res["status"] = "1";
            $budget_operateID = $this->input->post("budget_operateID");
            //get operate detail
            $res["data"] = $this->mbudget_operate->get($budget_operateID);
        }

        echo json_encode($res);
    }

    function ajax_remark_table()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_operateID"))
        {
            $budget_operateID = $this->input->post("budget_operateID");
            $data_render["budget_operateID"] = $budget_operateID;
            $data_render["dt"] = $this->mbudget_operate->get_all_budget_remark($budget_operateID);

            //check have parameter for show message? 
            if($this->input->post("show_msg"))
            {
                $show_msg = $this->input->post("show_msg");
                if($show_msg == "save_ok")
                {
                    $this->utils->msgbox(CON_MSG_SAVE_COMPELTE, "alert alert-success", "mdl_remark_msgbox");
                }
                else if($show_msg == "delete_ok")
                {
                    $this->utils->msgbox(CON_MSG_DEL_COMPLETE, "alert alert-success", "mdl_remark_msgbox");
                }
            }
            $res["data"] = $this->load->view("budget_operation_admin/ajax_remark_table", $data_render, TRUE);
        }
        echo json_encode($res);
    }
    
    function ajax_get_count_remark()
    {
        $res = array("status" => "0", "msg" => "", "res" => "0");
        if($this->input->post("budget_operateID"))
        {
            $budget_operateID = $this->input->post("budget_operateID");
            $res["res"] = $this->mbudget_operate->get_count_remark_by_budgetID($budget_operateID);            
        }
        echo json_encode($res);
    }

    /* =========================
     *        Edit
     * ======================= */
    function ajax_remark_edit()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_operate_remarkID"))
        {
            $budget_operate_remarkID = $this->input->post("budget_operate_remarkID");
            $data_render["dt"] = $this->mbudget_operate->get_budget_remark($budget_operate_remarkID);
            $res["data"] = $this->load->view("budget_operation_admin/ajax_remark_edit", $data_render, TRUE);
        }
        echo json_encode($res);
    }

    function ajax_do_edit_remark()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post())
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("remark", "เหตุผล", "trim|required");
            $this->form_validation->set_rules("mdl_remark_hdd_budget_operate_remarkID", "ID ของรายการ (ติดต่อ Programmer)", "trim|required");
            if($this->form_validation->run())
            {
                //update 
                $budget_operate_remarkID = $this->input->post("mdl_remark_hdd_budget_operate_remarkID");
                $remark = $this->input->post("remark");
                //prepare data
                $dr_br["remark"] = $remark;
                $dr_br["updateBy"] = $this->adminID;
                $dr_br["updateDate"] = date("Y-m-d H:i:s");
                $res_update = $this->mbudget_operate->update_remark($budget_operate_remarkID, $dr_br);
                if($res_update["status"])
                {
                    $res["status"] = "1";
                    $res["msg"] = CON_MSG_EDIT_COMPLETE;
                }
                else
                {
                    $res["msg"] = CON_MSG_ERROR;
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        echo json_encode($res);
    }

    /* =========================
     *        Create
     * ======================= */
    function ajax_remark_create()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post("budget_operateID"))
        {
            $budget_operateID = $this->input->post("budget_operateID");
            //just pass budget_operateID
            $data_render["budget_operateID"] = $budget_operateID;
            $res["data"] = $this->load->view("budget_operation_admin/ajax_remark_create", $data_render, TRUE);
        }
        echo json_encode($res);
    }

    function ajax_do_create_remark()
    {
        $res = array("status" => "0", "msg" => "");
        if($this->input->post())
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("remark", "เหตุผล", "trim|required");
            $this->form_validation->set_rules("mdl_remark_hdd_budget_operateID", "ID ของรายการ (ติดต่อ Programmer)", "trim|required");
            if($this->form_validation->run())
            {
                //update 
                $budget_operateID = $this->input->post("mdl_remark_hdd_budget_operateID");
                $remark = $this->input->post("remark");
                //prepare data
                $dr_br["budget_operateID"] = $budget_operateID;
                $dr_br["remark"] = $remark;
                $dr_br["createBy"] = $this->adminID;
                $dr_br["createDate"] = date("Y-m-d H:i:s");
                $res_create = $this->mbudget_operate->insert_budget_remark($dr_br);
                if($res_create["status"])
                {
                    $res["status"] = "1";
                    $res["msg"] = CON_MSG_SAVE_COMPELTE;
                }
                else
                {
                    $res["msg"] = CON_MSG_ERROR;
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        echo json_encode($res);
    }

    /* =========================
     *        Delete
     * ======================= */
    function ajax_remark_delete()
    {
        $res = array("status" => FALSE, "msg" => "");
        if($this->input->post("budget_operate_remarkID"))
        {
            $this->load->library("form_validation");
            $this->form_validation->set_rules("budget_operate_remarkID", "ID ของรายการ (ติดต่อ Programmer)", "trim|required");
            if($this->form_validation->run())
            {
                //delete 
                $budget_operate_remarkID = $this->input->post("budget_operate_remarkID");
                $res_delete = $this->mbudget_operate->delete_remark($budget_operate_remarkID);
                if($res_delete["status"])
                {
                    $res["status"] = "1";
                    $res["msg"] = CON_MSG_DEL_COMPLETE;
                }
                else
                {
                    $res["msg"] = CON_MSG_ERROR;
                }
            }
            else
            {
                $res["msg"] = validation_errors();
            }
        }
        echo json_encode($res);
    }

    private function _get_prepare_budget_update($pPost)
    {
        if(!empty($pPost))
        {
            unset($pPost["month"]);
            unset($pPost["year"]);

            $arr_data = array();
            foreach($pPost as $key => $value)
            {
                //split plan_x, actual_x
                $arr_word = explode("_", $key);
                if(count($arr_word) == 2)
                {
                    if($arr_word[0] == "plan")
                    {
                        $arr_data[$arr_word[1]]["plan"] = $this->remove_comma($value);
                    }
                    elseif($arr_word[0] == "actual")
                    {
                        $arr_data[$arr_word[1]]["actual"] = $this->remove_comma($value);
                    }
                }
            }

            return $arr_data;
        }
    }

//end function
}
