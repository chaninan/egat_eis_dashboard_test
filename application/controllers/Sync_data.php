<?php

class Sync_data extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->load->model("mlog");
        $this->data["dt_log"] = $this->mlog->get_summary_sync_log();
        $this->load->view("sync_data/home", $this->data);
    }

    /**
     * This function will synchronize data from database hr_employee @10.20.8.123 
     * เฉพาะข้อมูลพนักงาน อบก.
     * 
     * ขั้นตอนการทำงาน 
     * 1. get data from hr_employee 
     * 2. Truncate table #user_gsd ที่ eis_dashboard
     * 3. insert all to #user_gsd
     * 4. insert Log #user_gsd_log
     */
    function user_gsd($pVia = "manual", $pBy = "system")
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //1.
        $hr_employee = $this->load->database("hr_employee", TRUE);
        $hr_employee->where("fay", "อบก.");
        $q_hr = $hr_employee->get("egat_employee_all_data");
        $dt_hr = $q_hr->result_array();
        if (!empty($dt_hr))
        {
            //2. 
            $this->db->truncate("user_gsd");
            //3.
            $this->db->insert_batch("user_gsd", $dt_hr);

            //4.
            $user_gsd_log["serviceName"] = "user GSD";
            $user_gsd_log["status"] = "success";
            $user_gsd_log["update_via"] = $pVia;
            $user_gsd_log["updateBy"] = $pBy;
            $user_gsd_log["updateDate"] = date("Y-m-d H:i:s");
            $this->db->insert("user_gsd_sync_log", $user_gsd_log);

            $res["serviceName"] = "user GSD";
            $res["status"] = TRUE;
            $res["msg"] = "Sync ข้อมูลเรียบร้อย ";
        }
        else
        {
            $res["msg"] = "ไม่สามารถเข้าถึงข้อมูลที่ 10.20.8.123 Database hr_employee ได้";
        }

        $this->db->trans_complete();

        if ($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        var_dump($res);
        return $res;
    }

    /**
     * ฟังกฺชั่น sync ข้อมูล พนักงานอบก. ที่เกษียณ/ลาออก 
     * จาก database hr_employee.gsd_employee_out @10.20.8.123 
     * 
     * 1. get ข้อมูลจาก hr_employee.gsd_employee_out @10.20.8.123
     * 2. Truncate table user_gsd_out ที่ eis_dashboard
     * 3. insert all to #user_gsd_out
     * 4. insert Log #user_gsd_log
     */
    
    function user_gsd_out($pVia = "manual", $pBy = "system")
    {
        $res = array("status" => FALSE, "msg" => "");
        $this->db->trans_start();

        //1.
        $hr_employee = $this->load->database("hr_employee", TRUE);
        $q_hr = $hr_employee->get("gsd_employee_out");
        $dt_hr = $q_hr->result_array();
        
        if(!empty($dt_hr)){
            
            //2. 
            $this->db->truncate("user_gsd_out");
            //3.
            $this->db->insert_batch("user_gsd_out", $dt_hr);

            //4.
            $user_gsd_log["serviceName"] = "user GSD OUT";
            $user_gsd_log["status"] = "success";
            $user_gsd_log["update_via"] = $pVia;
            $user_gsd_log["updateBy"] = $pBy;
            $user_gsd_log["updateDate"] = date("Y-m-d H:i:s");
            $this->db->insert("user_gsd_sync_log", $user_gsd_log);

            $res["serviceName"] = "user GSD OUT";
            $res["status"] = TRUE;
            $res["msg"] = "Sync ข้อมูลเรียบร้อย ";
        }
        else
        {
            $res["msg"] = "ไม่สามารถเข้าถึงข้อมูลที่ 10.20.8.123 Database hr_employee ได้";
        }

        $this->db->trans_complete();
        
        if ($this->db->trans_status())
        {
            //success
            $res["status"] = TRUE;
        }
        else
        {
            //fail
            $res['msg'] = $this->db->_error_message();
            log_message("error", $this->db->_error_message());
        }//end if

        var_dump($res);
        return $res;
    }

}
